<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaseAkademiksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fase_akademiks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tahunAjaran',10);
            $table->date('faseRegistrasi');
            $table->date('awalKuliah');  
            $table->date('akhirKuliah');
            $table->date('faseEval');
            $table->string('semester',10);
            $table->enum('status', ['aktif', 'tidak'])->default('tidak');
            $table->timestamps();
        });

        DB::table('fase_akademiks')->insert([
            [
                'tahunAjaran' => '2016/2017',
                'faseRegistrasi' => '2019-03-07',
                'awalKuliah' => '2019-03-07',
                'akhirKuliah' => '2019-03-07',
                'faseEval' => '2019-03-15',
                'semester' => 'ganjil',
                'status' => 'tidak'
            ],
            [
                'tahunAjaran' => '2016/2017',
                'faseRegistrasi' => '2019-03-07',
                'awalKuliah' => '2019-03-07',
                'akhirKuliah' => '2019-03-07',
                'faseEval' => '2019-03-15',
                'semester' => 'genap',
                'status' => 'tidak'
            ],
            [
                'tahunAjaran' => '2017/2018',
                'faseRegistrasi' => '2019-03-07',
                'awalKuliah' => '2019-03-07',
                'akhirKuliah' => '2019-03-07',
                'faseEval' => '2019-03-15',
                'semester' => 'ganjil',
                'status' => 'tidak'
            ],
            [
                'tahunAjaran' => '2017/2018',
                'faseRegistrasi' => '2019-03-07',
                'awalKuliah' => '2019-03-07',
                'akhirKuliah' => '2019-03-07',
                'faseEval' => '2019-03-15',
                'semester' => 'genap',
                'status' => 'tidak'
            ],
            [
                'tahunAjaran' => '2018/2019',
                'faseRegistrasi' => '2019-03-07',
                'awalKuliah' => '2019-03-07',
                'akhirKuliah' => '2019-03-07',
                'faseEval' => '2019-03-15',
                'semester' => 'ganjil',
                'status' => 'aktif'
            ],
            // [
            //     'tahunAjaran' => '2018/2019',
            //     'faseRegistrasi' => '2019-03-07',
            //     'awalKuliah' => '2019-03-07',
            //     'akhirKuliah' => '2019-03-07',
            //     'faseEval' => '2019-03-15',
            //     'semester' => 'genap',
            //     'status' => 'aktif'
            // ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fase_akademiks');
    }
}

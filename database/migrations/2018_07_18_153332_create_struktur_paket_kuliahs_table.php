<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStrukturPaketKuliahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('struktur_paket_kuliahs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idPaketKuliah');
            $table->integer('idMatkul');
            $table->timestamps();
        });
        DB::table('struktur_paket_kuliahs')->insert([
            [
                'idPaketKuliah' => '1',
                'idMatkul' => '11'
            ],
            [
                'idPaketKuliah' => '1',
                'idMatkul' => '12'
            ],
            [
                'idPaketKuliah' => '2',
                'idMatkul' => '17'
            ],
            [
                'idPaketKuliah' => '2',
                'idMatkul' => '18'
            ],
            [
                'idPaketKuliah' => '3',
                'idMatkul' => '23'
            ],
            [
                'idPaketKuliah' => '3',
                'idMatkul' => '24'
            ],
            [
                'idPaketKuliah' => '4',
                'idMatkul' => '29'
            ],
            [
                'idPaketKuliah' => '4',
                'idMatkul' => '30'
            ],
            [
                'idPaketKuliah' => '5',
                'idMatkul' => '11'
            ],
            [
                'idPaketKuliah' => '5',
                'idMatkul' => '12'
            ],
            [
                'idPaketKuliah' => '6',
                'idMatkul' => '17'
            ],
            [
                'idPaketKuliah' => '6',
                'idMatkul' => '18'
            ],
            [
                'idPaketKuliah' => '7',
                'idMatkul' => '23'
            ],
            [
                'idPaketKuliah' => '7',
                'idMatkul' => '24'
            ],
            [
                'idPaketKuliah' => '8',
                'idMatkul' => '11'
            ],
            [
                'idPaketKuliah' => '8',
                'idMatkul' => '12'
            ],
            [
                'idPaketKuliah' => '9',
                'idMatkul' => '32'
            ],
            [
                'idPaketKuliah' => '9',
                'idMatkul' => '33'
            ],
             [
                'idPaketKuliah' => '10',
                'idMatkul' => '39'
            ],
            [
                'idPaketKuliah' => '10',
                'idMatkul' => '40'
            ],
             [
                'idPaketKuliah' => '11',
                'idMatkul' => '45'
            ],
            [
                'idPaketKuliah' => '11',
                'idMatkul' => '46'
            ],
             [
                'idPaketKuliah' => '12',
                'idMatkul' => '52'
            ],
            [
                'idPaketKuliah' => '12',
                'idMatkul' => '53'
            ],
             [
                'idPaketKuliah' => '13',
                'idMatkul' => '32'
            ],
            [
                'idPaketKuliah' => '13',
                'idMatkul' => '33'
            ],
             [
                'idPaketKuliah' => '14',
                'idMatkul' => '39'
            ],
            [
                'idPaketKuliah' => '14',
                'idMatkul' => '40'
            ],
             [
                'idPaketKuliah' => '15',
                'idMatkul' => '45'
            ],
            [
                'idPaketKuliah' => '15',
                'idMatkul' => '46'
            ],
             [
                'idPaketKuliah' => '16',
                'idMatkul' => '32'
            ],
            [
                'idPaketKuliah' => '16',
                'idMatkul' => '33'
            ],
            
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('struktur_paket_kuliahs');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKategoriPenggunasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategori_penggunas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama',25)->unique();
            $table->enum('status', ['aktif', 'tidak'])->default('tidak');
            $table->timestamps();
        });
        DB::table('kategori_penggunas')->insert([
            [
                'nama' => 'Admin',
                'status' => 'aktif'
            ],
            [
                'nama' => 'Akademik',
                'status' => 'aktif'
            ],
            [
                'nama' => 'Dosen',
                'status' => 'aktif'
            ],
             [
                'nama' => 'Mahasiswa',
                'status' => 'aktif'
            ],
            [
                'nama' => 'Koordinator',
                'status' => 'aktif'
            ],
            [
                'nama' => 'Ketua Jurusan',
                'status' => 'aktif'
            ],
            [
                'nama' => 'Ketua Program Studi',
                'status' => 'aktif'
            ],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kategori_penggunas');
    }
}

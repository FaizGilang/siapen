<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaketKuliahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paket_kuliahs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idKurikulum');
            $table->string('tahunAjaran',10);
            $table->integer('komulatif');
            $table->enum('status', ['belum','aktif', 'tidak'])->default('belum');
            $table->timestamps();
        });
        DB::table('paket_kuliahs')->insert([
            [
                'idKurikulum' => '1',
                'tahunAjaran' => '1',
                'komulatif' => '1',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '1',
                'tahunAjaran' => '2',
                'komulatif' => '2',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '1',
                'tahunAjaran' => '3',
                'komulatif' => '3',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '1',
                'tahunAjaran' => '4',
                'komulatif' => '4',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '1',
                'tahunAjaran' => '3',
                'komulatif' => '1',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '1',
                'tahunAjaran' => '4',
                'komulatif' => '2',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '1',
                'tahunAjaran' => '5',
                'komulatif' => '3',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '1',
                'tahunAjaran' => '5',
                'komulatif' => '1',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '2',
                'tahunAjaran' => '1',
                'komulatif' => '1',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '2',
                'tahunAjaran' => '2',
                'komulatif' => '2',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '2',
                'tahunAjaran' => '3',
                'komulatif' => '3',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '2',
                'tahunAjaran' => '4',
                'komulatif' => '4',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '2',
                'tahunAjaran' => '3',
                'komulatif' => '1',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '2',
                'tahunAjaran' => '4',
                'komulatif' => '2',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '2',
                'tahunAjaran' => '5',
                'komulatif' => '3',
                'status' => 'tidak'
            ],
            [
                'idKurikulum' => '2',
                'tahunAjaran' => '5',
                'komulatif' => '1',
                'status' => 'tidak'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paket_kuliahs');
    }
}

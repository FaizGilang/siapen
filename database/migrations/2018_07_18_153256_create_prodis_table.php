<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProdisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prodis', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode',5)->unique();
            $table->string('nama',50);
            $table->string('nomor',3)->unique();
            $table->enum('status', ['aktif', 'tidak'])->default('tidak');
            $table->timestamps();
        });
         DB::table('prodis')->insert([
            [
                'kode' => 'DGD',
                'nama' => 'Desain Grafis',
                'nomor' => '062',
                'status' => 'aktif'
            ],
            [
                'kode' => 'JPD',
                'nama' => 'Jasa Pariwisata',
                'nomor' => '052',
                'status' => 'aktif'
            ],
            [
                'kode' => 'TKRD',
                'nama' => 'Teknik Kendaraan Ringan',
                'nomor' => '022',
                'status' => 'aktif'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prodis');
    }
}

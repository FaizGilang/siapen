<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKurikulumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kurikulums', function (Blueprint $table) {
            $table->increments('id');
            $table->year('tahun');
            $table->integer('idProdi');
            $table->enum('status', ['aktif', 'tidak'])->default('tidak');
            $table->timestamps();
        });

        DB::table('kurikulums')->insert([
            [
                'tahun' => '2014',
                'idProdi' => '1',
                'status' => 'aktif'
            ],
            [
                'tahun' => '2014',
                'idProdi' => '2',
                'status' => 'aktif'
            ],
            [
                'tahun' => '2014',
                'idProdi' => '3',
                'status' => 'aktif'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kurikulums');
    }
}

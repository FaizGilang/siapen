<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomorInduk',25)->unique();
            $table->string('nama',50);
            $table->string('email',50)->unique();
            $table->string('password');
            $table->integer('kategori');
            $table->integer('prodi');
            $table->year('tahunMasuk');
            $table->enum('status', ['belum','aktif', 'tidak'])->default('belum');
            $table->string('file',25);
            $table->rememberToken();
            $table->timestamps();
        });
         DB::table('users')->insert([
            [
                'nomorInduk' => '1',
                'nama' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '1',
                'prodi' => '98',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-1'
            ],
            [
                'nomorInduk' => '2',
                'nama' => 'akademik',
                'email' => 'akademik@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '2',
                'prodi' => '98',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-2'
            ],
            [
                'nomorInduk' => '30101987',
                'nama' => 'Mulyono, S.Kom',
                'email' => 'mulyono@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '3',
                'prodi' => '1',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-3'
            ],
            [
                'nomorInduk' => '27101988',
                'nama' => 'Endah Ekasanti S., S.T.,M.Kom',
                'email' => 'endah@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '3',
                'prodi' => '1',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-4'
            ],
            [
                'nomorInduk' => '20021984',
                'nama' => 'Angga Amoriska, S.Sn.,S.Pd., M.Pd',
                'email' => 'angga@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '3',
                'prodi' => '1',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-5'
            ],
            [
                'nomorInduk' => '11',
                'nama' => 'Drs. Rohadi, M.A., M.Si.',
                'email' => 'rohadi@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '3',
                'prodi' => '1',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-6'
            ],
            [
                'nomorInduk' => '12',
                'nama' => 'Masri’an, S.Pd., M.M.',
                'email' => 'masri@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '3',
                'prodi' => '1',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-7'
            ],
            [
                'nomorInduk' => '09031988',
                'nama' => 'Eka Harisma W, S.Hum.,M.Hum',
                'email' => 'eka@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '3',
                'prodi' => '2',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-8'
            ],
            [
                'nomorInduk' => '011111978',
                'nama' => 'Trinani Puji Sumantri, S.Pd',
                'email' => 'trinani@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '3',
                'prodi' => '2',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-9'
            ],
            [
                'nomorInduk' => '12011978',
                'nama' => 'Nina Sulistyowati, S.S',
                'email' => 'nina@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '3',
                'prodi' => '2',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-10'
            ],
            
            [
                'nomorInduk' => '13051981',
                'nama' => 'Ir. Edy Ismail, S.Pd., M.Pd., IPP',
                'email' => 'edy@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '3',
                'prodi' => '3',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-11'
            ],
             [
                'nomorInduk' => '24101988',
                'nama' => 'Sugiyarto, S.Pd',
                'email' => 'sugiyarto@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '3',
                'prodi' => '3',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-12'
             ],
              [
                 'nomorInduk' => '1806211001',
                 'nama' => 'Mohammad Rizal Amrul Haq',
                 'email' => 'rizalamrul@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '1',
                 'tahunMasuk' => '2018',
                 'status' => 'aktif',
                'file' => 'Pengguna-13'
             ],
             [   'nomorInduk' => '1806211002',
                 'nama' => 'Yanuarinta Darisma Yanti',
                 'email' => 'yanuarinta@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '1',
                 'tahunMasuk' => '2018',
                 'status' => 'aktif',
                'file' => 'Pengguna-14'
             ],
             [   'nomorInduk' => '1806211003',
                 'nama' => 'Ani Rizki Fitriani',
                 'email' => 'anirizki@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '1',
                 'tahunMasuk' => '2018',
                 'status' => 'aktif',
                'file' => 'Pengguna-15'
             ],
             [
                 'nomorInduk' => '1805211001',
                 'nama' => 'Abdul Aziz',
                 'email' => 'abdulaziz@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '2',
                 'tahunMasuk' => '2018',
                 'status' => 'aktif',
                 'file' => 'Pengguna-16'
             ],
             [
                 'nomorInduk' => '1805211002',
                 'nama' => 'Agustin Kinthan Amilla',
                 'email' => 'agustin@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '2',
                 'tahunMasuk' => '2018',
                 'status' => 'aktif',
                 'file' => 'Pengguna-17'
             ],
             [
                 'nomorInduk' => '1805211003',
                 'nama' => 'Puja Novita Sari',
                 'email' => 'puja@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '2',
                 'tahunMasuk' => '2018',
                 'status' => 'aktif',
                 'file' => 'Pengguna-18'
             ],
             [
                 'nomorInduk' => '1802211001',
                 'nama' => 'Samsul Qomar',
                 'email' => 'samsul@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '3',
                 'tahunMasuk' => '2018',
                 'status' => 'aktif',
                 'file' => 'Pengguna-19'
             ],
             [
                 'nomorInduk' => '1802211002',
                 'nama' => 'Ery Supriyanto',
                 'email' => 'erysupriyanto@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '3',
                 'tahunMasuk' => '2018',
                 'status' => 'aktif',
                 'file' => 'Pengguna-20'
             ],
              [
                 'nomorInduk' => '1802211003',
                 'nama' => 'Wahyu Tanwirul MuIsyr',
                 'email' => 'wahyutanwirul@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '3',
                 'tahunMasuk' => '2018',
                 'status' => 'aktif',
                 'file' => 'Pengguna-21'
             ],
            [
                 'nomorInduk' => '1706211001',
                 'nama' => 'Rizal Rudiyanto Baharudin',
                 'email' => 'rizal@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '1',
                 'tahunMasuk' => '2017',
                 'status' => 'aktif',
                'file' => 'Pengguna-22'
             ],
             [   'nomorInduk' => '1706211002',
                 'nama' => 'Yanuarinta Dewi Lestari',
                 'email' => 'yanuarinta17@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '1',
                 'tahunMasuk' => '2017',
                 'status' => 'aktif',
                'file' => 'Pengguna-23'
             ],
             [   'nomorInduk' => '1706211003',
                 'nama' => 'Ani Yalianti',
                 'email' => 'ani@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '1',
                 'tahunMasuk' => '2017',
                 'status' => 'aktif',
                'file' => 'Pengguna-24'
             ],
             [
                 'nomorInduk' => '1705211001',
                 'nama' => 'Abdul Hasyim Uhib',
                 'email' => 'abdul@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '2',
                 'tahunMasuk' => '2017',
                 'status' => 'aktif',
                 'file' => 'Pengguna-25'
             ],
             [
                 'nomorInduk' => '1705211002',
                 'nama' => 'Agustin Ramadhania',
                 'email' => 'agustin17@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '2',
                 'tahunMasuk' => '2017',
                 'status' => 'aktif',
                 'file' => 'Pengguna-26'
             ],
             [
                 'nomorInduk' => '1705211003',
                 'nama' => 'Puja Pangestu Pandewo',
                 'email' => 'puja17@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '2',
                 'tahunMasuk' => '2017',
                 'status' => 'aktif',
                 'file' => 'Pengguna-27'
             ],
             [
                 'nomorInduk' => '1702211001',
                 'nama' => 'Samsul Baharudin Umam',
                 'email' => 'samsul17@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '3',
                 'tahunMasuk' => '2017',
                 'status' => 'aktif',
                 'file' => 'Pengguna-28'
             ],
             [
                 'nomorInduk' => '1702211002',
                 'nama' => 'Ery Yunitasari',
                 'email' => 'ery@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '3',
                 'tahunMasuk' => '2017',
                 'status' => 'aktif',
                 'file' => 'Pengguna-29'
             ],
              [
                 'nomorInduk' => '1702211003',
                 'nama' => 'Wahyu Darmawan',
                 'email' => 'wahyu@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '3',
                 'tahunMasuk' => '2017',
                 'status' => 'aktif',
                 'file' => 'Pengguna-30'
             ],
             [
                'nomorInduk' => '195909161983021002',
                'nama' => 'Dr. Trisyono, M.Pd',
                'email' => 'trisyono@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '5',
                'prodi' => '0',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-31'
            ],
            [
                'nomorInduk' => '197104221995012001',
                'nama' => 'Nurul Intan Pratiwi, S.Sos., M.Si.',
                'email' => 'nurulintan@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '6',
                'prodi' => '1',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-32'
            ],
            [
                'nomorInduk' => '197104221995012002',
                'nama' => 'Pratiwi, S.Sos., M.Si.',
                'email' => 'pratiwi@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '6',
                'prodi' => '2',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-33'
            ],
            [
                'nomorInduk' => '197104221995012003',
                'nama' => 'Intan Pratiwi, S.Sos., M.Si.',
                'email' => 'intanpratiwi@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '6',
                'prodi' => '3',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-34'
            ],
            [
                'nomorInduk' => '196409121986011002',
                'nama' => 'Hadi Waluyo, S.H.,M.Pd.',
                'email' => 'hadiwaluyo@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '7',
                'prodi' => '1',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-35'
            ],
            [
                'nomorInduk' => '196409121986011003',
                'nama' => 'Hadi, S.H.,M.Pd.',
                'email' => 'hadi@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '7',
                'prodi' => '2',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-36'
            ],
            [
                'nomorInduk' => '196409121986011004',
                'nama' => 'Waluyo, S.H.,M.Pd.',
                'email' => 'waluyo@gmail.com',
                'password' => bcrypt('12345'),
                'kategori' => '7',
                'prodi' => '3',
                'tahunMasuk' => '2014',
                'status' => 'aktif',
                'file' => 'Pengguna-37'
            ],
            
              [
                 'nomorInduk' => '1606211001',
                 'nama' => 'Harum Puji Lestari Jayanti',
                 'email' => 'tari@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '1',
                 'tahunMasuk' => '2016',
                 'status' => 'aktif',
                'file' => 'Pengguna-38'
             ],
             [   'nomorInduk' => '1606211002',
                 'nama' => 'Yanti Manda Safitri',
                 'email' => 'yanti@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '1',
                 'tahunMasuk' => '2016',
                 'status' => 'aktif',
                'file' => 'Pengguna-39'
             ],
             [   'nomorInduk' => '1606211003',
                 'nama' => 'Rani Pangestu Dewi',
                 'email' => 'rani@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '1',
                 'tahunMasuk' => '2016',
                 'status' => 'aktif',
                'file' => 'Pengguna-40'
             ],
             [
                 'nomorInduk' => '1605211001',
                 'nama' => 'Aziza Kezia Lina',
                 'email' => 'aziza@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '2',
                 'tahunMasuk' => '2016',
                 'status' => 'aktif',
                 'file' => 'Pengguna-41'
             ],
             [
                 'nomorInduk' => '1605211002',
                 'nama' => 'Milla Dewi Rianti',
                 'email' => 'milla@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '2',
                 'tahunMasuk' => '2016',
                 'status' => 'aktif',
                 'file' => 'Pengguna-42'
             ],
             [
                 'nomorInduk' => '1605211003',
                 'nama' => 'Sari Hanifa Linda',
                 'email' => 'sari@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '2',
                 'tahunMasuk' => '2016',
                 'status' => 'aktif',
                 'file' => 'Pengguna-43'
             ],
             [
                 'nomorInduk' => '1602211001',
                 'nama' => 'Omar Khairul Habib',
                 'email' => 'omar@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '3',
                 'tahunMasuk' => '2016',
                 'status' => 'aktif',
                 'file' => 'Pengguna-44'
             ],
             [
                 'nomorInduk' => '1602211002',
                 'nama' => 'Anto Kusumo Dewo',
                 'email' => 'anto@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '3',
                 'tahunMasuk' => '2016',
                 'status' => 'aktif',
                 'file' => 'Pengguna-45'
             ],
              [
                 'nomorInduk' => '1602211003',
                 'nama' => 'Muisyr Reno Fajar',
                 'email' => 'muisyr@gmail.com',
                 'password' => bcrypt('12345'),
                 'kategori' => '4',
                 'prodi' => '3',
                 'tahunMasuk' => '2016',
                 'status' => 'aktif',
                 'file' => 'Pengguna-46'
             ]

              ]);

       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

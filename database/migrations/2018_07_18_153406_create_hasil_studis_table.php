<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHasilStudisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasil_studis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idMatkul');
            $table->integer('idUser');  
            $table->integer('uts');
            $table->integer('uas');
            $table->integer('tugas');
            $table->integer('praktek');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasil_studis');
    }
}

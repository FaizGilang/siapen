<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranskripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transkrips', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUser');  
            $table->integer('idMatkul');
            $table->integer('komulatif');
            $table->string('nilaiAkhir',3);
            $table->timestamps();
        });
        DB::table('transkrips')->insert([
             [
                'idUser' => '13',
                'idMatkul' => '11',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '13',
                'idMatkul' => '12',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '14',
                'idMatkul' => '11',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '14',
                'idMatkul' => '12',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
           
            [
                'idUser' => '22',
                'idMatkul' => '11',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '22',
                'idMatkul' => '12',
                'komulatif' => '1',
                'nilaiAkhir' => 'B'
            ],
            [
                'idUser' => '23',
                'idMatkul' => '11',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '23',
                'idMatkul' => '12',
                'komulatif' => '1',
                'nilaiAkhir' => 'A-'
            ],
            
            [
                'idUser' => '22',
                'idMatkul' => '17',
                'komulatif' => '2',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '22',
                'idMatkul' => '18',
                'komulatif' => '2',
                'nilaiAkhir' => 'B'
            ],
           
            [
                'idUser' => '23',
                'idMatkul' => '17',
                'komulatif' => '2',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '23',
                'idMatkul' => '18',
                'komulatif' => '2',
                'nilaiAkhir' => 'A-'
            ],
            
             [
                'idUser' => '22',
                'idMatkul' => '23',
                'komulatif' => '3',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '22',
                'idMatkul' => '24',
                'komulatif' => '3',
                'nilaiAkhir' => 'B'
            ],
            
            [
                'idUser' => '23',
                'idMatkul' => '23',
                'komulatif' => '3',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '23',
                'idMatkul' => '24',
                'komulatif' => '3',
                'nilaiAkhir' => 'A-'
            ],
           
            
           
            [
                'idUser' => '38',
                'idMatkul' => '11',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '38',
                'idMatkul' => '12',
                'komulatif' => '1',
                'nilaiAkhir' => 'B'
            ],
            [
                'idUser' => '38',
                'idMatkul' => '17',
                'komulatif' => '2',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '38',
                'idMatkul' => '18',
                'komulatif' => '2',
                'nilaiAkhir' => 'B'
            ],
            [
                'idUser' => '38',
                'idMatkul' => '23',
                'komulatif' => '3',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '38',
                'idMatkul' => '24',
                'komulatif' => '3',
                'nilaiAkhir' => 'A-'
            ],
            [
                'idUser' => '38',
                'idMatkul' => '29',
                'komulatif' => '4',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '38',
                'idMatkul' => '30',
                'komulatif' => '4',
                'nilaiAkhir' => 'A-'
            ],
                      [
                'idUser' => '39',
                'idMatkul' => '11',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '39',
                'idMatkul' => '12',
                'komulatif' => '1',
                'nilaiAkhir' => 'B'
            ],
            [
                'idUser' => '39',
                'idMatkul' => '17',
                'komulatif' => '2',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '39',
                'idMatkul' => '18',
                'komulatif' => '2',
                'nilaiAkhir' => 'B'
            ],
            [
                'idUser' => '39',
                'idMatkul' => '23',
                'komulatif' => '3',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '39',
                'idMatkul' => '24',
                'komulatif' => '3',
                'nilaiAkhir' => 'A-'
            ],
            [
                'idUser' => '39',
                'idMatkul' => '29',
                'komulatif' => '4',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '39',
                'idMatkul' => '30',
                'komulatif' => '4',
                'nilaiAkhir' => 'A-'
            ],
            [
                'idUser' => '16',
                'idMatkul' => '32',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '16',
                'idMatkul' => '33',
                'komulatif' => '1',
                'nilaiAkhir' => 'A-'
            ],
            [
                'idUser' => '17',
                'idMatkul' => '32',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '17',
                'idMatkul' => '33',
                'komulatif' => '1',
                'nilaiAkhir' => 'A-'
            ],
            [
                'idUser' => '25',
                'idMatkul' => '32',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '25',
                'idMatkul' => '33',
                'komulatif' => '1',
                'nilaiAkhir' => 'A-'
            ],
             [
                'idUser' => '25',
                'idMatkul' => '39',
                'komulatif' => '2',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '25',
                'idMatkul' => '40',
                'komulatif' => '2',
                'nilaiAkhir' => 'A-'
            ],
             [
                'idUser' => '25',
                'idMatkul' => '45',
                'komulatif' => '3',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '25',
                'idMatkul' => '46',
                'komulatif' => '3',
                'nilaiAkhir' => 'A-'
            ],

            [
                'idUser' => '26',
                'idMatkul' => '32',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '26',
                'idMatkul' => '33',
                'komulatif' => '1',
                'nilaiAkhir' => 'A-'
            ],
             [
                'idUser' => '26',
                'idMatkul' => '39',
                'komulatif' => '2',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '26',
                'idMatkul' => '40',
                'komulatif' => '2',
                'nilaiAkhir' => 'A-'
            ],
             [
                'idUser' => '26',
                'idMatkul' => '45',
                'komulatif' => '3',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '26',
                'idMatkul' => '46',
                'komulatif' => '3',
                'nilaiAkhir' => 'A-'
            ],

            //2017

            [
                'idUser' => '41',
                'idMatkul' => '32',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '41',
                'idMatkul' => '33',
                'komulatif' => '1',
                'nilaiAkhir' => 'A-'
            ],
            [
                'idUser' => '41',
                'idMatkul' => '39',
                'komulatif' => '2',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '41',
                'idMatkul' => '40',
                'komulatif' => '2',
                'nilaiAkhir' => 'A-'
            ],
             [
                'idUser' => '41',
                'idMatkul' => '45',
                'komulatif' => '3',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '41',
                'idMatkul' => '46',
                'komulatif' => '3',
                'nilaiAkhir' => 'A-'
            ],
            [
                'idUser' => '41',
                'idMatkul' => '52',
                'komulatif' => '4',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '41',
                'idMatkul' => '53',
                'komulatif' => '4',
                'nilaiAkhir' => 'A-'
            ],
            //2017-2
             [
                'idUser' => '42',
                'idMatkul' => '32',
                'komulatif' => '1',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '42',
                'idMatkul' => '33',
                'komulatif' => '1',
                'nilaiAkhir' => 'A-'
            ],
            [
                'idUser' => '42',
                'idMatkul' => '39',
                'komulatif' => '2',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '42',
                'idMatkul' => '40',
                'komulatif' => '2',
                'nilaiAkhir' => 'A-'
            ],
             [
                'idUser' => '42',
                'idMatkul' => '45',
                'komulatif' => '3',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '42',
                'idMatkul' => '46',
                'komulatif' => '3',
                'nilaiAkhir' => 'A-'
            ],
            [
                'idUser' => '42',
                'idMatkul' => '52',
                'komulatif' => '4',
                'nilaiAkhir' => 'A'
            ],
            [
                'idUser' => '42',
                'idMatkul' => '53',
                'komulatif' => '4',
                'nilaiAkhir' => 'A-'
            ],




        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transkrips');
    }
}

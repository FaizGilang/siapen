<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKartuRencanasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kartu_rencanas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUser');
            $table->integer('idPaketKuliah');
            $table->integer('izin');
            $table->integer('alpa');
            $table->enum('status', ['aktif', 'tidak']);
            $table->timestamps();
        });
        DB::table('kartu_rencanas')->insert([
            [
                'idUser' => '13',
                'idPaketKuliah' => '8',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '14',
                'idPaketKuliah' => '8',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '22',
                'idPaketKuliah' => '5',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '23',
                'idPaketKuliah' => '5',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '22',
                'idPaketKuliah' => '6',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '23',
                'idPaketKuliah' => '6',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '22',
                'idPaketKuliah' => '7',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '23',
                'idPaketKuliah' => '7',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '38',
                'idPaketKuliah' => '1',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '39',
                'idPaketKuliah' => '1',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '38',
                'idPaketKuliah' => '2',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '39',
                'idPaketKuliah' => '2',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '38',
                'idPaketKuliah' => '3',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '39',
                'idPaketKuliah' => '3',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '38',
                'idPaketKuliah' => '4',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '39',
                'idPaketKuliah' => '4',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'tidak'
            ],
            [
                'idUser' => '16',
                'idPaketKuliah' => '16',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '17',
                'idPaketKuliah' => '16',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '25',
                'idPaketKuliah' => '13',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '26',
                'idPaketKuliah' => '13',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '25',
                'idPaketKuliah' => '14',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '26',
                'idPaketKuliah' => '14',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '25',
                'idPaketKuliah' => '15',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '26',
                'idPaketKuliah' => '15',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '41',
                'idPaketKuliah' => '9',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '42',
                'idPaketKuliah' => '9',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '41',
                'idPaketKuliah' => '10',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '42',
                'idPaketKuliah' => '10',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '41',
                'idPaketKuliah' => '11',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '42',
                'idPaketKuliah' => '11',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '41',
                'idPaketKuliah' => '12',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ],
            [
                'idUser' => '42',
                'idPaketKuliah' => '12',
                'izin' => '0',
                'alpa' => '0',
                'status' => 'aktif'
            ]

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kartu_rencanas');
    }
}

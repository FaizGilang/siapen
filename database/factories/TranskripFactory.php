<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

use App\Transkrip;
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Transkrip::class, function (Faker\Generator $faker) {
    return [
        'idUser' => $faker->buildingNumber,
        'idMatkul' => $faker->buildingNumber,
        'komulatif' => $faker->buildingNumber,
        'nilaiAkhir' => $faker->randomLetter
    ];
});
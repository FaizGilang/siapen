<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

use App\HasilStudi;
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Transkrip::class, function (Faker\Generator $faker) {
    return [
        'idUser' => $faker->buildingNumber,
        'idMatkul' => $faker->buildingNumber,
        'uts' => $faker->numberBetween($min = 10, $max = 100),
        'uas' => $faker->numberBetween($min = 10, $max = 100),
        'praktek' => $faker->numberBetween($min = 10, $max = 100),
        'tugas' => $faker->numberBetween($min = 10, $max = 100)
    ];
});
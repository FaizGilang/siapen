<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

use App\User;
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(User::class, function (Faker\Generator $faker) {
    return [
        'nama' => $faker->word,
        'nomorInduk' => $faker->word,
        'email' => $faker->word,
        'password' => $faker->word,
        'kategori' => $faker->buildingNumber,
        'prodi' => $faker->buildingNumber,
        'tahunMasuk' => $faker->year,
        'file' => $faker->word,
        'status' => 'aktif'
    ];
});
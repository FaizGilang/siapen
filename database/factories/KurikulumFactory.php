<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

use App\Kurikulum;
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Kurikulum::class, function (Faker\Generator $faker) {
    return [
        'idProdi' => $faker->buildingNumber,
        'tahun' => $faker->year,
        'status' => 'aktif'
    ];
});
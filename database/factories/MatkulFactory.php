<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

use App\Matkul;
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Matkul::class, function (Faker\Generator $faker) {
    return [
        'kodeprodi' => $faker->buildingNumber,
        'kode' => $faker->text($maxNbChars = 15),
        'nama' => $faker->word,
        'komulatif' => $faker->buildingNumber,
        'SKS' => $faker->buildingNumber,
        'SKSteori' => $faker->buildingNumber,
        'SKSpraktek' => $faker->buildingNumber,
        'jpm' => $faker->buildingNumber,
        'dosen1' => $faker->word,
        'dosen2' => $faker->word,
        'bobotUts' => $faker->numberBetween($min = 10, $max = 100),
        'bobotUas' => $faker->numberBetween($min = 10, $max = 100),
        'bobotTugas' => $faker->numberBetween($min = 10, $max = 100),
        'bobotPraktek' => $faker->numberBetween($min = 10, $max = 100),
        'status' => 'aktif'
    ];
});
<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

use App\Prodi;
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Prodi::class, function (Faker\Generator $faker) {
    return [
        'nama' => $faker->word,
        'kode' => $faker->text($maxNbChars = 5)  ,
        'nomor' => $faker->numberBetween($min = 10, $max = 100),
        'status' => 'aktif'
    ];
});
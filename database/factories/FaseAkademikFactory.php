<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

use App\FaseAkademik;
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(FaseAkademik::class, function (Faker\Generator $faker) {
    return [
        'tahunAjaran' => $faker->word,
        'faseRegistrasi' => $faker->date($format = 'Y-m-d'),
        'awalKuliah' => $faker->date($format = 'Y-m-d'),
        'akhirKuliah' => $faker->date($format = 'Y-m-d'),
        'faseEval' => $faker->date($format = 'Y-m-d'),
        'semester' => $faker->buildingNumber,
        'status' => 'aktif'
    ];
});
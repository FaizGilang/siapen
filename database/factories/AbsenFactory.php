<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

use App\KartuRencana;
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(KartuRencana::class, function (Faker\Generator $faker) {
    return [
        'idUser' => $faker->buildingNumber,
        'idPaketKuliah' => $faker->buildingNumber,
        'izin' => $faker->buildingNumber,
        'alpa' => $faker->buildingNumber,
        'status' => 'aktif'
    ];
});


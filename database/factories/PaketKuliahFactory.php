<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

use App\PaketKuliah;
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(PaketKuliah::class, function (Faker\Generator $faker) {
    return [
        'idKurikulum' => $faker->buildingNumber,
        'tahunAjaran' => $faker->word,
        'komulatif' => $faker->buildingNumber,
        'status' => 'aktif'
    ];
});
<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\HasilStudi;
use App\User;
use App\Matkul;

class UnggahNilaiTest extends TestCase
{

    public function test_unggah_nilai()
    {
       // $req = $this->call("GET","downloadExcel/{type}/{id}");

       // $this->assertTrue(strpos($req->content(), 'Format Nilai.xlsx') !== false);

    	// UploadedFile::fake()->create('document.pdf', $sizeInKilobytes);

     //    Storage::fake('hasil_studis');

    	// $file = UploadedFile::fake()->create('Format Nilai.xlsx', 1000);

     //    $response = $this->json('POST', 'importExcel', [
     //        'hasil_studis' => $file,
     //    ]);

     //    // Assert the file was stored...
     //    Storage::disk('hasil_studis')->assertExists($file->hashName());

    	$data = [
    		'idUser' => mt_rand(1,4),
    		'idMatkul' => mt_rand(1,4),
    		'uts' => mt_rand(1,100),
    		'uas' => mt_rand(1,100),
    		'tugas' => mt_rand(1,100),
    		'praktek' => mt_rand(1,100),
    		
    	];

    	$unggah = new HasilStudi;
    	$unggah->idUser = $data['idUser'];
    	$unggah->idMatkul = $data['idMatkul'];
    	$unggah->uts = $data['uts'];
    	$unggah->uas = $data['uas'];
    	$unggah->tugas = $data['tugas'];
    	$unggah->praktek = $data['praktek'];
    	$unggah->save();

        $this->assertInstanceOf(HasilStudi::class, $unggah);
        $this->assertEquals($data['idUser'], $unggah->idUser);
        $this->assertEquals($data['idMatkul'], $unggah->idMatkul);
        $this->assertEquals($data['uts'], $unggah->uts);
        $this->assertEquals($data['uas'],$unggah->uas);
        $this->assertEquals($data['tugas'],$unggah->tugas);
        $this->assertEquals($data['praktek'],$unggah->praktek);
    }

}

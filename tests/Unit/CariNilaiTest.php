<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\HasilStudi;

class CariNilaiTest extends TestCase
{
    public function test_cari_nilai()
	{
  		$response=$this->call('GET', '/halamanDosen/cariNilai');
        
  		$this->assertRegexp('/mahasiswa01/', $response-> getContent());
	}
}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Transkrip;

class CetakTranskripTest extends TestCase
{

    public function test_cetak_transkrip()
    {
       $req = $this->call("GET","/hasilStudi/pdfTranskrip/");

      $this->assertTrue(strpos($req->content(), 'Transkrip.pdf') !== false);
    }

}

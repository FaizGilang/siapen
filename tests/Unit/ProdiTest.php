<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Prodi;

class ProdiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

   /** @test */
    public function test_tambah_prodi()
    {	
    	$data = [
    		'kode' => str_random(5),
    		'nama' => str_random(50),
    		'nomor' => mt_rand(1,100),
    		'status' => 'aktif'
    	];

    	$prodi = new Prodi;
    	$prodi->kode = $data['kode'];
    	$prodi->nama = $data['nama'];
    	$prodi->nomor = $data['nomor'];
    	$prodi->status = $data['status'];
    	$prodi->save();

        $this->assertInstanceOf(Prodi::class, $prodi);
        $this->assertEquals($data['kode'], $prodi->kode);
        $this->assertEquals($data['nama'], $prodi->nama);
        $this->assertEquals($data['nomor'], $prodi->nomor);
        $this->assertEquals($data['status'],$prodi->status);
	}

	/** @test */
    public function test_tampilkan_prodi()
   {
      $factory_prodi = factory(Prodi::class)->create();
      $prodi = new Prodi;
      $found = $prodi->findOrFail($factory_prodi->id);

      $this->assertInstanceOf(Prodi::class, $found);
      $this->assertEquals($found->kode, $factory_prodi->kode);
      $this->assertEquals($found->nama, $factory_prodi->nama);
      $this->assertEquals($found->nomor, $factory_prodi->nomor);
      $this->assertEquals($found->status, $factory_prodi->status);
     
   }

   /** @test */
   public function test_ubah_prodi()
    {
      $factory_prodi = factory(Prodi::class)->create();
      $prodi = new Prodi;
      $prodi = $prodi->findOrFail($factory_prodi->id);     

      $data = [
        	'kode' => str_random(5),
    		'nama' => str_random(50),
    		'nomor' => mt_rand(1,100),
    		'status' => 'aktif'
      ];

      $update = $prodi->update($data);

      $this->assertTrue($update);
      $this->assertInstanceOf(Prodi::class, $prodi);
      $this->assertEquals($data['kode'],$prodi->kode);
      $this->assertEquals($data['nama'],$prodi->nama);
      $this->assertEquals($data['nomor'],$prodi->nomor);
      $this->assertEquals($data['status'],$prodi->status);

    }

    /** @test */
    public function test_hapus_prodi()
    {
        $factory_prodi = factory(Prodi::class)->create();
      
        $prodi = new Prodi;
        $prodi = $prodi->findOrFail($factory_prodi->id); 
        $delete = $prodi->delete();

        $this->assertTrue($delete);
    }


 

}
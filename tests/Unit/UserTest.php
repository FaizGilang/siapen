<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
   /** @test */
   public function test_tambah_user()
   {
   		$data = [
   			'nama' => str_random(50),
   			'nomorInduk' => str_random(25),
   			'email' => str_random(50),
   			'password' => str_random(50),
   			'kategori' => mt_rand(1,4),
   			'prodi' => mt_rand(1,3),
   			'tahunMasuk' => mt_rand(1,4),
        'file' => 'Pengguna-1000.jpg',
   			'status' => 'aktif'

   		];

   		$pengguna = new User;
   		$pengguna->nama = $data['nama'];
   		$pengguna->nomorInduk = $data['nomorInduk'];
   		$pengguna->email = $data['email'];
   		$pengguna->password = $data['password'];
   		$pengguna->kategori = $data['kategori'];
   		$pengguna->prodi = $data['prodi'];
      $pengguna->tahunMasuk = $data['tahunMasuk'];
   		$pengguna->file = $data['file'];
   		$pengguna->status = $data['status'];
   		$pengguna->save();

   		$this->assertInstanceOf(User::class, $pengguna);
   		$this->assertEquals($data['nama'],$pengguna->nama);
   		$this->assertEquals($data['nomorInduk'],$pengguna->nomorInduk);
   		$this->assertEquals($data['email'],$pengguna->email);
   		$this->assertEquals($data['password'],$pengguna->password);
   		$this->assertEquals($data['kategori'],$pengguna->kategori);
   		$this->assertEquals($data['prodi'],$pengguna->prodi);
      $this->assertEquals($data['tahunMasuk'],$pengguna->tahunMasuk);
   		$this->assertEquals($data['file'],$pengguna->file);
   		$this->assertEquals($data['status'],$pengguna->status);
   }

   /** @test */
   public function test_tampilkan_user()
   {
      $factory_pengguna = factory(User::class)->create();
      $pengguna = new User;
      $found = $pengguna->findOrFail($factory_pengguna->id);

      $this->assertInstanceOf(User::class, $found);
      $this->assertEquals($found->nama, $factory_pengguna->nama);
      $this->assertEquals($found->nomorInduk, $factory_pengguna->nomorInduk);
      $this->assertEquals($found->email, $factory_pengguna->email);
      $this->assertEquals($found->password, $factory_pengguna->password);
      $this->assertEquals($found->kategori, $factory_pengguna->kategori);
      $this->assertEquals($found->prodi, $factory_pengguna->prodi);
      $this->assertEquals($found->tahunMasuk, $factory_pengguna->tahunMasuk);
      $this->assertEquals($found->file, $factory_pengguna->file);
      $this->assertEquals($found->status, $factory_pengguna->status);
   }
  /** @test */
   public function test_ubah_user()
    {
      $factory_pengguna = factory(User::class)->create();
      $pengguna = new User;
      $pengguna = $pengguna->findOrFail($factory_pengguna->id);     

      $data = [
        'nama' => str_random(50),
        'nomorInduk' => str_random(25),
        'email' => str_random(50),
        'password' => str_random(50),
        'kategori' => mt_rand(1,4),
        'prodi' => mt_rand(1,3),
        'tahunMasuk' => mt_rand(1,4),
        'file' => 'Pengguna-1000.jpg',
        'status' => 'aktif'
      ];

      $update = $pengguna->update($data);

      $this->assertTrue($update);
      $this->assertInstanceOf(User::class, $pengguna);
      $this->assertEquals($data['nama'],$pengguna->nama);
      $this->assertEquals($data['nomorInduk'],$pengguna->nomorInduk);
      $this->assertEquals($data['email'],$pengguna->email);
      $this->assertEquals($data['password'],$pengguna->password);
      $this->assertEquals($data['kategori'],$pengguna->kategori);
      $this->assertEquals($data['prodi'],$pengguna->prodi);
      $this->assertEquals($data['tahunMasuk'],$pengguna->tahunMasuk);
      $this->assertEquals($data['file'],$pengguna->file);
      $this->assertEquals($data['status'],$pengguna->status);
    }

    /** @test */
    public function test_hapus_user()
    {
        $factory_pengguna = factory(User::class)->create();
      
        $pengguna = new User;
        $pengguna = $pengguna->findOrFail($factory_pengguna->id); 
        $delete = $pengguna->delete();

        $this->assertTrue($delete);
    }
        
       
}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\FaseAkademik;

class FaseAkademikTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
   public function test_tambah_fase_akademik()
   {
   		$data = [
   			'tahunAjaran' => str_random(10),
   			'faseRegistrasi' => date($format = 'Y-m-d'),
   			'awalKuliah' => date($format = 'Y-m-d'),
   			'akhirKuliah' => date($format = 'Y-m-d'),
   			'faseEval' => date($format = 'Y-m-d'),
   			'semester' => str_random(1,4),
   			'status' => 'tidak'

   		];

   		$FaseAkademik = new FaseAkademik;
   		$FaseAkademik->tahunAjaran = $data['tahunAjaran'];
   		$FaseAkademik->faseRegistrasi = $data['faseRegistrasi'];
   		$FaseAkademik->awalKuliah = $data['awalKuliah'];
   		$FaseAkademik->akhirKuliah = $data['akhirKuliah'];
   		$FaseAkademik->faseEval = $data['faseEval'];
   		$FaseAkademik->semester = $data['semester'];
   		$FaseAkademik->status = $data['status'];
   		$FaseAkademik->save();

   		$this->assertInstanceOf(FaseAkademik::class, $FaseAkademik);
   		$this->assertEquals($data['tahunAjaran'],$FaseAkademik->tahunAjaran);
   		$this->assertEquals($data['faseRegistrasi'],$FaseAkademik->faseRegistrasi);
   		$this->assertEquals($data['awalKuliah'],$FaseAkademik->awalKuliah);
   		$this->assertEquals($data['akhirKuliah'],$FaseAkademik->akhirKuliah);
   		$this->assertEquals($data['faseEval'],$FaseAkademik->faseEval);
   		$this->assertEquals($data['semester'],$FaseAkademik->semester);
   		$this->assertEquals($data['status'],$FaseAkademik->status);
   }


   /** @test */
   public function test_tampilkan_fase_akademik()
   {
      $factory_FaseAkademik = factory(FaseAkademik::class)->create();
      $FaseAkademik = new FaseAkademik;
      $found = $FaseAkademik->findOrFail($factory_FaseAkademik->id);

      $this->assertInstanceOf(FaseAkademik::class, $found);
      $this->assertEquals($found->tahunAjaran, $factory_FaseAkademik->tahunAjaran);
      $this->assertEquals($found->faseRegistrasi, $factory_FaseAkademik->faseRegistrasi);
      $this->assertEquals($found->awalKuliah, $factory_FaseAkademik->awalKuliah);
      $this->assertEquals($found->akhirKuliah, $factory_FaseAkademik->akhirKuliah);
      $this->assertEquals($found->faseEval, $factory_FaseAkademik->faseEval);
      $this->assertEquals($found->semester, $factory_FaseAkademik->semester);
      $this->assertEquals($found->status, $factory_FaseAkademik->status);
   }

   /** @test */
   public function test_ubah_fase_akademik()
    {
      $factory_FaseAkademik = factory(FaseAkademik::class)->create();
      $FaseAkademik = new FaseAkademik;
      $FaseAkademik = $FaseAkademik->findOrFail($factory_FaseAkademik->id);     

      $data = [
        	'tahunAjaran' => str_random(10),
   			'faseRegistrasi' => date($format = 'Y-m-d'),
   			'awalKuliah' => date($format = 'Y-m-d'),
   			'akhirKuliah' => date($format = 'Y-m-d'),
   			'faseEval' => date($format = 'Y-m-d'),
   			'semester' => str_random(1,4),
   			'status' => 'tidak'
      ];

      $update = $FaseAkademik->update($data);

      $this->assertTrue($update);
      $this->assertInstanceOf(FaseAkademik::class, $FaseAkademik);
      $this->assertEquals($data['tahunAjaran'],$FaseAkademik->tahunAjaran);
   	  $this->assertEquals($data['faseRegistrasi'],$FaseAkademik->faseRegistrasi);
   	  $this->assertEquals($data['awalKuliah'],$FaseAkademik->awalKuliah);
   	  $this->assertEquals($data['akhirKuliah'],$FaseAkademik->akhirKuliah);
   	  $this->assertEquals($data['faseEval'],$FaseAkademik->faseEval);
   	  $this->assertEquals($data['semester'],$FaseAkademik->semester);
   	  $this->assertEquals($data['status'],$FaseAkademik->status);
    }

    /** @test */
    public function test_hapus_fase_akademik()
    {
        $factory_FaseAkademik = factory(FaseAkademik::class)->create();
      
        $FaseAkademik = new FaseAkademik;
        $FaseAkademik = $FaseAkademik->findOrFail($factory_FaseAkademik->id); 
        $delete = $FaseAkademik->delete();

        $this->assertTrue($delete);
    }
}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\HasilStudi;
use App\User;
use App\Matkul;

class UnduhNilaiTest extends TestCase
{

    public function test_unduh_nilai()
    {
      $req = $this->call("GET","downloadExcel/{type}/{id}");
   $this->assertTrue(strpos($req->content(), 'Format Nilai.xlsx') !== true);

    }

}

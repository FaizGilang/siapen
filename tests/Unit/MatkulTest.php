<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Matkul;

class MatkulTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
   public function test_tambah_matkul()
   {
   		$data = [
   			'kodeprodi' => mt_rand(1,3),
   			'kode' => str_random(15),
   			'nama' => str_random(50),
   			'komulatif' => mt_rand(1,4),
   			'SKS' => mt_rand(1,4),
   			'SKSteori' => mt_rand(1,4),
   			'SKSpraktek' => mt_rand(1,4),
   			'jpm' => mt_rand(1,4),
   			'dosen1' => str_random(50),
   			'dosen2' => str_random(50),
        'bobotUts' => mt_rand(1,100),
        'bobotUas' => mt_rand(1,100),
        'bobotTugas' => mt_rand(1,100),
        'bobotPraktek' => mt_rand(1,100),
   			'status' => 'aktif'

   		];

   		$matkul = new Matkul;
   		$matkul->kodeprodi = $data['kodeprodi'];
   		$matkul->kode = $data['kode'];
   		$matkul->nama = $data['nama'];
   		$matkul->komulatif = $data['komulatif'];
   		$matkul->SKS = $data['SKS'];
   		$matkul->SKSteori = $data['SKSteori'];
   		$matkul->SKSpraktek = $data['SKSpraktek'];
   		$matkul->jpm = $data['jpm'];
   		$matkul->dosen1 = $data['dosen1'];
      $matkul->dosen2 = $data['dosen2'];
      $matkul->bobotUts = $data['bobotUts'];
      $matkul->bobotUas = $data['bobotUas'];
      $matkul->bobotTugas = $data['bobotTugas'];
   		$matkul->bobotPraktek = $data['bobotPraktek'];
   		$matkul->status = $data['status'];
   		$matkul->save();

   		$this->assertInstanceOf(Matkul::class, $matkul);
   		$this->assertEquals($data['kodeprodi'],$matkul->kodeprodi);
   		$this->assertEquals($data['kode'],$matkul->kode);
   		$this->assertEquals($data['nama'],$matkul->nama);
   		$this->assertEquals($data['komulatif'],$matkul->komulatif);
   		$this->assertEquals($data['SKS'],$matkul->SKS);
   		$this->assertEquals($data['SKSteori'],$matkul->SKSteori);
   		$this->assertEquals($data['SKSpraktek'],$matkul->SKSpraktek);
   		$this->assertEquals($data['jpm'],$matkul->jpm);
   		$this->assertEquals($data['dosen1'],$matkul->dosen1);
      $this->assertEquals($data['dosen2'],$matkul->dosen2);
      $this->assertEquals($data['bobotUts'],$matkul->bobotUts);
      $this->assertEquals($data['bobotUas'],$matkul->bobotUas);
      $this->assertEquals($data['bobotTugas'],$matkul->bobotTugas);
   		$this->assertEquals($data['bobotPraktek'],$matkul->bobotPraktek);
   		$this->assertEquals($data['status'],$matkul->status);
   }

   /** @test */
   public function test_tampilkan_matkul()
   {
      $factory_matkul = factory(Matkul::class)->create();
      $matkul = new Matkul;
      $found = $matkul->findOrFail($factory_matkul->id);

      $this->assertInstanceOf(Matkul::class, $found);
      $this->assertEquals($found->kodeprodi, $factory_matkul->kodeprodi);
      $this->assertEquals($found->kode, $factory_matkul->kode);
      $this->assertEquals($found->nama, $factory_matkul->nama);
      $this->assertEquals($found->komulatif, $factory_matkul->komulatif);
      $this->assertEquals($found->SKS, $factory_matkul->SKS);
      $this->assertEquals($found->SKSteori, $factory_matkul->SKSteori);
      $this->assertEquals($found->SKSpraktek, $factory_matkul->SKSpraktek);
      $this->assertEquals($found->jpm, $factory_matkul->jpm);
      $this->assertEquals($found->dosen1, $factory_matkul->dosen1);
      $this->assertEquals($found->dosen2, $factory_matkul->dosen2);
      $this->assertEquals($found->bobotUts, $factory_matkul->bobotUts);
      $this->assertEquals($found->bobotUas, $factory_matkul->bobotUas);
      $this->assertEquals($found->bobotTugas, $factory_matkul->bobotTugas);
      $this->assertEquals($found->bobotPraktek, $factory_matkul->bobotPraktek);
      $this->assertEquals($found->status, $factory_matkul->status);
   }

   /** @test */
   public function test_ubah_matkul()
    {
      $factory_matkul = factory(Matkul::class)->create();
      $matkul = new Matkul;
      $matkul = $matkul->findOrFail($factory_matkul->id);     

      $data = [
        	'kodeprodi' => mt_rand(1,3),
   			'kode' => str_random(15),
   			'nama' => str_random(50),
   			'komulatif' => mt_rand(1,4),
   			'SKS' => mt_rand(1,4),
   			'SKSteori' => mt_rand(1,4),
   			'SKSpraktek' => mt_rand(1,4),
   			'jpm' => mt_rand(1,4),
   			'dosen1' => str_random(50),
   			'dosen2' => str_random(50),
        'bobotUts' => mt_rand(1,100),
        'bobotUas' => mt_rand(1,100),
        'bobotTugas' => mt_rand(1,100),
        'bobotPraktek' => mt_rand(1,100),
   			'status' => 'aktif'
      ];

      $update = $matkul->update($data);

      $this->assertTrue($update);
      $this->assertInstanceOf(Matkul::class, $matkul);
      $this->assertEquals($data['kodeprodi'],$matkul->kodeprodi);
   	  $this->assertEquals($data['kode'],$matkul->kode);
   	  $this->assertEquals($data['nama'],$matkul->nama);
   	  $this->assertEquals($data['komulatif'],$matkul->komulatif);
   	  $this->assertEquals($data['SKS'],$matkul->SKS);
   	  $this->assertEquals($data['SKSteori'],$matkul->SKSteori);
   	  $this->assertEquals($data['SKSpraktek'],$matkul->SKSpraktek);
   	  $this->assertEquals($data['jpm'],$matkul->jpm);
   	  $this->assertEquals($data['dosen1'],$matkul->dosen1);
      $this->assertEquals($data['dosen2'],$matkul->dosen2);
      $this->assertEquals($data['bobotUts'],$matkul->bobotUts);
      $this->assertEquals($data['bobotUas'],$matkul->bobotUas);
      $this->assertEquals($data['bobotTugas'],$matkul->bobotTugas);
   	  $this->assertEquals($data['bobotPraktek'],$matkul->bobotPraktek);
   	  $this->assertEquals($data['status'],$matkul->status);
    }

     /** @test */
    public function test_hapus_matkul()
    {
        $factory_matkul = factory(Matkul::class)->create();
      
        $matkul = new Matkul;
        $matkul = $matkul->findOrFail($factory_matkul->id); 
        $delete = $matkul->delete();

        $this->assertTrue($delete);
    }
}

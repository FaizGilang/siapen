<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\HasilStudi;

class CetakHasilStudiTest extends TestCase
{

    public function test_cetak_hsm()
    {
       $req = $this->call("GET","/hasilStudi/pdfHSM/{id}");

      $this->assertTrue(strpos($req->content(), 'HSM.pdf') !== false);
    }

}

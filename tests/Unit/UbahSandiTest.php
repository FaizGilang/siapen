<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use Auth;
use DB;

class UbahSandiTest extends TestCase
{

    public function test_ubah_sandi()
	{
	   $factory_sandi = factory(User::class)->create();
	   $sandi = new User;
	   $sandi = $sandi->findOrFail($factory_sandi->id);     

	     $data = [
	            'password' => str_random(50)
	      ];

	  $update = $sandi->update($data);

	  $this->assertTrue($update);
	  $this->assertInstanceOf(User::class, $sandi);
	  $this->assertEquals($data['password'],$sandi->password);
	}


}

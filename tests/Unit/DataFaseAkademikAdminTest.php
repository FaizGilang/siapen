<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\FaseAkademik;

class FaseAkademikTest extends TestCase
{
    /** @test */
    public function test_fase_akademikA()
    {
      $factory_FaseAkademik = factory(FaseAkademik::class)-> create();
      $FaseAkademik = new FaseAkademik;
      $found = $FaseAkademik-> findOrFail($factory_FaseAkademik->id);

      $this->assertInstanceOf(FaseAkademik::class, $found);
      $this->assertEquals($found->tahunAjaran, $factory_FaseAkademik->tahunAjaran);
      $this->assertEquals($found->faseRegistrasi, $factory_FaseAkademik->faseRegistrasi);
      $this->assertEquals($found->awalKuliah, $factory_FaseAkademik->awalKuliah);
      $this->assertEquals($found->akhirKuliah, $factory_FaseAkademik->akhirKuliah);
      $this->assertEquals($found->faseEval, $factory_FaseAkademik->faseEval);
      $this->assertEquals($found->semester, $factory_FaseAkademik->semester);
      $this->assertEquals($found->status, $factory_FaseAkademik->status);

    }
}

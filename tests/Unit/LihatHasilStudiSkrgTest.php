<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Matkul;
use App\Transkrip;
use App\HasilStudi;

class LihatHasilStudiSkrgTest extends TestCase
{
    
    public function test_tampilkan_hsm_skrg()
    {
       $factory_nilai = factory(HasilStudi::class)-> create();
       $nilai = new HasilStudi;
       $found = $nilai->findOrFail($factory_nilai->id);

       $this->assertInstanceOf(HasilStudi::class, $found);
       $this->assertEquals($found->idUser, $factory_nilai->idUser);
       $this->assertEquals($found->idMatkul, $factory_nilai->idMatkul);
       $this->assertEquals($found->uts, $factory_nilai->uts);
       $this->assertEquals($found->uas, $factory_nilai->uas); 
       $this->assertEquals($found->praktek, $factory_nilai->praktek); 
       $this->assertEquals($found->tugas, $factory_nilai->tugas); 
       
    }

}

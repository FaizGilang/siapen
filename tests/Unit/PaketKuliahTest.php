<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\PaketKuliah;

class PaketKuliahTest extends TestCase
{
    
   public function test_tambah_paket_kuliah()
   {
   		$data = [
   			'idKurikulum' => mt_rand(1,15),
   			'tahunAjaran' => str_random(10),
   			'komulatif' => mt_rand(1,4),
   			'status' => 'aktif'
   		];

   		$PaketKuliah = new PaketKuliah;
   		$PaketKuliah->idKurikulum = $data['idKurikulum'];
   		$PaketKuliah->tahunAjaran = $data['tahunAjaran'];
   		$PaketKuliah->komulatif = $data['komulatif'];
   		$PaketKuliah->status = $data['status'];
   		$PaketKuliah->save();

   		$this->assertInstanceOf(PaketKuliah::class, $PaketKuliah);
   		$this->assertEquals($data['idKurikulum'],$PaketKuliah->idKurikulum);
   		$this->assertEquals($data['tahunAjaran'],$PaketKuliah->tahunAjaran);
   		$this->assertEquals($data['komulatif'],$PaketKuliah->komulatif);
   		$this->assertEquals($data['status'],$PaketKuliah->status);
   }

   /** @test */
   public function test_tampilkan_paket_kuliah()
   {
      $factory_PaketKuliah = factory(PaketKuliah::class)->create();
      $PaketKuliah = new PaketKuliah;
      $found = $PaketKuliah->findOrFail($factory_PaketKuliah->id);

      $this->assertInstanceOf(PaketKuliah::class, $found);
      $this->assertEquals($found->idKurikulum, $factory_PaketKuliah->idKurikulum);
      $this->assertEquals($found->tahunAjaran, $factory_PaketKuliah->tahunAjaran);
      $this->assertEquals($found->komulatif, $factory_PaketKuliah->komulatif);
      $this->assertEquals($found->status, $factory_PaketKuliah->status);
   }

   /** @test */
    public function test_hapus_paket_kuliah()
    {
        $factory_PaketKuliah = factory(PaketKuliah::class)->create();
      
        $PaketKuliah = new PaketKuliah;
        $PaketKuliah = $PaketKuliah->findOrFail($factory_PaketKuliah->id); 
        $delete = $PaketKuliah->delete();

        $this->assertTrue($delete);
    }
}

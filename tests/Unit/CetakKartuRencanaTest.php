<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\KartuRencana;

class CetakKartuRencanaTest extends TestCase
{
    public function test_cetak_krs()
    {
       $req = $this->call("GET","/kartuRencana/pdfKRS/");

      $this->assertTrue(strpos($req->content(), 'KRS.pdf') !== false);
    }

}

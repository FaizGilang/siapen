<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Kurikulum;

class KurikulumTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
     /** @test */
   public function test_tambah_kurikulum()
   {
   		$data = [
   			'tahun' => mt_rand(1,4),
   			'idProdi' => mt_rand(1,3),
   			'status' => 'aktif'
   		];

   		$kurikulum = new Kurikulum;
   		$kurikulum->tahun = $data['tahun'];
   		$kurikulum->idProdi = $data['idProdi'];
   		$kurikulum->status = $data['status'];
   		$kurikulum->save();

   		$this->assertInstanceOf(Kurikulum::class, $kurikulum);
   		$this->assertEquals($data['tahun'],$kurikulum->tahun);
   		$this->assertEquals($data['idProdi'],$kurikulum->idProdi);
   		$this->assertEquals($data['status'],$kurikulum->status);
   }

   /** @test */
   public function test_tampilkan_kurikulum()
   {
      $factory_kurikulum = factory(Kurikulum::class)->create();
      $kurikulum = new Kurikulum;
      $found = $kurikulum->findOrFail($factory_kurikulum->id);

      $this->assertInstanceOf(Kurikulum::class, $found);
      $this->assertEquals($found->tahun, $factory_kurikulum->tahun);
      $this->assertEquals($found->idProdi, $factory_kurikulum->idProdi);
      $this->assertEquals($found->status, $factory_kurikulum->status);
   }

   /** @test */
    public function test_ubah_kurikulum()
    {
      $factory_kurikulum = factory(Kurikulum::class)->create();
      $kurikulum = new Kurikulum;
      $kurikulum = $kurikulum->findOrFail($factory_kurikulum->id);     

      $data = [
        	'tahun' => mt_rand(1,4),
   			'idProdi' => mt_rand(1,3),
   			'status' => 'aktif'
      ];

      $update = $kurikulum->update($data);

      $this->assertTrue($update);
      $this->assertInstanceOf(Kurikulum::class, $kurikulum);
      $this->assertEquals($data['idProdi'],$kurikulum->idProdi);
      $this->assertEquals($data['tahun'],$kurikulum->tahun);
      $this->assertEquals($data['status'],$kurikulum->status);
    }

     /** @test */
    public function test_hapus_kurikulum()
    {
        $factory_kurikulum = factory(Kurikulum::class)->create();
      
        $kurikulum = new Kurikulum;
        $kurikulum = $kurikulum->findOrFail($factory_kurikulum->id); 
        $delete = $kurikulum->delete();

        $this->assertTrue($delete);
    }
}

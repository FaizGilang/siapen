<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Matkul;
use App\Transkrip;
use App\HasilStudi;

class LihatHasilStudiTest extends TestCase
{
    
    public function test_tampilkan_hsm()
    {
       $factory_nilai = factory(Transkrip::class)-> create();
       $nilai = new Transkrip;
       $found = $nilai->findOrFail($factory_nilai->id);

       $this->assertInstanceOf(Transkrip::class, $found);
       $this->assertEquals($found->idUser, $factory_nilai->idUser);
       $this->assertEquals($found->idMatkul, $factory_nilai->idMatkul);
       $this->assertEquals($found->komulatif, $factory_nilai->komulatif);
       $this->assertEquals($found->nilaiAkhir, $factory_nilai->nilaiAkhir); 
       
    }

}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\KategoriPengguna;

class KategoriPenggunaTest extends TestCase
{	
    /**
     * A basic test example.
     *
     * @return void
     */

    /** @test */
    public function test_hapus_kategori_pengguna()
    {
        $factory_kategoriPengguna = factory(KategoriPengguna::class)->create();
      
        $KategoriPengguna = new KategoriPengguna;
        $KategoriPengguna = $KategoriPengguna->findOrFail($factory_kategoriPengguna->id); 
        $delete = $KategoriPengguna->delete();
        
        $this->assertTrue($delete);
    }

    /** @test */
    public function test_ubah_kategori_pengguna()
    {
    	$factory_kategoriPengguna = factory(KategoriPengguna::class)->create();
    	$KategoriPengguna = new KategoriPengguna;
    	$KategoriPengguna = $KategoriPengguna->findOrFail($factory_kategoriPengguna->id);    	

    	$data = [
    		'nama' => str_random(5),
    		'status' => 'aktif'
    	];

    	$update = $KategoriPengguna->update($data);

    	$this->assertTrue($update);
        $this->assertEquals($data['nama'], $KategoriPengguna->nama);
        $this->assertEquals($data['status'], $KategoriPengguna->status);
    }

    /** @test */
    public function test_tampilkan_kategori_pengguna()
    {
    	$factory_kategoriPengguna = factory(KategoriPengguna::class)->create();
    	$KategoriPengguna = new KategoriPengguna;
    	$found = $KategoriPengguna->findOrFail($factory_kategoriPengguna->id);

    	$this->assertInstanceOf(KategoriPengguna::class, $found);
    	$this->assertEquals($found->nama, $factory_kategoriPengguna->nama);
        $this->assertEquals($found->status, $factory_kategoriPengguna->status);
    }

    /** @test */
    public function test_tambah_kategori_pengguna()
    {	
    	$data = [
    		'nama' => str_random(5),
    		'status' => 'aktif'
    	];

    	$KategoriPengguna = new KategoriPengguna;
    	$KategoriPengguna->nama = $data['nama'];
    	$KategoriPengguna->status = $data['status'];
    	$KategoriPengguna->save();

        $this->assertInstanceOf(KategoriPengguna::class, $KategoriPengguna);
        $this->assertEquals($data['nama'], $KategoriPengguna->nama);
        $this->assertEquals($data['status'], $KategoriPengguna->status);
    }

}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\HasilStudi;

class UbahNilaiTest extends TestCase
{

    public function test_ubah_nilai()
	{
	   $factory_nilai = factory(HasilStudi::class)->create();
	   $nilai = new HasilStudi;
	   $nilai = $nilai->findOrFail($factory_nilai->id);     

	     $data = [
	            'idUser' => mt_rand(1,3),
	            'idMatkul' => mt_rand(1,3),
	            'uts' => mt_rand(1,100),
	            'uas' => mt_rand(1,100),
	            'tugas' => mt_rand(1,100),
	            'praktek' => mt_rand(1,100)
	            
	      ];

	  $update = $nilai->update($data);

	  $this->assertTrue($update);
	  $this->assertInstanceOf(HasilStudi::class, $nilai);
	  $this->assertEquals($data['idUser'],$nilai->idUser);
	  $this->assertEquals($data['idMatkul'],$nilai->idMatkul);
	  $this->assertEquals($data['uts'],$nilai->uts);
	  $this->assertEquals($data['uas'],$nilai->uas);
	  $this->assertEquals($data['tugas'],$nilai->tugas);
	  $this->assertEquals($data['praktek'],$nilai->praktek);
	}


}

<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Matkul;
use App\KartuRencana;

class LihatKartuRencanaTest extends TestCase
{
    
    public function test_tampilan_krs()
    {
       $factory_krs = factory(KartuRencana::class)-> create();
       $krs = new KartuRencana;
       $found = $krs->findOrFail($factory_krs->id);

        $this->assertEquals($found->idUser, $factory_krs->idUser);
        $this->assertEquals($found->idPaketKuliah, $factory_krs->idPaketKuliah);
        $this->assertEquals($found->izin, $factory_krs->izin);
        $this->assertEquals($found->alpa, $factory_krs->alpa);  
        $this->assertEquals($found->status, $factory_krs->status);

    }

}

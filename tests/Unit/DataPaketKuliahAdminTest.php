<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\PaketKuliah;

class PaketKuliahTest extends TestCase
{
    
   /** @test */
    public function test_paket_kuliahA()
    {
      $factory_PaketKuliah = factory(PaketKuliah::class)-> create();
      $PaketKuliah = new PaketKuliah;
      $found = $PaketKuliah-> findOrFail($factory_PaketKuliah->id);

      $this->assertInstanceOf(PaketKuliah::class, $found);
      
      $this->assertEquals($found->idKurikulum, $factory_PaketKuliah->idKurikulum);
      $this->assertEquals($found->tahunAjaran, $factory_PaketKuliah->tahunAjaran);
      $this->assertEquals($found->komulatif, $factory_PaketKuliah->komulatif);
      $this->assertEquals($found->status, $factory_PaketKuliah->status);
    }

}

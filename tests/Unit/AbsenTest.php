<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\KartuRencana;


class AbsenTest extends TestCase
{
    /** @test */
    public function test_tampilkan_absen()
    {
       $factory_absen = factory(KartuRencana::class)-> create();
       $absen = new KartuRencana;
       $found = $absen->findOrFail($factory_absen->id);

       $this->assertInstanceOf(KartuRencana::class, $found);
       $this->assertEquals($found->idUser, $factory_absen->idUser);
       $this->assertEquals($found->idPaketKuliah, $factory_absen->idPaketKuliah);
       $this->assertEquals($found->izin, $factory_absen->izin);
       $this->assertEquals($found->alpa, $factory_absen->alpa);
    }

    /** @test */
    public function test_ubah_absen()
    {
       $factory_absen = factory(KartuRencana::class)-> create();
       $absen = new KartuRencana;
       $absen = $absen->findOrFail($factory_absen->id);     

       $data = [
            'idUser' => mt_rand(1,3),
            'idPaketKuliah' => mt_rand(1,3),
            'izin' => mt_rand(1,100),
            'alpa' => mt_rand(1,100)
       ];
       $update = $absen->update($data);

       $this->assertTrue($update);
       $this->assertInstanceOf(KartuRencana::class, $absen);
       $this->assertEquals($data['idUser'],$absen->idUser);
       $this->assertEquals($data['idPaketKuliah'],$absen->idPaketKuliah);
       $this->assertEquals($data['izin'],$absen->izin);
       $this->assertEquals($data['alpa'],$absen->alpa);
    }

}
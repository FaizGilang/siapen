<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\KartuRencana;


class AbsenTest extends TestCase
{
    /** @test */
    public function test_absenA()
    {
      $factory_absen = factory(KartuRencana::class)-> create();
      $absen = new KartuRencana;
      $found = $absen->findOrFail($factory_absen->id);

      $this->assertInstanceOf(KartuRencana::class, $found);
      $this->assertEquals($found->idUser, $factory_absen-> idUser);
      $this->assertEquals($found->idPaketKuliah, $factory_absen->idPaketKuliah);
      $this->assertEquals($found->izin, $factory_absen-> izin);
      $this->assertEquals($found->alpa, $factory_absen-> alpa);
    } 


}
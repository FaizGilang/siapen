<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use Auth;
use DB;
use AuthenticatesUsers;
use Illuminate\Foundation\Testing\Concerns\InteractsWithSession;


class LogoutTest extends TestCase
{

    public function test_logout()
    {
        Auth::logout();
        $response = $this->get('/');

        $response->assertStatus(200);
    }    
}



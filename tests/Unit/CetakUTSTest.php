<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\KartuRencana;

class CetakUTSTest extends TestCase
{

    public function test_cetak_uts()
    {
       $req = $this->call("GET","/cetak/pdfUTS/");

      $this->assertTrue(strpos($req->content(), 'UTS.pdf') == true);
    }

}

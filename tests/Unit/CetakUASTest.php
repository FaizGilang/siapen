<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\KartuRencana;

class CetakUASTest extends TestCase
{

    public function test_cetak_uas()
    {
       $req = $this->call("GET","/cetak/pdfUAS/");

      $this->assertTrue(strpos($req->content(), 'UAS.pdf') !== false);
    }

}

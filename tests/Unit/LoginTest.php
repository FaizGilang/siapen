<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;

class LoginTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */
    public function test_login()
    {
        
// $user = factory(User::class)->create([
//             'password' => bcrypt($password = 'i-love-laravel'),
//         ]);
//         $response = $this->post('/login', [
//             'email' => $user->email,
//             'password' => $password,
//         ]);
        
//         $response->assertRedirect('/home');
//         $this->assertAuthenticatedAs($user);
        $user = factory(User::class)->make();
        $response = $this->actingAs($user)->get('/login');
        $response->assertRedirect('/home');
    }


}

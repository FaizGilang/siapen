<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/404', function () {
	return view('404');
});
// route for Kategori Pengguna  (BENAR) ok
//kategori pengguna: index (menampilkan data kategori pengguna dlm bentuk tabel)
Route::get('/kategoriPengguna/lihat', 'KategoriPenggunaController@lihat');
//kategori pengguna: add (menampilkan form tambah kategori pengguna)
Route::get('/kategoriPengguna/tambah', 'KategoriPenggunaController@tambah');
//kategori pengguna: add (menyimpan data dari form yg sudah diisi)
Route::post('/kategoriPengguna/tambah', 'KategoriPenggunaController@simpan');
//kategori pengguna: edit (menampilkan form edit)
Route::get('/kategoriPengguna/ubah/{id}', 'KategoriPenggunaController@ubah');
//kategori pengguna: edit (menyimpan data yg sudah di edit)
Route::post('/kategoriPengguna/ubah/{id}', 'KategoriPenggunaController@perbarui');
//kategori pengguna: delete
Route::get('/kategoriPengguna/hapus/{id}', 'KategoriPenggunaController@hapus');
//kategori pengguna: sttaus
Route::get('/kategoriPengguna/status/{id}', 'KategoriPenggunaController@status');


// route for prodi (BENAR) ok
//prodi: tampilan data prodi semuanya
Route::get('/prodi/lihat', 'ProdiController@lihat');
//prodi: add (menampilkan)
Route::get('/prodi/tambah', 'ProdiController@tambah');
//prodi: add (menyimpan)
Route::post('/prodi/tambah', 'ProdiController@simpan');
//prodi: edit (menampilkan)
Route::get('/prodi/ubah/{id}', 'ProdiController@ubah');
//prodi: edit (menyimpan)
Route::post('/prodi/ubah/{id}', 'ProdiController@perbarui');
//prodi: delete
Route::get('/prodi/hapus/{id}', 'ProdiController@hapus');
//prodi: sttaus
Route::get('/prodi/status/{id}', 'ProdiController@status');



// route for user (BENAR) ok
//user: tampilan data USER semuanya
Route::get('/user/lihat', 'UserController@lihat');
Route::get('/user/lihat1', 'UserController@lihat1');
//user: detail
Route::get('/user/detail/{id}', 'UserController@detail');
//user: add (menampilkan)
Route::get('/user/tambah', 'UserController@tambah');
Route::get('/user/tambah1', 'UserController@tambah1');
//user: add (menyimpan)
Route::post('/user/tambah', 'UserController@simpan');
Route::post('/user/tambah1', 'UserController@simpan1');
//user: edit (menampilkan)
Route::get('/user/ubah/{id}', 'UserController@ubah');
Route::get('/user/ubah1/{id}', 'UserController@ubah1');
//user: edit (menyimpan)
Route::post('/user/ubah/{id}', 'UserController@perbarui');
Route::post('/user/ubah1/{id}', 'UserController@perbarui1');
//user: delete
Route::get('/user/hapus/{id}', 'UserController@hapus');
//route for profil
//profil: menampilkan profil pengguna
Route::get('/user/detailProfil', 'UserController@detailProfil');
//profil: edit (menampilkan)
// Route::get('/user/ubahProfil', 'UserController@ubahProfil');
//profil: edit (menyimpan)
// Route::post('/user/ubahProfil', 'UserController@simpanProfil');
//user: status
Route::get('/user/status/{id}', 'UserController@status');
//user:reset
Route::get('/user/reset/{id}','UserController@reset');
//user: ubahSandi
Route::get('/user/ubahKataSandi', 'UserController@ubahSandi');
//user: perbaruiSandi
Route::post('/user/ubahKataSandi', 'UserController@perbaruiSandi');
//cari
Route::post('/user/cari', 'UserController@cari');
Route::post('/user/cari1', 'UserController@cari1');



// route for matkul (BENAR) ok
//matkul: tampilan data matkul semuanya
Route::get('/matkul/lihat', 'MatkulController@lihat');
//matkul: detail
Route::get('/matkul/detail/{id}', 'MatkulController@detail');
//matkul: add (menampilkan)
Route::get('/matkul/tambah', 'MatkulController@tambah');
//matkul: add (menyimpan) 
Route::post('/matkul/tambah', 'MatkulController@simpan');
//matkul: dropdown dosen 1 dan dosen 2 dependent
Route::get('/matkul/tambah/getdosen/{id}', 'MatkulController@getdosen');
Route::get('/matkul/tambah/getdosen2/{id}', 'MatkulController@getdosen2');
//matkul: edit (menampilkan)
Route::get('/matkul/ubah/{id}', 'MatkulController@ubah');
//matkul: edit (menyimpan)
Route::post('/matkul/ubah/{id}', 'MatkulController@perbarui');
//matkul: dropdown dosen 1 dan dosen 2 dependent
Route::get('/matkul/ubah/getdosen/{id}', 'MatkulController@getdosen');
Route::get('/matkul/ubah/getdosen2/{id}', 'MatkulController@getdosen2');
//matkul: delete
Route::get('/matkul/hapus/{id}', 'MatkulController@hapus');
//matkul: sttaus
Route::get('/matkul/status/{id}', 'MatkulController@status');



// route for Kurikulum 2014 (BENAR)
//Kurikulum: tampilan data Kurikulum semuanya
Route::get('/kurikulum/lihat', 'KurikulumController@lihat');
//Kurikulum: add (menampilkan)
Route::get('/kurikulum/tambah', 'KurikulumController@tambah');
//Kurikulum: add (menyimpan) 
Route::post('/kurikulum/tambah', 'KurikulumController@simpan');
//Kurikulum: edit (menampilkan)
Route::get('/kurikulum/ubah/{id}', 'KurikulumController@ubah');
//Kurikulum: edit (menyimpan)
Route::post('/kurikulum/ubah/{id}', 'KurikulumController@perbarui');
//Kurikulum: delete
Route::get('/kurikulum/hapus/{id}', 'KurikulumController@hapus');
//
Route::get('/kurikulum/add/getKurikulum/{id}', 'KurikulumController@getKurikulum');
//kurikulum: sttaus
Route::get('/kurikulum/status/{id}', 'KurikulumController@status');



//route for PAKET KULIAH
//paketkuliah: form tambah paket kuliah OK
Route::get('/paketKuliah/tambah', 'PaketKuliahController@tambah');
//paketkuliah: menyimpan dari form paket kuliah OK
Route::post('/paketKuliah/tambah', 'PaketKuliahController@simpan');
//paketkuliah: tampilan tabel pilih matkul (checkbox) OK
Route::get('/paketKuliah/pilihpaket/{id}', 'PaketKuliahController@pilihPaket');
//paketkuliah: menyimpan pilihan matkul (checkbox)
Route::post('/paketKuliah/pilihpaket/{id}', 'PaketKuliahController@simpanPaket');
//paketkuliah: form lihat paket (dropdown)
Route::get('/paketKuliah/lihat', 'PaketKuliahController@lihatPaket');
//paketkuliah: detail paket kuliah beserta matkulnya berupa struktur paket kuliah 
Route::get('/paketKuliah/strukturPaketKuliah/{id}', 'PaketKuliahController@paketKuliah');
//paketkuliah: ubah paket
Route::get('/paketKuliah/ubah/{id}', 'PaketKuliahController@ubahPaket');
//paketkuliah: perbarui paket
Route::post('/paketKuliah/ubah/{id}', 'PaketKuliahController@perbaruiPaket');
//paketkuliah: status -> saat disave status belum, lalu ketika ada yg sama otomatis jd tidak
Route::get('/paketKuliah/status/{id}', 'PaketKuliahController@status');
//paketKuliah: hapus
Route::get('/paketKuliah/hapus/{id}', 'PaketKuliahController@hapus');



//route for kartuRencana
//kartuRencana: menampilkan KRS yg sudah dibuka
Route::get('/kartuRencana/lihatKRS','KartuRencanaController@lihatKRS');
//menampilkan form dropdown paket kuliah
Route::get('/kartuRencana/lihatPaket', 'KartuRencanaController@lihatPaket');
//form button buka KRS
Route::get('/kartuRencana/bukaKRS','KartuRencanaController@bukaKRS');
//buka KRS
Route::get('/kartuRencana/ubahKRS','KartuRencanaController@ubahKRS');
//ABSEN
//lihat absen
Route::get('/kartuRencana/lihatAbsen', 'KartuRencanaController@lihatAbsen');
//absen: edit (menampilkan)
Route::get('/kartuRencana/ubahAbsen/{id}', 'KartuRencanaController@ubahAbsen');
//absen: edit (menyimpan)
Route::post('/kartuRencana/ubahAbsen/{id}', 'KartuRencanaController@perbaruiAbsen');
//form absen 
Route::post('/kartuRencana/formAbsen', 'KartuRencanaController@formAbsen');
//form absen 
Route::get('/kartuRencana/formAbsen', 'KartuRencanaController@formAbsen');


//AKADEMIK:form tambah fase akademik
Route::get('/faseAkademik/tambah','FaseAkademikController@tambah');
//AKADEMIK:simpan form fase akademik
Route::post('/faseAkademik/tambah', 'FaseAkademikController@simpan');
//AKADEMIK: lihat fase akademik
Route::get('/faseAkademik/lihat','FaseAkademikController@lihat');
//AKADEMIK: form edit fase akademik
Route::get('/faseAkademik/ubah/{id}','FaseAkademikController@ubah');
//AKADEMIK: simpan hasil edit 
Route::post('/faseAkademik/ubah/{id}','FaseAkademikController@perbarui');
//AKADEMIK: delete
Route::get('/faseAkademik/hapus/{id}', 'FaseAkademikController@hapus');
//prodi: sttaus
Route::get('/faseAkademik/status/{id}', 'FaseAkademikController@status');



//route for HALAMAN DOSEN
//halaman dosen: menampilkan matkul yg diampu (dropdown)
Route::get('/halamanDosen/unggahNilai','HasilStudiController@unggahNilai');
//halaman dosen: menampilkan halaman untuk upload nilai (TIDAK DIPAKAI)
Route::get('/halamanDosen/uploadNilai','HasilStudiController@uploadNilai');
//halaman dosen: menampilkan halaman untuk informasi nilai (dropdown)
Route::get('/halamanDosen/lihatNilai','HasilStudiController@lihatNilai');
//halaman utk menampilkan tabel nilai setelah dipilih dari dropdown
Route::get('/halamanDosen/lihatNilai/tabelNilai/{id}','HasilStudiController@tabelNilai');
// halaman dosen: ubah nilai
Route::get('/halamanDosen/lihatNilai/{id}', 'HasilStudiController@ubahNilai');
// halaman dosen: perbarui nilai
Route::post('/halamanDosen/lihatNilai/{id}', 'HasilStudiController@perbaruiNilai');
// halaman dosen: cari nilai
Route::post('/halamanDosen/cariNilai', 'HasilStudiController@cari');




////////////////////////////////////////////////////////////////////////////
//halaman HSM
Route::get('hasilStudi/lihatHSM','HasilStudiController@lihatHSM');
//lihat HSM LALU
Route::get('hasilStudi/lihatHSM/{semester}','HasilStudiController@lihatHSMLalu');
//lihat transkrip
Route::get('hasilStudi/lihatTranskrip','HasilStudiController@lihatTranskrip');
//import excel
Route::get('halamanDosen/import_export', 'HasilStudiController@import_export');
//unduh excel
Route::get('downloadExcel/{type}/{id}', 'HasilStudiController@unduhExcel');
//unggah excel
Route::post('importExcel', 'HasilStudiController@unggahExcel');
//unggah nilai bagian akademik
Route::get('hasilStudi/unggahNilai','HasilStudiController@unggahNilaiA');
//unggah nilai bagian akademik
Route::post('hasilStudi/unggahNilai','HasilStudiController@simpanNilaiA');
//
Route::get('/hasilStudi/unggahNilai/getbobotUts/{id}', 'HasilStudiController@getbobotUts');
Route::get('/hasilStudi/unggahNilai/getbobotUas/{id}', 'HasilStudiController@getbobotUas');
Route::get('/hasilStudi/unggahNilai/getbobotPraktek/{id}', 'HasilStudiController@getbobotPraktek');
Route::get('/hasilStudi/unggahNilai/getbobotTugas/{id}', 'HasilStudiController@getbobotTugas');
//Route unggah nilai untuk akademik
Route::get('/hasilStudi/nilaiMahasiswa','HasilStudiController@unggahNilaiA');
Route::get('/hasilStudi/tabelNilai/{id}','HasilStudiController@tabelNilaiA');
Route::post('/hasilStudi/tabelNilai/{id}', 'HasilStudiController@simpanNilai');
Route::get('/hasilStudi/lihatMahasiswa', 'HasilStudiController@lihatMahasiswa');
//hasilStudi: detail nilai mahasiswa
Route::get('/hasilStudi/detailNilai/{id}', 'HasilStudiController@detailNilai');

// halaman dosen: ubah nilai
Route::get('/hasilStudi/detailNilai/{id1}/{id2}', 'TranskripController@ubahNilai');
// halaman dosen: perbarui nilai
Route::post('/hasilStudi/detailNilai/{id1}/{id2}', 'TranskripController@perbaruiNilai');
//cari nilai mhs
Route::post('/hasilStudi/lihatMahasiswa', 'HasilStudiController@cariMahasiswa');

//PRINT-PRINT
//print KRS
Route::get('/kartuRencana/pdfKRS/',  'KartuRencanaController@pdfKRS'); 
//print HASIL STUDI
Route::get('/hasilStudi/pdfHSM/{id}',  'HasilStudiController@pdfHSM'); 
Route::get('/hasilStudi/pdfHSMsekarang/',  'HasilStudiController@pdfHSMsekarang'); 
//print TRANSKRIP
Route::get('/hasilStudi/pdfTranskrip/','HasilStudiController@pdfTranskrip');
//KARTU UTS
Route::get('/cetak/pdfUTS/',  'KartuRencanaController@pdfUTS');	
//KARTU UAS
Route::get('/cetak/pdfUAS/',  'KartuRencanaController@pdfUAS');



Route::get('/home', 'HomeController@index')->name('home');

Route::get('/hasilStudi/lihatHSM/semester/{id}','HasilStudiController@semester');


//HALAMAN ADMIN
//ADMIN: lihat fase akademik
Route::get('/halamanAdmin/faseAkademik','FaseAkademikController@lihatA');
//paketkuliah: form lihat paket (dropdown)
Route::get('/halamanAdmin/paketKuliah', 'PaketKuliahController@lihatPaketA');
//ADMIN: lihat paket kuliah
Route::get('/halamanAdmin/strukturPaketKuliah/{id}', 'PaketKuliahController@paketKuliahA');
//ADMIN: lihat nilai
Route::get('/halamanAdmin/infoNilai', 'MatkulController@lihatNilaiA');
//halaman utk menampilkan tabel nilai setelah dipilih dari dropdown
Route::get('/halamanDosen/lihatNilai/tabelNilai/{id}','MatkulController@tabelNilaiA');
//ADMIN: lihat absen (dropdown)
Route::get('/halamanAdmin/daftarAbsen', 'KartuRencanaController@lihatAbsenA');
//ADMIN: lihat absen (tabel)
Route::post('/halamanAdmin/absenMahasiswa', 'KartuRencanaController@formAbsenA');

Route::get('/home', 'HomeController@dashboard');
-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 11 Mar 2019 pada 14.42
-- Versi server: 10.1.30-MariaDB
-- Versi PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siapen`
--
CREATE DATABASE IF NOT EXISTS `siapen` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `siapen`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `fase_akademiks`
--

CREATE TABLE `fase_akademiks` (
  `id` int(10) UNSIGNED NOT NULL,
  `tahunAjaran` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `faseRegistrasi` date NOT NULL,
  `awalKuliah` date NOT NULL,
  `akhirKuliah` date NOT NULL,
  `faseEval` date NOT NULL,
  `semester` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('aktif','tidak') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'tidak',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `fase_akademiks`
--

INSERT INTO `fase_akademiks` (`id`, `tahunAjaran`, `faseRegistrasi`, `awalKuliah`, `akhirKuliah`, `faseEval`, `semester`, `status`, `created_at`, `updated_at`) VALUES
(1, '2016/2017', '2019-03-07', '2019-03-07', '2019-03-07', '2019-03-15', 'ganjil', 'tidak', NULL, NULL),
(2, '2016/2017', '2019-03-07', '2019-03-07', '2019-03-07', '2019-03-15', 'genap', 'tidak', NULL, NULL),
(3, '2017/2018', '2019-03-07', '2019-03-07', '2019-03-07', '2019-03-15', 'ganjil', 'tidak', NULL, NULL),
(4, '2017/2018', '2019-03-07', '2019-03-07', '2019-03-07', '2019-03-15', 'genap', 'tidak', NULL, NULL),
(5, '2018/2019', '2019-03-07', '2019-03-07', '2019-03-07', '2019-03-15', 'ganjil', 'aktif', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `hasil_studis`
--

CREATE TABLE `hasil_studis` (
  `id` int(10) UNSIGNED NOT NULL,
  `idMatkul` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `uts` int(11) NOT NULL,
  `uas` int(11) NOT NULL,
  `tugas` int(11) NOT NULL,
  `praktek` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kartu_rencanas`
--

CREATE TABLE `kartu_rencanas` (
  `id` int(10) UNSIGNED NOT NULL,
  `idUser` int(11) NOT NULL,
  `idPaketKuliah` int(11) NOT NULL,
  `izin` int(11) NOT NULL,
  `alpa` int(11) NOT NULL,
  `status` enum('aktif','tidak') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kartu_rencanas`
--

INSERT INTO `kartu_rencanas` (`id`, `idUser`, `idPaketKuliah`, `izin`, `alpa`, `status`, `created_at`, `updated_at`) VALUES
(1, 13, 8, 0, 0, 'tidak', NULL, NULL),
(2, 14, 8, 0, 0, 'tidak', NULL, NULL),
(3, 22, 5, 0, 0, 'tidak', NULL, NULL),
(4, 23, 5, 0, 0, 'tidak', NULL, NULL),
(5, 22, 6, 0, 0, 'tidak', NULL, NULL),
(6, 23, 6, 0, 0, 'tidak', NULL, NULL),
(7, 22, 7, 0, 0, 'tidak', NULL, NULL),
(8, 23, 7, 0, 0, 'tidak', NULL, NULL),
(9, 38, 1, 0, 0, 'tidak', NULL, NULL),
(10, 39, 1, 0, 0, 'tidak', NULL, NULL),
(11, 38, 2, 0, 0, 'tidak', NULL, NULL),
(12, 39, 2, 0, 0, 'tidak', NULL, NULL),
(13, 38, 3, 0, 0, 'tidak', NULL, NULL),
(14, 39, 3, 0, 0, 'tidak', NULL, NULL),
(15, 38, 4, 0, 0, 'tidak', NULL, NULL),
(16, 39, 4, 0, 0, 'tidak', NULL, NULL),
(17, 13, 10, 0, 0, 'aktif', NULL, NULL),
(18, 14, 10, 0, 0, 'aktif', NULL, NULL),
(19, 22, 8, 0, 0, 'aktif', NULL, NULL),
(20, 23, 8, 0, 0, 'aktif', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori_penggunas`
--

CREATE TABLE `kategori_penggunas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('aktif','tidak') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'tidak',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategori_penggunas`
--

INSERT INTO `kategori_penggunas` (`id`, `nama`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'aktif', NULL, NULL),
(2, 'Akademik', 'aktif', NULL, NULL),
(3, 'Dosen', 'aktif', NULL, NULL),
(4, 'Mahasiswa', 'aktif', NULL, NULL),
(5, 'Koordinator', 'aktif', NULL, NULL),
(6, 'Ketua Jurusan', 'aktif', NULL, NULL),
(7, 'Ketua Program Studi', 'aktif', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kurikulums`
--

CREATE TABLE `kurikulums` (
  `id` int(10) UNSIGNED NOT NULL,
  `tahun` year(4) NOT NULL,
  `idProdi` int(11) NOT NULL,
  `status` enum('aktif','tidak') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'tidak',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kurikulums`
--

INSERT INTO `kurikulums` (`id`, `tahun`, `idProdi`, `status`, `created_at`, `updated_at`) VALUES
(1, 2014, 1, 'aktif', NULL, NULL),
(2, 2014, 2, 'aktif', NULL, NULL),
(3, 2014, 3, 'aktif', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `matkuls`
--

CREATE TABLE `matkuls` (
  `id` int(10) UNSIGNED NOT NULL,
  `kodeprodi` int(11) NOT NULL,
  `kode` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `komulatif` int(11) NOT NULL,
  `SKS` int(11) NOT NULL,
  `SKSteori` int(11) NOT NULL,
  `SKSpraktek` int(11) NOT NULL,
  `jpm` int(11) NOT NULL,
  `dosen1` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dosen2` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bobotUts` int(11) NOT NULL DEFAULT '30',
  `bobotUas` int(11) NOT NULL DEFAULT '30',
  `bobotTugas` int(11) NOT NULL DEFAULT '20',
  `bobotPraktek` int(11) NOT NULL DEFAULT '20',
  `status` enum('aktif','tidak') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'tidak',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `matkuls`
--

INSERT INTO `matkuls` (`id`, `kodeprodi`, `kode`, `nama`, `komulatif`, `SKS`, `SKSteori`, `SKSpraktek`, `jpm`, `dosen1`, `dosen2`, `bobotUts`, `bobotUas`, `bobotTugas`, `bobotPraktek`, `status`, `created_at`, `updated_at`) VALUES
(1, 99, 'AKND1101', 'Pendidikan Agama Islam', 1, 3, 3, 0, 3, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(2, 99, 'AKND1102', 'Pendidikan Agama Protestan', 1, 3, 3, 0, 3, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(3, 99, 'AKND1103', 'Pendidikan Agama Katolik', 1, 3, 3, 0, 3, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(4, 99, 'AKND1104', 'Pendidikan Agama Hindu', 1, 3, 3, 0, 3, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(5, 99, 'AKND1105', 'Pendidikan Agama Budha', 1, 3, 3, 0, 3, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(6, 99, 'AKND1106', 'Pendidikan Kewarganegaraan', 1, 3, 3, 0, 3, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(7, 99, 'AKND2101', 'Pancasila', 2, 3, 3, 0, 3, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(8, 99, 'AKND2102', 'Kewarganegaraan', 2, 3, 3, 0, 3, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(9, 99, 'AKND2103', 'Bahasa Inggris', 2, 2, 1, 1, 3, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(10, 99, 'AKND2107', 'Pancasila', 2, 3, 3, 0, 3, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(11, 1, 'DGD1101', 'Pengantar New Media', 1, 2, 2, 0, 2, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(12, 1, 'DGD1102', 'Media Digital Grafis 1', 1, 3, 0, 3, 6, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(13, 1, 'DGD1103', 'Sejarah Desain', 1, 2, 2, 0, 2, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(14, 1, 'DGD1104', 'Aplikasi Tipografi', 1, 3, 0, 3, 6, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(15, 1, 'DGD1105', 'Menggambar', 1, 3, 0, 3, 6, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(16, 1, 'DGD1106', 'Nirmana Dwimatra', 1, 3, 0, 3, 6, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(17, 1, 'DGD2101', 'Digital Desain 1', 2, 3, 0, 3, 6, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(18, 1, 'DGD2102', 'Media Digital Grafis', 2, 2, 1, 1, 3, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(19, 1, 'DGD2103', 'Creative Thinking', 2, 2, 1, 1, 3, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(20, 1, 'DGD2104', 'Aplikasi Tipografi Desain', 2, 3, 0, 3, 6, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(21, 1, 'DGD2105', 'Proses Komunikasi', 2, 2, 0, 2, 4, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(22, 1, 'DGD2106', 'Metode Reproduksi Grafika', 2, 2, 0, 2, 4, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(23, 1, 'DGD3101', 'Desain Digital 2', 3, 3, 0, 3, 6, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(24, 1, 'DGD3102', 'Media Digital Grafis 3', 3, 3, 0, 3, 6, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(25, 1, 'DGD3103', 'Ilustrasi Desain', 3, 3, 0, 3, 6, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(26, 1, 'DGD3104', 'Kewirausahaan', 3, 2, 1, 1, 2, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(27, 1, 'DGD3105', 'Teknik Presentasi', 3, 2, 1, 1, 2, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(28, 1, 'DGD3106', 'Fotografi', 3, 3, 0, 3, 6, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(29, 1, 'DGD4101', 'Media Digital Grafis 4', 4, 3, 0, 3, 6, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(30, 1, 'DGD4102', 'Desain Digital 3', 4, 3, 0, 3, 6, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(31, 1, 'DGD4103', 'Praktik Kerja Industri', 4, 6, 0, 6, 12, '4', '5', 30, 30, 20, 20, 'aktif', NULL, NULL),
(32, 2, 'JPD1301', 'Bahasa Inggris Pariwisata 1', 1, 2, 0, 2, 4, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(33, 2, 'JPD1302', 'Keterampilan Komputer', 1, 3, 0, 3, 6, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(34, 2, 'JPD1303', 'Sejarah dan Budaya Indonesia', 1, 2, 2, 0, 2, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(35, 2, 'JPD1304', 'Pengantar Pariwisata', 1, 2, 2, 0, 2, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(36, 2, 'JPD1305', 'Manajemen Biro Perjalanan Wisata', 1, 2, 0, 2, 4, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(37, 2, 'JPD1306', 'Pelayanan Prima', 1, 2, 0, 2, 4, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(38, 2, 'JPD1307', 'Industri Hospitality', 1, 3, 0, 3, 6, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(39, 2, 'JPD2308', 'Bahasa Inggris Pariwisata 2', 2, 2, 0, 2, 4, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(40, 2, 'JPD2309', 'Geografi Pariwisata', 2, 2, 2, 0, 2, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(41, 2, 'JPD2310', 'Tarif Dokumen Pasasi', 2, 3, 0, 3, 6, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(42, 2, 'JPD2311', 'Manajemen Event', 2, 3, 0, 3, 6, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(43, 2, 'JPD2312', 'Teknik Pemanduan Wisata 1', 2, 3, 0, 3, 6, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(44, 2, 'JPD2313', 'Akuntansi Biro Perjalanan Wisata', 2, 2, 0, 2, 2, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(45, 2, 'JPD3313', 'Perencanaan Perjalanan Wisata', 3, 3, 0, 3, 6, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(46, 2, 'JPD3314', 'Teknik Pemanduan Wisata 2', 3, 3, 0, 3, 6, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(47, 2, 'JPD3315', 'Pemasaran Produk Biro Perjalanan Wisata', 3, 3, 0, 3, 6, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(48, 2, 'JPD3316', 'Transportasi Wisata', 3, 3, 0, 3, 6, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(49, 2, 'JPD3317', 'Perhitungan Harga Paket Wisata', 3, 3, 0, 3, 6, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(50, 2, 'JPD3318', 'Kewirausahaan', 3, 2, 2, 0, 2, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(51, 2, 'JPD3319', 'Wisata Religi', 3, 2, 2, 0, 2, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(52, 2, 'JPD4224', 'Praktik Kerja Industri', 4, 6, 0, 6, 12, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(53, 2, 'JPD4325', 'Tugas Akhir', 4, 4, 0, 4, 8, '8', '9', 30, 30, 20, 20, 'aktif', NULL, NULL),
(54, 3, 'TKRD1101', 'Matematika Teknik', 1, 2, 2, 0, 4, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(55, 3, 'TKRD1102', 'Fisika Teknik', 1, 2, 2, 0, 4, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(56, 3, 'TKRD1103', 'Gambar Teknik', 1, 2, 0, 2, 4, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(57, 3, 'TKRD1104', 'Keselamatan dan Kesehatan Kerja', 1, 2, 0, 2, 4, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(58, 3, 'TKRD1105', 'Peralatan Otomotif', 1, 2, 0, 2, 4, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(59, 3, 'TKRD1106', 'Teknik Pengukuran Otomotif', 1, 2, 0, 2, 4, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(60, 3, 'TKRD1107', 'Elektronika Dasar', 1, 2, 0, 2, 4, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(61, 3, 'TKRD2101', 'CAD', 2, 2, 0, 2, 4, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(62, 3, 'TKRD2102', 'Mesin Konversi Energi', 2, 2, 0, 2, 4, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(63, 3, 'TKRD2103', 'Teknik Pengelasan,Pelapisan,& Pengecatan Kendaraan', 2, 3, 0, 3, 6, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(64, 3, 'TKRD2104', 'Sistem Pemindah Tenaga', 2, 3, 0, 3, 6, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(65, 3, 'TKRD2105', 'Sistem Chasis', 2, 3, 0, 3, 6, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(66, 3, 'TKRD2106', 'Kelistrikan Otomotif', 2, 3, 0, 3, 6, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(67, 3, 'TKRD2107', 'Sistem Bahan Bakar dan Pelumas Otomotif', 2, 3, 0, 3, 6, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(68, 3, 'TKRD3101', 'Teknologi Motor Bensin', 3, 3, 0, 3, 6, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(69, 3, 'TKRD3102', 'Sistem Asesoris dan Modifikasi Otomotif', 3, 3, 0, 3, 6, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(70, 3, 'TKRD3103', 'Teknologi Motor Diesel', 3, 3, 0, 3, 6, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(71, 3, 'TKRD3104', 'Manajemen Bengkel', 3, 2, 0, 2, 4, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(72, 3, 'TKRD3105', 'Kewirausahaan', 3, 2, 2, 0, 2, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(73, 3, 'TKRD3106', 'Engine Management System', 3, 3, 0, 3, 6, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(74, 3, 'TKRD3107', 'Sistem AC Mobil', 3, 2, 0, 2, 4, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(75, 3, 'TKRD4101', 'Overhoul Mesin Otomotif', 4, 3, 0, 3, 6, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL),
(76, 3, 'TKRD4102', 'Praktik Industri Otomotif', 4, 5, 0, 5, 10, '11', '12', 30, 30, 20, 20, 'aktif', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(649, '2014_10_12_000000_create_users_table', 1),
(650, '2014_10_12_100000_create_password_resets_table', 1),
(651, '2018_07_18_153211_create_kategori_penggunas_table', 1),
(652, '2018_07_18_153256_create_prodis_table', 1),
(653, '2018_07_18_153306_create_kurikulums_table', 1),
(654, '2018_07_18_153318_create_matkuls_table', 1),
(655, '2018_07_18_153332_create_struktur_paket_kuliahs_table', 1),
(656, '2018_07_18_153344_create_kartu_rencanas_table', 1),
(657, '2018_07_18_153355_create_paket_kuliahs_table', 1),
(658, '2018_07_18_153406_create_hasil_studis_table', 1),
(659, '2018_11_18_050050_create_fase_akademiks_table', 1),
(660, '2019_01_19_070725_create_transkrips_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `paket_kuliahs`
--

CREATE TABLE `paket_kuliahs` (
  `id` int(10) UNSIGNED NOT NULL,
  `idKurikulum` int(11) NOT NULL,
  `tahunAjaran` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `komulatif` int(11) NOT NULL,
  `status` enum('belum','aktif','tidak') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'belum',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `paket_kuliahs`
--

INSERT INTO `paket_kuliahs` (`id`, `idKurikulum`, `tahunAjaran`, `komulatif`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '1', 1, 'tidak', NULL, NULL),
(2, 1, '2', 2, 'tidak', NULL, NULL),
(3, 1, '3', 3, 'tidak', NULL, NULL),
(4, 1, '4', 4, 'tidak', NULL, NULL),
(5, 1, '3', 1, 'tidak', NULL, NULL),
(6, 1, '4', 2, 'tidak', NULL, NULL),
(7, 1, '5', 3, 'tidak', NULL, NULL),
(8, 1, '5', 1, 'tidak', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `prodis`
--

CREATE TABLE `prodis` (
  `id` int(10) UNSIGNED NOT NULL,
  `kode` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('aktif','tidak') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'tidak',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `prodis`
--

INSERT INTO `prodis` (`id`, `kode`, `nama`, `nomor`, `status`, `created_at`, `updated_at`) VALUES
(1, 'DGD', 'Desain Grafis', '062', 'aktif', NULL, NULL),
(2, 'JPD', 'Jasa Pariwisata', '052', 'aktif', NULL, NULL),
(3, 'TKRD', 'Teknik Kendaraan Ringan', '022', 'aktif', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `struktur_paket_kuliahs`
--

CREATE TABLE `struktur_paket_kuliahs` (
  `id` int(10) UNSIGNED NOT NULL,
  `idPaketKuliah` int(11) NOT NULL,
  `idMatkul` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `struktur_paket_kuliahs`
--

INSERT INTO `struktur_paket_kuliahs` (`id`, `idPaketKuliah`, `idMatkul`, `created_at`, `updated_at`) VALUES
(1, 1, 11, NULL, NULL),
(2, 1, 12, NULL, NULL),
(3, 2, 17, NULL, NULL),
(4, 2, 18, NULL, NULL),
(5, 3, 23, NULL, NULL),
(6, 3, 24, NULL, NULL),
(7, 4, 29, NULL, NULL),
(8, 4, 30, NULL, NULL),
(9, 5, 11, NULL, NULL),
(10, 5, 12, NULL, NULL),
(11, 6, 17, NULL, NULL),
(12, 6, 18, NULL, NULL),
(13, 7, 23, NULL, NULL),
(14, 7, 24, NULL, NULL),
(15, 8, 11, NULL, NULL),
(16, 8, 12, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `transkrips`
--

CREATE TABLE `transkrips` (
  `id` int(10) UNSIGNED NOT NULL,
  `idUser` int(11) NOT NULL,
  `idMatkul` int(11) NOT NULL,
  `komulatif` int(11) NOT NULL,
  `nilaiAkhir` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `transkrips`
--

INSERT INTO `transkrips` (`id`, `idUser`, `idMatkul`, `komulatif`, `nilaiAkhir`, `created_at`, `updated_at`) VALUES
(1, 13, 11, 1, 'A', NULL, NULL),
(2, 13, 12, 1, 'A', NULL, NULL),
(3, 14, 11, 1, 'A', NULL, NULL),
(4, 14, 12, 1, 'A', NULL, NULL),
(5, 22, 11, 1, 'A', NULL, NULL),
(6, 22, 12, 1, 'B', NULL, NULL),
(7, 23, 11, 1, 'A', NULL, NULL),
(8, 23, 12, 1, 'A-', NULL, NULL),
(9, 22, 17, 2, 'A', NULL, NULL),
(10, 22, 18, 2, 'B', NULL, NULL),
(11, 23, 17, 2, 'A', NULL, NULL),
(12, 23, 18, 2, 'A-', NULL, NULL),
(13, 22, 23, 3, 'A', NULL, NULL),
(14, 22, 24, 3, 'B', NULL, NULL),
(15, 23, 23, 3, 'A', NULL, NULL),
(16, 23, 24, 3, 'A-', NULL, NULL),
(17, 38, 11, 1, 'A', NULL, NULL),
(18, 38, 12, 1, 'B', NULL, NULL),
(19, 38, 17, 2, 'A', NULL, NULL),
(20, 38, 18, 2, 'B', NULL, NULL),
(21, 38, 23, 3, 'A', NULL, NULL),
(22, 38, 24, 3, 'A-', NULL, NULL),
(23, 38, 29, 4, 'A', NULL, NULL),
(24, 38, 30, 4, 'A-', NULL, NULL),
(25, 39, 11, 1, 'A', NULL, NULL),
(26, 39, 12, 1, 'B', NULL, NULL),
(27, 39, 17, 2, 'A', NULL, NULL),
(28, 39, 18, 2, 'B', NULL, NULL),
(29, 39, 23, 3, 'A', NULL, NULL),
(30, 39, 24, 3, 'A-', NULL, NULL),
(31, 39, 29, 4, 'A', NULL, NULL),
(32, 39, 30, 4, 'A-', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nomorInduk` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kategori` int(11) NOT NULL,
  `prodi` int(11) NOT NULL,
  `tahunMasuk` year(4) NOT NULL,
  `status` enum('belum','aktif','tidak') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'belum',
  `file` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `nomorInduk`, `nama`, `email`, `password`, `kategori`, `prodi`, `tahunMasuk`, `status`, `file`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, '1', 'admin', 'admin@gmail.com', '$2y$10$LV55CK0128sUTL6XmR4dDe0Fhzbp6gMxwdasbOj0DF1DeXX09ijk2', 1, 98, 2014, 'aktif', 'Pengguna-1', NULL, NULL, NULL),
(2, '2', 'akademik', 'akademik@gmail.com', '$2y$10$8Si2GaDqXB.59cYYbMSJJOLFGieOgzXHTn9VeXbBN2v7rlAQdSNPe', 2, 98, 2014, 'aktif', 'Pengguna-2', NULL, NULL, NULL),
(3, '30101987', 'Mulyono, S.Kom', 'mulyono@gmail.com', '$2y$10$KIu29C4.XTpRw5d5ND/VnOD/OiuEc2RloJHlEyhsh4BirGoVDj.3e', 3, 1, 2014, 'aktif', 'Pengguna-3', NULL, NULL, NULL),
(4, '27101988', 'Endah Ekasanti S., S.T.,M.Kom', 'endah@gmail.com', '$2y$10$LHAFf84HmcU12mNWXlFXJufw8CfR8ZfFbqWVKi883jZ4qnTQLFcVK', 3, 1, 2014, 'aktif', 'Pengguna-4', NULL, NULL, NULL),
(5, '20021984', 'Angga Amoriska, S.Sn.,S.Pd., M.Pd', 'angga@gmail.com', '$2y$10$Z.yBrSndWqkLDhz6muMyzOzvB5T6nS8TNcHrbF5BYFV9oxQCrzZQq', 3, 1, 2014, 'aktif', 'Pengguna-5', NULL, NULL, NULL),
(6, '11', 'Drs. Rohadi, M.A., M.Si.', 'rohadi@gmail.com', '$2y$10$T7V3sEAa3d1QPQ7BrY46CenyPKPD.vnZPxFZHGPypfJDg75jt.QL.', 3, 1, 2014, 'aktif', 'Pengguna-6', NULL, NULL, NULL),
(7, '12', 'Masri’an, S.Pd., M.M.', 'masri@gmail.com', '$2y$10$YS/UC.kNhLh2UjjHauV20eSP1ZXzmjv9MjDUYdvDT6N01dGgpziY6', 3, 1, 2014, 'aktif', 'Pengguna-7', NULL, NULL, NULL),
(8, '09031988', 'Eka Harisma W, S.Hum.,M.Hum', 'eka@gmail.com', '$2y$10$hTazZ35cUGIe/O3rH4oOmufeTUjNZpbJBjQhDMqNQiVFpbQ.1EmUG', 3, 2, 2014, 'aktif', 'Pengguna-8', NULL, NULL, NULL),
(9, '011111978', 'Trinani Puji Sumantri, S.Pd', 'trinani@gmail.com', '$2y$10$yOpMcEHoxVMZFMAWCl200OV1jKlhVusFgb3yuWwcpHZNtQwJ3K6.6', 3, 2, 2014, 'aktif', 'Pengguna-9', NULL, NULL, NULL),
(10, '12011978', 'Nina Sulistyowati, S.S', 'nina@gmail.com', '$2y$10$uFogCNK/psjNgc5BriRhouH3Zrk6a.hsQ0nMQRrH3A7hbFhiNZ2DG', 3, 2, 2014, 'aktif', 'Pengguna-10', NULL, NULL, NULL),
(11, '13051981', 'Ir. Edy Ismail, S.Pd., M.Pd., IPP', 'edy@gmail.com', '$2y$10$bzUHNA10APGcdO8GmsU8AeNeHEUv5qSOAhmLVacu4Frw84LiFka/C', 3, 3, 2014, 'aktif', 'Pengguna-11', NULL, NULL, NULL),
(12, '24101988', 'Sugiyarto, S.Pd', 'sugiyarto@gmail.com', '$2y$10$UOqSr7QNtACkyESdrQtIkuy4rALPm2vjgZrIuKtnQ93FZBYKciCLa', 3, 3, 2014, 'aktif', 'Pengguna-12', NULL, NULL, NULL),
(13, '1806211001', 'Mohammad Rizal Amrul Haq', 'rizalamrul@gmail.com', '$2y$10$.PcmIm/0RrjqUmcD/EqfD.1q7ZVOvr.qVkMlPNc437/Hd3HPiYL36', 4, 1, 2018, 'aktif', 'Pengguna-13', NULL, NULL, NULL),
(14, '1806211002', 'Yanuarinta Darisma Yanti', 'yanuarinta@gmail.com', '$2y$10$ycS5ctbGTQo0k/ico.uTy.CFiiM4tDPxQHXubBokhKE8VFM/8.V5y', 4, 1, 2018, 'aktif', 'Pengguna-14', NULL, NULL, NULL),
(15, '1806211003', 'Ani Rizki Fitriani', 'anirizki@gmail.com', '$2y$10$h.tA2/ycYgCWxZGtHXa8I.h5HiDGPvXbUA.DTkw0u6wASpgcAV9KC', 4, 1, 2018, 'aktif', 'Pengguna-15', NULL, NULL, NULL),
(16, '1805211001', 'Abdul Aziz', 'abdulaziz@gmail.com', '$2y$10$OK8ZCn.ABYsklp1T06UgiemPDMON6hAPN3v9hENjKEfmB0wTCyb5G', 4, 2, 2018, 'aktif', 'Pengguna-16', NULL, NULL, NULL),
(17, '1805211002', 'Agustin Kinthan Amilla', 'agustin@gmail.com', '$2y$10$.OhhUE29XqsE03UDH45BRe/Jb5l/ehpMzCUFrT6c9Vs7T7XlxRBIS', 4, 2, 2018, 'aktif', 'Pengguna-17', NULL, NULL, NULL),
(18, '1805211003', 'Puja Novita Sari', 'puja@gmail.com', '$2y$10$WGxHDWXuZsJeoLgVzc8QIe9nIDO8BLGXlkED71PFqZIOFy1H3HZIy', 4, 2, 2018, 'aktif', 'Pengguna-18', NULL, NULL, NULL),
(19, '1802211001', 'Samsul Qomar', 'samsul@gmail.com', '$2y$10$kWyMzX3ux5e.X8XlhSzB3.pwilwjCgjPEG.Ef8UYmTKWXe.gsMFyq', 4, 3, 2018, 'aktif', 'Pengguna-19', NULL, NULL, NULL),
(20, '1802211002', 'Ery Supriyanto', 'erysupriyanto@gmail.com', '$2y$10$bsaDCeYLnhpd4qD6ScRl9./vgDo1ml0Ib2lCBbBojzpBVpjii5jwu', 4, 3, 2018, 'aktif', 'Pengguna-20', NULL, NULL, NULL),
(21, '1802211003', 'Wahyu Tanwirul MuIsyr', 'wahyutanwirul@gmail.com', '$2y$10$Xih6C/R9E.J10136ag3z0ulC2it.RNc2Rli88A/7WZxNl2kykFBXq', 4, 3, 2018, 'aktif', 'Pengguna-21', NULL, NULL, NULL),
(22, '1706211001', 'Rizal Rudiyanto Baharudin', 'rizal@gmail.com', '$2y$10$eA1DMzT.fr7mRdZ46YZDnuaeOj0giH894wfwhn4vmwnvLUnezgos6', 4, 1, 2017, 'aktif', 'Pengguna-22', NULL, NULL, NULL),
(23, '1706211002', 'Yanuarinta Dewi Lestari', 'yanuarinta17@gmail.com', '$2y$10$s5xhSVvQQA2IlgVyTm4Yl.gMHTdc3TEdEy7qyQrSJXEpc0FGBZnA6', 4, 1, 2017, 'aktif', 'Pengguna-23', NULL, NULL, NULL),
(24, '1706211003', 'Ani Yalianti', 'ani@gmail.com', '$2y$10$UcxQrAme6WGVj4hcBD0hMOV4/SAwizbxxpPQsiOyOTSI9FqsVV9iO', 4, 1, 2017, 'aktif', 'Pengguna-24', NULL, NULL, NULL),
(25, '1705211001', 'Abdul Hasyim Uhib', 'abdul@gmail.com', '$2y$10$iJOw7MjZTRFenq38lzgI7..3wMjXScuOFIsN/QMk7Q7QrYG9M73Ya', 4, 2, 2017, 'aktif', 'Pengguna-25', NULL, NULL, NULL),
(26, '1705211002', 'Agustin Ramadhania', 'agustin17@gmail.com', '$2y$10$X2MZuoZZFvgqE5e3HrrAY.nAJQLFZlUV1uzwYw5Wzz4cGa2Rmliuq', 4, 2, 2017, 'aktif', 'Pengguna-26', NULL, NULL, NULL),
(27, '1705211003', 'Puja Pangestu Pandewo', 'puja17@gmail.com', '$2y$10$9GyvH9ysoKbGmlMFaXEXZ.rZFX1zf.cyr5WbHXEkSR8dItJRj3Dea', 4, 2, 2017, 'aktif', 'Pengguna-27', NULL, NULL, NULL),
(28, '1702211001', 'Samsul Baharudin Umam', 'samsul17@gmail.com', '$2y$10$WP1ikrS9sfulNqlR6gAK8.41NcE7d0803y0ZV8hI1rzjWgyCQyP3S', 4, 3, 2017, 'aktif', 'Pengguna-28', NULL, NULL, NULL),
(29, '1702211002', 'Ery Yunitasari', 'ery@gmail.com', '$2y$10$gcwRavsPmm1i.heBAR2hXOq5zkeG/w01goPCOjwMI81YCmNv8VYY2', 4, 3, 2017, 'aktif', 'Pengguna-29', NULL, NULL, NULL),
(30, '1702211003', 'Wahyu Darmawan', 'wahyu@gmail.com', '$2y$10$/rl1PF0WxqZBPyMcLFkYD.OlXMpWBbqR/k1VAHLnJReLvR/ICVBaO', 4, 3, 2017, 'aktif', 'Pengguna-30', NULL, NULL, NULL),
(31, '195909161983021002', 'Dr. Trisyono, M.Pd', 'trisyono@gmail.com', '$2y$10$PvuWJhlTRwyUX966rkn9EuAR4n0DyFpG5YW.SGv1RD86CWiXA8Yk6', 5, 0, 2014, 'aktif', 'Pengguna-31', NULL, NULL, NULL),
(32, '197104221995012001', 'Nurul Intan Pratiwi, S.Sos., M.Si.', 'nurulintan@gmail.com', '$2y$10$vrRBH753tcIFI0ZqX2jmtuRiBO0Rgg6ycqgzLoRhADoJqQVJHqGjO', 6, 1, 2014, 'aktif', 'Pengguna-32', NULL, NULL, NULL),
(33, '197104221995012002', 'Pratiwi, S.Sos., M.Si.', 'pratiwi@gmail.com', '$2y$10$Pv2SqzKOlDtxvASBSmg78OY5kmpvgnjPUFM6S4nGg17QdWXqAn7G2', 6, 2, 2014, 'aktif', 'Pengguna-33', NULL, NULL, NULL),
(34, '197104221995012003', 'Intan Pratiwi, S.Sos., M.Si.', 'intanpratiwi@gmail.com', '$2y$10$k3DRmOFJQT3LOIsrTC0ofO/kVhSfDCGjwU5BLRJhN0IUdxvDjZN/6', 6, 3, 2014, 'aktif', 'Pengguna-34', NULL, NULL, NULL),
(35, '196409121986011002', 'Hadi Waluyo, S.H.,M.Pd.', 'hadiwaluyo@gmail.com', '$2y$10$nm/sxbw/dIWwFrODqZXHnOPhz.Ohd93tT.JVbuqeH11t0oFT0yPdq', 7, 1, 2014, 'aktif', 'Pengguna-35', NULL, NULL, NULL),
(36, '196409121986011003', 'Hadi, S.H.,M.Pd.', 'hadi@gmail.com', '$2y$10$IaaJvK2bf9Yv.a3wr85Ad.GtLMDoPPCCPJkRo6ukWCqdCQzbBraVm', 7, 2, 2014, 'aktif', 'Pengguna-36', NULL, NULL, NULL),
(37, '196409121986011004', 'Waluyo, S.H.,M.Pd.', 'waluyo@gmail.com', '$2y$10$6fjVdCARZBqFtSG1B.da5en/0vElxFO2JECAVD6wWLK6A9pl3/0OO', 7, 3, 2014, 'aktif', 'Pengguna-37', NULL, NULL, NULL),
(38, '1606211001', 'Harum Puji Lestari Jayanti', 'tari@gmail.com', '$2y$10$Sb8tq3uqmPBlyW/epTcJf.DFgHtYcoriSkOSHb584gsYCMCDrcAPu', 4, 1, 2016, 'aktif', 'Pengguna-38', NULL, NULL, NULL),
(39, '1606211002', 'Yanti Manda Safitri', 'yanti@gmail.com', '$2y$10$vXfl2Kp9AZyYIWnQ1PjGRO/0OxjbAqct3oZFX1KkSplspVOT8j8Em', 4, 1, 2016, 'aktif', 'Pengguna-39', NULL, NULL, NULL),
(40, '1606211003', 'Rani Pangestu Dewi', 'rani@gmail.com', '$2y$10$VufNfGuO4co/gu5qvi5FAORXU8JAXMtt.qilZ5e3jsnaF01DZBg9y', 4, 1, 2016, 'aktif', 'Pengguna-40', NULL, NULL, NULL),
(41, '1605211001', 'Aziza Kezia Lina', 'aziza@gmail.com', '$2y$10$v/hYZ1X/oyTTYk.nIZ8gOOqtgGBHpwqcmTbCgb3Pmd/M7ZZm6qET2', 4, 2, 2016, 'aktif', 'Pengguna-41', NULL, NULL, NULL),
(42, '1605211002', 'Milla Dewi Rianti', 'milla@gmail.com', '$2y$10$RRm24KY1UoAhfcxW1JU4puBehXQAhOvXwnuNAZArPB8SKbzvcuxkW', 4, 2, 2016, 'aktif', 'Pengguna-42', NULL, NULL, NULL),
(43, '1605211003', 'Sari Hanifa Linda', 'sari@gmail.com', '$2y$10$SwoDaPAKYN5LTDyJW0a4Z.puRm2AhGSDrUlAJ6ersXZH1Swx6IC/a', 4, 2, 2016, 'aktif', 'Pengguna-43', NULL, NULL, NULL),
(44, '1602211001', 'Omar Khairul Habib', 'omar@gmail.com', '$2y$10$RwNwjjzC/VF40QVRyfu52.QryzKlaPxd9aF34VL1dI5cJPUUt7lS6', 4, 3, 2016, 'aktif', 'Pengguna-44', NULL, NULL, NULL),
(45, '1602211002', 'Anto Kusumo Dewo', 'anto@gmail.com', '$2y$10$IQZjytg6QF/HjQpt/tR8zuF4nGHyWOQBUPT3BPX4EUdxZKHSCgxg6', 4, 3, 2016, 'aktif', 'Pengguna-45', NULL, NULL, NULL),
(46, '1602211003', 'Muisyr Reno Fajar', 'muisyr@gmail.com', '$2y$10$SZb4wo5xV3YKIhtcAl9Mdudy2wO9dlsrdtI5WchWGryjr.xYV6KF.', 4, 3, 2016, 'aktif', 'Pengguna-46', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `fase_akademiks`
--
ALTER TABLE `fase_akademiks`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `hasil_studis`
--
ALTER TABLE `hasil_studis`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kartu_rencanas`
--
ALTER TABLE `kartu_rencanas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori_penggunas`
--
ALTER TABLE `kategori_penggunas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `kategori_penggunas_nama_unique` (`nama`);

--
-- Indeks untuk tabel `kurikulums`
--
ALTER TABLE `kurikulums`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `matkuls`
--
ALTER TABLE `matkuls`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `matkuls_kode_unique` (`kode`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `paket_kuliahs`
--
ALTER TABLE `paket_kuliahs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `prodis`
--
ALTER TABLE `prodis`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `prodis_kode_unique` (`kode`),
  ADD UNIQUE KEY `prodis_nomor_unique` (`nomor`);

--
-- Indeks untuk tabel `struktur_paket_kuliahs`
--
ALTER TABLE `struktur_paket_kuliahs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `transkrips`
--
ALTER TABLE `transkrips`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_nomorinduk_unique` (`nomorInduk`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `fase_akademiks`
--
ALTER TABLE `fase_akademiks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `hasil_studis`
--
ALTER TABLE `hasil_studis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `kartu_rencanas`
--
ALTER TABLE `kartu_rencanas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `kategori_penggunas`
--
ALTER TABLE `kategori_penggunas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `kurikulums`
--
ALTER TABLE `kurikulums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `matkuls`
--
ALTER TABLE `matkuls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=661;

--
-- AUTO_INCREMENT untuk tabel `paket_kuliahs`
--
ALTER TABLE `paket_kuliahs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `prodis`
--
ALTER TABLE `prodis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `struktur_paket_kuliahs`
--
ALTER TABLE `struktur_paket_kuliahs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `transkrips`
--
ALTER TABLE `transkrips`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

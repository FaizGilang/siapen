<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Matkul extends Model
{
    //
    public $fillable = ['SKS','kodeprodi','kode','nama','komulatif','SKSteori','SKSpraktek','jpm','dosen1','dosen2','bobotUts','bobotUas','bobotTugas','bobotPraktek'];
   
       public function hasil()
    {
        return $this->belongsTo('App\HasilStudi');
    }
}

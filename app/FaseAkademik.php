<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaseAkademik extends Model
{
    //
    public $fillable = ['tahunAjaran','faseRegistrasi','awalKuliah','akhirKuliah','faseEval','semester','status'];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriPengguna extends Model
{
     public $fillable = ['nama','status'];
}

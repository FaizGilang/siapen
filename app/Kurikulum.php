<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kurikulum extends Model
{
    //
    public $fillable = ['tahun','idProdi','status'];
}

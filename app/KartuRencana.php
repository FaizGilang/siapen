<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KartuRencana extends Model
{
    //
    public $fillable = ['idUser','idPaketKuliah','alpa','izin','status'];
}

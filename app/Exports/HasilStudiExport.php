<?php

namespace App\Exports;

use App\HasilStudi;
use Maatwebsite\Excel\Concerns\FromCollection;

class HasilStudiExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return HasilStudi::all();
    }
}

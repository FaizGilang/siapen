<?php

namespace App\Http\Controllers;
use DB;
use Auth;
use App\Prodi; 
use App\Matkul; 
use App\User; 
use Illuminate\Http\Request;
use App\Http\Requests\ValidasiProdi;
use App\Http\Requests\ValidasiUbahProdi;
use Alert;

class ProdiController extends Controller
{  


    
    public function lihat()
    {
        //tampilan detail prodi (dalam bentuk tabel)
        $prodi=Prodi::paginate(5);
        return view('prodi.lihat')->with('prodi',$prodi); //with 'prodi' memanggil prodi yg ada pada index
    }


    public function tambah()
    {
        //menampilkan form add 
        return view('prodi.tambah');
    }


    public function simpan(ValidasiProdi $request)
    {
        //menyimpan pada database dari form add             
        $prodi = new Prodi;
        $prodi->kode = $request->kode;
        $prodi->nomor = $request->nomor;
        $prodi->nama = $request->nama;
        $prodi->status = "tidak";

        $prodi->save();

        Alert('Data Prodi Berhasil Ditambah', 'Program Studi');
        return redirect('/prodi/lihat'); //disimpan lalu ditampilkan di file index
    }


  
    public function ubah(Prodi $prodi, $id)
    {
        //menampilkan form edit
        $prodi = Prodi::find($id);
        return view('prodi.ubah')->with('prodi',$prodi); //
    }


    public function perbarui(ValidasiUbahProdi $request, $id)
    {
        //menyimpan data yg sudah di edit
        $prodi = Prodi::find($id);
        $prodi->kode = $request->kode;
        $prodi->nomor = $request->nomor;
        $prodi->nama = $request->nama;

        $prodi->save();

        Alert('Data Prodi Berhasil Diubah', 'Program Studi');
        return redirect('/prodi/lihat');
    }


    public function hapus($id)
    {
        //menghapus data sesuai id yg dipilih
        $prodi = Prodi::findOrFail($id);
        $prodi->delete();

        Alert('Data Prodi Berhasil Dihapus', 'Program Studi');
        return response()->json($prodi);
    }

    

    public function status(Request $request, $id)
    {
        //
        $prodi = Prodi::find($id);
        $prodi->status = "aktif";

        $prodi->save();
        return redirect('/prodi/lihat');
    }
}

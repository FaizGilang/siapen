<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\User;
use App\Prodi;
use App\Matkul;
use App\KategoriPengguna;
use Illuminate\Http\Request;
use Hash;
use Validator;
use redirect;
use App\Http\Requests;
use Attribute;
use Value;
use Parameters;
use Excel;
use App\TahunAjaran;
use Carbon\Carbon;
use App\Http\Requests\ValidasiMahasiswa;
use App\Http\Requests\ValidasiUbahMahasiswa;
use App\Http\Requests\ValidasiDosen;
use App\Http\Requests\ValidasiUbahDosen;
use File;
use Storage;
use Illuminate\Support\Facades\Input;
use Alert;

class UserController extends Controller
{
    public function lihat(Request $request)
    {
        //menampilkan detail user (tabel)
        $user = DB::table('users')
        ->join('prodis','users.prodi', '=', 'prodis.id')
        ->join('kategori_penggunas','users.kategori', '=', 'kategori_penggunas.id')
        ->select('users.*','users.status', 'prodis.kode as kodeprodi',  'users.tahunMasuk')
        ->where('users.kategori','=',4)
        ->paginate(15);

        // $user->appends($request->only('q'));

        return view('user.lihat')->with('user',$user);
    }

    public function lihat1(Request $request)
    {
        //menampilkan detail user (tabel)
       $user = DB::table('users')
        ->join('prodis','users.prodi', '=', 'prodis.id')
        ->join('kategori_penggunas','users.kategori', '=', 'kategori_penggunas.id')
        ->select('users.*','users.status', 'prodis.kode as kodeprodi',  'users.tahunMasuk','kategori_penggunas.nama as namakat')
        ->orWhere('users.kategori','=',3)
        ->orWhere('users.kategori','=',6)
        ->orWhere('users.kategori','=',7)
        ->paginate(20);

       $userumum = DB::table('users')
        ->join('kategori_penggunas','users.kategori', '=', 'kategori_penggunas.id')
        ->select('users.*','users.status','kategori_penggunas.nama as namakat')
        ->orWhere('users.kategori','=',5)
        ->where('users.prodi','=',0)
        ->paginate(20);

   
        // $user->appends($request->only('r'));
         

        return view('user.lihat1')->with('user',$user)->with('userumum',$userumum);
    }

    public function cari(Request $request)
    {
    
        if(is_numeric($request->cari)){
        $data = DB::table('users')
         ->join('prodis','users.prodi', '=', 'prodis.id')
         ->where('users.status','aktif')
         ->where('users.nomorInduk','like','%'.$request->cari.'%')
         ->select('users.*','users.status', 'prodis.kode as kodeprodi',  'users.tahunMasuk')
         ->paginate(15);


        }else{
        $data = DB::table('users')
         ->join('prodis','users.prodi', '=', 'prodis.id')
         ->where('users.status','aktif')
         ->where('users.nama','like','%'.$request->cari.'%')
         ->select('users.*','users.status', 'prodis.kode as kodeprodi',  'users.tahunMasuk')
         ->paginate(15);
        }
            
        return view('user.lihat')  
        ->with('user',$data);   
    }

    public function cari1(Request $request)
    {
    
        if(is_numeric($request->cari1)){
            $data = DB::table('users')
             ->join('kategori_penggunas','users.kategori','=','kategori_penggunas.id')
             ->join('prodis','users.prodi', '=', 'prodis.id')
             ->where('users.status','aktif')
             ->where('users.kategori','like','%'.$request->cari1.'%')
             ->select('users.*','users.status',   'users.tahunMasuk','kategori_penggunas.nama as namakat', 'prodis.kode as kodeprodi')
             ->get();


              $data1 = DB::table('users')
             ->join('kategori_penggunas','users.kategori','=','kategori_penggunas.id')
             ->join('prodis','users.prodi', '=', 'prodis.id')
             ->where('users.status','aktif')
             ->where('users.prodi','=',0)
             ->where('users.kategori','=',5,'like','%'.$request->cari1.'%')
             ->select('users.*','users.status',  'users.tahunMasuk', 'kategori_penggunas.nama as namakat' , 'prodis.kode as kodeprodi')
             ->get();
        }else{
            $data = DB::table('users')
             ->join('prodis','users.prodi', '=', 'prodis.id')
             ->join('kategori_penggunas','users.kategori','=','kategori_penggunas.id')
             ->where('users.status','aktif')
             ->where('users.nama','like','%'.$request->cari1.'%')
             ->select('users.*','users.status', 'prodis.kode as kodeprodi',  'users.tahunMasuk','kategori_penggunas.nama as namakat')
             ->get();
             $data1 = DB::table('users')
             ->join('kategori_penggunas','users.kategori','=','kategori_penggunas.id')
             ->where('users.status','aktif')
             ->where('users.nama','like','%'.$request->cari1.'%')
             ->where('users.prodi','=',0)
             ->where('users.kategori','=',5)
             ->select('users.*','users.status',   'users.tahunMasuk','kategori_penggunas.nama as namakat')
             ->get();
        }
            
        return view('user.lihat1')  
        ->with('user',$data)   
        ->with('userumum',$data1);   
    }



    public function detail($id)
    {
        //menampilkan detail user 
        $user = User::find($id);

        if($user->kategori==5){
            $data = DB::table('users')
                ->join('kategori_penggunas','users.kategori', '=', 'kategori_penggunas.id')
                ->select('users.*','kategori_penggunas.nama as namakategori','users.prodi as namaprodi')
                ->where('users.kategori',5)
                ->where('users.prodi','=',0)
                ->get()->first();

        }else{
            $data = DB::table('users')
                ->join('prodis','users.prodi', '=', 'prodis.id')
                ->join('kategori_penggunas','users.kategori', '=', 'kategori_penggunas.id')
                ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakategori')
                ->where('users.id','=',$id)
                ->get()->first();
        }


    
        return view('user.detail')->with('data',$data);
    }

    public function detail1($id)
    {
        //menampilkan detail user 
        $data = DB::table('users')
        ->join('prodis','users.prodi', '=', 'prodis.id')
        ->join('kategori_penggunas','users.kategori', '=', 'kategori_penggunas.id')
        ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakategori')
        ->where('users.id','=',$id)
        ->get()->first();

        $data1 = DB::table('users')
        ->join('kategori_penggunas','users.kategori', '=', 'kategori_penggunas.id')
        ->select('users.*','users.status')
        ->orWhere('users.kategori','=',5)
        ->where('users.prodi','=',0)
        ->get()->first();
    
        return view('user.detail1')->with('data',$data)->with('data1',$data1);
    }

    public function tambah()
    {
        //
        $prodi = Prodi::where('status','aktif')->get();
        $kategori = KategoriPengguna::where('status','aktif')->get();
        
        return view('user.tambah')
        ->with('kategori',$kategori)
        ->with('prodi',$prodi);
    }

    public function tambah1()
    {
        //
        $prodi = Prodi::where('status','aktif')->get();
        $kategori = KategoriPengguna::where('status','aktif')->get();

        return view('user.tambah1')
        ->with('kategori',$kategori)
        ->with('prodi',$prodi);
    }

    
    public function simpan(ValidasiMahasiswa $request)
    {
        //menyimpan pada database
        $year = Carbon::createFromFormat('Y',$request->tahunMasuk); 
        $nomor = Prodi::find($request->prodi);
        $jumlah = User::where('tahunMasuk',$request->tahunMasuk)->where('prodi',$request->prodi)->where('kategori',4)->count();
        $urutan = $jumlah+1;

        if($urutan<10){
            $urutan = '0'.$urutan;
        }else{
            $urutan = $urutan;
        }

        $user = new User;
        $user->nomorInduk = $year->format('y').$nomor->nomor.'110'.$urutan;
        $user->nama = $request->nama;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->prodi = $request->prodi;
        $user->kategori = $request->kategori;
        $user->tahunMasuk = $request->tahunMasuk;
        $user->status ="belum";
        $user->file ="0";
        $user->save();

        $foto=User::find($user->id);
        $ext = Input::file('file')->getClientOriginalExtension();
        $filename = 'Pengguna-' . $user->id . '.' . $ext;
        $request->file('file')->move('FotoPengguna/', $filename);
        $foto->file = $filename;

        $foto->save();
        Alert('Data Mahasiswa Berhasil Ditambah', 'Mahasiswa');
        return redirect('/user/lihat');
    }

    public function simpan1(ValidasiDosen $request)
    {
        //menyimpan pada database dari form add
             
        $user = new User;
        $user->nomorInduk = $request->nomorInduk;
        $user->nama = $request->nama;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->prodi = $request->prodi;
        $user->kategori = $request->kategori;
        $user->tahunMasuk = $request->tahunMasuk;
        $user->status ="belum";
        $user->file ="0";
        $user->save();

        $foto=User::find($user->id);
        $ext = Input::file('file')->getClientOriginalExtension();
        $filename = 'Pengguna-' . $user->id . '.' . $ext;
        $request->file('file')->move('FotoPengguna/', $filename);
        $foto->file = $filename;

        $foto->save();

        Alert('Data Dosen Berhasil Ditambah', 'Dosen');

        return redirect('/user/lihat1');
    }


    public function ubah(User $user, $id)
    {
        //
        $prodi = Prodi::all();
        $user = DB::table('users')
        ->join('kategori_penggunas','users.kategori','=','kategori_penggunas.id')
        ->join('prodis','users.prodi','=','prodis.id')
        ->select('users.*','users.nomorInduk','kategori_penggunas.nama as namakategori','kategori_penggunas.id as idkategori','prodis.nama as namaprodi','prodis.id as idprodi','users.tahunMasuk')
        ->where('users.id','=',$id)
        ->where('kategori_penggunas.id','=',4)
        ->where('prodis.status','=','aktif')
        ->get()->first();

        return view('user.ubah')
        ->with('user',$user)
        ->with('prodi',$prodi);
    }

    public function ubah1(User $user, $id)
    {
        
        // $user = DB::table('users')
        // ->join('kategori_penggunas','users.kategori','=','kategori_penggunas.id')
        // ->join('prodis','users.prodi','=','prodis.id')
        // ->select('users.*','kategori_penggunas.nama as namakategori','kategori_penggunas.id as idkategori','prodis.nama as namaprodi','prodis.id as idprodi','users.tahunMasuk')
        // ->where('users.id','=',$id)
        // ->where('prodis.status','=','aktif')
        // ->get()->first();

        
        // $kategori = KategoriPengguna::all(); 
        // return view('user.ubah1')
        // ->with('user',$user)
        // ->with('kategori',$kategori); //value yg dikirim ke view


       
        $kategori = KategoriPengguna::all();
        $prodi = Prodi::all(); 
        $cek=User::find($id);
        $cek1=Prodi::find($cek->prodi);

        if($cek->prodi==0){
          $userumum = DB::table('users')
                ->join('kategori_penggunas','users.kategori','=','kategori_penggunas.id')
                ->select('users.*','kategori_penggunas.nama as namakategori','kategori_penggunas.id as idkategori','users.tahunMasuk','users.nomorInduk')
                ->where('users.id','=',$id)
                ->get()->first(); 
        }
        else{
            $user = DB::table('users')
                ->join('kategori_penggunas','users.kategori','=','kategori_penggunas.id')
                ->join('prodis','users.prodi','=','prodis.id')
                ->select('users.*','kategori_penggunas.nama as namakategori','kategori_penggunas.id as idkategori','prodis.nama as namaprodi','prodis.id as idprodi','users.tahunMasuk','users.nomorInduk')
                ->where('users.id','=',$id)
                ->get()->first();

        } 

        
        return view('user.ubah1')
        ->with('user',$user)
        ->with('prodi',$prodi)
        ->with('kategori',$kategori)->with('cek',$cek)->with('cek1',$cek1);
    }

    

    public function perbarui(ValidasiUbahMahasiswa $request, $id)
    {
        //
        $user = User::find($id);
        $user->nama = $request->nama;
        $user->email = $request->email;
        $user->prodi = $request->prodi;
        $user->kategori = $request->kategori;
        $user->tahunMasuk = $request->tahunMasuk;
        

        if ($request->file('file')) {
          File::delete('Pengguna/' . $user->file);

           $ext = Input::file('file')->getClientOriginalExtension();
           $filename = 'Pengguna-' . $user->id . '.' . $ext;
           $request->file('file')->move('FotoPengguna/', $filename);

           $user->file = $filename;
      }

        $user->save();
        Alert('Data Mahasiswa Berhasil Diubah', 'Mahasiswa');
        return redirect('/user/lihat');
    }

    public function perbarui1(ValidasiUbahDosen $request, $id)
    {
        //
        $user = User::find($id);
        $user->nomorInduk = $request->nomorInduk;
        $user->nama = $request->nama;
        $user->email = $request->email;
        $user->prodi = $request->prodi;
        $user->kategori = $request->kategori;
        $user->tahunMasuk = $request->tahunMasuk;
        

        if ($request->file('file')) {
          File::delete('Pengguna/' . $user->file);

           $ext = Input::file('file')->getClientOriginalExtension();
           $filename = 'Pengguna-' . $user->id . '.' . $ext;
           $request->file('file')->move('FotoPengguna/', $filename);

           $user->file = $filename;
      }

        $user->save();
        Alert('Data Dosen Berhasil Diubah', 'Dosen');
        return redirect('/user/lihat1');
    }

    

    public function hapus($id)
    {
        //
        $user = User::findOrFail($id);
        $user->delete();
        Alert('Data Pengguna Berhasil Dihapus', 'Pengguna');
        return response()->json($user);
    }


    //menampilkan detail profil pengguna sesuai hak akses login
    public function detailProfil()
    {
        //
        $profil = DB::table('users')
            ->join('prodis', 'users.prodi', '=', 'prodis.id')
            ->join('kategori_penggunas', 'users.kategori', '=', 'kategori_penggunas.id')
            ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakategori')
            ->where('users.id','=',Auth::user()->id)
             ->get()->first();

        return view('user.detailProfil')->with('data',$profil);
    }


    //form edit profil 
    public function ubahProfil(user $user)
    {
        $profil = DB::table('users')
            ->join('prodis', 'users.prodi', '=', 'prodis.id')
            ->join('kategori_penggunas', 'users.kategori', '=', 'kategori_penggunas.id')
            ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakategori')
            ->where('users.id','=',Auth::user()->id)
             ->get()->first();

        return view('user.ubahProfil')->with('data',$profil);
    }


    //update profil yg sudah di edit
    public function simpanProfil(Request $request)
    {
        //
        $profil = User::find(Auth::user()->id);
        $profil->nomorInduk = $request->nomorInduk;
        $profil->nama = $request->nama;
        $profil->email = $request->email;
        $profil->password = $request->password;
       
        $profil->save();
        return redirect('/user/detailProfil');
    }


    public function reset($id){
        $data = User::find($id);
        $data->password = bcrypt('123456789');
        $data->save();
        return redirect('/user/lihat');
    }


    public function status(Request $request, $id)
    {
        //
        $user = User::find($id);
        $user->status = "aktif";

        $user->save();
        return redirect('/user/lihat');
    }

    public function perbaruiSandi(Request $request)
    {
        $member = Auth::user();
        $this->validate($request, [
            'password'  => 'required|passcheck:' . $member->password,
            'new_password'  => 'required|confirmed|min:6',
            ],
            [

            'password.required' => 'Kata Sandi Tidak Boleh Kosong ',
            'new_password.required' => 'Kata Sandi Baru Tidak Boleh Kosong ',
            'password.passcheck' => 'Password lama tidak sesuai',
            'new_password.confirmed' => 'Password tidak sesuai'
            ]);

        $member->password = bcrypt($request->get('new_password'));
        $member->save();

        Alert('Kata Sandi Berhasil Diubah', 'Kata Sandi');
        return redirect('/home');
    }

    public function ubahSandi()
    {
        return view('user.ubahKataSandi');
    }     

    public function jumlahMatkul()
    {
        
    }
 
}

<?php

namespace App\Http\Controllers;
use DB;
use App\Prodi;
use App\Kurikulum;
use Illuminate\Http\Request;
use Session;
use App\Http\Requests\ValidasiKurikulum;
use Alert;

class KurikulumController extends Controller
{
   
    public function getKurikulum($id)
    {
        $kurikulum = DB::table('kurikulums')->where('idProdi',$id)->pluck('tahun','id'); 
            return json_encode($kurikulum);
    }

    public function lihat()
    {
        //
        $kurikulum = DB::table('kurikulums') //TABEL SG DIPIPLIH
            ->join('prodis', 'kurikulums.idProdi', '=', 'prodis.id')
            ->select('kurikulums.*', 'prodis.nama as namaprodi')
            ->get();

        return view('kurikulum.lihat')->with('kurikulum',$kurikulum);
        
    }

    public function tambah()
    {
        $prodi = Prodi::where('status','aktif')->get();
        return view('kurikulum.tambah')
        ->with('prodi',$prodi);
    }
   
    public function simpan(ValidasiKurikulum $request)
    {
        $validasi=Kurikulum::where('kurikulums.id','=',$request->idProdi)
                ->where('kurikulums.tahun','=',$request->tahun)
                ->count();
        
        if($validasi>0)
          {
            return redirect('/kurikulum/tambah/')
                ->with('alert-success',"Data Yang Sama Sudah Ada");
            
          }
          else{

            $kurikulum = new Kurikulum;
            $kurikulum->tahun = $request->tahun;
            $kurikulum->idProdi = $request->idProdi;
            $kurikulum->status = "tidak";

            $kurikulum->save();
    }
        Alert('Data Kurikulum Berhasil Ditambah', 'Kurikulum');
        return redirect('/kurikulum/lihat'); 
    }

    
    public function ubah(Kurikulum $kurikulum, $id)
    {
        
        $kurikulum = DB::table('kurikulums')
        ->join('prodis','kurikulums.idProdi','=','prodis.id')
        ->select('kurikulums.*','prodis.nama as namaprodi')
        ->where('kurikulums.id','=',$id)
        ->get()->first();

        $prodi = Prodi::where('status','aktif')->get();
        return view('kurikulum.ubah')
        ->with('kurikulum',$kurikulum)
        ->with('prodi', $prodi);
    }

    public function perbarui(ValidasiKurikulum $request, $id)
    {
        $kurikulum = Kurikulum::find($id);
        $kurikulum->idProdi = $request->idProdi;
        $kurikulum->tahun = $request->tahun;

        $kurikulum->save();

        Alert('Data Kurikulum Berhasil Diubah', 'Kurikulum');
        return redirect('/kurikulum/lihat');
    }

   
    public function hapus($id)
    {
        $kurikulum = Kurikulum::findOrFail($id);
        $kurikulum->delete();
     
        Alert('Data Kurikulum Berhasil Dihapus', 'Kurikulum');
        return response()->json($kurikulum);
    }

    public function status(Request $request, $id)
    {
        $kurikulum = Kurikulum::find($id);
        $kurikulum->status = "aktif";

        $kurikulum->save();
        return redirect('/kurikulum/lihat');
    }
}

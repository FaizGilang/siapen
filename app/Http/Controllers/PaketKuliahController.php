<?php

namespace App\Http\Controllers;
use App\Matkul;
use App\Prodi;
Use App\Kurikulum;
Use App\StrukturPaketKuliah;
use App\PaketKuliah;
use App\TahunAjaran;
use App\FaseAkademik;
use DB;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\ValidasiPaketKuliah;
use Alert;

class PaketKuliahController extends Controller
{
    public function tambah()
    {
        //menampilkan form add 
        $kurikulum = DB::table('kurikulums') //TABEL SG DIPIPLIH
            ->join('prodis', 'kurikulums.idProdi', '=', 'prodis.id')
            ->where('kurikulums.status','aktif')
            ->select('kurikulums.*', 'prodis.nama as namaprodi')
            ->get();

        
        $tahunAjaran = DB::table('fase_akademiks')
        ->select('fase_akademiks.*', 'fase_akademiks.semester as semester')
        ->get();

        return view('paketKuliah.tambah')
        ->with('tahunAjaran',$tahunAjaran)
        ->with('kurikulum',$kurikulum);       
    }


    public function simpan(ValidasiPaketKuliah $request)
    {
        $paket = new PaketKuliah;
        // $paket->idProdi = $request->prodi;
        $paket->tahunAjaran = $request->tahunAjaran;
        $paket->komulatif = $request->komulatif;
        $paket->idKurikulum = $request->kurikulum;
        $paket->status = "belum";
               
        $paket->save();
        $id = $paket->id;

        Alert('Data Paket Kuliah Berhasil Ditambah, Siliahkan Pilih Mata Kuliah', 'Paket Kuliah');
         return redirect('/paketKuliah/pilihpaket/'.$id)
        ->with('id',$id)
        // ->with('alert-success',$request->prodi)
        ->with('alert-success2',$request->tahunAjaran)
        ->with('alert-success3',$request->komulatif)
        ->with('alert-success4',$request->kurikulum);
    }

  
    public function pilihPaket($id)
    {
        //menampilkan detail matkul (tabel)
         $judul = DB::table('paket_kuliahs')
            ->join('kurikulums','paket_kuliahs.idKurikulum','=', 'kurikulums.id')  
            ->join('prodis','kurikulums.idProdi','=','prodis.id') 
            ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
            ->where('paket_kuliahs.id','=',$id)
            ->select('kurikulums.tahun','paket_kuliahs.tahunAjaran','paket_kuliahs.komulatif','fase_akademiks.tahunAjaran','prodis.nama as namaprodi')
            ->get()->first();
       

        $paketKuliah=DB::table('paket_kuliahs')
                ->join('kurikulums','paket_kuliahs.idKurikulum','=','kurikulums.id')
                ->where('paket_kuliahs.id',$id)
                ->get()->first();

        $prodi=$paketKuliah->idProdi;
        $matkul=Matkul::where('kodeprodi',$prodi)->where('status','aktif')->get();
        $matkul1=Matkul::where('kodeprodi',99)->where('status','aktif')->get();

        return view('paketKuliah.pilihpaket')->with('paketKuliah',$matkul)->with('judul',$judul)->with('paketKuliah1',$matkul1); 
    }

    public function simpanPaket(Request $request,$id)
    {
        if(empty($request->matkul)){
            return back()->with('error','Silahkan Pilih Mata Kuliah Dulu..');
        }
        else{
            foreach ($request->matkul as $key=>$iter){
            $pilihpaket = new StrukturPaketKuliah;
            $pilihpaket->idPaketKuliah = $id;
            $pilihpaket->idMatkul = $iter;
            $pilihpaket->save();

            
            }
            Alert('Data Struktur Paket Kuliah Berhasil Ditambah', 'Struktur Paket Kuliah');
            return redirect('/paketKuliah/lihat');  
        }

        
        
    }

    public function lihatPaket()
    {
        //menampilkan dropdown paketkuliah
         $paketKuliah = DB::table('paket_kuliahs')
                ->join('kurikulums', 'paket_kuliahs.idKurikulum', '=', 'kurikulums.id')
                ->join('prodis', 'kurikulums.idProdi', '=', 'prodis.id')
                ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
                ->select('paket_kuliahs.*',  'prodis.nama as namaprodi','kurikulums.tahun', 'fase_akademiks.tahunAjaran','fase_akademiks.semester')
                ->paginate(20);

            return view('paketKuliah.lihat')->with('paketKuliah',$paketKuliah);
    }

    public function paketKuliah(Request $request,$id)
    {
         $paketKuliah = DB::table('struktur_paket_kuliahs')
            ->join('paket_kuliahs', 'struktur_paket_kuliahs.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('matkuls', 'struktur_paket_kuliahs.idMatkul', '=', 'matkuls.id') 
            ->where('struktur_paket_kuliahs.idPaketKuliah','=',$id)
            ->select('struktur_paket_kuliahs.*','matkuls.*','paket_kuliahs.*')
            ->get();

        

        $coba = DB::table('paket_kuliahs')
            ->join('kurikulums','paket_kuliahs.idKurikulum','=', 'kurikulums.id')  
            ->join('prodis','kurikulums.idProdi','=','prodis.id') 
            ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
            ->where('paket_kuliahs.id','=',$id)
            ->select('kurikulums.tahun','paket_kuliahs.tahunAjaran','paket_kuliahs.komulatif','fase_akademiks.tahunAjaran','prodis.nama as namaprodi')
            ->get()->first();

        $jumlahsks = DB::table('struktur_paket_kuliahs')
            ->join('paket_kuliahs', 'struktur_paket_kuliahs.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('matkuls', 'struktur_paket_kuliahs.idMatkul', '=', 'matkuls.id')    
            ->where('struktur_paket_kuliahs.id','='.$id)
            ->sum('matkuls.SKS');

        return view('paketKuliah.strukturPaketKuliah')
        ->with('paketKuliah',$paketKuliah)
        ->with('coba',$coba)
        ->with('jumlahsks',$jumlahsks);
    }

    public function ubahPaket($id)
    {
        $judul = DB::table('paket_kuliahs')
            ->join('kurikulums','paket_kuliahs.idKurikulum','=', 'kurikulums.id')  
            ->join('prodis','kurikulums.idProdi','=','prodis.id') 
            ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
            ->where('paket_kuliahs.id','=',$id)
            ->select('kurikulums.tahun','paket_kuliahs.tahunAjaran','paket_kuliahs.komulatif','fase_akademiks.tahunAjaran','prodis.nama as namaprodi')
            ->get()->first();

        $data = StrukturPaketKuliah::where('idPaketKuliah',$id)->pluck('idMatkul');

        $lama = StrukturPaketKuliah::where('struktur_paket_kuliahs.idPaketKuliah',$id)
            ->join('matkuls','matkuls.id','struktur_paket_kuliahs.idMatkul')
            ->get();

        $paketKuliah=Matkul::whereNotIn('id',$data)->get();
        $paketKuliah1=Matkul::whereNotIn('id',$data)->get();

        return view('paketKuliah.pilihpaket')
        ->with('paketKuliah',$paketKuliah)
        ->with('paketKuliah1',$paketKuliah1)
        ->with('lama',$lama)
        ->with('judul',$judul); 
    }

    public function perbaruiPaket(Request $request, $id)
    {
        $data = StrukturPaketKuliah::where('idPaketKuliah',$id)->get();
        
        foreach ($data as $key ) {
            $delete = StrukturPaketKuliah::find($key->id);
            $delete->delete();
        }

        foreach ($request->matkul as $key=>$iter){
            $pilihpaket = new StrukturPaketKuliah;
            $pilihpaket->idPaketKuliah = $id;
            $pilihpaket->idMatkul = $iter;
            $pilihpaket->save();

            
        }
        Alert('Data Struktur Paket Kuliah Berhasil Diubah', 'Struktur Paket Kuliah');
        return redirect('/paketKuliah/lihat');

        
    }

    public function hapus($id)
    {
        //menghapus data sesuai id yg dipilih
        $paketKuliah = PaketKuliah::findOrFail($id);
        $paketKuliah->delete();

        $struktur = StrukturPaketKuliah::where('idPaketKuliah',$id)->get();
        foreach ($struktur as $key) {
            # code...
            $delete = StrukturPaketKuliah::find($key->id);
            $delete->delete();
        }
        Alert('Data Struktur Paket Kuliah Berhasil Dihapus', 'Struktur Paket Kuliah');
        return redirect('/paketKuliah/lihat');
        // return Alert('Data Struktur Paket Kuliah Berhasil Dihapus', 'Struktur Paket Kuliah');
        
    }


    public function status(Request $request, $id)
    {
        //status -> saat disave status belum, lalu ketika ada yg sama otomatis jd tidak
        $paketKuliah = PaketKuliah::find($id);
        $paketKuliah->status = "aktif";
    
        $cari=PaketKuliah::where('idKurikulum',$paketKuliah->idKurikulum)->where('tahunAjaran',$paketKuliah->tahunAjaran)->where('komulatif',$paketKuliah->komulatif)->get();
        foreach ($cari as $key){
            $ubah = PaketKuliah::find($key->id);
            $ubah->status = "tidak";
            $ubah->save();
        }
        $paketKuliah->save();
        return redirect('/paketKuliah/lihat');
    }

    
    //UNTUK HALAMAN ADMIN
    public function lihatPaketA()
    {
        //menampilkan dropdown paketkuliah
         $paketKuliah = DB::table('paket_kuliahs')
                ->join('kurikulums', 'paket_kuliahs.idKurikulum', '=', 'kurikulums.id')
                ->join('prodis', 'kurikulums.idProdi', '=', 'prodis.id')
                ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
                ->select('paket_kuliahs.*',  'prodis.nama as namaprodi','kurikulums.tahun', 'fase_akademiks.tahunAjaran')
                ->get();

            return view('halamanAdmin.paketKuliah')->with('paketKuliah',$paketKuliah);
    }


    public function paketKuliahA(Request $request,$id)
    {
        $paketKuliah = DB::table('struktur_paket_kuliahs')
            ->join('paket_kuliahs', 'struktur_paket_kuliahs.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('matkuls', 'struktur_paket_kuliahs.idMatkul', '=', 'matkuls.id') 
            ->where('struktur_paket_kuliahs.idPaketKuliah','=',$id)
            ->select('struktur_paket_kuliahs.*','matkuls.*','paket_kuliahs.*')
            ->get();

        $coba = DB::table('paket_kuliahs')
            ->join('kurikulums','paket_kuliahs.idKurikulum','=', 'kurikulums.id')  
            ->join('prodis','kurikulums.idProdi','=','prodis.id') 
            ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
            ->where('paket_kuliahs.id','=',$id)
            ->select('kurikulums.tahun','paket_kuliahs.tahunAjaran','paket_kuliahs.komulatif','fase_akademiks.tahunAjaran','prodis.nama as namaprodi')
            ->get()->first();

        return view('halamanAdmin.strukturPaketKuliah')
        ->with('paketKuliah',$paketKuliah)
        ->with('coba',$coba);
    }


}

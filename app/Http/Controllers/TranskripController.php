<?php

namespace App\Http\Controllers;

use App\Transkrip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Excel;
use DB;
use App\User;
use App\Matkul;
use App\Prodi;
use App\HasilStudi;
use App\KartuRencana;
use App\TahunAjaran;
use App\FaseAkademik;
Use App\PaketKuliah;
use Carbon\Carbon;
Use PDF;
Use App;
use App\Exports\HasilStudiExport; 
Use Alert;


class TranskripController extends Controller
{

    public function ubahNilai(Request $request,$id1,$id2)
    {        
        //menampilkan form edit
        $ubahNilai = DB::table('transkrips')
        ->join('matkuls','transkrips.idMatkul','=','matkuls.id')
        ->join('users','transkrips.idUser','=','users.id')
        ->select('matkuls.*','matkuls.nama as namamatkul','transkrips.idUser','transkrips.nilaiAkhir','users.nama as namauser')
         ->where('transkrips.idUser',$id1)
        ->where('transkrips.idMatkul',$id2)
        ->get()->first();

        return view('hasilStudi.ubahNilai')->with('ubahNilai',$ubahNilai); 
    }

    public function perbaruiNilai(Request $request, $id1,$id2)
    {
        //menyimpan data yg sudah di edit
         $id= DB::table('transkrips')->where('transkrips.idUser',$id1)->where('transkrips.idMatkul',$id2)->get()->first();

        $lihatNilai= Transkrip::find($id->id);
        $lihatNilai->nilaiAkhir = $request->nilaiAkhir;
        $lihatNilai->save();

        Alert('Data Nilai Berhasil Diubah', 'Transkrip Nilai');
        return redirect('/hasilStudi/lihatMahasiswa');


    }


}

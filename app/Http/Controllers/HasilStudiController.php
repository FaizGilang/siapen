<?php

namespace App\Http\Controllers;
use Auth;
use Excel;
use DB;
use App\User;
use App\Matkul;
use App\Prodi;
use App\HasilStudi;
use App\KartuRencana;
use App\TahunAjaran;
use App\FaseAkademik;
Use App\PaketKuliah;
Use App\Transkrip;
Use App\KategoriPengguna;
use Illuminate\Http\Request;
use Carbon\Carbon;
Use PDF;
Use App;
use App\Exports\HasilStudiExport;
use App\Http\Requests\ValidasiExcel; 
use App\Http\Requests\ValidasiUnggahTranskrip;


class HasilStudiController extends Controller
{
   public function import_export()
   {
        return view('halamanDosen.import_export');
   }


    public function unduhExcel(Request $request, $type, $id)
    {
        $krs =   User::join('kartu_rencanas', 'kartu_rencanas.idUser', '=', 'users.id')
            ->join('paket_kuliahs', 'kartu_rencanas.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('struktur_paket_kuliahs', 'struktur_paket_kuliahs.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('matkuls', 'struktur_paket_kuliahs.idMatkul', '=', 'matkuls.id')
            ->where('matkuls.id','=',$id)
            ->where('kartu_rencanas.status','=','aktif')
            ->select('users.id','users.nama', 'users.nomorInduk')
            ->get()->toArray();

        if(empty($krs)){
            return back()->with('error','Tidak ada data mahasiswa pada mata kuliah ini');
        }

        foreach ($krs as $key => $value) {
           $data[] = array_merge($value, array('uts'=> '0','uas'=> '0','tugas'=> '0','praktek'=> '0')); # code...
        }
        
        return Excel::create('Format Nilai', function($excel) use ($data){
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });

        })->download($type);
   }


   public function unggahExcel(Request $request)
   {
      if($request->hasFile('import_file')){
        $path = $request->file('import_file')->getRealPath();
        $data = Excel::load($path, function($reader){})->get();

          if(!empty($data) && $data->count()){
            foreach ($data->toArray() as $key => $value) 
            {    
            if (array_key_exists("uas",$value) AND array_key_exists("uts",$value) AND array_key_exists("tugas",$value) AND array_key_exists("praktek",$value)) {
                 

                if ($value['uts'] < 0 || $value['uts'] >100 || $value['uas'] < 0 || $value['uas'] >100 || $value['tugas'] < 0 || $value['tugas'] >100 || $value['praktek'] < 0 || $value['praktek'] >100) {
                  return back()->with('error','Data nilai ada yang salah kurang dari nol atau lebih dari 100. Silahkan isi nilai 10-100');  
                }

                elseif(!is_numeric($value['uts']) || !is_numeric($value['uas']) || !is_numeric($value['tugas']) || !is_numeric($value['praktek']) ){
                    return back()->with('error','Data nilai ada yang salah tidak berbentuk angka');  
                }  

              }else{
                return back()->with('error','Maaf, File Format tidak sesuai'); 
              } 
            }  

            foreach ($data->toArray() as $key => $value) 
            {                  
                $mhs =   DB::table('users')->where('users.nomorInduk','=', $value['nomorinduk'])
                  ->select('users.id')
                  ->get()->first();

                $insert[] = ['idUser' => $mhs->id,'uts' => $value['uts'],'uas' => $value['uas'], 'tugas' => $value['tugas'],'praktek' => $value['praktek'],'idMatkul' => $request->unggahNilai];            
            }       
                    $matkul = Matkul::find($request->unggahNilai);
                    $matkul->bobotUts = $request->bobotUts;
                    $matkul->bobotUas = $request->bobotUas;
                    $matkul->bobotTugas = $request->bobotTugas;
                    $matkul->bobotPraktek = $request->bobotPraktek;
                    $matkul->save();

          if(!empty($insert) ){
            HasilStudi::insert($insert);
            return back()->with('success','Data Berhasil Di Simpan');
          }
       }
    }
    return back()->with('error','Maaf, File Nilai Belum Ada');    
   }


   public function getbobotUts($id)
    {
       

            $bobotUts = DB::table('matkuls')->where('matkuls.id',$id)->pluck('bobotUts'); // listia
       
            return json_encode($bobotUts);

    }
    public function getbobotUas($id)
    {
       

            $bobotUas = DB::table('matkuls')->where('matkuls.id',$id)->pluck('bobotUas'); // listia
       
            return json_encode($bobotUas);

    }
    public function getbobotTugas($id)
    {
       

            $bobotTugas = DB::table('matkuls')->where('matkuls.id',$id)->pluck('bobotTugas'); // listia
       
            return json_encode($bobotTugas);

    }
    public function getbobotPraktek($id)
    {
       

            $bobotPraktek = DB::table('matkuls')->where('matkuls.id',$id)->pluck('bobotPraktek'); // listia
       
            return json_encode($bobotPraktek);

    }

    //HASIL STUDI 
    public function lihatHSM()
    {

        $time=Carbon::now();
        $year=$time->year;
        $data = DB::table('users')->where('users.id','=',Auth::user()->id)->select('users.tahunMasuk')->first();
       
        $tahunAjaran=DB::table('fase_akademiks')->where('status','aktif')->get()->first();

        if($tahunAjaran->semester=='ganjil'){ 
            if(($year-$data->tahunMasuk) == '0'){ //ganjil: 2018-2018 = 0
              $semester = 1;
            }elseif(($year-$data->tahunMasuk)== '1'){ //ganjil: 2018-2017 = 1
              $semester = 3;   
            }else{ //ganjil: 2018-2017 = 1
              $semester = 'lulus';   
            }
        }else{
            if(($year-$data->tahunMasuk) == '1'){ //genap: 2019-2018 = 1
              $semester = 2;
            }elseif(($year-$data->tahunMasuk)== '2'){ //genap: 2019-2017 = 2
              $semester = 4;
            }else{ //ganjil: 2018-2017 = 1
              $semester = 'lulus';   
            }
        }


         $semt = DB::table('fase_akademiks')
                ->where('fase_akademiks.status','=','aktif')
                ->select('fase_akademiks.semester as semt','fase_akademiks.tahunAjaran as tahun')
                ->get()->first();

      
        
          $nilaib =KartuRencana::join('users', 'users.id', '=', 'kartu_rencanas.idUser')  
            ->join('hasil_studis','hasil_studis.idUser','=','users.id')
            ->join('matkuls', 'hasil_studis.idMatkul','=', 'matkuls.id')
            ->join('paket_kuliahs', 'kartu_rencanas.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->where('paket_kuliahs.komulatif','=',$semester)
            ->where('kartu_rencanas.status','=','aktif')
            ->where('kartu_rencanas.idUser','=',Auth::user()->id)
            ->select('matkuls.nama','matkuls.SKS','matkuls.jpm','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek','hasil_studis.uts','hasil_studis.uas','hasil_studis.praktek','hasil_studis.tugas')
            ->get();



    
          $profil = DB::table('users')
            ->join('prodis', 'users.prodi', '=', 'prodis.id')
            ->join('kategori_penggunas', 'users.kategori', '=', 'kategori_penggunas.id')
            ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakat')
            ->where('users.id','=',Auth::user()->id)
            ->get()->first();

         if($semester=='lulus'){
            $lulus = 'lulus';
        }else{
            $lulus = 'aktif';
        }
         

          return view('hasilStudi.lihatHSMSekarang')->with('profil',$profil)->with('semester', $semester)->with('nilaib',$nilaib)->with('semt',$semt)->with('lulus',$lulus);
    }



    public function lihatHSMLalu($semester)
    {
        $time=Carbon::now();
        $year=$time->year;
        $data = DB::table('users')->where('users.id','=',Auth::user()->id)->select('users.tahunMasuk')->first();
       
        $tahunAjaran=DB::table('fase_akademiks')->where('status','aktif')->get()->first();

        if($tahunAjaran->semester=='ganjil'){ 
            if(($year-$data->tahunMasuk) == '0'){ //ganjil: 2018-2018 = 0
              $mhs = 1;
            }elseif(($year-$data->tahunMasuk)== '1'){ //ganjil: 2018-2017 = 1
              $mhs = 3;   
            }else{ //genap: 2019-2017 = 2
              $mhs = 5; 
            }
        }else{
            if(($year-$data->tahunMasuk) == '1'){ //genap: 2019-2018 = 1
              $mhs = 2;
            }elseif(($year-$data->tahunMasuk)== '2'){ //genap: 2019-2017 = 2
              $mhs = 4;
            }else{ //genap: 2019-2017 = 2
              $mhs = 6;
            }
        }
        // dd($tahunAjaran->semester);die;
        if($mhs > $semester ){
            
            $idx=Auth::user()->id;



            $nilai = Transkrip::join('matkuls','transkrips.idMatkul','=','matkuls.id')
                ->where('transkrips.komulatif','=',$semester)
                ->where('transkrips.idUser','=',$idx)
                ->get();


            $profil = DB::table('users')
                ->join('prodis', 'users.prodi', '=', 'prodis.id')
                ->join('kategori_penggunas', 'users.kategori', '=', 'kategori_penggunas.id')
                ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakat')
                ->where('users.id','=',$idx)
                ->get()->first();

                $tahunAkademik = DB::table('kartu_rencanas')
                    ->join('paket_kuliahs','kartu_rencanas.idPaketKuliah','=','paket_kuliahs.id')
                    ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
                    ->select('fase_akademiks.*','fase_akademiks.tahunAjaran as tahunAkademik')
                    ->where('kartu_rencanas.idUser','=',$idx)
                    ->where('paket_kuliahs.komulatif','=',$semester)
                    ->get()->first();

            // $tahunAkademik = DB::table('kartu_rencanas')
            //     ->join('paket_kuliahs','kartu_rencanas.idPaketKuliah','=','paket_kuliahs.id')
            //     ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
            //     ->select('fase_akademiks.*','fase_akademiks.tahunAjaran as tahunAkademik')
            //     ->where('kartu_rencanas.idUser','=',$idx)
            //     ->get()->first();
                // dd($tahunAkademik);



        return view('hasilStudi.lihatHSM')->with('profil',$profil)->with('semester', $semester)->with('nilai',$nilai)->with('tahunAkademik', $tahunAkademik);

                // return view('hasilStudi.lihatHSM')->with('profil',$profil)->with('semester', $semester)->with('nilai',$nilai);
        }

        else
        {
            
            return redirect('hasilStudi/lihatHSM/');
        }

      
    }

    public function lihatTranskrip()
    {
        $idx=Auth::user()->id;


        $nilai =Transkrip::join('matkuls', 'transkrips.idmatkul', '=','matkuls.id')
            ->where('transkrips.idUser','=',$idx)
            ->select('transkrips.*','matkuls.nama','matkuls.SKS','matkuls.kode','transkrips.nilaiAkhir')
            ->get();


        $profil = DB::table('users')
            ->join('prodis', 'users.prodi', '=', 'prodis.id')
            ->join('kategori_penggunas', 'users.kategori', '=', 'kategori_penggunas.id')
            ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakat')
            ->where('users.id','=',Auth::user()->id)
            ->get()->first();
            
        return view('hasilStudi.lihatTranskrip')->with('profil',$profil)->with('nilai',$nilai);               
    }

    public function pdfHSM($semester){

      $time=Carbon::now();
        $year=$time->year;
        $data = DB::table('users')->where('users.id','=',Auth::user()->id)->select('users.tahunMasuk')->first();
       
        $tahunAjaran=DB::table('fase_akademiks')->where('status','aktif')->get()->first();

        if($tahunAjaran->semester=='ganjil'){ 
            if(($year-$data->tahunMasuk) == '0'){ //ganjil: 2018-2018 = 0
              $mhs = 1;
            }elseif(($year-$data->tahunMasuk)== '1'){ //ganjil: 2018-2017 = 1
              $mhs = 3;   
            }else{ //genap: 2019-2017 = 2
              $mhs = 5;
            }
        }else{
            if(($year-$data->tahunMasuk) == '1'){ //genap: 2019-2018 = 1
              $mhs = 2;
            }elseif(($year-$data->tahunMasuk)== '2'){ //genap: 2019-2017 = 2
              $mhs = 4;
            }else{ //genap: 2019-2017 = 2
              $mhs = 6;
            }
        }





        if($mhs > $semester ){
            $idx=Auth::user()->id;
            $profil = DB::table('users')
                ->join('prodis', 'users.prodi', '=', 'prodis.id')
                ->join('kategori_penggunas', 'users.kategori', '=', 'kategori_penggunas.id')
                ->select('users.*', 'prodis.nama as namaprodi','prodis.kode as kodeprodi', 'kategori_penggunas.nama as namakat')
                ->where('users.id','=',Auth::user()->id)
                ->get()->first();

           $kajur = DB::table('users')
                ->join('prodis','users.prodi','=','prodis.id')
                ->where('users.prodi','=',Auth::user()->prodi)
                ->where('users.kategori','=',6)
                ->select('users.*', 'users.nomorInduk as nip','users.nama as kajur')
                ->get()->first();

            

            $kaprodi = DB::table('users')
                ->join('prodis','users.prodi','=','prodis.id')
                ->where('users.prodi','=',Auth::user()->prodi)
                ->where('users.kategori','=',7)
                ->select('users.*', 'users.nomorInduk as nip1','users.nama as kaprodi1')
                ->get()->first();

            

            $nilai = Transkrip::join('matkuls','transkrips.idMatkul','=','matkuls.id')
                ->where('transkrips.komulatif','=',$semester)
                ->where('transkrips.idUser','=',$idx)
                ->get();



             $tahunAkademik = DB::table('kartu_rencanas')
                    ->join('paket_kuliahs','kartu_rencanas.idPaketKuliah','=','paket_kuliahs.id')
                    ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
                    ->select('fase_akademiks.*','fase_akademiks.tahunAjaran as tahunAkademik')
                    ->where('kartu_rencanas.idUser','=',$idx)
                    ->where('paket_kuliahs.komulatif','=',$semester)
                    ->get()->first();

            

            $absen = DB::table('kartu_rencanas')
                ->join('users','kartu_rencanas.idUser','=','users.id')
                ->where('users.id','=',Auth::user()->id)
                ->select('kartu_rencanas.izin as izin','kartu_rencanas.alpa as alpa')
                ->get()->first();


           

            $jurusan = DB::table('users')->join('prodis','users.prodi','=','prodis.id')
                    ->where('users.id','=',Auth::user()->id)
                    ->select('prodis.kode as kodeprodi')   
                    ->get()
                    ->first();

            if($jurusan->kodeprodi=='DGD'){
                $jur="TEKNIK GRAFIKA DAN PENERBITAN";
            }elseif($jurusan->kodeprodi=='TKRD'){
                $jur="TEKNIK MESIN";
            }else{
                $jur="PARIWISATA";
            }



            $pdf = PDF::loadview('hasilStudi.pdfHSM', compact('profil','nilai','tahunAkademik','absen','semester','nama','nip','kaprodi','kajur','jur'));
            return $pdf->stream('HSM.pdf');

            // $pdf = PDF::loadview('hasilStudi.pdfHSM', compact('profil','nilai','absen','semester','nama','nip','kaprodi','kajur','jur'));
            // return $pdf->stream('HSM.pdf');
           
        }
        else
        {
            $idx=Auth::user()->id;
            $profil = DB::table('users')
                ->join('prodis', 'users.prodi', '=', 'prodis.id')
                ->join('kategori_penggunas', 'users.kategori', '=', 'kategori_penggunas.id')
                ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakat')
                ->where('users.id','=',Auth::user()->id)
                ->get()->first();


            $kajur = DB::table('users')
                ->join('prodis','users.prodi','=','prodis.id')
                ->where('users.prodi','=',Auth::user()->prodi)
                ->where('users.kategori','=',6)
                ->select('users.*', 'users.nomorInduk as nip','users.nama as kajur')
                ->get()->first();

            

            $kaprodi = DB::table('users')
                ->join('prodis','users.prodi','=','prodis.id')
                ->where('users.prodi','=',Auth::user()->prodi)
                ->where('users.kategori','=',7)
                ->select('users.*', 'users.nomorInduk as nip1','users.nama as kaprodi1')
                ->get()->first();


            $nilai = Transkrip::join('matkuls','transkrips.idMatkul','=','matkuls.id')
                ->where('transkrips.komulatif','=',$semester)
                ->where('transkrips.idUser','=',$idx)
                ->get();


            

            $tahunAkademik = DB::table('kartu_rencanas')
                ->join('paket_kuliahs','kartu_rencanas.idPaketKuliah','=','paket_kuliahs.id')
                ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
                ->where('kartu_rencanas.idUser','=',$idx)
                ->where('fase_akademiks.id','=',$semester)
                ->select('fase_akademiks.tahunAjaran as tahunAkademik')
                ->get()->first();

            $absen = DB::table('kartu_rencanas')
                ->join('users','kartu_rencanas.idUser','=','users.id')
                ->where('users.id','=',Auth::user()->id)
                ->select('kartu_rencanas.izin as izin','kartu_rencanas.alpa as alpa')
                ->get()->first();

            $jurusan = DB::table('users')->join('prodis','users.prodi','=','prodis.id')
                    ->where('users.id','=',Auth::user()->id)
                    ->select('prodis.kode as kodeprodi')   
                    ->get()
                    ->first();

            if($jurusan->kodeprodi=='DGD'){
                $jur="TEKNIK GRAFIKA DAN PENERBITAN";
            }elseif($jurusan->kodeprodi=='TKRD'){
                $jur="TEKNIK MESIN";
            }else{
                $jur="PARIWISATA";
            }

        $pdf = PDF::loadview('hasilStudi.pdfHSM', compact('profil','nilai','tahunAkademik','absen','mhs','semester','nama','nip','kaprodi','kajur','jur'));
            return $pdf->stream('HSM.pdf');
        }

        
            

        
    }

    public function pdfHSMsekarang()
     {
        
      $time=Carbon::now();
        $year=$time->year;
        $data = DB::table('users')->where('users.id','=',Auth::user()->id)->select('users.tahunMasuk')->first();
       
        $tahunAjaran=DB::table('fase_akademiks')->where('status','aktif')->get()->first();

        if($tahunAjaran->semester=='ganjil'){ 
            if(($year-$data->tahunMasuk) == '0'){ //ganjil: 2018-2018 = 0
              $semester = 1;
            }elseif(($year-$data->tahunMasuk)== '1'){ //ganjil: 2018-2017 = 1
              $semester = 3;   
            }
        }else{
            if(($year-$data->tahunMasuk) == '1'){ //genap: 2019-2018 = 1
              $semester = 2;
            }elseif(($year-$data->tahunMasuk)== '2'){ //genap: 2019-2017 = 2
              $semester = 4;
            }
        }

      
        
          $nilaib =KartuRencana::join('users', 'users.id', '=', 'kartu_rencanas.idUser')  
            ->join('hasil_studis','hasil_studis.idUser','=','users.id')
            ->join('matkuls', 'hasil_studis.idMatkul','=', 'matkuls.id')
            ->join('paket_kuliahs', 'kartu_rencanas.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->where('paket_kuliahs.komulatif','=',$semester)
            ->where('kartu_rencanas.status','=','aktif')
            ->where('kartu_rencanas.idUser','=',Auth::user()->id)
            ->select('matkuls.nama','matkuls.SKS','matkuls.jpm','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek','hasil_studis.uts','hasil_studis.uas','hasil_studis.praktek','hasil_studis.tugas')
            ->get();


    
          $profil = DB::table('users')
            ->join('prodis', 'users.prodi', '=', 'prodis.id')
            ->join('kategori_penggunas', 'users.kategori', '=', 'kategori_penggunas.id')
            ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakat')
            ->where('users.id','=',Auth::user()->id)
            ->get()->first();


           $jurusan = DB::table('users')->join('prodis','users.prodi','=','prodis.id')
                    ->where('users.id','=',Auth::user()->id)
                    ->select('prodis.kode as kodeprodi')   
                    ->get()
                    ->first();

            if($jurusan->kodeprodi=='DGD'){
                $jur="TEKNIK GRAFIKA DAN PENERBITAN";
            }elseif($jurusan->kodeprodi=='TKRD'){
                $jur="TEKNIK MESIN";
            }else{
                $jur="PARIWISATA";
            }

        



            $kajur = DB::table('users')
                ->join('prodis','users.prodi','=','prodis.id')
                ->where('users.prodi','=',Auth::user()->prodi)
                ->where('users.kategori','=',6)
                ->select('users.*', 'users.nomorInduk as nip','users.nama as kajur')
                ->get()->first();

            $kaprodi = DB::table('users')
                ->join('prodis','users.prodi','=','prodis.id')
                ->where('users.prodi','=',Auth::user()->prodi)
                ->where('users.kategori','=',7)
                ->select('users.*', 'users.nomorInduk as nip1','users.nama as kaprodi1')
                ->get()->first();

            $semt = DB::table('fase_akademiks')
                ->where('fase_akademiks.status','=','aktif')
                ->select('fase_akademiks.semester as semt','fase_akademiks.tahunAjaran as tahun')
                ->get()->first();

            $absen = DB::table('kartu_rencanas')
                ->join('users','kartu_rencanas.idUser','=','users.id')
                ->where('kartu_rencanas.status','=','aktif')
                ->where('users.id','=',Auth::user()->id)
                ->select('kartu_rencanas.izin as izin','kartu_rencanas.alpa as alpa')
                ->get()->first();

          
          $pdf = PDF::loadview('hasilStudi.pdfHSMsekarang', compact('profil','nilaib','idx','semt','absen','semester','nama','nip','kaprodi','kajur','jurusan','jur'));
        return $pdf->stream('HSM.pdf');
    }

     public function pdfTranskrip()
     {
        
      $idx=Auth::user()->id;        

        
        $nilai =Transkrip::join('matkuls', 'transkrips.idmatkul', '=','matkuls.id')
            ->where('transkrips.idUser','=',$idx)
            ->select('transkrips.*','matkuls.nama','matkuls.SKS','matkuls.kode','transkrips.nilaiAkhir')
            ->get();

        $profil = DB::table('users')
            ->join('prodis', 'users.prodi', '=', 'prodis.id')
            ->join('kategori_penggunas', 'users.kategori', '=', 'kategori_penggunas.id')
            ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakat')
            ->where('users.id','=',Auth::user()->id)
            ->get()->first();

        $jurusan = DB::table('users')->join('prodis','users.prodi','=','prodis.id')
                    ->where('users.id','=',Auth::user()->id)
                    ->select('prodis.kode as kodeprodi')   
                    ->get()
                    ->first();

            if($jurusan->kodeprodi=='DGD'){
                $jur="TEKNIK GRAFIKA DAN PENERBITAN";
            }elseif($jurusan->kodeprodi=='TKRD'){
                $jur="TEKNIK MESIN";
            }else{
                $jur="PARIWISATA";
            }



       
      

            $kaprodi = DB::table('users')
                ->join('prodis','users.prodi','=','prodis.id')
                ->where('users.prodi','=',Auth::user()->prodi)
                ->where('users.kategori','=',7)
                ->select('users.*', 'users.nomorInduk as nip1','users.nama as kaprodi1')
                ->get()->first();

        $pdf = PDF::loadview('hasilStudi.pdfTranskrip', compact('profil','nilai','idx','kaprodi','jur'));
        return $pdf->stream('Transkrip.pdf');
    }


     //HALAMAN DOSEN: MENAMPILKAN DROPDOWN MATKUL YG DIAMPU untuk menu UPLOAD NILAI
    public function unggahNilai()
    {

        $cek = FaseAkademik::where('status','=', 'aktif')->get()->first();
        $matkul = HasilStudi::pluck('idMatkul');
        if($cek->semester=='ganjil'){
             $data = Matkul::whereNotIn('matkuls.id',$matkul)
                ->join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                 ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                 ->where('paket_kuliahs.status','=','aktif')
                 ->where('matkuls.dosen1','=',Auth::user()->id)
                 ->select('matkuls.*','paket_kuliahs.komulatif','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
                 ->get();

                 $data1 = Matkul::whereNotIn('matkuls.id',$matkul)
                 ->join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                 ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                 ->where('paket_kuliahs.status','=','aktif')
                 ->where('matkuls.dosen2','=',Auth::user()->id)
                 ->select('matkuls.*','paket_kuliahs.komulatif','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
                 ->get(); 
                 $smt = 'ganjil';
        }
        else{
            $data = Matkul::whereNotIn('matkuls.id',$matkul)
                  ->join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                 ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                 ->where('paket_kuliahs.status','=','aktif')
                 ->where('matkuls.dosen1','=',Auth::user()->id)
                 ->select('matkuls.*','paket_kuliahs.komulatif','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
                 ->get();
            $data1 = Matkul::whereNotIn('matkuls.id',$matkul)
                ->join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                 ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                 ->where('paket_kuliahs.status','=','aktif')
                 ->where('matkuls.dosen2','=',Auth::user()->id)
                 ->select('matkuls.*','paket_kuliahs.komulatif','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
                 ->get();
                 $smt = 'genap';
        } 

       


        if(Carbon::now() < $cek->akhirKuliah){
           $upload = 'wait';
        }elseif($cek->faseEval > Carbon::now()){
           $upload = 'true';
        }else{
           $upload = 'false';
        }

        return view('halamanDosen.unggahNilai')
        ->with('unggahNilai1',$data1)
        ->with('unggahNilai',$data)
        ->with('smt',$smt)
        ->with('fase',$upload);
    }

    public function uploadNilai()
    {
       return view('halamanDosen.uploadNilai');
    }

    //HALAMAN DOSEN MENAMPILKAN DROPDOWN MATKUL YG DIAMPU untuk menu INFORMASI NILAI
    public function lihatNilai()
    {
        
        $cek = FaseAkademik::where('status','=', 'aktif')->get()->first();

        if($cek->semester=='ganjil'){
             $data = Matkul::join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                 ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                 ->where('paket_kuliahs.status','=','aktif')
                 ->where('matkuls.dosen1','=',Auth::user()->id)
                 ->select('matkuls.*','paket_kuliahs.komulatif')
                 ->get();

                 $data1 = Matkul::join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                 ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                 ->where('paket_kuliahs.status','=','aktif')
                 ->where('matkuls.dosen2','=',Auth::user()->id)
                 ->select('matkuls.*','paket_kuliahs.komulatif')
                 ->get(); 
                 $smt = 'ganjil';
        }
        else{
            $data = Matkul::join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                 ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                 ->where('paket_kuliahs.status','=','aktif')
                 ->where('matkuls.dosen1','=',Auth::user()->id)
                 ->select('matkuls.*','paket_kuliahs.komulatif')
                 ->get();
                 
            $data1 = Matkul::join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                 ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                 ->where('paket_kuliahs.status','=','aktif')
                 ->where('matkuls.dosen2','=',Auth::user()->id)
                 ->select('matkuls.*','paket_kuliahs.komulatif')
                 ->get();
                 $smt = 'genap';
        } 

        if($cek->faseEval > Carbon::now()){
           $upload = 'true';
        }else{
         $upload = 'false';
        }
        return view('halamanDosen.lihatNilai')
        ->with('unggahNilai1',$data1)
        ->with('unggahNilai',$data)
        ->with('smt',$smt)
        ->with('fase',$upload)       
;        
    }

    //HALAMAN DOSEN: MENAMPILKAN NILAI DALAM BENTUK TABEL untuk menu INFORMASI NILAI
    public function tabelNilai($id)
    {
        
         $data = DB::table('hasil_studis')
         ->join('users','users.id','=','hasil_studis.idUser')
         ->join('matkuls', 'hasil_studis.idMatkul','=', 'matkuls.id')
         ->where('hasil_studis.idMatkul',$id)
         ->select('hasil_studis.uts','hasil_studis.uas','hasil_studis.praktek','hasil_studis.tugas','users.nama','users.nomorInduk','hasil_studis.id as idhsm','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
         ->get()->toArray();
            return json_encode($data);
    }

    //HALAMAN DOSEN:cari nilai
     public function cari(Request $request)
    {
        
        $cek = FaseAkademik::where('status','=', 'aktif')->get()->first();

        if($cek->semester=='ganjil'){
             $data = Matkul::join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                 ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                 ->where('paket_kuliahs.status','=','aktif')
                 ->where('matkuls.dosen1','=',Auth::user()->id)
                 ->select('matkuls.*','paket_kuliahs.komulatif','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
                 ->get();

                 $data1 = Matkul::join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                 ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                 ->where('paket_kuliahs.status','=','aktif')
                 ->where('matkuls.dosen2','=',Auth::user()->id)
                 ->select('matkuls.*','paket_kuliahs.komulatif','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
                 ->get(); 
                 $smt = 'ganjil';
        }
        else{
            $data = Matkul::join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                 ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                 ->where('paket_kuliahs.status','=','aktif')
                 ->where('matkuls.dosen1','=',Auth::user()->id)
                 ->select('matkuls.*','paket_kuliahs.komulatif','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
                 ->get();
                 
            $data1 = Matkul::join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                 ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                 ->where('paket_kuliahs.status','=','aktif')
                 ->where('matkuls.dosen2','=',Auth::user()->id)
                 ->select('matkuls.*','paket_kuliahs.komulatif','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
                 ->get();
                 $smt = 'genap';
        } 


        if(is_numeric($request->cari)){
        $data3 = DB::table('hasil_studis')
         ->join('users','users.id','=','hasil_studis.idUser')
         ->join('matkuls','hasil_studis.idMatkul','=','matkuls.id')
         ->where('hasil_studis.idMatkul',$request->lihatNilai)
         ->where('users.nomorInduk','like','%'.$request->cari.'%')
         ->select('users.nomorInduk','hasil_studis.uts','hasil_studis.uas','hasil_studis.praktek','hasil_studis.tugas','users.nama','hasil_studis.idUser','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
         ->get();
        }else{
            $data3 = DB::table('hasil_studis')
         ->join('users','users.id','=','hasil_studis.idUser')
         ->join('matkuls','hasil_studis.idMatkul','=','matkuls.id')
         ->where('hasil_studis.idMatkul',$request->lihatNilai)
         ->where('users.nama','like','%'.$request->cari.'%')
         ->select('users.nomorInduk','hasil_studis.uts','hasil_studis.uas','hasil_studis.praktek','hasil_studis.tugas','users.nama','hasil_studis.idUser','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
         ->get();
        }

         $lihatNilai = DB::table('matkuls')
            ->where('matkuls.dosen1','=',Auth::user()->id)
            ->orWhere('matkuls.dosen2','=',Auth::user()->id) 
            ->get();
            
        return view('halamanDosen.lihatNilai')
        ->with('lihatNilai',$lihatNilai) 
        ->with('unggahNilai',$data) 
        ->with('unggahNilai1',$data1) 
        ->with('smt',$smt)  
        ->with('cari',$data3);   
    }

 
    //HALAMAN DOSEN: ubah nilai
    public function ubahNilai(Request $request,$id)
    {        
        //menampilkan form edit
        $lihatNilai = DB::table('hasil_studis')
        ->join('users','hasil_studis.idUser','=','users.id')
        ->select('users.nama','users.nomorInduk','hasil_studis.*')
        ->where('hasil_studis.id',$id)->get()->first();
        return view('halamanDosen.ubahNilai')->with('lihatNilai',$lihatNilai); 
    }


    //HALAMAN DOSEN: perbatui nilai
    public function perbaruiNilai(Request $request, $id)
    {
        //menyimpan data yg sudah di edit
      
        $lihatNilai= HasilStudi::find($id);
        $lihatNilai->uts = $request->uts;
        $lihatNilai->uas = $request->uas;
        $lihatNilai->praktek = $request->praktek;
        $lihatNilai->tugas = $request->tugas;
        $lihatNilai->save();

        Alert('Data Nilai Berhasil Diubah', 'Nilai Mahasiswa');
        return redirect('/halamanDosen/lihatNilai');
    }

    public function unggahNilaiA()
    {
        
        $cek = FaseAkademik::where('status','=', 'aktif')->get()->first();
        $matkul = Transkrip::join('users','transkrips.idUser','=','transkrips.id')->pluck('idMatkul','idUser');
        if($cek->semester=='ganjil'){
             $unggahNilai = Matkul::whereNotIn('matkuls.id',$matkul)
                ->join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
                ->join('prodis','matkuls.kodeprodi','=','prodis.id')
                ->where('fase_akademiks.status','=','aktif')
                ->select('matkuls.*','prodis.nama as namaprodi','matkuls.nama as namamatkul','matkuls.kode as kodematkul' )
                ->paginate(20);


            $unggahNilai1 = Matkul::whereNotIn('matkuls.id',$matkul)
                ->join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
                ->where('fase_akademiks.status','=','aktif')
                ->where('matkuls.kodeprodi', '=', 99)
                ->select('matkuls.*','matkuls.nama as namamatkul','matkuls.kode as kodematkul')
                ->paginate(20);

            $smt = 'ganjil';
        }
        else{
            $unggahNilai = Matkul::whereNotIn('matkuls.id',$matkul)->whereNotIn('users.id',$matkul)
                ->join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
                ->join('prodis','matkuls.kodeprodi','=','prodis.id')
                ->where('fase_akademiks.status','=','aktif')
                ->select('matkuls.*','prodis.nama as namaprodi','matkuls.nama as namamatkul','matkuls.kode as kodematkul' )
                ->paginate(20);

            $unggahNilai1 = Matkul::whereNotIn('matkuls.id',$matkul)
                ->join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
                ->join('paket_kuliahs','struktur_paket_kuliahs.idPaketKuliah','=','paket_kuliahs.id')
                ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
                ->where('fase_akademiks.status','=','aktif')
                ->where('matkuls.kodeprodi', '=', 99)
                ->select('matkuls.*','matkuls.nama as namamatkul','matkuls.kode as kodematkul')
                ->paginate(20);
           
            $smt = 'genap';
        } 

       
       
            
        return view('hasilStudi.nilaiMahasiswa')->with('unggahNilai',$unggahNilai)->with('unggahNilai1',$unggahNilai1)->with('smt',$smt);

            
      
    }

    public function tabelNilaiA($id)
    {
       
        // $cek = DB::table('fase_akademiks')
        //     ->join('paket_kuliahs','fase_akademiks.tahunAjaran','=','paket_kuliahs.id')
        //     ->where('fase_akademiks.status','=', 'aktif')
        //     ->where('paket_kuliahs.status','=', 'aktif')
        //     ->select('fase_akademiks.tahunAjaran')
        //     ->get()->first();


        $tabelNilaiA = DB::table('matkuls')
            ->join('struktur_paket_kuliahs','matkuls.id','=','struktur_paket_kuliahs.idMatkul')
            ->join('kartu_rencanas','struktur_paket_kuliahs.idPaketKuliah','=','kartu_rencanas.idPaketKuliah')
            ->join('users','kartu_rencanas.idUser','=','users.id')
            ->where('matkuls.id','=',$id)
            ->where('kartu_rencanas.status','=','aktif')
            ->select('users.id','users.nama', 'users.nomorInduk','matkuls.nama as namamatkul', 'kartu_rencanas.idUser')
            ->get();


         $coba = DB::table('matkuls')
            ->join('prodis','matkuls.kodeprodi','=','prodis.id')
            ->where('matkuls.id','=',$id)
            ->select('matkuls.nama as namamatkul', 'prodis.nama as namaprodi')
            ->get()->first();


        return view('hasilStudi.tabelNilai')
        ->with('coba',$coba)
        ->with('tabelNilaiA',$tabelNilaiA);
    }

    public function simpanNilai(ValidasiUnggahTranskrip $request,$id)
    {
        foreach ($request->nama as $key=>$iter){
            $nilaiAkhir = new Transkrip;
            $nilaiAkhir->idMatkul = $id;
            $nilaiAkhir->komulatif = $request->komulatif;
            $nilaiAkhir->idUser = $iter;
            $nilaiAkhir->nilaiAkhir = $request->nilaiAkhir[$key];
            $nilaiAkhir->save();
        }
        Alert('Data Nilai Berhasil Ditambah', 'Nilai Mahasiswa');
        return redirect('/hasilStudi/nilaiMahasiswa'); //disimpan lalu ditampilkan di file index
    }

    public function lihatMahasiswa(Request $request)
    {
        $user = DB::table('users')
            ->join('prodis','users.prodi', '=', 'prodis.id')
            ->join('kategori_penggunas','users.kategori', '=', 'kategori_penggunas.id')
            ->where('users.kategori','=','4')
            ->where('users.nomorInduk', 'like', "%{$request->q}%")
            ->select('users.*','users.kategori','users.status', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakategori', 'users.tahunMasuk')
            ->paginate(10);


        return view('hasilStudi.lihatMahasiswa')->with('user',$user);
    }

     public function cariMahasiswa(Request $request)
    {
    
        if(is_numeric($request->cari)){
        $data = DB::table('users')
         ->join('prodis','users.prodi', '=', 'prodis.id')
         ->join('kategori_penggunas','users.kategori', '=', 'kategori_penggunas.id')
         ->where('users.status','aktif')
         ->where('users.nomorInduk','like','%'.$request->cari.'%')
         ->select('users.*','users.kategori','users.status', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakategori', 'users.tahunMasuk')
         ->get();
        }else{
        $data = DB::table('users')
         ->join('prodis','users.prodi', '=', 'prodis.id')
         ->join('kategori_penggunas','users.kategori', '=', 'kategori_penggunas.id')
         ->where('users.status','aktif')
         ->where('users.nama','like','%'.$request->cari.'%')
         ->select('users.*','users.kategori','users.status', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakategori', 'users.tahunMasuk')
         ->get();
        }
            
        return view('hasilStudi.lihatMahasiswa')  
        ->with('user',$data);   
    }

    public function detailNilai($id)
    {
        //menampilkan detail nilai mahasiswa 
        $data = DB::table('transkrips')
            ->join('matkuls', 'transkrips.idMatkul', '=','matkuls.id')
            ->where('transkrips.idUser','=',$id)
            ->select('matkuls.*','transkrips.nilaiAkhir','matkuls.nama as namamatkul','matkuls.kode as kodematkul')
            ->get();


        $profil = DB::table('users')
          ->join('prodis','users.prodi','=','prodis.id')
          ->select('users.nama as namamhs','users.nomorInduk','prodis.nama as namaprodi')
          ->where('users.id','=',$id)
          ->get()->first();
    
        return view('hasilStudi.detailNilai')->with('data',$data)->with('profil',$profil);
    }



    

  
}

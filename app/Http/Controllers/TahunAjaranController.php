<?php

namespace App\Http\Controllers;

use App\TahunAjaran;
use Illuminate\Http\Request;

class TahunAjaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lihat()
    {
        //lihat fase akademik
        $lihatFase = TahunAjaran::paginate(10);
        return view('faseAkademik.lihatFase')->with('lihatFase',$lihatFase);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah()
    {
        //
        return view('faseAkademik.tambahFase');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpan(Request $request)
    {
        //
        $tahunAjaran = new TahunAjaran;
        $tahunAjaran->tahunAjaran = $request->tahunAjaran;
        $tahunAjaran->faseRegistrasi = $request->faseRegistrasi;
        $tahunAjaran->awalKuliah = $request->awalKuliah;
        $tahunAjaran->akhirKuliah = $request->akhirKuliah;
        $tahunAjaran->faseEval = $request->faseEval;
        $tahunAjaran->semester = $request->semester;
        $tahunAjaran->status = "tutup";
        
        $tahunAjaran->save();
        return redirect('/faseAkademik/lihatFase'); 
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\TahunAjaran  $tahunAjaran
     * @return \Illuminate\Http\Response
     */
    public function show(TahunAjaran $tahunAjaran)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TahunAjaran  $tahunAjaran
     * @return \Illuminate\Http\Response
     */
    public function ubah(TahunAjaran $tahunAjaran,$id)
    {
        //form ubah 
        $fase = TahunAjaran::find($id);
        return view('faseAkademik.ubahFase')->with('fase',$fase);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TahunAjaran  $tahunAjaran
     * @return \Illuminate\Http\Response
     */
    public function perbarui(Request $request, $id)
    {
        //menyimpan data yg sudah di edit
        $fase = TahunAjaran::find($id);
        $fase->tahunAjaran = $request->tahunAjaran;
        $fase->faseRegistrasi = $request->faseRegistrasi;
        $fase->awalKuliah = $request->awalKuliah;
        $fase->akhirKuliah = $request->akhirKuliah;
        $fase->faseEval = $request->faseEval;
        $fase->semester = $request->semester;

        $fase->save();
        return redirect('/faseAkademik/lihatFase');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TahunAjaran  $tahunAjaran
     * @return \Illuminate\Http\Response
     */
    public function destroy(TahunAjaran $tahunAjaran)
    {
        //
    }
}

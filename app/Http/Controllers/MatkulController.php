<?php

namespace App\Http\Controllers;
use Auth;
use DB;
use App\Prodi;
use App\User;
use App\Matkul;
use App\HasilStudi;
use App\TahunAjaran;
use App\FaseAkademik;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Http\Requests\ValidasiMatkul;
use App\Http\Requests\ValidasiUbahMatkul;
use Alert;

class MatkulController extends Controller
{
    
   
   

    public function lihat(Request $request)
    {
        //menampilkan data mata kuliah dalam bentuk tabel
      

        $matkul = Matkul::join('prodis','matkuls.kodeprodi', '=', 'prodis.id')
            ->join('users','matkuls.dosen1', '=', 'users.id' )
            ->select('matkuls.id','matkuls.nama','matkuls.kode','matkuls.komulatif','matkuls.sks','matkuls.SKSteori','matkuls.SKSpraktek', 'prodis.kode as kodeprodi', 'users.nama as dosen1','users.nama as dosen2','matkuls.status','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
            ->where('matkuls.kode', 'like', "%{$request->q}%")
            ->orWhere('matkuls.nama', 'like', "%{$request->q}%")
            ->paginate(10);
    
        $matkul->appends($request->only('q'));

        $matkulumum = Matkul::join('users','matkuls.dosen1', '=', 'users.id' )
            ->where('matkuls.kodeprodi', '=', 99)
            ->select('matkuls.id','matkuls.nama','matkuls.kode','matkuls.komulatif','matkuls.sks','matkuls.SKSteori','matkuls.SKSpraktek', 'users.nama as dosen1','users.nama as dosen2','matkuls.status','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
            ->where('matkuls.kode', 'like', "%{$request->s}%")
            ->Where('matkuls.nama', 'like', "%{$request->s}%")
            ->paginate(10);

        $matkulumum->appends($request->only('s'));
            
            return view('matkul.lihat')
            ->with('matkul',$matkul)
            ->with('matkulumum',$matkulumum);
    }

    public function detail($id)
    {
        //menampilkan detail user 
        $cek=matkul::find($id);
        if($cek->kodeprodi==99){
             $data = Matkul::join('users','matkuls.dosen1', '=', 'users.id' )
             ->select('matkuls.*','matkuls.nama','matkuls.kode','matkuls.komulatif','matkuls.sks','matkuls.SKSteori','matkuls.SKSpraktek',  'users.nama as dosen1','users.nama as dosen2','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
             ->where('matkuls.id','=',$id)
             ->get(); 

             $data1 = Matkul::join('users','matkuls.dosen2', '=', 'users.id' )
             ->select('matkuls.*','matkuls.nama','matkuls.kode','matkuls.komulatif','matkuls.sks','matkuls.SKSteori','matkuls.SKSpraktek',  'users.nama as dosen1','users.nama as dosen2','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
             ->where('matkuls.id','=',$id)
             ->get(); 
        }
        else{
            $data = Matkul::join('prodis','matkuls.kodeprodi', '=', 'prodis.id')
            ->join('users','matkuls.dosen1','=', 'users.id' )
            ->select('matkuls.*','matkuls.nama','matkuls.kode','matkuls.komulatif','matkuls.sks','matkuls.SKSteori','matkuls.SKSpraktek', 'prodis.nama as namaprodi', 'users.nama as dosen1','users.nama as dosen2','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
            ->where('matkuls.id','=',$id)
            ->get();

            $data1 = Matkul::join('prodis','matkuls.kodeprodi', '=', 'prodis.id')
            ->join('users','matkuls.dosen2','=', 'users.id' )
            ->select('matkuls.*','matkuls.nama','matkuls.kode','matkuls.komulatif','matkuls.sks','matkuls.SKSteori','matkuls.SKSpraktek', 'prodis.nama as namaprodi', 'users.nama as dosen1','users.nama as dosen2','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
            ->where('matkuls.id','=',$id)
            ->get();


        } 
    
        return view('matkul.detail')->with('data',$data)->with('data1',$data1)->with('cek',$cek);
    }

    public function tambah()
    {
        //menampilkan form add
        $prodi = Prodi::where('status','aktif')->get();
        return view('matkul.tambah')
        ->with('prodi',$prodi);
    }

    //MENAMPILKAN DROPDOWN DOSEN1
    public function getdosen($id)
    {
        if($id == "99"){
            $dosen1 = DB::table('users')->where('kategori','3')->orWhere('kategori','7')->where('status','aktif')->pluck('nama','id');            
        }else{

            $dosen1 = DB::table('users')->where('prodi',$id)->where('kategori','3')->orWhere('kategori','7')->where('status','aktif')->pluck('nama','id'); // listia
        }
            return json_encode($dosen1);

    }
     //MENAMPILKAN DROPDOWN DOSEN2
    public function getdosen2($id)
    {
        if($id == "99"){
            $dosen2 = DB::table('users')->where('kategori','3')->orWhere('kategori','7')->where('status','aktif')->pluck('nama','id');
        }else{
            $dosen2 = DB::table('users')->where('prodi',$id)->where('kategori','3')->orWhere('kategori','7')->where('status','aktif')->pluck('nama','id');


        } //wardhani
            return json_encode($dosen2);
    }
   
    public function simpan(ValidasiMatkul $request)
    {
        //menyimpan data matkul ke database
        $kode = Prodi::find($request->prodi);
        $jumlah = Matkul::where('kodeprodi',$request->prodi)->where('komulatif',$request->komulatif)->count();
        $urutan = $jumlah+1;

        if($urutan<10){
            $urutan = '0'.$urutan;
        }else{
            $urutan = $urutan;
        }

        if($request->komulatif==1){
            $komulatif = '1';
        }elseif ($request->komulatif==2) {
            $komulatif = '2';
        }elseif ($request->komulatif==3) {
            $komulatif = '3';
        }elseif ($request->komulatif==4) {
            $komulatif = '4';
        }

        

        $matkul = new Matkul;
        $matkul->kodeprodi = $request->prodi;
        //urutan kode matkul
        if($request->prodi=='99'){
            $matkul->kode = 'AKND'   .$komulatif.'1'   .$urutan;
        }
        else{
            $matkul->kode = $kode->kode.$komulatif.'1'.$urutan;
        }
        $matkul->nama = $request->nama;
        $matkul->SKS = $request->SKS;
        $matkul->SKSteori = $request->SKSteori;
        $matkul->SKSpraktek = $request->SKSpraktek;
        $matkul->komulatif = $request->komulatif;
        $matkul->jpm = $request->jpm;
        $matkul->dosen1 = $request->dosen1;
        $matkul->dosen2 = $request->dosen2;
        $matkul->status = "tidak";

        $matkul->save();

        if($request->prodi=='99'){
    }else{
        $update = Prodi::find($request->prodi);
        $update->status="tidak";
        $update->save();
    }

        Alert('Data Matkul Berhasil Ditambah', 'Mata Kuliah');
        return redirect('/matkul/lihat'); 
    }
  
    public function ubah(Matkul $matkul, $id)
    {
        //menampilkan form yg aka di edit sesuai id yg telah dipilih

        $cek=Matkul::find($id);
        $cek1=Prodi::find($cek->kodeprodi);

        if($cek->kodeprodi=='99'){
             $data = Matkul::join('users','matkuls.dosen1', '=', 'users.id' )
                 ->select('matkuls.*','matkuls.nama','matkuls.kode','matkuls.komulatif','matkuls.sks','matkuls.SKSteori','matkuls.SKSpraktek',  'users.nama as dosen1','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
                 ->where('matkuls.id','=',$id)
                 ->where('users.status','=','aktif')
                 ->get();

            $data1 = Matkul::join('users','matkuls.dosen2', '=', 'users.id' )
                 ->select('users.*','matkuls.*','matkuls.nama','matkuls.kode','matkuls.komulatif','matkuls.sks','matkuls.SKSteori','matkuls.SKSpraktek',  'users.nama as namadosen2','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
                 ->where('matkuls.id','=',$id)
                 ->where('users.status','=','aktif')
                 ->get(); 


        }
        else{
            $data = Matkul::join('prodis','matkuls.kodeprodi', '=', 'prodis.id')
                ->join('users','matkuls.dosen1', '=', 'users.id' )
                ->select('matkuls.*','matkuls.nama','matkuls.kode','matkuls.komulatif','matkuls.sks','matkuls.SKSteori','matkuls.SKSpraktek', 'prodis.nama as namaprodi', 'users.nama as dosen1','users.nama as dosen2','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
                ->where('matkuls.id','=',$id)
                ->where('users.status','=','aktif')
                ->get();
        } 

        $prodi = Prodi::where('status','aktif')->get();
        return view('matkul.ubah')->with('matkul',$matkul)
        ->with('prodi',$prodi)->with('cek',$cek)->with('cek1',$cek1);
    }

    public function perbarui(Request $request, $id)
    {
        //menyimpan data yang telah diedit
        $matkul = Matkul::find($id);
        $matkul->kodeprodi = $request->prodi;
        $matkul->nama = $request->nama;
        $matkul->SKS = $request->SKS;
        $matkul->SKSteori = $request->SKSteori;
        $matkul->SKSpraktek = $request->SKSpraktek;
        $matkul->dosen1 = $request->dosen1;
        $matkul->dosen2 = $request->dosen2;
        $matkul->bobotUts = "30";
        $matkul->bobotUas = "30";
        $matkul->bobotTugas = "30";
        $matkul->bobotPraktek = "30";

        $matkul->save();

        Alert('Data Matkul Berhasil Diubah', 'Mata Kuliah');
        return redirect('/matkul/lihat'); 

    }

    public function hapus($id)
    {
        //menghapus data sesuai data
        $matkul = Matkul::findOrFail($id);
        $matkul->delete();

        Alert('Data Matkul Berhasil Dihapus', 'Mata Kuliah');
        return response()->json($matkul);
    }

    public function status(Request $request, $id)
    {
        //

        $matkul = Matkul::find($id);
        $matkul->status = "aktif";

        $matkul->save();
        return redirect('/matkul/lihat');
    }



   

    public function lihatNilaiA()
    {
        $lihatNilai = DB::table('matkuls')
           ->where('matkuls.status','=','aktif')
           ->get();
            
        return view('halamanAdmin.infoNilai')
        ->with('lihatNilai',$lihatNilai);        
    }

    public function tabelNilaiA($id)
    {
        
         $data = DB::table('hasil_studis')
         ->join('users','users.id','=','hasil_studis.idUser')
         ->join('matkuls', 'hasil_studis.idMatkul','=', 'matkuls.id')
         ->where('hasil_studis.idMatkul',$id)
         ->select('hasil_studis.uts','hasil_studis.uas','hasil_studis.praktek','hasil_studis.tugas','users.nama','users.nomorInduk','hasil_studis.id as idhsm','matkuls.bobotUts','matkuls.bobotUas','matkuls.bobotTugas','matkuls.bobotPraktek')
         ->get()->toArray();
            return json_encode($data);
    }

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use App\User;
use App\Matkul;
use App\Prodi;
use App\HasilStudi;
use App\KartuRencana;
use App\TahunAjaran;
use App\FaseAkademik;
Use App\PaketKuliah;
Use App\Transkrip;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Alert('Selamat Datang', 'SIAPEN');
        // return view('home');

        // if(Auth::user()->id==1)
        // {
        //     return redirect('/home');
        // }else{
        //     return redirect('/welcome');
        // } 
            
    }

    public function dashboard()
    {
        $mahasiswa =  User::where('kategori',4)->get();
    
        $prodi =  Prodi::where('status','aktif')->count();
        $matkul =  Matkul::where('status','aktif')->count();
        $mhs =  User::where('status','aktif')->where('kategori',4)->count();
        $dosen =  User::where('status','aktif')->where('kategori',3)->count();

        $ipkcumlaude = 0;
        $ipkdibawah = 0;
       
        foreach ($mahasiswa as $key) {
                $trans = Transkrip::join('matkuls','matkuls.id','=','transkrips.idMatkul')
                ->where('transkrips.idUser',$key->id)->get();
        $ipk = 0; 
        $nxk = 0; 
        $sks = 0;     
                foreach ($trans as $value) {
                        if($value->nilaiAkhir === "A"){
                            $nilai = 4;       
                        }elseif($value->nilaiAkhir === "A-"){
                            $nilai = 3.7;
                        }elseif ($value->nilaiAkhir === "B+") {
                            $nilai = 3.3;
                        }elseif ($value->nilaiAkhir === "B") {
                            $nilai = 3.0;
                        }elseif ($value->nilaiAkhir === "B-") {
                            $nilai = 2.7;
                        }elseif ($value->nilaiAkhir === "C+") {
                            $nilai = 2.5;
                        }elseif ($value->nilaiAkhir === "C") {
                            $nilai = 2.0;
                        }elseif ($value->nilaiAkhir === "D") {
                            $nilai = 1.0;
                        }else {
                            $nilai = 0;
                        }
                        $sks = $sks + $value->SKS;
                        $nxk= $nxk + ($nilai * $value->SKS);

                } 
             if($sks != 0){
                 $ipk=$nxk / $sks;
                if($ipk >= 3.51 ){
                        $ipkcumlaude = $ipkcumlaude+1;
                }elseif($ipk < 2){
                        $ipkdibawah= $ipkdibawah+1;
                }
             }
        }

        // $ipk =Transkrip::join('matkuls', 'transkrips.idmatkul', '=','matkuls.id')
        //     ->where('transkrips.idUser')
        //     ->get();

        //     if ($ipk>3.5){
        //         count();
        //     }



           
        

        return view('halamanAdmin/dashboard')
        ->with('prodi',$prodi)
        ->with('matkul',$matkul)
        ->with('dosen',$dosen)
        ->with('mhs',$mhs)
        ->with('ipk',$ipkcumlaude)
        ->with('ipkdibawah',$ipkdibawah);
          
    }

}

<?php

namespace App\Http\Controllers;

use App\FaseAkademik;
use Illuminate\Http\Request;
use App\Http\Requests\ValidasiFaseAkademik;
use App\Http\Requests\ValidasiUbahFaseAkademik;
use Alert;

class FaseAkademikController extends Controller
{
    public function lihat()
    {

        
        $lihat = FaseAkademik::paginate(10);
        return view('faseAkademik.lihat')->with('lihat',$lihat);
    }

    public function lihatA()
    {
        $lihat = FaseAkademik::paginate(10);
        return view('halamanAdmin.faseAkademik')->with('lihat',$lihat);
    }

    public function tambah()
    {
        return view('faseAkademik.tambah');
    }

    public function simpan(ValidasiFaseAkademik $request)
    {
        $fase = new FaseAkademik;
        $fase->tahunAjaran = $request->tahunAjaran;
        $fase->faseRegistrasi = $request->faseRegistrasi;
        $fase->awalKuliah = $request->awalKuliah;
        $fase->akhirKuliah = $request->akhirKuliah;
        $fase->faseEval = $request->faseEval;
        $fase->semester = $request->semester;
        $fase->status = "tidak";
        
        $fase->save();

        Alert('Data Fase Akademik Berhasil Ditambah', 'Fase Akademik');
        return redirect('/faseAkademik/lihat'); 
    }
    
    public function ubah(FaseAkademik $faseAkademik,$id)
    {
        $fase = FaseAkademik::find($id);
        return view('faseAkademik.ubah')->with('fase',$fase);
    }

    public function perbarui(ValidasiUbahFaseAkademik $request, $id)
    {
        $fase = FaseAkademik::find($id);
        $fase->tahunAjaran = $request->tahunAjaran;
        $fase->faseRegistrasi = $request->faseRegistrasi;
        $fase->awalKuliah = $request->awalKuliah;
        $fase->akhirKuliah = $request->akhirKuliah;
        $fase->faseEval = $request->faseEval;
        $fase->semester = $request->semester;

        $fase->save();

        Alert('Data Fase Akademik Berhasil Diubah', 'Fase Akademik');
        return redirect('/faseAkademik/lihat');
    }

    public function hapus($id)
    {
        $hapus = FaseAkademik::findOrFail($id);
        $hapus->delete();

        Alert('Data Fase Akademik Berhasil Dihapus', 'Fase Akademik');
        return redirect('/faseAkademik/lihat');
    }

    public function status(Request $request, $id)
    {
        $fase = FaseAkademik::find($id);
        $fase->status = "aktif";
        
        $fase->save();
        return redirect('/faseAkademik/lihat');
    }
}
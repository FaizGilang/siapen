<?php

namespace App\Http\Controllers;

use App\KategoriPengguna;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\User;
use App\Http\Requests\ValidasiKategoriPengguna;
use App\Http\Requests\ValidasiUbahKategoriPengguna;
use Alert;

class KategoriPenggunaController extends Controller
{
    public function lihat()
    {
        $kategori = KategoriPengguna::all();
        return view('kategoriPengguna.lihat')
        ->with('kategori',$kategori);
    }
    public function tambah()
    {
        return view('kategoriPengguna.tambah');
    }

    public function simpan(ValidasiKategoriPengguna $request)
    {      
        $kategori = new KategoriPengguna;
        $kategori->nama = $request->nama;
        $kategori->status = "tidak";

        $kategori->save();

        Alert('Data Kategori Pengguna Berhasil Ditambah', 'Kategori Pengguna');
        return redirect('/kategoriPengguna/lihat');
    }

    public function ubah(KategoriPengguna $kategoriPengguna, $id)
    {
        $kategori = KategoriPengguna::find($id);
        return view('kategoriPengguna.ubah')->with('kategoriPengguna',$kategori);
    }

    public function perbarui(ValidasiUbahKategoriPengguna $request, $id)
    {
        $kategori = KategoriPengguna::find($id);
        $kategori->nama = $request->nama;
        
        $kategori->save();
        Alert('Data Kategori Pengguna Berhasil Diubah', 'Kategori Pengguna');
        return redirect('/kategoriPengguna/lihat');
    }

    public function status(Request $request, $id)
    {
        $kategori = KategoriPengguna::find($id);
        $kategori->status = "aktif";
        
        $kategori->save();
        return redirect('/kategoriPengguna/lihat');
    }

    public function hapus($id)
    {
        $kategori = KategoriPengguna::findOrFail($id);
        $kategori->delete();

        Alert('Data Kategori Pengguna Berhasil Dihapus', 'Kategori Pengguna');
     
        return response()->json($kategori);
    }


    

}

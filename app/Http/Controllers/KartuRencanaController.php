<?php

namespace App\Http\Controllers;
use Auth;
Use DB;
use App\Matkul;
Use App\Prodi;
Use App\User;
Use App\KartuRencana;
Use App\PaketKuliah;
Use App\StrukturPaketKuliah;
Use App\TahunAjaran;
Use App\HasilStudi;
Use App\FaseAkademik;
Use App\Transkrip;
use Illuminate\Http\Request;
use Carbon\Carbon;
use PDF;
use Alert;

class KartuRencanaController extends Controller
{
 
    public function lihatKRS()
    {
        $cek = FaseAkademik::where('status','=', 'aktif')->get()->first();
        $time=Carbon::now();
        $year=$time->year;
        $data = DB::table('users')->where('users.id','=',Auth::user()->id)->select('users.tahunMasuk')->first();
       
        $tahunAjaran=DB::table('fase_akademiks')->where('status','aktif')->get()->first();  
        if($tahunAjaran->semester=='ganjil'){ 
            if(($year-$data->tahunMasuk) == '0'){ //ganjil: 2018-2018 = 0
              $semester = 1;
            }elseif(($year-$data->tahunMasuk)== '1'){ //ganjil: 2018-2017 = 1
              $semester = 3;   
            }else{ //genap: 2019-2017 = 2
              $semester ='lulus';
            }
        }else{
            if(($year-$data->tahunMasuk) == '1'){ //genap: 2019-2018 = 1
              $semester = 2;
            }elseif(($year-$data->tahunMasuk)== '2'){ //genap: 2019-2017 = 2
              $semester = 4;
            }else{ //genap: 2019-2017 = 2
              $semester ='lulus';
            }

        }



        
      
         $kartuRencana = DB::table('kartu_rencanas')
            ->join('paket_kuliahs', 'kartu_rencanas.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('struktur_paket_kuliahs', 'struktur_paket_kuliahs.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('matkuls', 'struktur_paket_kuliahs.idMatkul', '=', 'matkuls.id')
            ->join('users', 'matkuls.dosen1', '=', 'users.id')
            ->where('kartu_rencanas.idUser','=',Auth::user()->id)
            ->where('kartu_rencanas.status','=','aktif')
            ->where('paket_kuliahs.komulatif','=',$semester)
            ->select('matkuls.nama as namamatkul','matkuls.kode as kodematkul', 'matkuls.SKS', 'matkuls.SKSteori', 'matkuls.SKSpraktek','matkuls.jpm', 'users.nama as dosen')
            ->get();

            

        $profil = DB::table('users')
            ->join('prodis', 'users.prodi', '=', 'prodis.id')
            ->join('kategori_penggunas', 'users.kategori', '=', 'kategori_penggunas.id')
            ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakat')
            ->where('users.id','=',Auth::user()->id)
            ->get()->first();

         $semt = DB::table('fase_akademiks')
                ->where('fase_akademiks.status','=','aktif')
                ->select('fase_akademiks.semester as semt','fase_akademiks.tahunAjaran as tahun')
                ->get()->first();

        if(Carbon::now() > $cek->faseRegistrasi){
           $lihat = 'true';
        }else{
           $lihat = 'false';
        }

        if($semester=='lulus'){
            $lulus = 'lulus';
        }else{
            $lulus = 'aktif';
        }


            return view('kartuRencana.lihatKRS')
                ->with('kartuRencana',$kartuRencana)
                ->with('profil',$profil)
                ->with('semester',$semester)
                ->with('semt',$semt)
                ->with('lulus',$lulus)
                ->with('fase',$lihat);
    }

    public function lihatPaket()
    {
        $lihatPaket = DB::table('paket_kuliahs')
                ->join('prodis','paket_kuliahs.idProdi','=','prodis.id')
                ->join('kurikulums', 'paket_kuliahs.idKurikulum', '=', 'kurikulums.id')
                ->select('paket_kuliahs.*','prodis.nama as namaprodi',  'kurikulums.tahun')
                ->get();

              

        return view('kartuRencana.lihatPaket')->with('lihatPaket',$lihatPaket);


    }

    public function bukaKRS()
    {
        $data = DB::table('fase_akademiks')->latest()->first();


        return view('KartuRencana.bukaKRS')->with('status',$data);
    }

    public function ubahKRS()
    {

        $transkrip = DB::table('hasil_studis')->join('matkuls','hasil_studis.idMatkul','=','matkuls.id')->get();
        foreach ($transkrip as $data) {
        $time=Carbon::now();
        $year=$time->year;
        $data1 = DB::table('users')->where('users.id','=',$data->idUser)->select('users.tahunMasuk')->first();
       
        $tahunAjaran=DB::table('fase_akademiks')->latest()->first();

        if($tahunAjaran->semester=='ganjil'){ 
            if(($year-$data1->tahunMasuk) == '0'){ //ganjil: 2019-2019 = 0
              $semester = 1;
            }elseif(($year-$data1->tahunMasuk)== '1'){ //ganjil: 2019-2018 = 1
              $semester = 3;   
            }
        }else{
            if(($year-$data1->tahunMasuk) == '1'){ //genap: 2019-2018 = 1
              $semester = 2;
            }elseif(($year-$data1->tahunMasuk)== '2'){ //genap: 2019-2017 = 2
              $semester = 4;
            }
        }
            //fungsi unuk pindah nilai hasil_studis ke transkrip
            $move = new Transkrip; 
            $move->idUser = $data->idUser;
            $move->idMatkul = $data->idMatkul;

            $nilaiakhir = (($data->uts)*($data->bobotUts/100)) + (($data->uas)*($data->bobotUas/100)) + (($data->tugas)*($data->bobotTugas/100)) + (($data->praktek)*($data->bobotPraktek/100));



            if($nilaiakhir>=79.6 && $nilaiakhir<=100) {
                $nilaihuruf= "A";
            }
            elseif($nilaiakhir>=75.6 && $nilaiakhir<=79.5){
                $nilaihuruf= "A-";
            }
            elseif($nilaiakhir>=71.6 && $nilaiakhir<=75.5){
                $nilaihuruf= "B+";
            }
            elseif($nilaiakhir>=67.6 && $nilaiakhir<=71.5){
                $nilaihuruf= "B";
            }
            elseif($nilaiakhir>=63.6 && $nilaiakhir<=67.5){
                $nilaihuruf= "B-";
            }
            elseif($nilaiakhir>=59.6 && $nilaiakhir<=63.5){
                $nilaihuruf="C+";
            }
            elseif($nilaiakhir>=55.6 && $nilaiakhir<=59.5){
                $nilaihuruf ="C";
            }
            elseif($nilaiakhir>=40.6 && $nilaiakhir<=55.5){
                $nilaihuruf= "D";
            }
            elseif($nilaiakhir>=0 && $nilaiakhir<=40.5){
                $nilaihuruf="E";
            }
            elseif($nilaiakhir<=0){
                $nilaihuruf= "-";
            }
            elseif($nilaiakhir>100){
                $nilaihuruf="-";
            }
            
            $move->komulatif = $semester-1;
            $move->nilaiAkhir = $nilaihuruf;
            $move->save();
        }
           
        //query semua mhs krs
        $tutup = KartuRencana::all();
        foreach ($tutup as $status) {
            $close = KartuRencana::find($status->id);
            $close->status = "tidak";  
            $close->save();
        }

        $tutup = FaseAkademik::all();
        foreach ($tutup as $status) {
            $close = FaseAkademik::find($status->id);
            $close->status = "tidak";  
            $close->save();
        }
        
        $semester=DB::table('fase_akademiks')->latest()->first();
        $time=Carbon::now();
        $year=$time->year;
        $prodi= Prodi::all();
        if($semester->semester=='ganjil'){
        foreach ($prodi as $key ) {
            $check1 = DB::table('paket_kuliahs')
                    ->join('kurikulums', 'paket_kuliahs.idKurikulum', '=', 'kurikulums.id')
                    ->join('prodis','kurikulums.idProdi','=','prodis.id')
                    ->where('kurikulums.idProdi',$key->id)
                    ->where('paket_kuliahs.status','aktif')
                    ->where('komulatif','=',1)
                    ->count();
            if($check1 < 1)
            {
                return redirect('/home');
            }
            $check3 = DB::table('paket_kuliahs')
                    ->join('kurikulums', 'paket_kuliahs.idKurikulum', '=', 'kurikulums.id')
                    ->join('prodis','kurikulums.idProdi','=','prodis.id')
                    ->where('kurikulums.idProdi',$key->id)
                    ->where('paket_kuliahs.status','aktif')
                    ->where('komulatif','=',3)
                    ->count();
            if($check3 < 1)
            {
                return redirect('/home');
            }
         } 

        }else{
        foreach ($prodi as $key) {
            $check2  = DB::table('paket_kuliahs')
                    ->join('kurikulums', 'paket_kuliahs.idKurikulum', '=', 'kurikulums.id')
                    ->join('prodis','kurikulums.idProdi','=','prodis.id')
                    ->where('kurikulums.idProdi',$key->id)
                    ->where('paket_kuliahs.status','aktif')
                    ->where('komulatif','=',2)
                    ->count();
            if($check2 < 1)
            {
                return redirect('/home');
            }
            $check4 = DB::table('paket_kuliahs')
                    ->join('kurikulums', 'paket_kuliahs.idKurikulum', '=', 'kurikulums.id')
                    ->join('prodis','kurikulums.idProdi','=','prodis.id')
                    ->where('kurikulums.idProdi',$key->id)
                    ->where('paket_kuliahs.status','aktif')
                    ->where('komulatif','=',4)
                    ->count();
            if($check4 < 1)
            {
                return redirect('/home');
            }
        
        }

            
        }

        $mahasiswa = DB::table('users')->where('users.kategori','=','4')->select('users.*')->get();
        //kode non aktif KRS 4BARIS cari semua krs, foreach semua krs, ganti status, save
        foreach ($mahasiswa as $data) {
            # code... 
            if($semester->semester=='ganjil'){ 
            if(($year-$data->tahunMasuk) == '0'){ //ganjil: 2018-2018 = 0

                $krs = new KartuRencana;
                $paket=DB::table('paket_kuliahs')
                    ->join('kurikulums', 'paket_kuliahs.idKurikulum', '=', 'kurikulums.id')
                    ->join('prodis','kurikulums.idProdi','=','prodis.id')
                    ->where('idProdi','=', $data->prodi)
                    ->where('komulatif','=',1)
                    ->where('paket_kuliahs.status','=','aktif')
                    ->select('paket_kuliahs.id')
                    ->get()->first();
                $krs->idUser = $data->id;   
                $krs->idPaketKuliah =  $paket->id;   
                $krs->status = "aktif";  
                $krs->izin =0;  
                $krs->alpa =0;  
                $krs->save();

            }elseif(($year-$data->tahunMasuk)== '1'){ //ganjil: 2018-2017 = 1

                $krs = new KartuRencana;
                $paket=DB::table('paket_kuliahs')
                    ->join('kurikulums', 'paket_kuliahs.idKurikulum', '=', 'kurikulums.id')
                    ->join('prodis','kurikulums.idProdi','=','prodis.id')
                    ->where('idProdi','=', $data->prodi)
                    ->where('komulatif','=',3)
                    ->where('paket_kuliahs.status','=','aktif')
                    ->select('paket_kuliahs.id')
                    ->get()->first();
                $krs->idUser = $data->id;   
                $krs->idPaketKuliah =  $paket->id;   
                $krs->status = "aktif";  
                $krs->izin = 0;  
                $krs->alpa = 0; 
                $krs->save();

            }
        }else{
            if(($year-$data->tahunMasuk) == '1'){ //genap: 2019-2018 = 1

                $krs = new KartuRencana;
                $paket=DB::table('paket_kuliahs')
                    ->join('kurikulums', 'paket_kuliahs.idKurikulum', '=', 'kurikulums.id')
                    ->join('prodis','kurikulums.idProdi','=','prodis.id')
                    ->where('idProdi','=', $data->prodi)
                    ->where('komulatif','=',2)
                    ->where('paket_kuliahs.status','=','aktif')
                    ->select('paket_kuliahs.id')
                    ->get()->first();
                $krs->idUser = $data->id;   
                $krs->idPaketKuliah =  $paket->id;   
                $krs->status = "aktif";
                $krs->izin = 0;  
                $krs->alpa = 0; 
                $krs->save();

            }elseif(($year-$data->tahunMasuk)== '2'){ //genap: 2019-2017 = 2

                $krs = new KartuRencana;
                $paket=DB::table('paket_kuliahs')
                    ->join('kurikulums', 'paket_kuliahs.idKurikulum', '=', 'kurikulums.id')
                    ->join('prodis','kurikulums.idProdi','=','prodis.id')
                    ->where('idProdi','=', $data->prodi)
                    ->where('komulatif','=',4)
                    ->where('paket_kuliahs.status','=','aktif')
                    ->select('paket_kuliahs.id')
                    ->get()->first();
                $krs->idUser = $data->id;   
                $krs->idPaketKuliah =  $paket->id;   
                $krs->status = "aktif";  
                $krs->izin = 0;  
                $krs->alpa = 0; 
                $krs->save();

            }
        }
        $alumni = KartuRencana::where('idUser', $data->id)->where('status','tidak')->count();
        if($alumni >= 4){
            $help = User::find($data->id);
            $help->status = 'tidak';
            $help->save();
        }
        }

        $delete = HasilStudi::all();
        foreach ($delete as $key) {
           $del = HasilStudi::find($key->id);
           $del->delete();

        }

         $data = DB::table('fase_akademiks')->latest()->limit(1)->update(['status' => 'aktif']);

         // $lulus = User::where('users.kategori','=',4)->where('users.status','=','aktif')->get();

         // foreach ($mhslulus as $key => $value) {
         //     # code...
         //    $data = KartuRencana::find($key->id);
         //    $data->status = "tidak";
         //    $data->save();


         // }

        return redirect('/kartuRencana/bukaKRS');
    }



    public function pdfKRS()
    {
        $time=Carbon::now();
        $year=$time->year;
        $data = DB::table('users')->where('users.id','=',Auth::user()->id)->select('users.tahunMasuk')->first();
       
        $tahunAjaran=DB::table('fase_akademiks')->where('status','aktif')->get()->first();

        if($tahunAjaran->semester=='ganjil'){ 
            if(($year-$data->tahunMasuk) == '0'){ //ganjil: 2018-2018 = 0
              $semester = 1;
            }elseif(($year-$data->tahunMasuk)== '1'){ //ganjil: 2018-2017 = 1
              $semester = 3;   
            }
        }else{
            if(($year-$data->tahunMasuk) == '1'){ //genap: 2019-2018 = 1
              $semester = 2;
            }elseif(($year-$data->tahunMasuk)== '2'){ //genap: 2019-2017 = 2
              $semester = 4;
            }
        }
        
        
        $kartuRencana = DB::table('kartu_rencanas')
            ->join('paket_kuliahs', 'kartu_rencanas.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('struktur_paket_kuliahs', 'struktur_paket_kuliahs.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('matkuls', 'struktur_paket_kuliahs.idMatkul', '=', 'matkuls.id')
            ->join('users', 'matkuls.dosen1', '=', 'users.id')
            ->where('kartu_rencanas.idUser','=',Auth::user()->id)
            ->where('kartu_rencanas.status','=','aktif')
            ->where('paket_kuliahs.komulatif','=',$semester)
            ->select('matkuls.nama as namamatkul','matkuls.kode as kodematkul', 'matkuls.SKS', 'matkuls.SKSteori', 'matkuls.SKSpraktek','matkuls.jpm', 'users.nama as dosen')
            ->get();

        $profil = DB::table('users')
            ->join('prodis', 'users.prodi', '=', 'prodis.id')
            ->join('kategori_penggunas', 'users.kategori', '=', 'kategori_penggunas.id')
            ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakat')
            ->where('users.id','=',Auth::user()->id)
             ->get()->first();

        $jurusan = DB::table('users')->join('prodis','users.prodi','=','prodis.id')
                    ->where('users.id','=',Auth::user()->id)
                    ->select('prodis.kode as kodeprodi')   
                    ->get()
                    ->first();

         if($jurusan->kodeprodi=='DGD'){
                $jur="TEKNIK GRAFIKA DAN PENERBITAN";
            }elseif($jurusan->kodeprodi=='TKRD'){
                $jur="TEKNIK MESIN";
            }else{
                $jur="PARIWISATA";
            }

        $semt = DB::table('fase_akademiks')
                ->where('fase_akademiks.status','=','aktif')
                ->select('fase_akademiks.semester as semt','fase_akademiks.tahunAjaran as tahun')
                ->get()->first();

       

            $kaprodi = DB::table('users')
                ->join('prodis','users.prodi','=','prodis.id')
                ->where('users.prodi','=',Auth::user()->prodi)
                ->where('users.kategori','=',7)
                ->select('users.*', 'users.nomorInduk as nip1','users.nama as kaprodi1')
                ->get()->first();
         
         $pdf = PDF::loadview('kartuRencana.pdfKRS', compact('profil','kartuRencana','semester','kaprodi','semt','jur'));
         return $pdf->stream('KRS.pdf');
    }

    public function pdfUTS()
    {
        $cek = FaseAkademik::where('status','=', 'aktif')->get()->first();
        $time=Carbon::now();
        $year=$time->year;
        $data = DB::table('users')->where('users.id','=',Auth::user()->id)->select('users.tahunMasuk')->first();
       
        $tahunAjaran1=DB::table('fase_akademiks')->where('status','aktif')->get()->first();

        if($tahunAjaran1->semester=='ganjil'){ 
            if(($year-$data->tahunMasuk) == '0'){ //ganjil: 2018-2018 = 0
              $semester = 1;
            }elseif(($year-$data->tahunMasuk)== '1'){ //ganjil: 2018-2017 = 1
              $semester = 3;   
            }else{ //ganjil: 2018-2017 = 1
              $semester = 'lulus';   
            }
        }else{
            if(($year-$data->tahunMasuk) == '1'){ //genap: 2019-2018 = 1
              $semester = 2;
            }elseif(($year-$data->tahunMasuk)== '2'){ //genap: 2019-2017 = 2
              $semester = 4;
            }else{ //ganjil: 2018-2017 = 1
              $semester = 'lulus';   
            }
        }

    

        $kartuRencana = DB::table('kartu_rencanas')
            ->join('paket_kuliahs', 'kartu_rencanas.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('struktur_paket_kuliahs', 'struktur_paket_kuliahs.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('matkuls', 'struktur_paket_kuliahs.idMatkul', '=', 'matkuls.id')
            ->where('kartu_rencanas.idUser','=',Auth::user()->id)
            ->where('paket_kuliahs.komulatif','=',$semester)
            ->where('kartu_rencanas.status','=','aktif')
            ->select('matkuls.nama as namamatkul','matkuls.kode as kodematkul', 'matkuls.SKS', 'matkuls.SKSteori', 'matkuls.SKSpraktek','paket_kuliahs.tahunAjaran')
            ->get();


        $profil = DB::table('users')
            ->join('prodis', 'users.prodi', '=', 'prodis.id')
            ->join('kategori_penggunas', 'users.kategori', '=', 'kategori_penggunas.id')
            ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakat')
            ->where('users.id','=',Auth::user()->id)
             ->get()->first();

        $semt = DB::table('fase_akademiks')
            ->where('fase_akademiks.status','=','aktif')
            ->select('fase_akademiks.semester as semt','fase_akademiks.tahunAjaran as tahun')
            ->get()->first();

        $koor = DB::table('users')
            ->where('users.kategori','=',5)
            ->select('users.*', 'users.nomorInduk as nip','users.nama as namakoor')
            ->get()->first();

        if(Carbon::now() > $cek->faseRegistrasi){
           $lihat = 'true';
        }else{
           $lihat = 'false';
        }

        if($semester=='lulus'){
            $lulus = 'lulus';
        }else{
            $lulus = 'aktif';
        }
         
 
        $pdf = PDF::loadview('cetak.pdfUTS', compact('profil','kartuRencana','semester','semt','lihat','koor','lulus'));
        return $pdf->stream('UTS.pdf');
    }

    public function pdfUAS()
    {
        $cek = FaseAkademik::where('status','=', 'aktif')->get()->first();
        $time=Carbon::now();
        $year=$time->year;
        $data = DB::table('users')->where('users.id','=',Auth::user()->id)->select('users.tahunMasuk')->first();
       
        // $tahunAjaran=DB::table('fase_akademiks')->latest()->first();
        $tahunAjaran=DB::table('fase_akademiks')->where('status','aktif')->get()->first();

        if($tahunAjaran->semester=='ganjil'){ 
            if(($year-$data->tahunMasuk) == '0'){ //ganjil: 2018-2018 = 0
              $semester = 1;
            }elseif(($year-$data->tahunMasuk)== '1'){ //ganjil: 2018-2017 = 1
              $semester = 3;   
            }else{ //ganjil: 2018-2017 = 1
              $semester = 'lulus';   
            }
        }else{
            if(($year-$data->tahunMasuk) == '1'){ //genap: 2019-2018 = 1
              $semester = 2;
            }elseif(($year-$data->tahunMasuk)== '2'){ //genap: 2019-2017 = 2
              $semester = 4;
            }else{ //ganjil: 2018-2017 = 1
              $semester = 'lulus';   
            }
        }

        $kartuRencana = DB::table('kartu_rencanas')
            ->join('paket_kuliahs', 'kartu_rencanas.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('struktur_paket_kuliahs', 'struktur_paket_kuliahs.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('matkuls', 'struktur_paket_kuliahs.idMatkul', '=', 'matkuls.id')
            ->where('kartu_rencanas.idUser','=',Auth::user()->id)
            ->where('paket_kuliahs.komulatif','=',$semester)
            ->where('kartu_rencanas.status','=','aktif')
            ->select('matkuls.nama as namamatkul','matkuls.kode as kodematkul', 'matkuls.SKS', 'matkuls.SKSteori', 'matkuls.SKSpraktek')
            ->get();

        $profil = DB::table('users')
            ->join('prodis', 'users.prodi', '=', 'prodis.id')
            ->join('kategori_penggunas', 'users.kategori', '=', 'kategori_penggunas.id')
            ->select('users.*', 'prodis.nama as namaprodi', 'kategori_penggunas.nama as namakat')
            ->where('users.id','=',Auth::user()->id)
             ->get()->first();

        $semt = DB::table('fase_akademiks')
            ->where('fase_akademiks.status','=','aktif')
            ->select('fase_akademiks.semester as semt','fase_akademiks.tahunAjaran as tahun')
            ->get()->first();

        $koor = DB::table('users')
            ->where('users.kategori','=',5)
            ->select('users.*', 'users.nomorInduk as nip','users.nama as namakoor')
            ->get()->first();

        if(Carbon::now() > $cek->faseRegistrasi){
           $lihat = 'true';
        }else{
           $lihat = 'false';
        }

        if($semester=='lulus'){
            $lulus = 'lulus';
        }else{
            $lulus = 'aktif';
        }
         
         
        $pdf = PDF::loadview('cetak.pdfUAS', compact('profil','kartuRencana','semester','semt','lihat','koor','lulus'));
        return $pdf->stream('UAS.pdf');
    }

    public function lihatAbsen()
    {
         $paketKuliah = DB::table('paket_kuliahs')
                ->join('kurikulums', 'paket_kuliahs.idKurikulum', '=', 'kurikulums.id')
                ->join('prodis','kurikulums.idProdi','=','prodis.id')
                ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
                ->select('paket_kuliahs.*','prodis.nama as namaprodi',  'kurikulums.tahun', 'fase_akademiks.tahunAjaran')
                ->where('paket_kuliahs.status','=','aktif')
                ->get(); 

        return view('kartuRencana.lihatAbsen')->with('paketKuliah',$paketKuliah);
    }

    public function formAbsen(Request $request)
    {
        $absen = DB::table('kartu_rencanas')
            ->join('paket_kuliahs', 'kartu_rencanas.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('users', 'kartu_rencanas.idUser', '=', 'users.id') 
            ->join('fase_akademiks', 'paket_kuliahs.tahunAjaran', '=', 'fase_akademiks.id') 
            ->where('kartu_rencanas.idPaketKuliah','=',$request->paketKuliah)
            ->where('kartu_rencanas.status','=','aktif')
            ->where('fase_akademiks.status','=','aktif')
            ->where('paket_kuliahs.status','=','aktif')
            ->select('kartu_rencanas.*','paket_kuliahs.*','users.nomorInduk', 'users.id as iduser','users.nama')
            ->get();


        $coba = DB::table('paket_kuliahs')
            ->join('kurikulums','paket_kuliahs.idKurikulum','=', 'kurikulums.id')   
            ->join('prodis','kurikulums.idProdi','=','prodis.id')
            ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
            ->where('paket_kuliahs.id','=',$request->paketKuliah)
            ->select('kurikulums.tahun','prodis.nama as namaprodi','paket_kuliahs.tahunAjaran','paket_kuliahs.komulatif','fase_akademiks.tahunAjaran')
            ->get()->first();

        return view('kartuRencana.formAbsen')
        ->with('absen',$absen)
        ->with('coba',$coba);
    }

    public function ubahAbsen(KartuRencana $KartuRencana, $id)
    {        
        $absen = DB::table('kartu_rencanas')
            ->join('paket_kuliahs', 'kartu_rencanas.idPaketKuliah', '=', 'paket_kuliahs.id')
            ->join('users', 'kartu_rencanas.idUser', '=', 'users.id')
            ->join('fase_akademiks', 'paket_kuliahs.tahunAjaran', '=', 'fase_akademiks.id') 
            ->select('kartu_rencanas.*','paket_kuliahs.*','users.nomorInduk', 'users.nama')
            ->where('kartu_rencanas.status','=',"aktif")
            ->where('fase_akademiks.status','=',"aktif")
            ->where('kartu_rencanas.idUser','=',$id)
            ->get()->first();


        $paketKuliah = PaketKuliah::where('paket_kuliahs.status','=',"aktif");
        $user = User::all(); 

        return view('kartuRencana.ubahAbsen')
        ->with('absen',$absen)
        ->with('paketKuliah', $paketKuliah)
        ->with('user', $user);
    }

    public function perbaruiAbsen(Request $request, $id)
    {
        $absen = KartuRencana::where('idUser',$id)->where('kartu_rencanas.status','=',"aktif")->get()->first();
        $absen->izin = $request->izin;
        $absen->alpa = $request->alpa;
        $absen->save();

        Alert('Data Absen Berhasil Diubah', 'Absen Mahasiswa');
        return redirect('/kartuRencana/lihatAbsen');
    }

    //HALAMAN ADMIN
    public function lihatAbsenA()
    {

        //menampilkan dropdown paketkuliah
          $paketKuliah = DB::table('paket_kuliahs')
                ->join('kurikulums', 'paket_kuliahs.idKurikulum', '=', 'kurikulums.id')
                ->join('prodis','kurikulums.idProdi','=','prodis.id')
                ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
                ->where('fase_akademiks.status','=','aktif')
                ->select('paket_kuliahs.*','prodis.nama as namaprodi',  'kurikulums.tahun', 'fase_akademiks.tahunAjaran')
                ->get(); 

            return view('halamanAdmin.daftarAbsen')->with('paketKuliah',$paketKuliah);
    }

    public function formAbsenA(Request $request)
    {
        $paketKuliah = DB::table('kartu_rencanas')
           ->join('paket_kuliahs', 'kartu_rencanas.idPaketKuliah', '=', 'paket_kuliahs.id')
           ->join('users', 'kartu_rencanas.idUser', '=', 'users.id') 
           ->where('kartu_rencanas.idPaketKuliah','=',$request->paketKuliah)
           ->where('kartu_rencanas.status','=','aktif')
           ->select('kartu_rencanas.*','paket_kuliahs.*','users.nomorInduk', 'users.id as iduser')
           ->get();

        $format = DB::table('paket_kuliahs')
           ->join('kurikulums','paket_kuliahs.idKurikulum','=', 'kurikulums.id')   
           ->join('prodis','kurikulums.idProdi','=','prodis.id')
           ->join('fase_akademiks','paket_kuliahs.tahunAjaran','=','fase_akademiks.id')
           ->where('paket_kuliahs.id','=',$request->paketKuliah)
           ->select('kurikulums.tahun','prodis.nama','paket_kuliahs.tahunAjaran','paket_kuliahs.komulatif','fase_akademiks.tahunAjaran')
           ->get()->first();  

        return view('halamanAdmin.absenMahasiswa')
            ->with('paketKuliah',$paketKuliah)
            ->with('format',$format);
    }




}
<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidasiUnggahTranskrip extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            //
            'komulatif' => 'required',
            'nilaiAkhir' => 'required'
        ];
    }

    public function messages()
    {
        
        return [
            
            'komulatif.required' => 'Semester Komulatif Tidak Boleh Kosong ',
            'nilaiAkhir.required' => 'Nilai Tidak Boleh Kosong '
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidasiUbahMahasiswa extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            //
            'kategori' => 'required|not_in:-- Pilih Kategori --',
            'prodi' => 'required',
            'nama' => 'required|max:50',
            'email' => 'required|email|unique:users,email,'.$this->id,
     //       'password' => 'required|max:50',
            'tahunMasuk' => 'required|date_format:Y',
            'file' => 'mimes:jpg,jpeg|max:1000'
        ];
    }

    public function messages()
    {
        
        return [
            
            'kategori.required' => 'Kategori Pengguna Tidak Boleh Kosong ',
            'kategori.not_in' => 'Kategori Pengguna Tidak Boleh Kosong ',
            'prodi.required' => 'Prodi Tidak Boleh Kosong ',
            'nama.max' => 'Nama maksimal 50 karakter',
            'nama.required' => 'Nama Tidak Boleh Kosong',
            'email.required' => 'Email Tidak Boleh Kosong',
            'email.unique' => 'Email Sudah Ada',
            'password.required' => 'Password Tidak Boleh Kosong',
            'password.unique' => 'Password Sudah Ada',
            'tahunMasuk.required' => 'Tahun Masuk Tidak Boleh Kosong ',
            'tahunMasuk.date' => 'Format Tahun (ex: 2018)',
            'file.required' => 'Foto Tidak Boleh Kosong',
            'file.mimes' => 'Ekstensi Harus .jpg atau .jpeg',
            'file.max' => 'File maksimum 1000kb'
        ];
    }
}

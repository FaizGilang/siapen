<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidasiUbahProdi extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'kode' => 'required|min:2',
            'nomor' => 'required|min:2',
            'nama' => 'required'
        ];
        
    }
    public function messages()
    {
        
        return [
            'kode.required' => 'Kode Prodi Tidak Boleh Kosong ',
            'nomor.required' => 'Nomor Prodi Tidak Boleh Kosong ',
            'kode.min' => 'Kode Prodi Minimal 2 Karakter Huruf Kapital',
            'nomor.min' => 'Nomor Prodi Minimal 2 Angka',
            'nama.required' => 'Nama Prodi Tidak Boleh Kosong'
        ];
    }
}

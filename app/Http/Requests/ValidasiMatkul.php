<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidasiMatkul extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            //
            'prodi' => 'required',
            'dosen1' => 'required',
            'dosen2' => 'required',
            'nama' => 'required|max:50',
            'komulatif' => 'required|max:1',
            'SKS' => 'required|max:1',
            'SKSteori' => 'required|max:1',
            'SKSpraktek' => 'required|max:1',
            'jpm' => 'required|max:2',
        ];
    }

    public function messages()
    {
        
        return [
            'prodi.required' => 'Prodi Tidak Boleh Kosong ',
            'dosen1.required' => 'Dosen 1 Tidak Boleh Kosong ',
            'dosen2.required' => 'Dosen 2 Tidak Boleh Kosong ',
            'nama.max' => 'Nama maksimal 50 karakter',
            'nama.required' => 'Nama Tidak Boleh Kosong',
            'SKS.required' => 'SKS Tidak Boleh Kosong',
            'SKS.max' => 'Isian Berupa Angka, Maksimal 1 angka',
            'SKSteori.required' => 'Teori Tidak Boleh Kosong',
            'SKSteori.max' => 'Isian Berupa Angka, Maksimal 1 angka',
            'SKSpraktek.required' => 'Praktek Tidak Boleh Kosong',
            'SKSpraktek.max' => 'Isian Berupa Angka, Maksimal 1 angka',
            'jpm.required' => 'JPM Tidak Boleh Kosong',
            'jpm.max' => 'Isian Berupa Angka, Maksimal 1 angka',
            
        ];
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidasiProdi extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            //
            'kode' => 'required|min:2|unique:prodis',
            'nomor' => 'required|min:2|unique:prodis',
            'nama' => 'required'
        ];
    }

    public function messages()
    {
        
        return [
            
            'kode.required' => 'Kode Prodi Tidak Boleh Kosong ',
            'nomor.required' => 'Nomor Prodi Tidak Boleh Kosong ',
            'kode.unique' => 'Kode Prodi Sudah Ada ',
            'nomor.unique' => 'Nomor Prodi Sudah Ada ',
            'kode.min' => 'Kode Prodi Minimal 2 Karakter Huruf Kapital',
            'nomor.min' => 'Nomor Prodi Minimal 2 Angka',
            'nama.required' => 'Nama Prodi Tidak Boleh Kosong'
        ];
    }
}

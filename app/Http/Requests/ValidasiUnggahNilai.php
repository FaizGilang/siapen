<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidasiUnggahNilai extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            //
            'file' => 'required|mimes:xlsx'
        ];
    }

    public function messages()
    {
        
        return [
            'file.mimes' => 'Ekstensi Harus .xlsx',
            'fie.required' => ' Ekstensi Harus .xlsx, Sillahkan Download Format Pada Link Dibawah ini.',
            
        ];
    }
}

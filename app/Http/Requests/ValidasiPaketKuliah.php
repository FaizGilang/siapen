<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidasiPaketKuliah extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            //
            'kurikulum' => 'required',
            'tahunAjaran' => 'required',
            'komulatif' => 'required'
        ];
    }

    public function messages()
    {
        
        return [
            'kurikulum.required' => 'Kurikulum Tidak Boleh Kosong ',
            'tahunAjaran.required' => 'Tahun Ajaran Tidak Boleh Kosong ',
            'komulatif.required' => 'Semester Komulatif Tidak Boleh Kosong '
        ];
    }
}

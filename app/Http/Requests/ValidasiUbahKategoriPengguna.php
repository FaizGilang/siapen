<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidasiUbahKategoriPengguna extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nama' => 'required|min:3'
        ];
        
    }
    public function messages()
    {
        
        return [
           
            
            'nama.min' => 'Nama Kategori Minimal 3 Karakter',
            'nama.required' => 'Nama Kategori Tidak Boleh Kosong'
        ];
    }
}

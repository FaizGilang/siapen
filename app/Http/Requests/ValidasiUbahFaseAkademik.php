<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidasiUbahFaseAkademik extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
         return [
            //
            'tahunAjaran' => 'required',
            'faseRegistrasi' => 'required',
            'awalKuliah' => 'required',
            'akhirKuliah' => 'required',
            'faseEval' => 'required',
            'semester' => 'required'
        ];
    }

    public function messages()
    {
        
        return [
            'tahunAjaran.required' => 'Tahun Ajaran Tidak Boleh Kosong ',
            'faseRegistrasi.required' => 'Fase Registrasi Tidak Boleh Kosong ',
            'awalKuliah.required' => 'Awal Kuliah Tidak Boleh Kosong ',
            'akhirKuliah.required' => 'Akhir Kuliah Tidak Boleh Kosong',
            'faseEval.required' => 'Fase Eval Tidak Boleh Kosong',
            'semester.required' => 'Semester Tidak Boleh Kosong',
        ];
    }
}

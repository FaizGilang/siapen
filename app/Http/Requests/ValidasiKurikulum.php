<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class ValidasiKurikulum extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'tahun' => 'required|date_format:Y',
            'idProdi' => 'required'
        ];
    }

    public function messages()
    {
        
        return [
            
            'tahun.required' => 'Tahun Kurikulum Tidak Boleh Kosong ',
            'tahun.date' => 'Format Tahun (ex: 2018)',
            'idProdi.required' => 'Prodi Tidak Boleh Kosong'
        ];
    }
}

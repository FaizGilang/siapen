<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidasiKategoriPengguna extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'nama' => 'required|unique:kategori_penggunas|min:3'
        ];
        
    }
    public function messages()
    {
        
        return [
           
            'nama.unique' => 'Nama Kategori Sudah Ada ',
            'nama.min' => 'Nama Kategori Minimal 3 Karakter',
            'nama.required' => 'Nama Kategori Tidak Boleh Kosong'
        ];
    }
}

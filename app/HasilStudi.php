<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilStudi extends Model
{
    
    public $fillable = ['idUser','idMatkul','uts','uas','tugas','praktek','bobotUts','bobotUas','bobotTugas','bobotPraktek'];
   

        public function getNilaibAttribute()
    {

        $nilaib = $this->nilai;
        if ($nilaib>=79.6 && $nilaib<=100) {
            $nilaib = 'A';
        }elseif ($nilaib>=75.6 && $nilaib<=79.5) {
            $nilaib = 'A-';
        }elseif ($nilaib>=71.6 && $nilaib<=75.5) {
            $nilaib = 'B+';
        }elseif ($nilaib>=67.6 && $nilaib<=71.5) {
            $nilaib = 'B';
        }elseif ($nilaib>=63.6 && $nilaib<=67.5) {
            $nilaib = 'B-';
        }elseif ($nilaib>=59.6 && $nilaib<=63.5) {
            $nilaib = 'C+';
        }elseif ($nilaib>=55.6 && $nilaib<=59.5) {
            $nilaib = 'C';
        }elseif ($nilaib>=40.6 && $nilaib<=55.5) {
            $nilaib = 'D';
        }elseif ($nilaib>=0 && $nilaib<=40.5) {
            $nilaib = 'E';
        }elseif ($nilaib<=0) {
            $nilaib = '-';
        }elseif ($nilaib>100) {
            $nilaib = '-';
        }
        return $nilaib;
    }

    public function getBobotAttribute(){
        $bobot = $this->nilaib;
        if ($bobot == 'A') {
            $bobot = 4.0 ;
        }elseif ($bobot == 'A-') {
            $bobot = 3.7 ;
        }elseif ($bobot == 'B+') {
            $bobot = 3.3 ;
        }elseif ($bobot == 'B') {
            $bobot = 3.0 ;
        }elseif ($bobot == 'B-') {
            $bobot = 2.7 ;
        }elseif ($bobot == 'C+') {
            $bobot = 2.5 ;
        }elseif ($bobot == 'C') {
            $bobot = 2.0 ;
        }elseif ($bobot == 'D') {
            $bobot = 1.0 ;
        }elseif ($bobot == 'E') {
            $bobot = 0 ;
        }elseif ($bobot == '-') {
            $bobot = 0;
        }
        return $bobot;
    }
public function getNilaibobotAttribute(){
    $nilaibobot = Matkul::where('id',$this->idMatkul)->value('SKS');
    $nilaibobot = $nilaibobot*$this->bobot;
    return $nilaibobot; 
}
}

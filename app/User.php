<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

  
    protected $fillable = [
        'nama','nomorInduk','email','password','kategori','prodi','tahunMasuk','status','file'
    ];

 
    protected $hidden = [
        'password', 'remember_token',
    ];

   

 

   

    
}

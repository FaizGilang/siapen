<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prodi extends Model
{
    //
    public $fillable = ['kode','nomor','nama','status'];
}

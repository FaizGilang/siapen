<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaketKuliah extends Model
{
    //
    public $fillable = ['idProdi','idKurikulm','tahunAjaran','semester','status'];
}

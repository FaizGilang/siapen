@extends('layouts.login')

@section('content')
<div class="container">
</div>
<div class="col-md-12">
    <div class="row">
    
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <div class="panel-transparent">
            <div style="font-size: 28px;" align="center" class="panel-heading" >SISTEM INFORMASI PENGELOLAAN NILAI MAHASISWA</div>
            <div style="margin-bottom: 50px; font-size: 28px;" align="center" class="panel-heading" >AKN-DEMAK</div>
                <div class="panel-heading">Login</div>
                <div class="panel-body">

                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">User ID:</label>

                            <div class="col-md-6">
                                <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" style="background-color: transparent; color:#fff" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password:</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" style="background-color: transparent; color:#fff" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">

                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" data-toggle="modal" data-target="#myModal">
                                    Lupa Password Anda?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

   <div class="container">      
   
   
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- konten modal-->
            <div class="modal-content">
                <!-- heading modal -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4  class="modal-title">Bagian heading modal</h4>
                </div>
                <!-- body modal -->
                <div class="modal-body">
                    <h2 class="text-info">Silahkan Hubungi Admin..</h2>
                </div>
               
            </div>
        </div>
    </div>
   </div>
</body>
</html>
@endsection
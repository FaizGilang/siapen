

@extends('layouts.home')

@section('content')
<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
    
}
</style>
    <form role="form">
        <div class="card-body">
          <div class="card card-primary">
              <div class="card-header">
                <h1 align="center" class="card-title" style="font-family: Times New Rowman;">Profil Anda</h1>
              </div><br> 
              <div style="margin-left: 400px;" class="col-lg-1">
        @if($data->file == "")
        <img src="{{URL::asset('/FotoPengguna/Pengguna-Default.jpg')}}" alt=profile Pic" height="180" width="130">
        @else
        <img src="{{URL::asset('/FotoPengguna/'.$data->file)}}" alt=profile Pic" height="200" width="180">
        @endif
        </div><br>     
        <pre>
                1. Nomor Induk       : {{ $data->nomorInduk }}<br>
                2. Nama Pengguna     : {{ $data->nama }}<br>
                3. Email             : {{ $data->email }}<br>
              @if($data->namaprodi === 0)  4. Program Studi     : AKND<br>
               @else 4. Program Studi     : {{ $data->namaprodi }}<br>
               @endif 5. Kategori Pengguna : {{ $data->namakategori }}<br>    
                6. Tahun Masuk       : {{ $data->tahunMasuk }}<br>   
                7. Status            : {{ $data->status }}<br>   
        </pre>    
        </div>          
      </div>
    
  </form>
@endsection


 
@extends('layouts.home')

@section('heading')
<!-- Page Heading -->
<header class="head">
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;user</h3>
    </div>
    <!-- /.main-bar -->
</header>
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url()->current() }}" enctype="multipart/form-data">

            {{ csrf_field() }}

            <div class="card card-primary">
              <div class="card-header">
                <h3 style="margin-left: 500px;" class="card-title">Ubah Pengguna</h3>
              </div>
               <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">

            <div class="form-group {{ $errors->has('nomorInduk') ? 'has-error' : '' }}">
                <label>Nomor Induk:</label>
                <input type="text" name="nomorInduk"  class="form-control" placeholder="Nomor Induk" value="{{ $user->nomorInduk }}">
                {!! $errors->first('nomorInduk', '<p class="text-danger">:message</p>') !!}
            </div>

             <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                <label>Nama Pengguna:</label>
                <input type="text" name="nama" class="form-control" placeholder="Nama Pengguna" value="{{ $user->nama }}">
                {!! $errors->first('nama', '<p class="text-danger">:message</p>') !!}
            </div>

             <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label>Email:</label>
                <input type="text" name="email" class="form-control" placeholder="Email" value="{{ $user->email }}">
                {!! $errors->first('email', '<p class="text-danger">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('prodi') ? 'has-error' : '' }}">
                <label>Program Studi:</label>
                <select class="form-control" name="prodi">
                     @if($cek->prodi=='0')
                        <option value="0" >AKND</option>
                    @else   
                        @foreach($prodi as $a)
                        <option value="{{ $a->id }}">{{ $a->nama }}</option>
                        @endforeach
                    @endif                        

                        
                </select>
                        
            </div>

           <div>
                    <label>Kategori Pengguna:</label>
                    <select  class="form-control" name="kategori">
                        <option value="{{ $user->kategori }}">{{$user->namakategori}}</option>
                        <option value="3">Dosen</option>
                        <option value="5">Koordinator AKND</option>
                        <option value="7">Koordinator Prodi</option>
                        
                    </select>
                </div>

            <div class="form-group {{ $errors->has('tahunMasuk') ? 'has-error' : '' }}">
                <label>Tahun Masuk:</label>
                <input type="text" name="tahunMasuk" class="form-control" placeholder="tahunMasuk" value="{{ $user->tahunMasuk }}">
                {!! $errors->first('tahunMasuk', '<p class="text-danger">:message</p>') !!}
            </div>
            <div>
            @if($user->file == "")
                <img src="{{URL::asset('/FotoPengguna/Pengguna-Default.jpg')}}" alt=profile Pic" height="100" width="100">

                <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                    <label>Foto:</label>
                    <input type="file" name="file" class="form-control" value="{{ old('file') }}">
                    {!! $errors->first('file', '<p class="text-danger">:message</p>') !!}
                </div>
            @else
                <img src="{{URL::asset('/FotoPengguna/'.$user->file)}}" alt=profile Pic" height="100" width="100">
                
                <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                    <label>Foto:</label>
                    <input type="file" name="file" class="form-control" value="{{ old('file') }}">
                    {!! $errors->first('file', '<p class="text-danger">:message</p>') !!}
                </div>
            @endif
            </div>

             <div class="card-footer">
             <a style="margin-left: 10px" href="{{ url('/user/lihat1') }}" class="fa fa-arrow-circle-left fa-2x"/></a>              
                <input style="margin-left: 450px" type="submit" name="save" value="Perbarui" class="btn btn-md btn-success">
                <a href="{{ url('/user/lihat') }}"  name="reset" class="btn btn-danger" value="Batal" /> Batal   </a>             
            </div>
        </form>
    </div>
</div>
@endsection

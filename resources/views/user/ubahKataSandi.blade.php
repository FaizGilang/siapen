<!-- TAMPILAN FORM TAMBAH PROGRAM STUDI BARU -->

@extends('layouts.pengguna')


@section('content')
<div class="row">
    <div class="col-lg-12">
         <div class="card card-primary">
              <div class="card-header">
                <h1 align="center" class="card-title" style="font-family: Times New Rowman;">Ubah Kata Sandi</h1>
              </div>
            <br>

            <div class="members-oks">
            {!! Form::open(['url' => url('/user/ubahKataSandi'),  
            'method' => 'post', 'class'=>'form-horizontal']) !!}
            
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <b>   &emsp; &emsp; Kata Sandi Lama:</b>
              <div style="margin-left: 35px;">
                {!! Form::password('password', ['class'=>'form-control']) !!}
                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
              </div>
            </div>

            <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
              <b> &emsp; &emsp; Kata Sandi Baru:</b>
              <div style="margin-left: 35px;">
                {!! Form::password('new_password', ['class'=>'form-control']) !!}
                {!! $errors->first('new_password', '<p class="help-block">:message</p>') !!}
              </div>
            </div>

            <div class="form-group{{ $errors->has('new_password_confirmation') ? ' has-error' : '' }}">
              <b> &emsp; &emsp;Konfirmasi Kata Sandi Baru:</b>
              <div style="margin-left: 35px;">
                {!! Form::password('new_password_confirmation', ['class'=>'form-control']) !!}
                {!! $errors->first('new_password_confirmation', '<p class="help-block">:message</p>') !!}
              </div>
            </div>

        </div><br><br>
        <div align="center">
                 {!! Form::submit('Simpan', ['class'=>'btn btn-md btn-success']) !!} 
            </div><br>
            {!! Form::close() !!}
          </div>
          </div>
        </form>   
@endsection
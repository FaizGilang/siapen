@extends('layouts.home')

@section('heading')
<header class="head">
    <div class="search-bar">
        <div class="main-search">
            <div class="input-group">
                <input type="text" onkeyup="search()" class="form-control" id="search" placeholder="Live Search ...">
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm text-muted" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>  
        </div>      
                                    <!-- /.main-search -->                             
    </div>
                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;User</h3>
    </div>
                            <!-- /.main-bar -->
</header>
                        <!-- /.head -->
@endsection

@section('content')
<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}

input[type=text] {
    width: 130px;
    box-sizing: border-box;
    border: 2px solid #ccc;
    border-radius: 4px;
    font-size: 16px;
    background-color: white;
    background-image: url('searchicon.png');
    background-position: 10px 10px; 
    background-repeat: no-repeat;
    padding: 12px 20px 12px 40px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
}

input[type=text]:focus {
    width: 100%;
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="row">
          <div class="col-md-12">
            <div class="row">
            </div>
          </div>
          </div>
<div class="row">
    <div class="col-lg-12"><br>
        <h1 style="font-family: Times New Roman; font-weight: bold;" class="page-header" align="center">
            DATA DOSEN 
        </h1>
        <br>
        <ol class="breadcrumb">
    </div>
</div>
 <div class="row">
            
            </div>

<!-- /.row -->
<div class="card-body">
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
<form class="form-inline ml-3" method="post"  action="{{ url('/user/cari1') }}">
  {{ csrf_field() }}s
  
       <div style="margin-left: -20px;" class="col-md-2">
              <a href="{{ url('/user/tambah1') }}" class="btn btn-info" style="width:100%; margin-right:0px !important; margin-left:0px !important;">
                <h4 class="fa fa-plus nav-icon"> Dosen</h4>
              </a>
        </div>

       <div class="input-group input-group-sm">
        <input style="margin-left: 800px;" class="form-control form-control-navbar" type="text" name="cari1" class="form-control" id="cari1" placeholder="Search" aria-label="Search"> {!! $errors->first('kode', '<p class="help-block">:message</p>') !!}

        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>

      </div>

</form>
<br>
        <table class="table table-bordered table-striped table-condensed cf">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Nomor Induk Dosen</th>
                <th style="text-align:center;">Nama</th>
                <th style="text-align:center;">Prodi</th>    
                <th style="text-align:center;">Aksi</th>
               
            </tr>
        </thead>
            
        <tbody>
       <?php $count = 1; ?> 
            @foreach($user as $data) 
            <tr> 
                <td style="text-align:center;">{{ $count }}.</td> 
                <td width="150px" style="text-align:left;">{{ $data->namakat }}</td>
                <td style="text-align:left;">{{ $data->nama }}</td> 
                <td style="text-align:left;">{{ $data->kodeprodi }}</td> 
                <td>
                 @if($data->status==="tidak")
                 <a style="margin-left: 0px;" data-placement="top" data-original-title="Status" data-toggle="tooltip" href="{{ url('/user/status/' . $data->id) }}" class="btn btn-success" align="center"><i style="color: white" class="fa  fa-check-circle"">  Aktif</i></a>
                  <a class="delete-modal btn btn-danger" data-id="{{$data->id}}" data-nama="{{$data->nama}}"><i class="fa fa-trash" style="color: white" class="glyphicon glyphicon-trash">  Hapus     </i></a>
                   @else
                 <span style="margin-left: 0px;" class="btn btn-default"><i class="fa fa-check-circle">  Aktif</i></span> 
                  <a data-placement="top" data-original-title="Delete" data-toggle="tooltip" value="hidden" class="btn btn-default" ><i class="fa fa-trash">  Hapus     </i></a> 
                @endif 
                    <a  data-placement="top" data-original-title="Edit" data-toggle="tooltip" href="{{ url('/user/ubah1/' . $data->id) }}" class="btn btn-warning" align="center"><i style="color: white" class="fa  fa-pencil">  Ubah</i></a> 

                    <a  data-placement="top" data-original-title="Detail" data-toggle="tooltip" href="{{ url('/user/detail/' . $data->id) }}" class="btn btn-info" align="center"><i style="color: white" class="fa  fa-eye">  Detail</i></a> 

                    <a  data-placement="top" data-original-title="Reset" data-toggle="tooltip" href="{{ url('/user/reset/' . $data->id) }}" class="btn btn-primary" align="center"><i style="color: white" class="fa  fa-key">  Reset</i></a>     

                   </td>
            </tr> 
            <?php $count++; ?> 
            @endforeach 
            @foreach($userumum as $data1) 
            <tr> 
                <td style="text-align:center;">{{ $count }}.</td> 
                <td width="150px" style="text-align:left;">{{ $data1->namakat }}</td>
                <td style="text-align:left;">{{ $data1->nama }}</td> 
                <td style="text-align:left;">AKND</td> 
                <td>
                 @if($data1->status==="tidak")
                 <a style="margin-left: 0px;" data-placement="top" data-original-title="Status" data-toggle="tooltip" href="{{ url('/user/status/' . $data1->id) }}" class="btn btn-success" align="center"><i style="color: white" class="fa  fa-check-circle"">  Aktif</i></a>
                  <a class="delete-modal btn btn-danger" data-id="{{$data->id}}" data-nama="{{$data->nama}}"><i class="fa fa-trash" style="color: white" class="glyphicon glyphicon-trash">  Hapus     </i></a>
                   @else
                 <span style="margin-left: 0px;" class="btn btn-default"><i class="fa fa-check-circle">  Aktif</i></span> 
                  <a data-placement="top" data-original-title="Delete" data-toggle="tooltip" value="hidden" class="btn btn-default" ><i class="fa fa-trash">  Hapus     </i></a> 
                @endif 
                    <a  data-placement="top" data-original-title="Edit" data-toggle="tooltip" href="{{ url('/user/ubah1/' . $data1->id) }}" class="btn btn-warning" align="center"><i style="color: white" class="fa  fa-pencil">  Ubah</i></a> 

                    <a  data-placement="top" data-original-title="Detail" data-toggle="tooltip" href="{{ url('/user/detail/' . $data1->id) }}" class="btn btn-info" align="center"><i style="color: white" class="fa  fa-eye">  Detail</i></a> 

                    <a  data-placement="top" data-original-title="Reset" data-toggle="tooltip" href="{{ url('/user/reset/' . $data1->id) }}" class="btn btn-primary" align="center"><i style="color: white" class="fa  fa-key">  Reset</i></a>     

                   </td>
            </tr> 
            <?php $count++; ?> 
            @endforeach 
        </tbody>
        </tbody>
     </table> 
     <br>
      
    </div>
    </div>
    </div>

<!-- Modal form to delete a form -->
     <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    
                </div>
                <div class="modal-body">
                    <h5 class="text-center">Apakah Anda yakin ingin menghapus data ini?</h5>
                    <br />
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id">ID:</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="id_delete" disabled>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-5" for="nama">Nama:</label>
                            <div class="col-sm-2">
                                <input type="text" class="form-control" id="nama_delete" disabled>
                            </div>
                        </div>
                   </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-trash'></span> Hapus
                        </button>
                        <button type="button" style="color:white" class="btn btn-info" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Batal
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <script type="text/javascript">
// delete a post
        // delete a post
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#id_delete').val($(this).data('id'));          
            $('#nama_delete').val($(this).data('nama'));          
            $('#deleteModal').modal('show');
            id = $('#id_delete').val();
        });
        $('.modal-footer').on('click', '.delete', function() {
            $.ajax({
                type: 'get',
                url: '/user/hapus/' + id,
                success: function(data) {
                    window.location.href = '{{ url('/user/lihat')}}';
                  }
            });
        });
        </script>
@endsection

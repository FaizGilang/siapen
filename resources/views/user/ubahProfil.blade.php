@extends('layouts.pengguna')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url()->current() }}">

            {{ csrf_field() }}

            <div class="card card-info">
              <div class="card-header">
                <h2 align="center" class="card-title">Edit Profil Pengguna</h2>
              </div>
               <!-- /.card-header -->
              <!-- form start -->
           <form role="form">
                <div class="card-body">

            <div class="form-group {{ $errors->has('nomorInduk') ? 'has-error' : '' }}">
                 <b>Nomor Induk:</b>
                 <input type="string" name="nomorInduk" class="form-control" placeholder="nomor induk pengguna" value="{{ $data->nomorInduk }}">
                {!! $errors->first('nomorInduk', '<p class="help-block">:message</p>') !!}
            </div>

             <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                <b>Nama Pengguna:</b>
                <input type="text" name="nama" class="form-control" placeholder="Nama Pengguna" value="{{ $data->nama }}">
                {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
            </div>

             <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <b>Email:</b>
                <input type="text" name="email" class="form-control" placeholder="Email" value="{{ $data->email }}">
                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
            </div>

            <div align="center" class="card-footer">
                <input type="submit" name="save" value="Update" class="btn btn-md btn-success">
                <input type="reset" name="reset" class="btn btn-danger" value="Batal" />
            </div>
        </form>
    </div>
</div>

@endsection

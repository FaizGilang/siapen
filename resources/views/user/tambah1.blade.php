@extends('layouts.home')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;pengguna</h3>
    </div>       <!-- /.main-bar -->
</header>
                        <!-- /.head -->
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url('/user/tambah1') }}" enctype="multipart/form-data">

            {{ csrf_field() }}

          <div class="card card-primary">
              <div class="card-header">
                <h2 style="margin-left: 450px; font-family: Times New Rowman;" class="card-title">Tambah Dosen Baru</h2>
              </div>
              
              <!-- /.card-header -->
              <!-- form start -->

              <form role="form">
                <div class="card-body">

                <div>
                    <label>Kategori Pengguna:</label>
                    <select class="form-control" name="kategori">
                        <option value="3">Dosen</option>
                        <option value="6">Ketua Jurusan</option>
                        <option value="7">Ketua Prodi</option>
                    </select>
                    {!! $errors->first('kategori', '<p class="text-danger">:message</p>') !!}
                </div><br>

                 <div>
                    <label>Program Studi:</label>
                    <select class="form-control" name="prodi">
                        <option value="" selected disabled>-Pilih Prodi-</option>
                        <option value="99" >AKND (Untuk Mata Kuliah Umum)</option>
                        @foreach($prodi as $a)
                        <option value="{{ $a->id }}">{{ $a->nama }}</option>
                        @endforeach
                    </select>
                     
                    
                    {!! $errors->first('prodi', '<p class="text-danger">:message</p>') !!}
                </div><br>

                <div class="form-group {{ $errors->has('nomorInduk') ? 'has-error' : '' }}">
                    <label>NIP:</label>
                    <input type="string" name="nomorInduk" class="form-control" value="{{ old('nomorInduk') }}" placeholder="NIP">
                    {!! $errors->first('nomorInduk', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                    <label>Nama:</label>
                    <input type="string" name="nama" class="form-control" value="{{ old('nama') }}" placeholder="Nama">
                    {!! $errors->first('nama', '<p class="text-danger">:message</p>') !!}
                  </div>

                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                   <label>Email:</label>
                   <input type="text" name="email" class="form-control" value="{{ old('email') }}" placeholder="Alamat Email">
                   {!! $errors->first('email', '<p class="text-danger">:message</p>') !!}
                </div>

                <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                   <label>Password:</label>
                   <input type="password" name="password" class="form-control" value="{{ old('password') }}" placeholder="Password">
                   {!! $errors->first('password', '<p class="text-danger">:message</p>') !!}
                </div>

               <div class="form-group {{ $errors->has('tahunMasuk') ? 'has-error' : '' }}">
                    <label>Tahun Masuk:</label>
                    <input type="string" name="tahunMasuk" class="form-control" placeholder="Tahun Masuk" value="{{ old('tahunMasuk') }}">
                     {!! $errors->first('tahunMasuk', '<p class="text-danger">:message</p>') !!}
                </div>

               <div class="form-group {{ $errors->has('file') ? 'has-error' : '' }}">
                    <label>Foto:</label>
                    <input type="file" name="file" class="form-control" value="{{ old('file') }}">
                    {!! $errors->first('file', '<p class="text-danger">:message</p>') !!}
                </div>


                <div class="card-footer">
                    <a style="margin-left: 10px" href="{{ url('/user/lihat') }}" class="fa fa-arrow-circle-left fa-2x"/></a>            
                    <input style="margin-left: 450px" type="submit" name="save" value="Simpan" class="btn btn-md btn-success">
                    <input type="reset" name="reset" class="btn btn-danger" value="Batal" />                 
                </div>
              </form>
            </div>
        </form>
    </div>
</div>
@endsection
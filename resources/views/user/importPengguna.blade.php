@extends('layouts.pengguna')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Informasi Mata Kuliah</h3>
    </div>       <!-- /.main-bar -->
</header>
                        <!-- /.head -->
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
         <form style="margin-top: 15px;padding: 20px;" action="{{ URL::to('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}

        @if ($message = Session::get('success'))
          <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
          </div>
        @endif

        @if ($message = Session::get('error'))
          <div class="alert alert-danger" role="alert">
            {{ Session::get('error') }}
          </div>
        @endif
          <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Mata Kuliah Yang Diampu</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                <div>
                      <label>Pilih Tahun Ajaran:</label>
                    <select class="form-control" name="tahun" id="tahun">
                        <option value="">-Pilih Tahun-</option>
                        @foreach($tahunAjaran as $a)
                        <option value="{{ urlencode(base64_encode($a->tahun_ajaran)) }}">{{ $a->tahun_ajaran }}</option>
                        @endforeach
                    </select>
                </div> <br>
                <div>
                      <label>Pilih Mata Kuliah:</label>
                    <select class="form-control" name="informasiMatkul" id="matkul">
                        <option value="">-Pilih Mata Kuliah-</option>
                        @foreach($informasiMatkul as $a)
                        <option value="{{ $a->idMatkul }}">{{ $a->nama }}</option>
                        @endforeach
                    </select>
                </div> <br>
                <div id="upload"></div>
              </form>
              <div class="card-footer">
                  <a style="margin-left: 0px" href="{{ url('/halamanDosen/informasiNilai') }}" class="fa fa-arrow-circle-left fa-2x"/></a>
                </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    jQuery(document).ready(function ()
    {
            jQuery('select[name="informasiMatkul"]').on('change',function(){
              var id = jQuery(this).val();
              var id = "/"+id;

              var tahun = jQuery('select[name="tahun"]').val();
              if (tahun == "") {
                $('#button1').remove();
                $('#button2').remove();
                $('#button3').remove();
                alert('pilih tahun dulu');

              }else{

              var d_tahun = "/"+tahun;
              // alert(tahun);
                $('#button1').remove();
                $('#button2').remove();
                $('#button3').remove();
                $('#upload').append(' <input id="button1" class="form-control" type="file" name="import_file" />','<br><button id="button2" class="btn btn-md btn-success">Upload File</button>','<br><a id="button3" value="'+id+'" href="{{ url('downloadExcel/xlsx/') }}'+id+''+d_tahun+'"><br> Download Template Nilai Disini</a>');
              }
            });
    });
</script>
@endsection
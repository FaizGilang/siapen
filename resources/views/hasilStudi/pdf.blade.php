<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<style>
img{
    height: 120px;
    width: 80px;
    padding: -30px 30px 10px 10px; 
}
.label1 {;
  min-width: 8px !important;
  display: inline-block !important
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td{
    padding: 5px 35px 15px 20px;
    text-align: center;   

}
tr{
    text-align: center;
}
p{
    text-align: center;
    font-size: 18;
    margin-left: 150px;
}
h6{
    text-align: center;
    font-size: 18;
    margin-left: 200px;
}
</style>

<div class="card card-primary">
<form role="form">
<div class="card-body">
<div class="row">
    <div class="col-lg-12">

            <img style="float: left;" src="admin/img/PNJ.jpg" ><img style="float: right; margin-left: 0px;"  src="admin/img/admin.jpg" >
           <h3 class="img"  style=" font-family: Times New Roman; font-weight: bold; margin-left: 30px; " align="center">
            POLITEKNIK NEGERI JAKARTA <br> 
            PROGRAM STUDI DILUAR DOMISILI (PDD) <br> 
            AKADEMI KOMUNITAS NEGERI DEMAK <br>
            Alamat: Jl. Sultan Trenggono No. 61 Demak 59511</h3><hr/>
           <h3 style="font-family: Times New Roman; font-weight: bold;" align="center">
            TRANSKRIP NILAI</h3><hr/>
            <pre style=" font-family: Times New Roman; ">
                <br>
                Nama                : {{ $profil->nama }}<br>
                NIM                  : {{ $profil->nomorInduk }}<br>
                Program Studi  : {{ $profil->namaprodi }}<br>
                 
            </pre>
    </div>
</div>

<!-- /.row -->
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered">
        <thead>
           <tr>
               <th width="10px;" style="text-align:center;">No.</th>
                <th style="text-align:center;">Mata Kuliah</th>
                <th style="text-align:center;">Semst Kom</th>
                <th style="text-align:center;">Nilai (N)</th>
                <th style="text-align:center;">Kredit (K)</th>
                <th style="text-align:center;">N x K</th>
                <th style="text-align:center;">JPS</th>
            </tr>
        </thead>
            
         <tbody>
             <?php 
              $count = 1;
              $jumlah = 0;
              $jumlahsks = 0;
              $jumlahjpm = 0;
              ?> 


            @foreach($nilaib as $data)


            <tr> 
               <td style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:left;">{{ $data->nama }}</td>

                <?php
                $data->nilai = (($data->uts)*($data->bobotUts/100)) + (($data->uas)*($data->bobotUas/100)) + (($data->tugas)*($data->bobotTugas/100)) + (($data->praktek)*($data->bobotPraktek/100));
                ?>
               
                @if($data->nilai>=79.6 && $data->nilai<=100) 
                    <td style="text-align:center;">A</td>
                    <?php $bobot=4.0 ?>
                    @elseif($data->nilai>=75.6 && $data->nilai<=79.5)
                    <td style="text-align:center;">A-</td>
                    <?php $bobot=3.7 ?>
                    @elseif($data->nilai>=71.6 && $data->nilai<=75.5)
                    <td style="text-align:center;">B+</td>
                    <?php $bobot=3.3 ?>
                    @elseif($data->nilai>=67.6 && $data->nilai<=71.5)
                    <td style="text-align:center;">B</td>
                    <?php $bobot=3.0 ?>
                    @elseif($data->nilai>=63.6 && $data->nilai<=67.5)
                    <td style="text-align:center;">B-</td>
                    <?php $bobot=2.7 ?>
                    @elseif($data->nilai>=59.6 && $data->nilai<=63.5)
                    <td style="text-align:center;">C+</td>
                    <?php $bobot=2.5 ?>
                    @elseif($data->nilai>=55.6 && $data->nilai<=59.5)
                    <td style="text-align:center;">C</td>
                    <?php $bobot=2.0 ?>
                    @elseif($data->nilai>=40.6 && $data->nilai<=55.5)
                    <td style="text-align:center;">D</td>
                    <?php $bobot=1.0 ?>
                    @elseif($data->nilai>=0 && $data->nilai<=40.5)
                    <td style="text-align:center;">E</td>
                    <?php $bobot=0 ?>
                    @elseif($data->nilai<=0)
                    <td style="text-align:center;">-</td>
                    <?php $bobot=0 ?>
                    @elseif($data->nilai>100)
                    <td style="text-align:center;">-</td>
                    <?php $bobot=0 ?>
                    @endif 
                     <td style="text-align:center;">{{ $data->SKS }}</td>
                     <td style="text-align:center;">{{ $data->SKS * $bobot}}</td>
                     <td style="text-align:center;">{{ $data->jpm*16 }}</td>

                    <?php $jumlah = $jumlah+$data->SKS * $bobot ?>
                    <?php $jumlahsks = $jumlahsks+$data->SKS ?>
                    <?php $jumlahjpm = $jumlahjpm+$data->jpm*16 ?>
            </tr>
              <?php $count++; ?>
            @endforeach 

           
            <tr>
                <td style="text-align:center;" colspan="2">Jumlah:</td>

                @if($jumlahsks == 0)
                <td style="text-align:center;" colspan="1"> - </td>
                @else
                <td style="text-align:center;" colspan="1"></td>
                <td style="text-align:center;" colspan="1"></td>
            
                <td style="text-align:center;" colspan="1">{{ $jumlahsks }}</td>
                @endif
                
                <td style="text-align:center;" colspan="">{{ $jumlah}}</td>
                <td style="text-align:center;" colspan="">{{ $jumlahjpm}}</td>
            </tr>
            
            <tr>
                <td style="text-align:center;" colspan="2">Indeks Prestasi Komulatif (IPK):</td>    
                @if($jumlahsks == 0)
                <td style="text-align:center;" colspan="1"> - </td>
                @else
                <td style="text-align:left;" colspan="5">&emsp; <?php $ipk=$jumlah/$jumlahsks; $hasil=round($ipk,2); echo "$hasil"; ?></td>
                @endif    
            </tr>   
        </tbody>
     </table> 

           


</body>
<br>
<br>
     <?php
    function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    ?>

        <p align="right"><?php echo "Demak, " . (tgl_indo(date('Y-m-d')));?> &emsp; &emsp; </p>
        <h6 class="center" style="text-align:center">
        &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; Koordinator, <br><br><br>
        </h6>
        <p class="center" style="text-align:center">
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;   Dr. Trisyono, M.Pd  <br> 
          &emsp; &emsp; &emsp;  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;  195909161983021002 
        </p>

</html>


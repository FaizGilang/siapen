    
@extends('layouts.pengguna')

@section('content')
<style>
.img1{
    height: 100px;
    width: 150px;
    padding: -30px 30px 10px 10px; 
}
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;    
}
</style>



<div class="row">
    <div class="col-md-12">
      <form method="post" action="{{ url('/hasilStudi/lihatHSM/semester') }}">

            {{ csrf_field() }}
          </form>
        <div class="card">
          <div class="row">
          <div class="col-md-12">
            <div class="row">
            </div>
          </div>
          </div>
        
        <!-- <img class="img1" style="float: left; margin-left: 50px;" src="{{asset('admin/img/PNJ.jpg')}}" ><img class="img1" style="float: right; margin-right: : 800px;"  src="{{asset('admin/img/admin.jpg')}}" >
        <h5 class="header" style=" font-family: Times New Roman; font-weight: bold; margin-left: 0px; " align="center">
            POLITEKNIK NEGERI JAKARTA <br> 
            PROGRAM STUDI DILUAR DOMISILI (PDD) <br> 
            AKADEMI KOMUNITAS NEGERI DEMAK <br>
            Alamat: Jl. Sultan Trenggono No. 61 Demak 59511</h5><hr/> -->
            
            <h3 style="font-family: Times New Roman; font-weight: bold;" align="center">
            HASIL STUDI MAHASISWA (HSM)</h3><hr/>
            <div class="col-md-12">
            <pre>
            <div class="col-md-6" style="margin-left: -40px; font-size: 16px; font-family: Times New Rowman;">
              Nama                : {{ $profil->nama }} 
              NIM                  : {{ $profil->nomorInduk }}
              Program Studi  : {{ $profil->namaprodi }} 
            </div>
            <div class="col-md-4" style="margin-left: 140px; font-size: 16px; font-family: Times New Rowman;">
             &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Semester              :  {{ $semester}}
             &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Tahun Akademik : {{$semt->tahun}}
            </div>
              
            </pre>
                
              
              
            </div>
                 
            
        <div  class="form-group">

                  <label style="margin-left: 30px;">Pilih Semester:</label>
                  <a  data-placement="top"  data-toggle="tooltip" href="{{ url('/hasilStudi/lihatHSM/1' ) }}" class="btn btn-default" align="center"><i style="color: black" class="fa  fa-eye">  Smt 1</i></a>
                  <a  data-placement="top"  data-toggle="tooltip" href="{{ url('/hasilStudi/lihatHSM/2' ) }}" class="btn btn-default" align="center"><i style="color: black" class="fa  fa-eye">  Smt 2</i></a>
                  <a  data-placement="top"  data-toggle="tooltip" href="{{ url('/hasilStudi/lihatHSM/3' ) }}" class="btn btn-default" align="center"><i style="color: black" class="fa  fa-eye">  Smt 3</i></a>
                  <a  data-placement="top"  data-toggle="tooltip" href="{{ url('/hasilStudi/lihatHSM/4' ) }}" class="btn btn-default" align="center"><i style="color: black" class="fa  fa-eye">  Smt 4</i></a>
                </div>
   

@if($lulus === 'lulus')
    <h3 style="font-family: Times New Rowman; margin-left: 250px;" >Maaf, Anda sudah tidak aktif di semester ini. Silahkan Hubungi Admin.</h3>
@else
<!-- /.row -->
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center; ">Mata Kuliah</th>
                <th style="text-align:center;">Nilai (N)</th>
                <th style="text-align:center;">Kredit (K)</th>
                <th style="text-align:center;">N x K</th>
                <th style="text-align:center;">JPS</th>
            </tr>
        </thead>
            
        <tbody>
             <?php 
              $count = 1;
              $Ki = 0;
              $jumKiNi = 0;
              $jumlahjpm = 0;
              ?> 


            @foreach($nilaib as $data)


            <tr> 
               <td style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:left;">{{ $data->nama }}</td>

                <?php
                $data->nilai = (($data->uts)*($data->bobotUts/100)) + (($data->uas)*($data->bobotUas/100)) + (($data->tugas)*($data->bobotTugas/100)) + (($data->praktek)*($data->bobotPraktek/100));
                ?>
               
                @if($data->nilai>=79.6 && $data->nilai<=100) 
                    <td style="text-align:center;">A</td>
                    <?php $bobot=4.0 ?>
                    @elseif($data->nilai>=75.6 && $data->nilai<=79.5)
                    <td style="text-align:center;">A-</td>
                    <?php $bobot=3.7 ?>
                    @elseif($data->nilai>=71.6 && $data->nilai<=75.5)
                    <td style="text-align:center;">B+</td>
                    <?php $bobot=3.3 ?>
                    @elseif($data->nilai>=67.6 && $data->nilai<=71.5)
                    <td style="text-align:center;">B</td>
                    <?php $bobot=3.0 ?>
                    @elseif($data->nilai>=63.6 && $data->nilai<=67.5)
                    <td style="text-align:center;">B-</td>
                    <?php $bobot=2.7 ?>
                    @elseif($data->nilai>=59.6 && $data->nilai<=63.5)
                    <td style="text-align:center;">C+</td>
                    <?php $bobot=2.5 ?>
                    @elseif($data->nilai>=55.6 && $data->nilai<=59.5)
                    <td style="text-align:center;">C</td>
                    <?php $bobot=2.0 ?>
                    @elseif($data->nilai>=40.6 && $data->nilai<=55.5)
                    <td style="text-align:center;">D</td>
                    <?php $bobot=1.0 ?>
                    @elseif($data->nilai>=0 && $data->nilai<=40.5)
                    <td style="text-align:center;">E</td>
                    <?php $bobot=0 ?>
                    @elseif($data->nilai<=0)
                    <td style="text-align:center;">-</td>
                    <?php $bobot=0 ?>
                    @elseif($data->nilai>100)
                    <td style="text-align:center;">-</td>
                    <?php $bobot=0 ?>
                    @endif 
                     <td style="text-align:center;">{{ $data->SKS }}</td>
                     <td style="text-align:center;">{{ $data->SKS * $bobot}}</td>
                     <td style="text-align:center;">{{ $data->jpm*16 }}</td>

                    <?php $jumKiNi = $jumKiNi+$data->SKS * $bobot ?>
                    <?php $Ki = $Ki+$data->SKS ?>
                    <?php $jumlahjpm = $jumlahjpm+$data->jpm*16 ?>
            </tr>
              <?php $count++; ?>
            @endforeach 

            {{-- {{ $data->bobot*$data->SKS++}} --}}

    <tr>
         <td style="text-align:center;" colspan="2">JUMLAH:</td>  
                @if($Ki == 0)
                <td style="text-align:center;" colspan="1"> - </td>
                @else
                <td style="text-align:center;" colspan="1"></td>
                @endif
                <td style="text-align:center;" colspan="1">{{ $Ki}}</td> 
                <td style="text-align:center;" colspan="1">{{ $jumKiNi}}</td> 
                <td style="text-align:center;" colspan="1">{{ $jumlahjpm}}</td> 
    </tr>
                <td style="text-align:center;" colspan="2">Indeks Prestasi (IP):</td>    
                @if($Ki == 0)
                @else
                <td style="text-align:left;" colspan="4"> &emsp; &emsp; &emsp; <?php $ipk=$jumKiNi/$Ki; $hasil=round($ipk,2); echo "$hasil"; ?></td>
                @endif    
            </tr>   
        </tbody>
     </table> 
      
     <div align="center">
        <a class="btn btn-md btn-success" href="/hasilStudi/pdfHSMsekarang">Cetak HSM</a>
    </div>
    <tbody id="table">
          </tbody>
@endif
    <br>
    </div>
  </div>
</div>
</div>
@endsection


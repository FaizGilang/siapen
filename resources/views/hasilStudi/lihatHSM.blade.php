    
@extends('layouts.pengguna')

@section('content')
<style>
.img1{
    height: 100px;
    width: 150px;
    padding: -30px 30px 10px 10px; 
}
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td {
    padding: 5px;
    text-align: left;    
}
</style>



<div class="row">
    <div class="col-md-12">
      <form method="post" action="{{ url('/hasilStudi/lihatHSM/semester') }}">

            {{ csrf_field() }}
          </form>
        <div class="card">
          <div class="row">
          <div class="col-md-12">
            <div class="row">
            </div>
          </div>
          </div>
        <div class="row">
        <div class="col-lg-12"><br>
        <!-- <img class="img1" style="float: left; margin-left: 50px;" src="{{asset('admin/img/PNJ.jpg')}}" ><img class="img1" style="float: right; margin-right: : 800px;"  src="{{asset('admin/img/admin.jpg')}}" > -->
       <!--  <h5 class="header" style=" font-family: Times New Roman; font-weight: bold; margin-left: 0px; " align="center">
            POLITEKNIK NEGERI JAKARTA <br> 
            PROGRAM STUDI DILUAR DOMISILI (PDD) <br> 
            AKADEMI KOMUNITAS NEGERI DEMAK <br>
            Alamat: Jl. Sultan Trenggono No. 61 Demak 59511</h5><hr/> -->
            
            <h3 style="font-family: Times New Roman; font-weight: bold;" align="center">
            HASIL STUDI MAHASISWA (HSM)</h3><hr/>
             <div class="col-md-12">
            <pre>
            <div class="col-md-6" style="margin-left: -40px; font-size: 16px; font-family: Times New Rowman;">
              Nama                : {{ $profil->nama }} 
              NIM                  : {{ $profil->nomorInduk }}
              Program Studi  : {{ $profil->namaprodi }} 
            </div>
            <div class="col-md-4" style="margin-left: 140px; font-size: 16px; font-family: Times New Rowman;">
             &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Semester                : <!--  -->  {{ $semester}}
             &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Tahun Akademik   : {{$tahunAkademik->tahunAkademik}}
            </div>
              
            </pre>
                
              
              
            </div>
        <div class="form-group">

                  <label style="margin-left: 30px;">Pilih Semester:</label>
                  <a  data-placement="top"  data-toggle="tooltip" href="{{ url('/hasilStudi/lihatHSM/1' ) }}" class="btn btn-default" align="center"><i style="color: black" class="fa  fa-eye">  Semt 1</i></a>
                  <a  data-placement="top"  data-toggle="tooltip" href="{{ url('/hasilStudi/lihatHSM/2' ) }}" class="btn btn-default" align="center"><i style="color: black" class="fa  fa-eye">  Semt 2</i></a>
                  <a  data-placement="top"  data-toggle="tooltip" href="{{ url('/hasilStudi/lihatHSM/3' ) }}" class="btn btn-default" align="center"><i style="color: black" class="fa  fa-eye">  Semt 3</i></a>
                  <a  data-placement="top"  data-toggle="tooltip" href="{{ url('/hasilStudi/lihatHSM/4' ) }}" class="btn btn-default" align="center"><i style="color: black" class="fa  fa-eye">  Semt 4</i></a>
                </div>
           
    </div>
</div>

<!-- /.row -->
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center; ">Mata Kuliah</th>
                <th style="text-align:center;">Nilai (N)</th>
                <th style="text-align:center;">Kredit (K)</th>
                <th style="text-align:center;">N x K</th>
                <th style="text-align:center;">JPS</th>
            </tr>
        </thead>
            
        <tbody>

          <?php 
              $count = 1;
              $jumKiNi = 0;
              $Ki = 0;
              $jumlahjpm = 0;
              $bobot = 0;
              
          ?> 
            
            @foreach($nilai as $data)
                 
            <tr> 
               <td style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:left;">{{ $data->nama }}</td>
                <td style="text-align:center;">{{ $data->nilaiAkhir }}</td>
                <td style="text-align:center;">{{ $data->SKS }}</td>
                @if($data->nilaiAkhir==='A') 
                    <?php $bobot=4.0 ?>
                    <td style="text-align:center;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='A-')
                    <?php $bobot=3.7 ?>
                    <td style="text-align:center;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='B+')
                    <?php $bobot=3.3 ?>
                    <td style="text-align:center;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='B')
                    <?php $bobot=3.0 ?>
                    <td style="text-align:center;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='B-')
                    <?php $bobot=2.7 ?>
                    <td style="text-align:center;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='C+')
                    <?php $bobot=2.5 ?>
                    <td style="text-align:center;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='C')
                    <?php $bobot=2.0 ?>
                    <td style="text-align:center;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='D')
                    <?php $bobot=1.0 ?>
                    <td style="text-align:center;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='E')
                    <?php $bobot=0 ?>
                   <td style="text-align:center;">{{ $data->SKS * $bobot}}</td>
                   @elseif($data->nilaiAkhir==='-')
                    <?php $bobot=0 ?>
                   <td style="text-align:center;">{{ $data->SKS * $bobot}}</td>
                   
                @endif 
                
                    <td style="text-align:center;">{{ $data->jpm*16 }}</td>

                    <?php $jumKiNi = $jumKiNi+$data->SKS * $bobot ?>
                    <?php $Ki = $Ki+$data->SKS ?>
                    <?php $jumlahjpm = $jumlahjpm+$data->jpm*16 ?>
            </tr>
              <?php $count++; ?>
            @endforeach 

           

    <tr>
         <td style="text-align:center;" colspan="2">JUMLAH:</td>  
                @if($Ki == 0)
                <td style="text-align:center;" colspan="1"> - </td>
                @else
                <td style="text-align:center;" colspan="1"></td>
                @endif
                <td style="text-align:center;" colspan="1">{{ $Ki}}</td> 
                <td style="text-align:center;" colspan="1">{{ $jumKiNi}}</td> 
                <td style="text-align:center;" colspan="1">{{ $jumlahjpm}}</td> 
    </tr>
                <td style="text-align:center;" colspan="2">Indeks Prestasi (IP):</td>    
                @if($Ki == 0)
                <td style="text-align:left;" colspan="4"> &emsp; &emsp; &emsp; &emsp; &emsp; -</td>
                @else
                <td style="text-align:left;" colspan="4"> &emsp; &emsp; &emsp; <?php $ipk=$jumKiNi/$Ki; $hasil=round($ipk,2); echo "$hasil"; ?></td>
                @endif    
             
        </tbody>
     </table> 
      
     <div align="center">
        <a class="btn btn-md btn-success" href="/hasilStudi/pdfHSM/{{$semester}}">Cetak HSM</a>
    </div>
    <tbody id="table">
          </tbody>
    <br>
    </div>
  </div>
</div>
</div>

@endsection


@extends('layouts.pengguna')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;pengguna</h3>
    </div>       <!-- /.main-bar -->
</header>
                        <!-- /.head -->
<!-- /.row -->
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <form method="get" action="{{ url('/hasilStudi/unggahNilai') }}"> <!--sama route get-->

            {{ csrf_field() }}

          <div class="card card-info">
              <div class="row">
        <div class="col-lg-12"><br>
        <h1 style="font-family: Times New Roman; font-weight: bold;" align="center">
           INFORMASI NILAI
        </h1><br>
                <ol class="breadcrumb">
            </div>
        </div>
              <!-- /.card-header -->
              <!-- form start -->
               <table class="table table-bordered table-striped table-condensed cf">
                <thead>
                <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Kode MK</th>
                <th style="text-align:center;">Nama MK</th>
                <th style="text-align:center;">Prodi</th>
                <th style="text-align:center;">Lihat Nilai</th>

                
                </tr>
                </thead>    
            <tbody>
          <?php $count = 1; ?> 
            @foreach($unggahNilai as $data) 
            <tr>
                <td style="text-align:center;">{{ $count }}.</td>
                <td style="text-align:left;">{{ $data->kodematkul }}</td> 
                <td style="text-align:left;">{{ $data->namamatkul }}</td> 
                <td style="text-align:left;">{{ $data->namaprodi }}</td> 
                 <td style="text-align: center;"> <a  href="{{ url('/hasilStudi/tabelNilai/' . $data->id) }}" data-placement="top" data-original-title="Detail" data-toggle="tooltip"  class="btn btn-info" align="center"><i style="color: white" class="fa  fa-eye">  Detail</i></a></td> 

            </tr> 
            <?php $count++; ?> 
            @endforeach 

             @foreach($unggahNilai1 as $data) 
            <tr>
                <td style="text-align:center;">{{ $count }}.</td>
                <td style="text-align:left;">{{ $data->kodematkul }}</td> 
                <td style="text-align:left;">{{ $data->namamatkul }}</td> 
                <td style="text-align:left;">Mata Kuliah Umum</td> 
                 <td style="text-align: center;"> <a  href="{{ url('/hasilStudi/tabelNilai/' . $data->id) }}" data-placement="top" data-original-title="Detail" data-toggle="tooltip"  class="btn btn-info" align="center"><i style="color: white" class="fa  fa-eye">  Detail</i></a></td> 

            </tr> 
            <?php $count++; ?> 
            @endforeach       
            

        </tbody>
     </table> 
   </ol>
 </div>
</div>
</div>
</form>
</div>
</div>
{{ $unggahNilai->links() }}

@endsection
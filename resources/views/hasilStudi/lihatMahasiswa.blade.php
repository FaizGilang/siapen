@extends('layouts.pengguna')

@section('heading')
<header class="head">
    <div class="search-bar">
        <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
          
                                    <!-- /.main-search -->                             
    </div>
                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;User</h3>
    </div>
                            <!-- /.main-bar -->
</header>
                        <!-- /.head -->
@endsection

@section('content')
<style>
{
  font-family: Arial;
}

* {
  box-sizing: border-box;
}

form.example input[type=text] {
  padding: 10px;
  font-size: 17px;
  border: 2px solid grey;
  float: left;
  width: 80%;
  background: #f1f1f1;
}

form.example button {
  float: left;
  width: 20%;
  padding: 30px;
  background: #2196F3;
  color: white;
  font-size: 17px;
  border: 1px solid grey;
  border-left: none;
  cursor: pointer;
}

form.example button:hover {
  background: #0b7dda;
}

form.example::after {
  content: "";
  clear: both;
  display: table;
}

</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="row">
          <div class="col-md-12">
            <div class="row">
            </div>
          </div>
          </div>
<div class="row">
    <div class="col-lg-12"><br>
        <h1 style="font-family: Times New Roman; font-weight: bold;"  align="center">
            DAFTAR MAHASISWA
        </h1>
        
        <ol class="breadcrumb">
    </div>
</div>
 <div class="row">
            
            </div>

<!-- /.row -->
<div class="card-body">
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
<form class="form-inline ml-3" method="post"  action="{{ url('/hasilStudi/lihatMahasiswa') }}">

            {{ csrf_field() }}
  
      




</form>
<br>
        <table class="table table-bordered table-striped table-condensed cf">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Nomor Induk Mahasiswa</th>
                <th style="text-align:center;">Nama Mahasiswa</th>
                <th style="text-align:center;">Program Studi</th>
                <th style="text-align:center;">Aksi</th>
            </tr>
        </thead>
            
        <tbody>
       <?php $count = 1; ?> 
            @foreach($user as $data) 
            <tr> 
                <td style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:left;">{{ $data->nomorInduk }}</td>
                <td style="text-align:left;">{{ $data->nama }}</td> 
                <td style="text-align:left;">{{ $data->namaprodi }}</td> 
                <td>
                    <a style="margin-left: 130px;" data-placement="top" data-original-title="Detail" data-toggle="tooltip" href="{{ url('/hasilStudi/detailNilai/' . $data->id) }}" class="btn btn-info" align="center"><i style="color: white" class="fa  fa-eye">  Detail Nilai</i></a> 
                   </td>
            </tr> 
            <?php $count++; ?> 
            @endforeach 
        </tbody>
     </table> 
 {{ $user->links() }}
   
     <br>
    
    </div>
    </div>
    </div>


@endsection

<!-- TAMPILAN FORM EDIT PROGRAM STUDI BARU -->

@extends('layouts.pengguna')


@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}
            <div class="card card-primary">
              <div class="card-header">
                <h1 style="margin-left: 500px; font-family: Times New Rowman;"  class="card-title">Ubah Nilai Mahasiswa</h1>
              </div>
               <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">

            <div class="form-group {{ $errors->has('idUser') ? 'has-error' : '' }}">
                <label>Nomor Induk Mahasiswa:</label>
                <input selected disabled type="text" name="idUser" class="form-control" value="{{ $ubahNilai->namauser }}" placeholder="NIM">
                {!! $errors->first('idUser', '<p class="help-block">:message</p>') !!}
            </div>

             <div class="form-group {{ $errors->has('idMatkul') ? 'has-error' : '' }}">
                <label>Mata Kuliah:</label>
                <input selected disabled type="text" name="idMatkul" class="form-control" value="{{ $ubahNilai->namamatkul }}" placeholder="NIM">
                {!! $errors->first('idMatkul', '<p class="help-block">:message</p>') !!}
            </div>


            <div class="form-group {{ $errors->has('nilaiAkhir') ? 'has-error' : '' }}">
                <label>Nilai Akhir:</label>
                <select name="nilaiAkhir" class="form-control" placeholder="Nilai Akhir" value="{{ $ubahNilai->nilaiAkhir }}">
                
                        <option  value="" selected disabled>{{ $ubahNilai->nilaiAkhir }}</option>
                        <option value="A">A</option>
                        <option value="A-">A-</option>
                        <option value="B+">B+</option>
                        <option value="B">B</option>
                        <option value="B-">B-</option>
                        <option value="C+">C+</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                        <option value="E">E</option>
                    </select>
                {!! $errors->first('nilaiAkhir', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="card-footer">   
            <a style="margin-left: 10px" href="{{ url('/hasilStudi/lihatMahasiswa') }}" class="fa fa-arrow-circle-left fa-2x"/></a>           
                <input style="margin-left: 500px" type="submit" name="save" value="Perbarui" class="btn btn-md btn-success">
                <a href="{{ url('/hasilStudi/lihatMahasiswa') }}"  name="reset" class="btn btn-danger" value="Batal" /> Batal   </a>             
            </div>
        </form>
    </div>
</div>
@endsection


@extends('layouts.pengguna')

@section('content')
<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
    
}
</style>
 <div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="row">
          <div class="col-md-12">
            <div class="row">
            </div>
          </div>
          </div>
        <div class="row">
        <div class="col-lg-12"><br>
        <h5 class="header" style=" font-family: Times New Roman; font-weight: bold; margin-left: 0px; " align="center">
            DETAIL NILAI MAHASISWA <br> 
            <hr/>
            
    </div>
     <pre style=" font-family: Times New Roman; ">
                <br>
                Nama                : {{ $profil->namamhs }}<br>
                NIM                  : {{ $profil->nomorInduk }}<br>
                Program Studi  : {{ $profil->namaprodi }}
                 
            </pre>
</div>
        
       <!-- /.row -->
<form class="col-md-12">
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered table-condensed cf">
        <thead>
           <tr>
               <th style="text-align:center;">No.</th>
               <th style="text-align:center;">Kode MK</th>
                <th style="text-align:center;">Mata Kuliah</th>
                <th style="text-align:center;">Semester</th>
                <th style="text-align:center;">Nilai Akhir</th>
                <th style="text-align:center;">Aksi</th>
            </tr>
        </thead>
            
         <tbody>
          <?php 
              $count = 1;
          ?> 
            
            @foreach($data as $data)
            <tr> 
               <td style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:left;">{{ $data->kodematkul }}</td>
                <td style="text-align:left;">{{ $data->namamatkul }}</td>
                <td style="text-align:center;">{{ $data->komulatif }}</td>
                <td style="text-align:center;">{{ $data->nilaiAkhir }}</td>
                <td style="text-align:center;"><a  data-placement="top" data-original-title="Edit" data-toggle="tooltip" href="{{ url()->current() . '/' . $data->id }}" class="btn btn-warning" align="center"><i style="color: white" class="fa  fa-pencil">  Ubah</i></a>
        
                </td>

           </tr>
           <?php $count++; ?> 
           @endforeach
         </tbody>

     </table> 
     <div class="card-footer">
       <a style="margin-left: 10px" href="{{ url('/hasilStudi/lihatMahasiswa') }}" class="fa fa-arrow-circle-left fa-2x"/></a>   
        
    </div> 
      </div>
      </form>
     <!--  <div align="center">
        <a class="btn btn-md btn-success" href="/hasilStudi/pdfTranskrip">Cetak Transkrip</a>
    </div> -->
    <br>
     <br>
     </div>
</form>   


    
@endsection



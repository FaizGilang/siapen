<!-- MENAMPILKAN STRUKTUR KURIKULUM YANG SUDAH DIPILIH MATKULNYA -->

@extends('layouts.pengguna')

@section('content')

<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}

</style>

     <div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="row">
          <div class="col-md-12">
            <div class="row">
            </div>
          </div>
          </div>

   
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="row">
          <div class="col-md-12">
            <div class="row">
            </div>
          </div>
          </div>
        <br>
        <div class="row">
        <div class="col-lg-12"><br>
      
        <center><h1 style="font-family: Times New Roman; font-weight: bold;">INPUT NILAI MAHASISWA</h1>{{ $coba->namaprodi}}_{{ $coba->namamatkul}}</center><br>
                <ol class="breadcrumb">
            </div>
        </div>
        <div class="card-body">
        <div class="col-lg-12">
            <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}
             <div>
                    <label>Semester Komulatif:</label>
                    <select class="form-control" name="komulatif">
                        <option value="" selected disabled>-Pilih Semester Komulatif-</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select>
                    {!! $errors->first('komulatif', '<p class="text-danger">:message</p>') !!}
                </div><br>
            <table class="table table-bordered table-striped table-condensed cf">
                <thead>
                <tr>
                <th style="text-align:center;">NIM</th>
                <th style="text-align:center;">Nama</th>
                <th style="text-align:center;">Nilai</th>
                
                </tr>
                </thead>    
            <tbody>
          <?php $count = 1; ?> 
            @foreach($tabelNilaiA as $data) 
            <tr>
                <input  type="hidden"  name="nama[]" class="form-control" placeholder="Nama Kategori Pengguna" value="{{ $data->id}}">
                <td style="text-align:left;">{{ $data->nomorInduk }} </td>
                <td style="text-align:left;">{{ $data->nama }} </td>
                <td>
                    <select class="form-control" required name="nilaiAkhir[]">
                        <option value="" selected disabled>-Pilih Nilai-</option>
                        <option value="A">A</option>
                        <option value="A-">A-</option>
                        <option value="B+">B+</option>
                        <option value="B">B</option>
                        <option value="B-">B-</option>
                        <option value="C+">C+</option>
                        <option value="C">C</option>
                        <option value="D">D</option>
                        <option value="E">E</option>
                    </select>
                    {!! $errors->first('nilaiAkhir', '<p class="text-danger">:message</p>') !!}
                </td>

            </tr> 
            <?php $count++; ?> 
            @endforeach    

        </tbody>
     </table>
     <div class="card-footer">
       <a style="margin-left: 10px" href="{{ url('/hasilStudi/nilaiMahasiswa') }}" class="fa fa-arrow-circle-left fa-2x"/></a>   
        <input style="margin-left: 600px" type="submit" name="save" value="Simpan" class="btn btn-md btn-success">
    </div> 
    </div>
    <br>
    </form>
     <br>
     <br>
        <br>
          <br>
          <br>
          <br>
          <br>
             <br>
          <br>
          <br>
          <br>
          <br>
     <br>
@endsection
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<style>
img{
    height: 120px;
    width: 80px;
    padding: -30px 30px 10px 10px; 
}
.label1 {;
  min-width: 8px !important;
  display: inline-block !important
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td{
    padding: 5px 35px 15px 20px;
    text-align: center;   

}
tr{
    text-align: center;
}
p{
    text-align: center;
    font-size: 18;
    margin-left: 150px;
}
h6{
    text-align: center;
    font-size: 18;
    margin-left: 200px;
}
</style>

<div class="card card-primary">
<form role="form">
<div class="card-body">
<div class="row">
    <div class="col-lg-12">

            <img style="float: left;" src="admin/img/PNJ.jpg" ><img style="float: right; margin-left: 0px;"  src="admin/img/admin.jpg" >
           <h3 class="img"  style=" font-family: Times New Roman; font-weight: bold; margin-left: 30px; " align="center">
            POLITEKNIK NEGERI JAKARTA <br> JURUSAN {{$jur}} <br> PROGRAM STUDI DILUAR DOMISILI DIPLOMA 2 AKN KABUPATEN DEMAK</h3><hr/>
           <h3 style="font-family: Times New Roman; font-weight: bold;" align="center">
            TRANSKRIP NILAI</h3><hr/>
            <pre style=" font-family: Times New Roman; ">
                <br>
                Nama                : {{ $profil->nama }}<br>
                NIM                  : {{ $profil->nomorInduk }}<br>
                Program Studi  : {{ $profil->namaprodi }}<br>
               
            </pre>
    </div>
</div>

<!-- /.row -->
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered">
        <thead>
           <tr>
               <th  style="text-align:center; font-size: 10px;">No.</th>
                <th  style="text-align:center; font-size: 10px;">Kode MK</th>
                <th  style="text-align:center; font-size: 10px;">Mata Kuliah</th>
                <th  style="text-align:center; font-size: 10px;">SMT</th>
                <th  style="text-align:center; font-size: 10px;">Nilai (N)</th>
                <th  style="text-align:center; font-size: 10px;">Kredit (K)</th>
                <th  style="text-align:center; font-size: 10px;">NxK</th>
            </tr>
        </thead>
            
         <tbody>

            <?php 
              $count = 1;
              $jumKiNi = 0;
              $Ki = 0;
              $bobot = 0;
          ?> 
            
            @foreach($nilai as $data)
                 
            <tr> 
               <td  style="text-align:center; font-size: 10px;">{{ $count }}.</td> 
                <td style="text-align:center; font-size: 10px;">{{ $data->kode }}</td>  
                <td style="text-align:left; font-size: 10px; width: 250px;">{{ $data->nama }}</td>
                <td style="text-align:center; font-size: 10px;">{{ $data->komulatif }}</td>
                <td style="text-align:center; font-size: 10px;">{{ $data->nilaiAkhir }}</td>
                <td style="text-align:center; font-size: 10px;">{{ $data->SKS }}</td>
                 @if($data->nilaiAkhir==='A') 
                    <?php $bobot=4.0 ?>
                    <td style="text-align:center; font-size: 10px;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='A-')
                    <?php $bobot=3.7 ?>
                    <td style="text-align:center; font-size: 10px;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='B+')
                    <?php $bobot=3.3 ?>
                    <td style="text-align:center; font-size: 10px;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='B')
                    <?php $bobot=3.0 ?>
                    <td style="text-align:center; font-size: 10px;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='B-')
                    <?php $bobot=2.7 ?>
                    <td style="text-align:center; font-size: 10px;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='C+')
                    <?php $bobot=2.5 ?>
                    <td style="text-align:center; font-size: 10px;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='C')
                    <?php $bobot=2.0 ?>
                    <td style="text-align:center; font-size: 10px;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='D')
                    <?php $bobot=1.0 ?>
                    <td style="text-align:center; font-size: 10px;">{{ $data->SKS * $bobot}}</td>
                    @elseif($data->nilaiAkhir==='E')
                    <?php $bobot=0 ?>
                   <td style="text-align:center; font-size: 10px;">{{ $data->SKS * $bobot}}</td>
                   @elseif($data->nilaiAkhir==='-')
                    <?php $bobot=0 ?>
                   <td style="text-align:center; font-size: 10px;">{{ $data->SKS * $bobot}}</td>
                   
                @endif 
                 
                    <?php $jumKiNi = $jumKiNi+$data->SKS * $bobot ?>
                    <?php $Ki = $Ki+$data->SKS ?>
                                 
            </tr>
             <?php $count++; ?> 
            @endforeach  

           
            <tr>
                <td style="text-align:center; font-size: 10px;" colspan="5">Jumlah:</td>
                @if($Ki == 0)
                <td style="text-align:center; font-size: 10px;" colspan="1"> - </td>
                @else
                <td style="text-align:center; font-size: 10px;" colspan="1">{{ $Ki }}</td>
                @endif    
                <td style="text-align:center; font-size: 10px;" colspan="">{{ $jumKiNi}}</td>
            </tr>
            
            <tr>
                <td style="text-align:center; font-size: 10px;" colspan="5">Indeks Prestasi Komulatif (IPK):</td>    
                @if($Ki == 0)
                <td style="text-align:center; font-size: 10px;" colspan="1"> - </td>
                @else
                <td style="text-align:left; font-size: 10px;" colspan="2">&emsp; <?php $ipk=$jumKiNi/$Ki; $hasil=round($ipk,2); echo "$hasil"; ?></td>
                @endif    
            </tr>   
        </tbody>
     </table> 

           


</body>
<br>
<br>
     <?php
    function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    ?>

        <p align="right"><?php echo "Demak, " . (tgl_indo(date('Y-m-d')));?> &emsp; &emsp; </p>
        <h6 class="center" style="text-align:center">
        &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; Ketua Program Studi, <br><br><br>
        </h6>
        <p class="center" style="text-align:center">
          &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;   {{$kaprodi->kaprodi1}}  <br> 
          &emsp; &emsp; &emsp;  &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;  {{$kaprodi->nip1}} 
        </p>

</html>


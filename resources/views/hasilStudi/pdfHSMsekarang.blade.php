<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<style>
img{
    height: 100px;
    width: 80px;
    padding: -30px 30px 10px 10px; 
}
.label1 {;
  min-width: 8px !important;
  display: inline-block !important
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td{
    padding: 5px 35px 15px 20px;
    text-align: center;   

}
tr{
    text-align: center;
}
p{
    text-align: center;
    font-size: 18;
    margin-left: 150px;
}
h6{
    text-align: center;
    font-size: 18;
    margin-left: 200px;
}
</style>

<div class="card card-primary">
<form role="form">
<div class="card-body">
<div class="row">
    <div class="col-lg-12">
        <img style="float: left;" src="admin/img/PNJ.jpg" ><img style="float: right; margin-left: 0px;"  src="admin/img/admin.jpg" >
         <h3 class="img"  style=" font-family: Times New Roman; font-weight: bold; margin-left: 30px; " align="center">
       
            POLITEKNIK NEGERI JAKARTA <br> JURUSAN {{$jur}} <br> PROGRAM STUDI DILUAR DOMISILI DIPLOMA 2 AKN DEMAK
           <hr/> <br> HASIL STUDI MAHASISWA </h3><hr/>

            <pre style="font-family: Times New Roman; ">
                <br>
                Nama          : {{ $profil->nama }} &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;  Program Studi  : {{ $profil->namaprodi }}<br>
                NIM            : {{ $profil->nomorInduk }}&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;   Tahun Akademik : <?php echo strtoupper ($semt->tahun) ?><br>
                Semester    : {{ $semester}} 
            </pre>
    </div>
</div>

<!-- /.row -->
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered">
        <thead>
           <tr>
            
                <th colspan="2" style="text-align:center; ">Mata Kuliah</th>
                <th  style="text-align:center;">Nilai (N)</th>
                <th style="text-align:center;">Kredit (K)</th>
                <th style="text-align:center;">N x K</th>
                <th style="text-align:center;">JPS</th>
            </tr>
        </thead>
            
        <tbody>
             <?php 
              $count = 1;
              $jumKiNi = 0;
              $Ki = 0;
              $jumlahjpm = 0;
              ?> 


            @foreach($nilaib as $data)


            <tr> 
               <td width="5px" style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:left;">{{ $data->nama }}</td>

                <?php
                $data->nilai = (($data->uts)*($data->bobotUts/100)) + (($data->uas)*($data->bobotUas/100)) + (($data->tugas)*($data->bobotTugas/100)) + (($data->praktek)*($data->bobotPraktek/100));
                ?>
               
                @if($data->nilai>=79.6 && $data->nilai<=100) 
                    <td style="text-align:center;">A</td>
                    <?php $bobot=4.0 ?>
                    @elseif($data->nilai>=75.6 && $data->nilai<=79.5)
                    <td style="text-align:center;">A-</td>
                    <?php $bobot=3.7 ?>
                    @elseif($data->nilai>=71.6 && $data->nilai<=75.5)
                    <td style="text-align:center;">B+</td>
                    <?php $bobot=3.3 ?>
                    @elseif($data->nilai>=67.6 && $data->nilai<=71.5)
                    <td style="text-align:center;">B</td>
                    <?php $bobot=3.0 ?>
                    @elseif($data->nilai>=63.6 && $data->nilai<=67.5)
                    <td style="text-align:center;">B-</td>
                    <?php $bobot=2.7 ?>
                    @elseif($data->nilai>=59.6 && $data->nilai<=63.5)
                    <td style="text-align:center;">C+</td>
                    <?php $bobot=2.5 ?>
                    @elseif($data->nilai>=55.6 && $data->nilai<=59.5)
                    <td style="text-align:center;">C</td>
                    <?php $bobot=2.0 ?>
                    @elseif($data->nilai>=40.6 && $data->nilai<=55.5)
                    <td style="text-align:center;">D</td>
                    <?php $bobot=1.0 ?>
                    @elseif($data->nilai>=0 && $data->nilai<=40.5)
                    <td style="text-align:center;">E</td>
                    <?php $bobot=0 ?>
                    @elseif($data->nilai<=0)
                    <td style="text-align:center;">-</td>
                    <?php $bobot=0 ?>
                    @elseif($data->nilai>100)
                    <td style="text-align:center;">-</td>
                    <?php $bobot=0 ?>
                    @endif 
                     <td style="text-align:center;">{{ $data->SKS }}</td>
                     <td  style="text-align:center;">{{ $data->SKS * $bobot}}</td>
                     <td style="text-align:center;">{{ $data->jpm*16 }}</td>

                    <?php $jumKiNi = $jumKiNi+$data->SKS * $bobot ?>
                    <?php $Ki = $Ki+$data->SKS ?>
                    <?php $jumlahjpm = $jumlahjpm+$data->jpm*16 ?>
            </tr>
              <?php $count++; ?>
            @endforeach 
            {{-- {{ $data->bobot*$data->SKS++}} --}}

    <tr>
         <td style="text-align:center;" colspan="2">JUMLAH:</td>  
                @if($Ki == 0)
                <td style="text-align:center;" colspan="1"> - </td>
               
                @else
                <td style="text-align:center;" colspan="1"></td>
                @endif
                <td style="text-align:center;" colspan="1">a= {{ $Ki}}</td> 
                <td style="text-align:center;" colspan="1">b= {{ $jumKiNi}}</td> 
                <td style="text-align:center;" colspan="1">{{ $jumlahjpm}}</td> 
    </tr>
                  
            
            <tr>

                 <td style="text-align:center;" colspan="1">Indeks Prestasi</td>    
                 <td style="text-align:center;" colspan="1">Kelakuan</td>    
                
                <td style="text-align:center;" colspan="2">Absensi</td>    
                 <td style="text-align:center;" colspan="2">Status</td>   
              
            </tr> 
            <tr>
                {{$absen->izin}}
                {{$absen->alpa}}
                <?php $jumlahabsen = $absen->izin+$absen->alpa  ?> Jumlah: {{$jumlahabsen}}

                @if($Ki == 0)
                 <td style="text-align:center;" colspan="1"> IP=b/a= - </td>
                 <td style="text-align:center;" colspan="1"> - </td>
                 <td style="text-align:left;" colspan="2"> Izin: 0 <br> Tanpa Izin: 0 <br> Jumlah: 0</td>
                 <td style="text-align:center;" colspan="2"> - </td>
                
                @else
                 <td style="text-align:center;" colspan="1">IP=b/a=<?php $ip=$jumKiNi/$Ki; $hasil=round($ip,2); echo "$hasil"; ?></td>    
                 <td style="text-align:center;" colspan="1"><?php if($jumlahabsen<=5) echo "BAIK"; else echo "CUKUP BAIK"?></td>    
                
                <td  style="text-align:left;" colspan="2">Izin: {{$absen->izin}} <br> Tanpa Izin: {{$absen->alpa}} <br>  <?php $jumlahabsen = $absen->izin+$absen->alpa  ?> Jumlah: {{$jumlahabsen}}</td>    
                 <td style="text-align:center;" colspan="2"><?php if($ip>=2.5) echo "LULUS"; else echo "TIDAK LULUS"?></td>  
                @endif    
            </tr>  
            <tr>
                 <td style="text-align:center;" colspan="3">Ketua Jurusan</td>    
                 <td style="text-align:center;" colspan="3">Koordinator Program</td>       
                
            </tr>
            <tr>
                 <td style="text-align:center;" colspan="3"><?php echo (tgl_indo(date('Y-m-d')));?></td>    
                 <td style="text-align:center;" colspan="3"><?php echo (tgl_indo(date('Y-m-d')));?></td>       
                
            </tr>
             <tr>
              <td style="text-align:center;" colspan="3"><br><br><br>{{$kajur->kajur}}<br>{{$kajur->nip}}</td>
              <td style="text-align:center;" colspan="3"><br><br><br>{{$kaprodi->kaprodi1}}<br>{{$kaprodi->nip1}}</td>
            </tr>  
            <tr>
                 <td style="text-align:left;" colspan="3">Keterangan:<br>a = jumlah kredit <br> b = jumlah (N x K)<br>IP = Indeks Prestasi</td>    
                 <td style="text-align:left;" colspan="3">
                    A  = 4   (Sangat Istimewa) <br>
                    A- = 3.7 (Istimewa) <br>
                    B+ = 3.3 (Lebih dari Baik) <br>
                    B- = 3   (Baik) <br>
                    C+ = 2.7 (Cukup Baik) <br>
                    C  = 2.3 (Lebih dari Cukup) <br>
                    D  = 1   (Kurang) <br>
                    E  = 0   (Gagal) <br>
                </td>       
                
            </tr>   
        </tbody>
     </table> 

           


</body>
<br>
<br>
    <?php
    function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    ?>

</html>


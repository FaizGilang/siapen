
 @extends('layouts.pengguna')

@section('content')

<div  class="">
  
    <div >
      <div >
        <div style="margin-left: 500px; margin-top: 50px;" class="images"> <br>
          <img src="{{URL::asset('/FotoPengguna/'.Auth::user()->file)}}" height="180" width="130">
        </div>
        <div>
          <h1 style="margin-left: 430px; font-size: 150px;">Hai <b>{{Auth::user()->nama}}..</b></h1><br>
          <h5 style="margin-left: 430px;">Selamat Datang di Sistem Ini! </h5><br><br><br>
              
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                    {{ session('status') }}
              </div>
            @endif
        </div>
      </div>
    </div>
  
</div>
@endsection



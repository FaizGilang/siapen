<!-- TAMPILAN FORM TAMBAH PROGRAM STUDI BARU -->

@extends('layouts.home')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;prodi</h3>
    </div>   
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>    <!-- /.main-bar -->
</header>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url('/prodi/tambah') }}">  <!-- url ROUTE /prodi/add -->
            {{ csrf_field() }}
          <div class="card card-primary">
              <div class="card-header">
                <h3 style="margin-left: 450px; font-family: Times New Rowman;" class="card-title">Tambah Program Studi Baru</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  
                  <div class="form-group {{ $errors->has('kode') ? 'has-error' : '' }}">
                    <label>Kode Program Studi:</label>
                    <input type="string" name="kode" class="form-control" value="{{ old('kode') }}" placeholder="Kode Program Studi">
                    {!! $errors->first('kode', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div class="form-group {{ $errors->has('nomor') ? 'has-error' : '' }}">
                    <label>Nomor Program Studi:</label>
                    <input type="string" name="nomor" class="form-control" value="{{ old('nomor') }}" placeholder="Nomor Program Studi">
                    {!! $errors->first('nomor', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                    <label>Nama Program Studi:</label>
                    <input type="string" name="nama" class="form-control" placeholder="Nama Program Studi" value="{{ old('nama') }}">
                     {!! $errors->first('nama', '<p class="text-danger">:message</p>') !!}
                  </div>
<br>
<br>
                </div>
                <br>
<br>
<br>
<br>
                <!-- /.card-body -->
                <div class="card-footer">
                    <a style="margin-left: 10px" href="{{ url('/prodi/index') }}" class="fa fa-arrow-circle-left fa-2x"/></a>                     <input style="margin-left: 450px" type="submit" name="save" value="Simpan" class="btn btn-md btn-success">
                    <input type="reset" name="reset" class="btn btn-danger" value="Batal" />                 
                </div>

              </form>
            </div>

        </form>
    </div>
<br>
<br>
<br>
<br>
</div>

@endsection

<?php if(Session::has('register_success')): ?>
    <div class="message message-success">
        <span class="close"></span>
        <?php echo Session::get('register_success') ?>
    </div>
<?php endif; ?>
<!-- TAMPILAN FORM EDIT PROGRAM STUDI BARU -->

@extends('layouts.home')

@section('heading')
<!-- Page Heading -->
<header class="head">
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Prodi</h3>
    </div>
    <!-- /.main-bar -->
</header>
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}
            <div class="card card-primary">
              <div class="card-header">
                <h3 style="margin-left: 500px; font-family: Times New Rowman;" class="card-title">Ubah Program Studi</h3>
              </div>
               <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">

            <div class="form-group {{ $errors->has('kode') ? 'has-error' : '' }}">
                <label>Kode Program Studi:</label>
                <input type="text" name="kode" class="form-control" value="{{ $prodi->kode }}" placeholder="Kode Program Studi">
                {!! $errors->first('kode', '<p class="text-danger">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('nomor') ? 'has-error' : '' }}">
                    <label>Nomor Program Studi:</label>
                    <input type="string" name="nomor" class="form-control" value="{{ $prodi->nomor }}" placeholder="Nomor Program Studi">
                    {!! $errors->first('nomor', '<p class="text-danger">:message</p>') !!}
                  </div>

            <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                <label>Nama Program Studi:</label>
                <input type="text" name="nama" class="form-control" placeholder="Nama Program Studi" value="{{ $prodi->nama }}">
                {!! $errors->first('nama', '<p class="text-danger">:message</p>') !!}
            </div>
                <br>
<br>
<br>
<br>
                <br>
<br>
<br>
            <div class="card-footer"> 
                <a style="margin-left: 10px" href="{{ url('/prodi/lihat') }}" class="fa fa-arrow-circle-left fa-2x"/></a>          
                <input style="margin-left: 450px" type="submit" name="save" value="Perbarui" class="btn btn-md btn-success">
                <a href="{{ url('/prodi/lihat') }}"  name="reset" class="btn btn-danger" value="Batal" /> Batal   </a>             
            </div>
        </form>
    </div>
</div>
@endsection

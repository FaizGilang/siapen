<!-- TAMPILAN FORM TAMBAH PROGRAM STUDI BARU -->

@extends('layouts.home')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;prodi</h3>
    </div>       <!-- /.main-bar -->
</header>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url('/kurikulum/tambah') }}">  <!-- url ROUTE /prodi/add -->
            {{ csrf_field() }}
          <div class="card card-primary">
              <div class="card-header">
                <h3 style="margin-left: 450px;  font-family: Times New Rowman;" class="card-title">Tambah Kurikulum</h3>
              </div>

            @if(Session::has('alert-success'))
                {{Session::get('alert-success')}}<br>
            @endif
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                
                   <div>
                      <label>Program Studi:</label>
                      <select class="form-control" name="idProdi">
                        <option value="" selected disabled>-Pilih Prodi-</option>
                        @foreach($prodi as $a)
                        <option value="{{ $a->id }}">{{ $a->nama }}</option>
                        @endforeach
                      </select>
                      {!! $errors->first('idProdi', '<p class="text-danger">:message</p>') !!}
                    </div>

                    <br>
                  
                  <div class="form-group {{ $errors->has('tahun') ? 'has-error' : '' }}">
                    <label>Tahun:</label>
                    <input type="integer" name="tahun" class="form-control" value="{{ old('tahun') }}" placeholder="ex: 2018">
                    {!! $errors->first('tahun', '<p class="text-danger">:message</p>') !!}
                  </div>                

                </div>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
                <!-- /.card-body -->
                <div class="card-footer">
                    <a style="margin-left: 10px" href="{{ url('/kurikulum/lihat') }}" class="fa fa-arrow-circle-left fa-2x"/></a>               
                    <input  style="margin-left: 450px" type="submit" name="save" value="Simpan" class="btn btn-md btn-success">
                    <input type="reset" name="reset" class="btn btn-danger" value="Batal" />                 
                </div>

              </form>
            </div>
        </form>
    </div>
</div>

    <!DOCTYPE html>
<html>
<head>
    <title>Bootstrap Part 15 : Membuat Modal dengan Bootstrap</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.js"></script>  
</head>
<body>
   <div class="container">      
   
   
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- konten modal-->
            <div class="modal-content">
                <!-- heading modal -->
               
                <!-- body modal -->
                <div class="modal-body">
                    <h2 class="text-danger">Data Yang Ada Sudah Ada</h2>
                </div>
               
            </div>
        </div>
    </div>
   </div>
</body>
</html>
@endsection
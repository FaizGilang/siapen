<!-- TAMPILAN PROGRAM STUDI (tabel prodi) -->

@extends('layouts.home')

@section('heading')
<header class="head">
    <div class="search-bar">
        <div class="main-search">
            <div class="input-group">
                <input type="text" onkeyup="search()" class="form-control" id="search" placeholder="Live Search ...">
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm text-muted" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>  
        </div>      
                                    <!-- /.main-search -->                             
    </div>                     
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Kurikulum</h3>
    </div>
                            <!-- /.main-bar -->
</header>
                        <!-- /.head -->
@endsection

@section('content')
<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="row">
          <div class="col-md-12">
            <div class="row">
            </div>
          </div>
          </div>
        <div class="row">
            <div class="col-lg-12">
                <h1 style="font-family: Times New Roman; font-weight: bold;" class="page-header" align="center"><br>
                    DATA KURIKULUM
                </h1>
                <br>
                <ol class="breadcrumb">
            </div>
        </div>
         <div class="row">
            <div style="margin-left: 30px;" class="col-md-2">
              <a href="{{ url('/kurikulum/tambah') }}" class="btn btn-info" style="width:100%; margin-right:0px !important; margin-left:0px !important;">
                <h4 class="fa fa-plus nav-icon"> Kurikulum</h4>
              </a>
            </div>
            </div>

<!-- /.row -->
<div class="card-body">
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered table-striped table-condensed cf">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Daftar Kurikulum</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Aksi</th>
            </tr>
        </thead>
            
        <tbody>
            <?php $count = 1; ?> 
            @foreach($kurikulum as $data) 
            <tr> 
                <td style="text-align:center;">{{ $count }}.</td>  
                <td style="text-align:left;">{{ $data->namaprodi }} / {{ $data->tahun}}</td> 
                <td style="text-align:center;">{{ $data->status }} </td> 
                
                <td> 
                @if($data->status==="tidak")
                 <a style="margin-left: 150px;" data-placement="top" data-original-title="Status" data-toggle="tooltip" href="{{ url('/kurikulum/status/' . $data->id) }}" class="btn btn-success" align="center"><i style="color: white" class="fa  fa-check-circle"">  Aktif</i></a>||
                 <a class="delete-modal btn btn-danger" data-id="{{$data->id}}" ><i class="fa fa-trash" style="color: white" class="glyphicon glyphicon-trash">  Hapus     </i></a>  ||  
                 @else
                 <span style="margin-left: 150px;" class="btn btn-default"><i class="fa fa-check-circle">  Aktif</i></span> ||
                 <a data-placement="top" data-original-title="Delete" data-toggle="tooltip" value="hidden" class="btn btn-default"><i class="fa fa-trash">  Hapus     </i></a> ||
                @endif
                 
                    <a  data-placement="top" data-original-title="Edit" data-toggle="tooltip" href="{{ url('/kurikulum/ubah/' . $data->id) }}" class="btn btn-warning" align="center"><i style="color: white" class="fa  fa-pencil">  Ubah</i></a>
            </tr> 
            <?php $count++; ?> 
            @endforeach 
        </tbody>
     </table> 
    </div>
                <br>
<br>
<br>
<br>
                <br>
                

<!-- Modal form to delete a form -->
     <div id="deleteModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    
                </div>
                <div class="modal-body">
                    <h5 class="text-center">Apakah Anda yakin ingin menghapus data ini?</h5>
                    <br />
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id">ID:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="id_delete" disabled>
                            </div>
                        </div>
                      
                   </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger delete" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-trash'></span> Hapus
                        </button>
                        <button type="button" style="color:white" class="btn btn-info" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Batal
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <script type="text/javascript">
// delete a post
        // delete a post
        $(document).on('click', '.delete-modal', function() {
            $('.modal-title').text('Delete');
            $('#id_delete').val($(this).data('id'));              
            $('#deleteModal').modal('show');
            id = $('#id_delete').val();
        });
        $('.modal-footer').on('click', '.delete', function() {
            $.ajax({
                type: 'get',
                url: '/kurikulum/hapus/' + id,
                success: function(data) {
                    window.location.href = '{{ url('/kurikulum/lihat')}}';
                  }
            });
        });
        </script>
@endsection


<!-- TAMPILAN FORM EDIT PROGRAM STUDI BARU -->

@extends('layouts.home')

@section('heading')
<!-- Page Heading -->
<header class="head">
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Prodi</h3>
    </div>
    <!-- /.main-bar -->
</header>
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}
            <div class="card card-primary">
              <div class="card-header">
                <h3 style="margin-left: 500px; font-family: Times New Rowman;" class="card-title"> Ubah Kurikulum</h3>
              </div>
               <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">

            <div class="form-group {{ $errors->has('idProdi') ? 'has-error' : '' }}">
                <label>Program Studi:</label>
                <select class="form-control" name="idProdi">
                    <option value="{{ $kurikulum->idProdi }}">{{$kurikulum->namaprodi}}</option>
                        @foreach($prodi as $a)
                        <option value="{{ $a->id }}">{{ $a->nama }}</option>
                        @endforeach
                    </select>
            </div>

            <div class="form-group {{ $errors->has('tahun') ? 'has-error' : '' }}">
                <label>Tahun:</label>
                <input type="text" name="tahun" class="form-control" placeholder="Tahun" value="{{ $kurikulum->tahun }}">
                {!! $errors->first('tahun', '<p class="text-danger">:message</p>') !!}
            </div>
                               <br>
<br>
<br>
<br>
                <br>
<br>
<br>
<br>
                <br>
<br>
<br>
            <div class="card-footer">  
                <a style="margin-left: 10px" href="{{ url('/kurikulum/lihat') }}" class="fa fa-arrow-circle-left fa-2x"/></a>           
                <input style="margin-left: 500px" type="submit" name="save" value="Perbarui" class="btn btn-md btn-success">
                <a href="{{ url('/kurikulum/lihat') }}"  name="reset" class="btn btn-danger" value="Batal" /> Batal   </a>             
            </div>
        </form>
    </div>
</div>
@endsection

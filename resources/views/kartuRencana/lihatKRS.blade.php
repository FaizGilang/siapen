<!-- TAMPILAN MATA KULIAH (tabel prodi) -->

@extends('layouts.pengguna')

@section('content')
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<style>
.img1{
    height: 110px;
    width: 150px;
    padding: -30px 30px 10px 10px; 
}
.header{
    
    width: 1800px;
     
}
.label1 {;
  min-width: 8px !important;
  display: inline-block !important
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td{
    padding: 6px 35px 15px 20px;
    text-align: center;   

}
tr{
    text-align: center;
}
p{
    text-align: center;
    font-size: 18;
    margin-left: 150px;
}
h6{
    text-align: center;
    font-size: 18;
    margin-left: 200px;
}
</style>
<div class="card card-primary">
<form role="form">
<div class="card-body">
<div class="row">
    <div class="col-lg-12">
          <!-- <img class="img1" style="float: left; margin-left: 50px;" src="{{asset('admin/img/PNJ.jpg')}}" ><img class="img1" style="float: right; margin-right: : 500px;"  src="{{asset('admin/img/admin.jpg')}}" > -->
          <!-- <h5 class="header" style=" font-family: Times New Roman; font-weight: bold; margin-left: -350px; " align="center">
            POLITEKNIK NEGERI JAKARTA <br> 
            PROGRAM STUDI DILUAR DOMISILI (PDD) <br> 
            AKADEMI KOMUNITAS NEGERI DEMAK <br>
            Alamat: Jl. Sultan Trenggono No. 61 Demak 59511</h5><hr/> -->
            
            <h3 style="font-family: Times New Roman; font-weight: bold;" align="center">
            KARTU RENCANA STUDI (KRS)</h3><hr/>
@if($lulus === 'lulus')
    <h3 style="font-family: Times New Rowman; margin-left: 250px;" >Maaf Anda sudah tidak aktif lagi di semester ini. Silahkan Hubungi Admin.</h3>

@else
           <div class="col-md-12">
            <pre>
            <div class="col-md-6" style="margin-left: -40px; font-size: 16px; font-family: Times New Rowman;">
              Nama                : {{ $profil->nama }} 
              NIM                  : {{ $profil->nomorInduk }}
              Program Studi  : {{ $profil->namaprodi }} 
            </div>
            <div class="col-md-4" style="margin-left: 140px; font-size: 16px; font-family: Times New Rowman;">
             &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Semester              :  {{ $semester}}
             &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Tahun Akademik : {{$semt->tahun}}
            </div>
              
            </pre>
                
              
              
            </div>

    </div>
</div>
@if($fase === 'false')
    <h3 style="font-family: Times New Rowman; margin-left: 250px;" >Kartu Rencana Studi Belum Dibuka. Silahkan Hubungi Admin.</h3>

@else
<!-- /.row -->
<form class="col-md-12">
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered table-condensed cf">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Kode MK</th>
                <th style="text-align:center;">Mata Kuliah</th>
                <th style="text-align:center;">Dosen</th>
                <th style="text-align:center;">JPM</th>
                <th style="text-align:center;">Praktek</th>
                <th style="text-align:center;">Teori</th>
                <th style="text-align:center;">SKS</th>
                
            </tr>
        </thead>
        
        <tbody>
       <?php $count = 1; 
             $jumlahsks = 0;
       ?> 
            @foreach($kartuRencana as $data) 
            <tr> 
                <td style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:center;">{{ $data->kodematkul }}</td>
                <td style="text-align:left;">{{ $data->namamatkul }}</td> 
                <td style="text-align:left;">{{ $data->dosen }}</td> 
                <td style="text-align:center;">{{ $data->jpm }}</td>   
                <td style="text-align:center;">{{ $data->SKSpraktek }}</td> 
                <td style="text-align:center;">{{ $data->SKSteori }}</td> 
                <td style="text-align:center;">{{ $data->SKS }}</td>  
               
                <?php $jumlahsks = $jumlahsks+$data->SKS ?>            
            </tr> 
            <?php $count++; ?> 
            @endforeach 
            <td style="text-align:center;" colspan="7">Jumlah SKS:</td> 
            <td style="text-align:center;" colspan="1">{{ $jumlahsks }}</td>
        </tbody>
     </table>  
    </div>
    
      </form>
      <div align="center">
        <a class="btn btn-md btn-success" href="/kartuRencana/pdfKRS">Cetak KRS</a>
    </div>
     <br>
   

                
    @endif

     </div>
              </form>
            @endif
              
    </html>
@endsection

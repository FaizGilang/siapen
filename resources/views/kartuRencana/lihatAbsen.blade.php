
@extends('layouts.pengguna')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;pengguna</h3>
    </div>       <!-- /.main-bar -->
</header>
                        <!-- /.head -->
<!-- /.row -->
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url('/kartuRencana/formAbsen') }}"> <!--sama route get-->

            {{ csrf_field() }}

          <div class="card card-primary">
              <div class="card-header">
                <h1 align="center" class="card-title" style="font-family: Times New Rowman;">Lihat Absen Mahasiswa</h1>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                     <div>
                      <label>-Paket Kuliah-</label>
                    <select class="form-control" name="paketKuliah">
                        <option value="">-Pilih Paket Kuliah-</option>
                        @foreach($paketKuliah as $a)
                        <option value="{{ $a->id }}">{{ $a->tahun }}_{{ $a->namaprodi }}_{{ $a->tahunAjaran }}_{{ $a->komulatif }}</option>
                        @endforeach
                    </select>
                </div>
                <br>
                 
  
                <div class="card-footer">
                  <input type="submit" name="save" value="Lihat" class="btn btn-md btn-success">
                </div>
              </form>
            </div>
            @include('sweet::alert')
   <br>
    <br>
    <br>
    <br>
    <br>
        </form>
    </div>
    
   <br>
  
     <br>
          <br>
          <br>
          
</div>

 
@endsection
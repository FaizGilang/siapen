@extends('layouts.pengguna')

@section('heading')
<header class="head">
    <div class="search-bar">
        <div class="main-search">
            <div class="input-group">
                <input type="text" onkeyup="search()" class="form-control" id="search" placeholder="Live Search ...">
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm text-muted" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>  
        </div>      
                                    <!-- /.main-search -->                             
    </div>
                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Buka Kartu Rencana Studi</h3>
    </div>
                            <!-- /.main-bar -->
</header>
                        <!-- /.head -->
@endsection

@section('content')
<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}
</style>
<H1 style="font-family: Times New Roman; font-weight: bold;"  align="center">Silahkan klik tombol BUKA KRS untuk mengaktifkan KRS Semester ini.</H1><br><br>
  <form role="form">
        <div class="card">
          <div class="row">



          <div class="col-md-12">
            <div class="row">
            <div class="col-md-6">

              @if($status->status === 'tidak')
              <a href="{{ url('/kartuRencana/ubahKRS') }}" class="btn btn-primary" style="width:100%; margin-right:0px !important; margin-left:0px !important;">
                <h3 style="font-family: Times New Rowman;">Buka KRS</h3>
              </a>
              @else
            </div>
             
               
             <div class="col-md-6">
              <a href="{{ url('/kartuRencana/ubahKRS') }}" class="btn btn-primary" style="width:100%; margin-right:0px !important; margin-left:0px !important;">
                <h3 style="font-family: Times New Rowman;">Tutup KRS</h3>
              </a>
              @endif
            </div>
            </div>
            </div>
            </div>
          </div>


         
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
          <br>
        </form>
       
 
@endsection

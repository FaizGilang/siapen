<!-- TAMPILAN FORM EDIT PROGRAM STUDI BARU -->

@extends('layouts.pengguna')

@section('heading')
<!-- Page Heading -->
<header class="head">
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Prodi</h3>
    </div>
    <!-- /.main-bar -->
</header>
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}
            <div class="card card-primary">
              <div class="card-header">
                <h1 style="margin-left: 500px; font-family: Times New Roman;" class="card-title">Ubah Absen Mahasiswa</h1>
              </div>
               <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">

            <div class="form-group {{ $errors->has('nomorInduk') ? 'has-error' : '' }}">
                <label>Nomor Induk Mahasiswa:</label>
                <input type="text" name="nomorInduk" class="form-control" disabled value="{{ $absen->nomorInduk }}" placeholder="NIM">
                {!! $errors->first('nomorInduk', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                <label>Nama:</label>
                <input type="text" name="nama" class="form-control" disabled value="{{ $absen->nama }}" placeholder="Nama">
                {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('izin') ? 'has-error' : '' }}">
                <label>Izin:</label>
                <input type="integer" name="izin" class="form-control" placeholder="Izin" value="{{ $absen->izin }}">
                {!! $errors->first('izin', '<p class="help-block">:message</p>') !!}
            </div>

             <div class="form-group {{ $errors->has('alpa') ? 'has-error' : '' }}">
                <label>Alpa:</label>
                <input type="integer" name="alpa" class="form-control" placeholder="Alpa" value="{{ $absen->alpa }}">
                {!! $errors->first('alpa', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="card-footer">              
                <input href="{{ url('/kartuRencana/formAbsen'. $absen->id) }}" style="margin-left: 500px" type="submit" name="save" value="Perbarui" class="btn btn-md btn-success">
                <a style="color:white;" name="reset" href="{{ url('/kartuRencana/lihatAbsen') }}" class="btn btn-danger" value="Batal" /> Batal   </a>             
            </div>
        </form>
    </div>
</div>
@include('sweet::alert')
@endsection


<!-- TAMPILAN MATA KULIAH (tabel prodi) -->

@extends('layouts.home')

@section('heading')
<header class="head">
    <div class="search-bar">
        <div class="main-search">
            <div class="input-group">
                <input type="text" onkeyup="search()" class="form-control" id="search" placeholder="Live Search ...">
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm text-muted" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
            </div>  
        </div>      
                                    <!-- /.main-search -->                             
    </div>
                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;KRS</h3>
    </div>
                            <!-- /.main-bar -->
</header>
                        <!-- /.head -->
@endsection

@section('content')
<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="row">
          <div class="col-md-12">
            <div class="row">
            </div>
          </div>
          </div>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header" align="center">
            Pengisian Kartu Rencana Studi 
        </h1>
        <br>
        <ol class="breadcrumb">
    </div>
</div>

<!-- /.row -->
<div class="card-body">
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered table-striped table-condensed cf">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Kode</th>
                <th style="text-align:center;">Nama</th>
                <th style="text-align:center;">SKS</th>
                <th style="text-align:center;">Teori</th>
                <th style="text-align:center;">Praktek</th>
            </tr>
        </thead>
            
        <tbody>
       <?php $count = 1; ?> 
            @foreach($kartuRencana as $data) 
            <tr> 
                <td style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:center;">{{ $data->kodematkul }}</td>
                <td style="text-align:left;">{{ $data->namamatkul }}</td>  
                <td style="text-align:center;">{{ $data->SKS }}</td> 
                <td style="text-align:center;">{{ $data->teori }}</td> 
                <td style="text-align:center;">{{ $data->praktek }}</td>             
            </tr> 
           
            <?php $count++; ?> 
            @endforeach 
        </tbody>
     </table> 
     <div class="card-footer">
          <input type="submit" name="save" value="Her Registrasi" class="btn btn-md btn-success">
          <input type="submit" name="save" value="Cuti" class="btn btn-md btn-danger">    
      </div>
    </div>
@endsection

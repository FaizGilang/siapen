
@extends('layouts.home')

@section('heading')
<header class="head">
    <div class="search-bar">
        <div class="main-search">
            <div class="input-group">
                <input type="text" onkeyup="search()" class="form-control" id="search" placeholder="Live Search ...">
                    <span class="input-group-btn">
                        <button class="btn btn-primary btn-sm text-muted" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>  
        </div>      
                                    <!-- /.main-search -->                             
    </div>
                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Kategori Pengguna</h3>
    </div>
                            <!-- /.main-bar -->
</header>
                        <!-- /.head -->
@endsection

@section('content')
<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}

</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <br>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header" align="center">
            Daftar Kategori Pengguna
        </h1>
        <br>
        <ol class="breadcrumb">
    </div>
</div>

<!-- /.row -->
<div class="card-body">
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered table-striped table-condensed cf">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Nama</th>
                <th style="text-align:center;">Aksi</th>
            </tr>
        </thead>
            
        <tbody>
       <?php $count = 1; ?> 
            @foreach($lihatpaket as $data) 
            <tr> 
                <td style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:center;">{{ $data->nama }}</td> 
                <td style="text-align:center;">{{ $a->tahun }}_{{ $a->namaprodi }}_{{ $a->tahunAjaran }}_{{ $a->semester }}</td> 
                <td style="text-align:center;"> 
                    <a data-placement="top" data-original-title="Edit" data-toggle="tooltip" href="{{ url('/kategoriPengguna/edit/' . $data->id) }}" class="btn btn-xs btn-default" align="center"><i class="fa fa-fw fa-pencil"></i></a>  
               
                   <a data-placement="top" data-original-title="Delete" data-toggle="tooltip" href="{{ url('/kategoriPengguna/delete/' . $data->id) }}" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-trash"></i></a>  
                </td>
            </tr> 
            <?php $count++; ?> 
            @endforeach 
        </tbody>
     </table> 
    </div>
<!-- modal delete 
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <a data-placement="top" data-original-title="Delete" data-toggle="tooltip" href="{{ url('/user/delete/' . $data->id) }}" class="btn btn-xs btn-danger"><i class="fa fa-fw fa-trash"></i></a>  
      </div>
    </div>
  </div>
</div>
-->
@endsection

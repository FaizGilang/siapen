<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<style>
img{
    height: 120px;
    width: 80px;
    padding: -30px 30px 10px 10px; 
}
.label1 {;
  min-width: 8px !important;
  display: inline-block !important
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td{
    padding: 5px 35px 15px 20px;
    text-align: center;   

}
tr{
    text-align: center;
}
p{
    text-align: center;
    font-size: 18;
    margin-left: 150px;
}
h6{
    text-align: center;
    font-size: 18;
    margin-left: 200px;
}
</style>

<div class="card card-primary">
<form role="form">
<div class="card-body">
<div class="row">
    <div class="col-lg-12">
<hr/>
        <img style="float: left;" src="admin/img/PNJ.jpg" ><img style="float: right; margin-left: 0px;"  src="admin/img/admin.jpg" >
         <h3 class="img"  style=" font-family: Times New Roman; font-weight: bold; margin-left: 30px; " align="center">
            POLITEKNIK NEGERI JAKARTA <br> JURUSAN {{$jur}}<br> PROGRAM STUDI DILUAR DOMISILI DIPLOMA 2 AKN DEMAK</h3><hr/>
            
            <h3 style="font-family: Times New Roman; font-weight: bold;" align="center">
            KARTU RENCANA STUDI (KRS)</h3><hr/>
            <pre style=" font-family: Times New Roman; ">
                <br>
                Nama                  : {{ $profil->nama }}<br>
                NIM                    : {{ $profil->nomorInduk }}<br>
                Program Studi    : {{$profil->namaprodi}}<br>
                Smt/Smtkum       : {{$semt->semt}} / {{ $semester}}<br>
                Tahun Akademik : {{$semt->tahun}}<br>
                 
            </pre>
    </div>
</div>
        
<!-- /.row -->
<form class="col-md-12">
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered table-condensed cf">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Kode MK</th>
                <th style="text-align:center;">Mata Kuliah</th>
                <th style="text-align:center;">Dosen</th>
                <th style="text-align:center;">JPM</th>
                <th style="text-align:center;">Praktek</th>
                <th style="text-align:center;">Teori</th>
                <th style="text-align:center;">SKS</th>
            </tr>
        </thead>
        <tbody>
       <?php $count = 1; 
             $jumlahsks = 0;
       ?> 
            @foreach($kartuRencana as $data) 
            <tr> 
                <td style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:center;">{{ $data->kodematkul }}</td>
                <td style="text-align:left;">{{ $data->namamatkul }}</td>  
                <td style="text-align:left;">{{ $data->dosen }}</td>  
                <td style="text-align:center;">{{ $data->jpm }}</td>  
                <td style="text-align:center;">{{ $data->SKSpraktek }}</td> 
                <td style="text-align:center;">{{ $data->SKSteori }}</td> 
                <td style="text-align:center;">{{ $data->SKS }}</td>  
                <?php $jumlahsks = $jumlahsks+$data->SKS ?>            
            </tr> 
            <?php $count++; ?> 
            @endforeach 
            <tr>
            <td style="text-align:center;" colspan="7"><b>Jumlah SKS:</b></td> 
            <td style="text-align:center;" colspan="1">{{ $jumlahsks }}</td>
        </tr>
        </tbody>
     </table>  



</body>
<br>
<br>
    <?php
    function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    ?>

        <p align="right"><?php echo "Demak, " . (tgl_indo(date('Y-m-d')));?> &emsp; &emsp; <br>Mengetahui dan menyetujui</p>
        <h6  style="text-align:left">
        Mahasiswa, &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;     Ketua Program Studi, <br><br><br>
        </h6>
        <p  style="text-align:left">
        &emsp; &emsp; {{ $profil->nama }}  &emsp; &emsp; &emsp;{{$kaprodi->kaprodi1}}  <br> 
        &emsp; &emsp;{{ $profil->nomorInduk}} &emsp; &emsp; &emsp; &emsp; &nbsp;  &emsp; &emsp; &emsp; &emsp; &emsp;  {{$kaprodi->nip1}} 
        </p>

</html>


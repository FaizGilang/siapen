<!-- TAMPILAN FORM EDIT PROGRAM STUDI BARU -->

@extends('layouts.pengguna')


@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}
            <div class="card card-primary">
              <div class="card-header">
                <h3 style="margin-left: 500px; font-family: Times New Rowman;" class="card-title">Ubah Nilai Mahasiswa</h3>
              </div>
               <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">

           

             <div class="form-group {{ $errors->has('nomorInduk') ? 'has-error' : '' }}">
                <label>Nomor Induk Mahasiswa:</label>
                <input selected disabled type="text" name="nomorInduk" class="form-control" value="{{ $lihatNilai->nomorInduk }}" placeholder="NIM">
                {!! $errors->first('nomorInduk', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                <label>Nama:</label>
                <input selected disabled type="text" name="nama" class="form-control" value="{{ $lihatNilai->nama }}" placeholder="nama">
                {!! $errors->first('nama', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('uts') ? 'has-error' : '' }}">
                <label>Nilai UTS:</label>
                <input type="text" name="uts" class="form-control" placeholder="Nilai" value="{{ $lihatNilai->uts }}">
                {!! $errors->first('uts', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('uas') ? 'has-error' : '' }}">
                <label>Nilai UAS:</label>
                <input type="text" name="uas" class="form-control" placeholder="Nilai" value="{{ $lihatNilai->uas }}">
                {!! $errors->first('uas', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('praktek') ? 'has-error' : '' }}">
                <label>Nilai Praktek:</label>
                <input type="text" name="praktek" class="form-control" placeholder="Nilai" value="{{ $lihatNilai->praktek }}">
                {!! $errors->first('praktek', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="form-group {{ $errors->has('tugas') ? 'has-error' : '' }}">
                <label>Nilai Tugas:</label>
                <input type="text" name="tugas" class="form-control" placeholder="Nilai" value="{{ $lihatNilai->tugas }}">
                {!! $errors->first('tugas', '<p class="help-block">:message</p>') !!}
            </div>

            <div class="card-footer">              
                <input style="margin-left: 500px" type="submit" name="save" value="Perbarui" class="btn btn-md btn-success">
                <a href="{{ url('/halamanDosen/lihatNilai') }}"  name="reset" class="btn btn-danger" value="Batal" /> Batal   </a>             
            </div>
        </form>
    </div>
</div>
</div>
@endsection

@extends('layouts.home')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Halaman Dosen</h3>
    </div>       <!-- /.main-bar -->
</header>
                        <!-- /.head -->
<!-- /.row -->
@endsection

@section('content')
 <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <ul class="nav nav-treeview">
                      <li class="nav-item">
                        <a href="{{ url('admin/img/Template_Nilai.xlsx') }}" download="Template Nilai" class="nav-link">
                           Unduh Template Nilai Disini.
                        </a>
                      </li>
                    </ul>
              <div class="card-header">
                <h3 class="card-title">Upload File Nilai</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  <div class="form-group">      
                    <label for="exampleInputFile">File input</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" id="UploadNilai">
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <input type="submit" name="save" value="Submit" class="btn btn-md btn-success">
                </div>
              </form>
            </div>
@endsection
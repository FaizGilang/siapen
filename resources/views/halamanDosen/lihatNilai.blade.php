@extends('layouts.pengguna')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Informasi Nilai</h3>
    </div><!-- /.main-bar -->
</header>
<!-- /.head -->
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
         <form style="margin-top: 15px;padding: 20px; margin-left: -60px;" method="post" action="{{ url('/halamanDosen/cariNilai') }}">

            {{ csrf_field() }}

          <div style="margin-left: 40px;" class="card card-primary">
              <div class="card-header">
                <h3 class="card-title" align="center" style="font-family: Times New Rowman;">Informasi Nilai Mata Kuliah Yang Diampu</h3>
              </div><br>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                   <div class="search-bar">
   
            <!--  <div class="input-group input-group-sm">
        <input style="margin-left: 30px;" class="form-control form-control-navbar" type="string" name="q" placeholder="Search" aria-label="Search"> {!! $errors->first('kode', '<p class="help-block">:message</p>') !!}

        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>

      </div> -->
        </div>      
                                 
                <div class="card-body">
                <div class="col-lg-6">
                      <label>Pilih Mata Kuliah:</label>
                      <select class="form-control" name="lihatNilai">
                        <option value="">-Pilih Mata Kuliah-</option>
                         @if($smt ==='ganjil')
                        @foreach($unggahNilai as $a)
                          @if($a->komulatif === 1 || $a->komulatif === 3 )
                            <option value="{{ $a->id }}">{{ $a->nama }}</option> 
                          @endif                                          
                        @endforeach
                        
                        @foreach($unggahNilai1 as $b)
                          @if($b->komulatif === 1 || $b->komulatif === 3 )
                            <option value="{{ $b->id }}">{{ $b->nama }}</option>
                          @endif
                        @endforeach
                      @else
                        @foreach($unggahNilai as $a)
                          @if($a->komulatif === 2 || $a->komulatif === 4 )
                            <option value="{{ $a->id }}">{{ $a->nama }}</option> 
                          @endif                                          
                        @endforeach
                        
                        @foreach($unggahNilai1 as $b)
                          @if($b->komulatif === 2 || $b->komulatif === 4 )
                            <option value="{{ $b->id }}">{{ $b->nama }}</option>
                          @endif
                        @endforeach
                      @endif
                      </select>

      </div>
             <div  class="col-lg-6" class="main-search">

                  <label>Cari Mahasiswa:</label>
                <input  type="text" name="cari" class="form-control" id="cari" placeholder="Cari ...">
                {!! $errors->first('kode', '<p class="help-block">:message</p>') !!}

        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>

       
            </div><br><br>
      <div>
        <br><table class="table table-bordered" >
         <thead>
          <tr>
            
            <td style="text-align:center;">NIM</td>
            <td style="text-align:center;">Nama</td>
            <td style="text-align:center;">UTS</td>
            <td style="text-align:center;">UAS</td>
            <td style="text-align:center;">Praktek</td>
            <td style="text-align:center;">Tugas</td>
            <td style="text-align:center;">Nilai Akhir</td>
            <td style="text-align:center;">Nilai Huruf</td>
            <td style="text-align:center;">Aksi</td><br>
          </tr>
        </thead>
        <tbody id="table">
        @if(empty($cari))
        @else
           @foreach($cari as $data) 
            <tr> 
                 
                <td>{{ $data->nomorInduk }}</td> 
                <td>{{ $data->nama }}</td>
                <td style="text-align:center;">{{ $data->uts }}</td> 
                <td style="text-align:center;">{{ $data->uas }}</td> 
                <td style="text-align:center;">{{ $data->praktek }}</td> 
                <td style="text-align:center;">{{ $data->tugas }}</td> 
                <?php
                $data->uts;
                $data->uas;
                $data->tugas; 
                $data->praktek;
                $data->bobotUts;
                $data->bobotUas;
                $data->bobotTugas;
                $data->bobotPraktek;
                    //cek cek
                $nilaiakhir1 = ($data->uts)*($data->bobotUts/100) + ($data->uas)*($data->bobotUas/100) + ($data->tugas)*($data->bobotTugas/100) + ($data->praktek)*($data->bobotPraktek/100);

                $nilaiakhir=round($nilaiakhir1,2);

            ?>
               <td >{{ $nilaiakhir }}</td>
                @if($nilaiakhir>=79.6 && $nilaiakhir<=100) 
                    <td style="text-align:center;">A</td>
                    <?php $bobot=4.0 ?>
                    @elseif($nilaiakhir>=75.6 && $nilaiakhir<=79.5)
                    <td style="text-align:center;">A-</td>
                    <?php $bobot=3.7 ?>
                    @elseif($nilaiakhir>=71.6 && $nilaiakhir<=75.5)
                    <td style="text-align:center;">B+</td>
                    <?php $bobot=3.3 ?>
                    @elseif($nilaiakhir>=67.6 && $nilaiakhir<=71.5)
                    <td style="text-align:center;">B</td>
                    <?php $bobot=3.0 ?>
                    @elseif($nilaiakhir>=63.6 && $nilaiakhir<=67.5)
                    <td style="text-align:center;">B-</td>
                    <?php $bobot=2.7 ?>
                    @elseif($nilaiakhir>=59.6 && $nilaiakhir<=63.5)
                    <td style="text-align:center;">C+</td>
                    <?php $bobot=2.5 ?>
                    @elseif($nilaiakhir>=55.6 && $nilaiakhir<=59.5)
                    <td style="text-align:center;">C</td>
                    <?php $bobot=2.0 ?>
                    @elseif($nilaiakhir>=40.6 && $nilaiakhir<=55.5)
                    <td style="text-align:center;">D</td>
                    <?php $bobot=1.0 ?>
                    @elseif($nilaiakhir>=0 && $nilaiakhir<=40.5)
                    <td style="text-align:center;">E</td>
                    <?php $bobot=0 ?>
                    @elseif($nilaiakhir<=0)
                    <td style="text-align:center;">-</td>
                    <?php $bobot=0 ?>
                    @elseif($nilaiakhir>100)
                    <td style="text-align:center;">-</td>
                    <?php $bobot=0 ?>
                @endif 

                
          <td align="center"><a  data-placement="top" data-original-title="Edit" " data-toggle="tooltip" href="{{ url('/halamanDosen/lihatNilai/data->id') }}"  class="btn btn-warning" align="center"><i style="color: white" class="fa  fa-pencil">  Ubah</i></a></td>
            </tr> 
            @endforeach 
          @endif
          </tbody>
        </table>
          
    
      
        </div>
              
                </div> 
        </div>
    </div>

</div>
</form>
</div>
</form>
</div>
</div>
<br>
<br>


@endsection


@section('script')

 <script type="text/javascript">
    jQuery(document).ready(function ()
    {

            jQuery('select[name="lihatNilai"]').on('click',function(){
               var countryID = jQuery(this).val();
               if(countryID)
               {
                  jQuery.ajax({
                     url : '/halamanDosen/lihatNilai/tabelNilai/' +countryID,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);

                        
                        jQuery('#table').empty();
                        jQuery.each(data, function(key,value){
                           if (((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))>=79.6 && ((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))<=100)
                           {
                             var huruf='A';
                           }
                          else if (((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))>=75.6 && ((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))<=79.5)
                          {
                             var huruf='A-';
                          }
                          else if (((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))>=71.6 && ((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))<=75.5)
                          {
                             var huruf='B+';
                          }
                          else if (((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))>=67.6 && ((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))<=71.5)
                          {
                             var huruf='B';
                          }
                          else if (((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))>=63.6 && ((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))<=67.5)
                          {
                             var huruf='B-';
                          }
                          else if (((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))>=59.6 && ((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))<=63.5)
                          {
                             var huruf='C+';
                          }
                          else if (((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))>=55.6 && ((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))<=59.5)
                          {
                             var huruf='C';
                          }
                          else if (((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))>=40.6 && ((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))<=55.5)
                          {
                             var huruf='D';
                          }
                          else if (((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))>=0 && ((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100))<=40.5)
                          {
                             var huruf='E';
                          }
                          else 
                          {
                             var huruf='-';
                          }

                         $("#table").append('<tr> <td align="left" >'+ value.nomorInduk +'</td><td align="left">'+ value.nama +'</td><td align="center">'+ value.uts +'</td><td align="center">'+ value.uas +'</td><td align="center">'+ value.praktek +'</td><td align="center">'+ value.tugas +'</td><td align="left" >'+ ((value.uts*value.bobotUts/100) + (value.uas*value.bobotUas/100) + (value.tugas*value.bobotTugas/100) + (value.praktek*value.bobotPraktek/100)  ) +'</td><td align="left" >'+huruf +'</td><td align="center"><a  data-placement="top" data-original-title="Edit" " data-toggle="tooltip" href="{{ url('/halamanDosen/lihatNilai') }}/'+ value.idhsm +'"  class="btn btn-warning" align="center"><i style="color: white" class="fa  fa-pencil">  Ubah</i></a></td></tr>');
                        });
                     }
                  });
               }
            });
    });
    </script>

@endsection

<!--  -->
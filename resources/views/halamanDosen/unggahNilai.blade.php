@extends('layouts.pengguna')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Informasi Mata Kuliah</h3>
    </div>       <!-- /.main-bar -->
</header>
                        <!-- /.head -->
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
         <form style="margin-top: 15px;padding: 20px; margin-left: -60px;" action="{{ URL::to('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">

            {{ csrf_field() }}

        @if ($message = Session::get('success'))
          <div style="margin-left: 40px;" class="alert alert-success" role="alert">
            {{ Session::get('success') }}
          </div>
        @endif

        @if ($message = Session::get('error'))
          <div style="margin-left: 40px;" class="alert alert-danger" role="alert">
            {{ Session::get('error') }}
          </div>
        @endif
        {!! $errors->first('uts', '<p class="text-danger">:message</p>') !!}
          <div style="margin-left: 40px;" class="card card-primary">
              <div class="card-header">
                <h3 class="card-title" style="font-family: Times New Rowman; margin-left: 500px;" >Mata Kuliah Yang Diampu</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
           
           @if($fase === 'true')
                <div>
                    <label>Pilih Mata Kuliah:</label>
                    <select class="form-control" name="unggahNilai" id="matkul">
                      {!! $errors->first('file', '<p class="text-danger">:message</p>') !!}
                        <option value="">-Pilih Mata Kuliah-</option>
                      @if($smt ==='ganjil')
                        @foreach($unggahNilai as $a)
                          @if($a->komulatif === 1 || $a->komulatif === 3 )
                            <option value="{{ $a->id }}">{{ $a->nama }}</option> 
                          @endif                                          
                        @endforeach
                        
                        @foreach($unggahNilai1 as $b)
                          @if($b->komulatif === 1 || $b->komulatif === 3 )
                            <option value="{{ $b->id }}">{{ $b->nama }}</option>
                          @endif
                        @endforeach
                      @else
                        @foreach($unggahNilai as $a)
                          @if($a->komulatif === 2 || $a->komulatif === 4 )
                            <option value="{{ $a->id }}">{{ $a->nama }}</option> 
                          @endif                                          
                        @endforeach
                        
                        @foreach($unggahNilai1 as $b)
                          @if($b->komulatif === 2 || $b->komulatif === 4 )
                            <option value="{{ $b->id }}">{{ $b->nama }}</option>
                          @endif
                        @endforeach
                      @endif
                    </select>
                    {!! $errors->first('file', '<p class="text-danger">:message</p>') !!}
                </div> 
            @elseif($fase === 'wait')
              <h3 style="font-family: Times New Rowman;">Maaf, waktu untuk upload nilai belum dibuka. Silahkan Hubungi Akademik.</h3>
            @else

                <h3 style="font-family: Times New Rowman;" >Maaf, waktu untuk upload nilai sudah habis. Silahkan Hubungi Akademik.</h3>
            @endif
                
                <div id="upload"></div>
              </form>
              <div class="card-footer">
                  <a style="margin-left: 0px" href="{{ url('/halamanDosen/unggahNilai') }}" class="fa fa-arrow-circle-left fa-2x"/></a>
                </div>
        
        </div>
    </div>
    
</div>
</form>

</div>
</form>
</div>
</div>
<br>
<br>
<br>
<br>
<br>
@endsection

@section('script')
<script type="text/javascript">
  jQuery(document).ready(function()
  {
      jQuery('select[name="unggahNilai"]').on('change',function()
      {
          var id = jQuery(this).val();
          var A = jQuery(this).val();
          var id = "/"+id;
            $('#button1').remove();
                $('#button2').remove();
                $('#button3').remove();

                jQuery.ajax({
                     url : '/hasilStudi/unggahNilai/getbobotUas/' +A,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
               
                        jQuery.each(data, function(key,value){
                            $('#upload').append('Bobot UTS:<input name="bobotUas" value='+value+' class="form-control">');
                        });
                     }
                  });
               
                  jQuery.ajax({
                     url : '/hasilStudi/unggahNilai/getbobotUts/' +A,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
               
                        jQuery.each(data, function(key,value){
                              $('#upload').append('Bobot UAS:<input name="bobotUts" value='+value+' class="form-control">');
                        });
                     }
                  });

                  jQuery.ajax({
                     url : '/hasilStudi/unggahNilai/getbobotTugas/' +A,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
               
                        jQuery.each(data, function(key,value){
                            $('#upload').append('Bobot Tugas:<input name="bobotTugas" value='+value+' class="form-control">');
                        });
                     }
                  });

                  jQuery.ajax({
                     url : '/hasilStudi/unggahNilai/getbobotPraktek/' +A,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
               
                        jQuery.each(data, function(key,value){
                            $('#upload').append('Bobot Praktek:<input name="bobotPraktek" value='+value+' class="form-control">');
                  $('#upload').append('<br><input class="form-control" type="file" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"name="import_file"/>','<br><button class="btn btn-md btn-success"> Upload File</button>','<br><a href="{{ url('downloadExcel/xlsx/')}}'+id+'">Download Template Disini</a>');
                        });
                     }
                  });



            }); 
                  
          
      });
    
  

</script>

@endsection
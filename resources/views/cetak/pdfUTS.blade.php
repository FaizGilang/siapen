<html>
<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8">
<style>
img{
    height: 120px;
    width: 80px;
    padding: -30px 30px 10px 10px; 
}
.label1 {;
  min-width: 8px !important;
  display: inline-block !important
}
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
th, td{
    padding: 6px 35px 15px 20px;
    text-align: center;   

}
tr{
    text-align: center;
}
p{
    text-align: center;
    font-size: 18;
    margin-left: 150px;
}
h6{
    text-align: center;
    font-size: 18;
    margin-left: 200px;
}
</style>

<div class="card card-primary">
<form role="form">
<div class="card-body">
<div class="row">
    <div class="col-lg-12">
            
            <img style="float: left;" src="admin/img/PNJ.jpg" ><img style="float: right; margin-left: 0px;"  src="admin/img/admin.jpg" >
            <h3 class="img"  style=" font-family: Times New Roman; font-weight: bold; margin-left: 30px; " align="center">
            POLITEKNIK NEGERI JAKARTA <br> 
            PROGRAM STUDI DILUAR DOMISILI (PDD) <br> 
            AKADEMI KOMUNITAS NEGERI DEMAK <br>
            Alamat: Jl. Sultan Trenggono No. 61 Demak 59511</h3><hr/>
            <br>
@if($lulus === 'lulus')
    <h3 style="font-family: Times New Rowman; margin-left: 50px;" >Maaf Anda sudah tidak aktif lagi di semester ini. Silahkan Hubungi Admin.</h3>

@else
            <h3 style="font-family: Times New Roman; font-weight: bold;" align="center">
            KARTU UJIAN TENGAH SEMESTER <?php echo strtoupper ($semt->semt) ?><br>
            TAHUN AJARAN <?php echo strtoupper ($semt->tahun) ?> </h3><hr/>
            <!-- <img style="float: right; margin-left: 0px;"  src="" > -->
            <pre style=" font-family: Times New Roman; ">
                <br>
                Nama                : {{ $profil->nama }}<br>  
                NIM                  : {{ $profil->nomorInduk }}<br>
                Semester          : {{ $semester}}<br>
                Program Studi  : {{ $profil->namaprodi }}<br>
                 
            </pre>
    </div>
</div>
@if($lihat === 'false')
    <h3 style="font-family: Times New Rowman; margin-left: 85px;" >Kartu Rencana Studi Belum Dibuka. Silahkan Hubungi Admin.</h3>
@else
<!-- /.row -->
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered table-condensed cf">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th width="250px" width="10px" style="text-align:center;">Mata Kuliah</th>
                <th style="text-align:center;">SKS</th>
                <th style="text-align:center;">Pengawas</th>
                <th width="200px" colspan="" style="text-align:center;">TTD Pengawas</th>
            </tr>
        </thead>
        <tbody>
       <?php $count = 1; ?> 
            @foreach($kartuRencana as $data) 
            <tr> 
                <td style="text-align:center;">{{ $count }}.</td> 
                <td width="250px" style="text-align:left;">{{ $data->namamatkul }} </td>  
                <td style="text-align:center;">{{ $data->SKS }}</td>              
                <td></td>              
                <td width="200px" style="text-align:left;">{{ $count }}.</td>              
            </tr> 
            <?php $count++; ?> 
            @endforeach 
        </tbody>
     </table>  
     <i>Catatan: Kartu harus selalu dibawa saat ujian</i>



</body>
<br>
<br>
    <?php
    function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);
        
        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
     
        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    ?>

        <p align="right"><?php echo "Demak, " . (tgl_indo(date('Y-m-d')));?> &emsp; &emsp; </p>
        <p class="center" style="text-align:center">
            &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; Koordinator, <br><br><br>
        </p>
        <p class="center" style="text-align:center"><b>
           &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;  {{$koor->namakoor}} </b> <br> 
           &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; {{$koor->nip}} </b> <br> 
          
        </p>
 @endif
 @endif
</html>


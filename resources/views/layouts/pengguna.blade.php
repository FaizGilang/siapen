

<!-- DOCTYPE -->
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- Viewport Meta Tag -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
      AKN Demak
    </title>
    <title>@yield('page-title')</title>
    <link rel="icon" href="{{asset('admin/img/logoAKN.png')}}" />
    <!-- Bootstrap -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/main.css')}}">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/responsive.css')}}">
    <!--Fonts-->
    <link rel="stylesheet" media="screen" href="{{asset('assets/fonts/font-awesome/font-awesome.min.css')}}">
    <link rel="stylesheet" media="screen" href="{{asset('assets/fonts/simple-line-icons.css')}}">    
     
    <!-- Extras -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/extras/owl/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/extras/owl/owl.theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/extras/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/extras/normalize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/extras/settings.css')}}">

    <!-- Color CSS Styles  -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/colors/green.css" media="screen')}}" />       
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js">
    </script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js">
    </script>
    <![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('admin/css/adminlte.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('plugins/iCheck/flat/blue.css')}}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{asset('plugins/morris/morris.css')}}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
    <!-- Date Picker -->
    <link rel="stylesheet" href="{{asset('plugins/datepicker/datepicker3.css')}}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker-bs3.css')}}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
   <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    </head>
    <style type="text/css">
 
.dropdown {
  position: relative;
  display: inline-block;
  color: blue;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(1,1,1,0.8);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}

.dropdown:hover .dropbtn {
  background-color: #3e8e41;
}
      /* .jumbotron {
      position: relative;
      background:url("{{asset('admin/img/qoute1.jpg')}}");
      width: 100%;
      height: 100%;
      background-size: cover;
      overflow: hidden;
}*/
      .footer {
   position:absolute;
   bottom:0;
   width:100%;
   height:100px;   /* tinggi dari footer */
   
}
    }
    #header.header-scrolled {
    background: #fff;
    padding: 20px 0;
    height: 72px;
    transition: all 0.5s;
}
#header {
    padding: 30px 0;
    height: 125px;
    position: fixed;
    left: 0;
    top: 0;
    right: 0;
    transition: all 0.5s;
    z-index: 997;
    background-color: #fff;
    box-shadow: 5px 0px 15px #c3c3c3;
}
#header #logo h1 {
    font-size: 34px;
    margin: 0;
    padding: 0;
    line-height: 1;
    font-family: "Montserrat", sans-serif;
    font-weight: 700;
    letter-spacing: 3px;
}
#header #logo h1 a, #header #logo h1 a:hover {
    color: #000;
    padding-left: 10px;
    border-left: 4px solid #18d26e;
}
#nav-menu-container {
    float: right;
    margin: 0;
}
.nav-menu > li {
    margin-left: 10px;
}
.nav-menu > li {
    float: left;
}
.nav-menu li {
    position: relative;
    white-space: nowrap;
}
.nav-menu, .nav-menu * {
    margin: 0;
    padding: 0;
    list-style: none;
}
.header-scrolled .nav-menu li:hover > a, .header-scrolled .nav-menu > .menu-active > a {
    color: #18d26e;
}
.header-scrolled .nav-menu a {
    color: black;
}
.nav-menu li:hover > a, .nav-menu > .menu-active > a {
    color: #18d26e;
}
.nav-menu a {
    padding: 0 8px 10px 8px;
    text-decoration: none;
    display: inline-block;
    color: #000;
    font-family: "Montserrat", sans-serif;
    font-weight: 700;
    font-size: 13px;
    text-transform: uppercase;
    outline: none;
}
#mobile-nav-toggle {
    display: inline;
}
#mobile-nav-toggle {
    position: fixed;
    right: 0;
    top: 0;
    z-index: 999;
    margin: 20px 20px 0 0;
    border: 0;
    background: none;
    font-size: 24px;
    display: none;
    transition: all 0.4s;
    outline: none;
    cursor: pointer;
}
#mobile-body-overly {
    width: 100%;
    height: 100%;
    z-index: 997;
    top: 0;
    left: 0;
    position: fixed;
    background: rgba(0, 0, 0, 0.7);
    display: none;
}
body.mobile-nav-active #mobile-nav {
    left: 0;
}
#mobile-nav {
    position: fixed;
    top: 0;
    padding-top: 18px;
    bottom: 0;
    z-index: 998;
    background: rgba(0, 0, 0, 0.8);
    left: -260px;
    width: 260px;
    overflow-y: auto;
    transition: 0.4s;
}
#mobile-nav ul {
    padding: 0;
    margin: 0;
    list-style: none;
}
#mobile-nav ul li {
    position: relative;
}
#mobile-nav ul li a {
    color: #fff;
    font-size: 13px;
    text-transform: uppercase;
    overflow: hidden;
    padding: 10px 22px 10px 15px;
    position: relative;
    text-decoration: none;
    width: 100%;
    display: block;
    outline: none;
    font-weight: 700;
    font-family: "Montserrat", sans-serif;
}
#mobile-nav ul .menu-has-children i.fa-chevron-up {
    color: #18d26e;
}
#mobile-nav ul .menu-has-children i {
    position: absolute;
    right: 0;
    z-index: 99;
    padding: 15px;
    cursor: pointer;
    color: #fff;
}
#mobile-nav ul .menu-item-active {
    color: #18d26e;
}
#mobile-nav ul li li {
    padding-left: 30px;
}

.menu-has-children ul
{display: none;}


@media (max-width: 768px){
#nav-menu-container {
    display: none;
}

#mobile-nav-toggle {
    display: inline;
}
}
  </style>


 @if(Auth::user()->kategori == 1)
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/login" class="nav-link">Home</a>
      </li>
     
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->     
          <a class="nav-link dropdown-item" href="{{ route('logout') }}"onclick="event.preventDefault();
            document.getElementById('logout-form').submit();" class="btn btn-metis-1 btn-sm" role="button" aria-haspopup="true" aria-expanded="false">
          <i class="fa fa-power-off"> Logout</i>
          </a>                                    
        
        <div class="btn-group">
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
          </form>
        </div> 
      </div>    
    </ul>
  </nav>
</div>
</body>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <br>
  <br>
  <h1 style="font-family: Times New Roman; font-weight: bold; border:solid 2px black;"> &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;  &emsp;  &emsp; Informasi Data Akademik AKN DEMAK</h1><br>
 <br>
   <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        
            @yield('board')
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/home" class="brand-link">
      <img style="margin-left: 10px" src="{{asset('admin/img/admin.jpg')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AKN Demak</span>
    </a>
    <!-- Sidebar -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
         <li class="nav-item has-treeview">
            <a href="{{ url('/home') }}" class="nav-link">
              <i class="nav-icon fa fa-info"></i>
              <p>
                Dashboard
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
          </li>                     
         
          <li class="nav-item has-treeview">
            <a href="{{ url('/kategoriPengguna/lihat') }}" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Kategori Pengguna
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
          </li>
           <li class="nav-item has-treeview">
            <a href="{{ url('/user/lihat') }}" class="nav-link">
              <i class="nav-icon fa fa-users"></i>
              <p>
                Pengguna
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/user/lihat1') }}" class="nav-link">
                  <i class="fa fa-eye nav-icon"></i>
                  <p>Lihat Data Dosen</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/user/lihat') }}" class="nav-link">
                  <i class="fa fa-eye nav-icon"></i>
                  <p>Lihat Data Mahasiswa</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{ url('/prodi/lihat') }}" class="nav-link">
              <i class="nav-icon fa fa-building"></i>
              <p>
                Program Studi
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{ url('/kurikulum/lihat') }}" class="nav-link">
              <i class="nav-icon fa fa-file"></i>
              <p>
                Kurikulum
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
          </li>
           <li class="nav-item has-treeview">
            <a href="{{ url('/matkul/lihat') }}" class="nav-link">
              <i class="nav-icon fa fa-book"></i>
              <p>
                Mata Kuliah
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            </li>
             <li class="nav-item has-treeview">
            <a href="{{ url('/halamanAdmin/faseAkademik') }}" class="nav-link">
              <i class="nav-icon fa fa-calendar"></i>
              <p>
                Fase Akademik
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            </li>
            <li class="nav-item has-treeview">
            <a href="{{ url('/halamanAdmin/paketKuliah') }}" class="nav-link">
              <i class="nav-icon fa fa-folder"></i>
              <p>
                Paket Kuliah
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            </li>
            
            <li class="nav-item has-treeview">
            <a href="{{ url('/halamanAdmin/daftarAbsen') }}" class="nav-link">
              <i class="nav-icon fa fa-database"></i>
              <p>
                Absen Mahasiswa
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            </li>
  
@endif                

@if(Auth::user()->kategori == 2)   
<div class="jumbotron">
    <header id="header-wrap">
      <section id="header">
        <nav class="navbar navbar-light" data-spy="affix" data-offset-top="250">
          <div class="container">
            <div  id="main-menu">

              <a class="navbar-brand" href="/home">
                <img style="margin-left: -100px; margin-bottom: 0px; margin-top: -40px;" src="{{asset('admin/img/utama.png')}}" alt="">
              </a>

              <div>
                <ul style="margin-left: 228px; margin-top: 0px; margin-bottom: -95px;"  class="nav nav-inline" >
                <li>
                  <a class="nav-link active" href="/home" role="button" aria-haspopup="true" aria-expanded="false">Beranda</a>         
                </li> 

                <li class="dropdown">
                  <a class="nav-link active" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    Fase Akademik
                  </a>
                  <div class="dropdown-content">                      
                    <a class="dropdown-item" href="/faseAkademik/tambah">Tambah Fase Akademik</a>
                    <a class="dropdown-item" href="/faseAkademik/lihat">Lihat Fase Akademik</a>
                  </div>
                </li>                                 

                <li class="dropdown">
                  <a class="nav-link active" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    Paket Kuliah
                  </a>
                  <div class="dropdown-content">                      
                    <a class="dropdown-item" href="/paketKuliah/tambah">Input Paket Kuliah</a>
                    <a class="dropdown-item" href="/paketKuliah/lihat">Lihat Paket Kuliah</a>
                  </div>
                </li> 

                <li>
                  <a class="nav-link active" href="/kartuRencana/bukaKRS" role="button" aria-haspopup="true" aria-expanded="false">Buka KRS</a>                 
                </li>   

                <li>
                  <a class="nav-link active" href="/kartuRencana/lihatAbsen" role="button" aria-haspopup="true" aria-expanded="false">Absen Mahasiswa</a>                 
                </li>

                 <li class="dropdown">
                  <a class="nav-link active" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    Nilai
                  </a>
                  <div class="dropdown-content">                      
                    <a class="dropdown-item" href="/hasilStudi/nilaiMahasiswa">Unggah Nilai</a>
                    <a class="dropdown-item" href="/hasilStudi/lihatMahasiswa">Lihat Nilai</a>
                  </div>
                </li> 
                  
                <li>
                  <a class="nav-link dropdown-item" href="{{ route('logout') }}"onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" class="btn btn-metis-1 btn-sm" role="button" aria-haspopup="true" aria-expanded="false">
                    <b><i>Logout</b></i>
                  </a>                                    
                </li> 
                </ul>
                </div>
          
              <ul>   
                <div class="btn-group">
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>         
              </ul>  

            </div> 
          </div>
        </nav>
      </section>
    </header>
  </div>

     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-12">
          <div  class="col-lg-12" style="width:10%; margin-right:0px !important; margin-left:0px !important;"> 
            @yield('content')
          </div><!-- /.col -->
          </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  

<footer class="main-footer" style="margin-left: 0 !important;  margin-bottom: 0 !important "  >
    <strong>   <a href="">SIAPEN</a>.</strong>
    Akademi Komunitas Negeri Demak. <strong>2019</strong>
    <div class="float-right d-none d-sm-inline-block">
       <a><i class="fa fa-phone"></i> (0291) 6910780</a>
    </div>
  </footer>
@endif
@if(Auth::user()->kategori == 3) 
<div class="jumbotron">
    <header id="header-wrap">
      <section id="header">
        <nav class="navbar navbar-light" data-spy="affix" data-offset-top="250">
          <div class="container">
            <div  id="main-menu">

              <a class="navbar-brand" href="/home">
                <img style="margin-left: -100px; margin-bottom: 0px; margin-top: -40px;" src="{{asset('admin/img/utama.png')}}" alt="">
              </a>

              <div>
                <ul style="margin-left: 500px; margin-top: 0px; margin-bottom: -95px;"  class="nav nav-inline" >
                <li>
                  <a class="nav-link active" href="/home" role="button" aria-haspopup="true" aria-expanded="false">Beranda</a>         
                </li> 

                <li class="dropdown">
                  <a class="nav-link active" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    Profil
                  </a>
                  <div class="dropdown-content">                      
                    <a class="dropdown-item" href="/user/detailProfil">Lihat Profil</a>
                    <!-- <a class="dropdown-item" href="/user/ubahProfil">Edit Profil</a> -->
                    <a class="dropdown-item" href="/user/ubahKataSandi">Ubah Kata Sandi</a>
                  </div>
                </li>                                   

                <li class="dropdown">
                  <a class="nav-link dropdown-item" href="{{ url('/halamanDosen/unggahNilai') }}" role="button" aria-haspopup="true" aria-expanded="false">
                    Unggah Nilai
                  </a>                     
                </li>
                <li class="dropdown">
                  <a class="nav-link dropdown-item" href="{{ url('/halamanDosen/lihatNilai') }}" role="button" aria-haspopup="true" aria-expanded="false">
                    Informasi Nilai
                  </a>                                    
                </li> 
                  
                <li>
                  <a class="nav-link dropdown-item" href="{{ route('logout') }}"onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" class="btn btn-metis-1 btn-sm" role="button" aria-haspopup="true" aria-expanded="false">
                    <b><i>Logout</b></i>
                  </a>                                    
                </li> 
                </ul>
                </div>
          
              <ul>   
                <div class="btn-group">
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>         
              </ul>  

            </div> 
          </div>
        </nav>
      </section>
    </header>
  </div>

     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-12">
          <div  class="col-lg-12" style="width:10%; margin-right:0px !important; margin-left:0px !important;"> 
            @yield('content')
          </div><!-- /.col -->
          </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  

<footer class="main-footer" style="margin-left: 0 !important;  margin-bottom: 0 !important "  >
    <strong>   <a href="">SIAPEN</a>.</strong>
    Akademi Komunitas Negeri Demak. <strong>2019</strong>
    <div class="float-right d-none d-sm-inline-block">
       <a><i class="fa fa-phone"></i> (0291) 6910780</a>
    </div>
  </footer>
@endif    
@if(Auth::user()->kategori == 4) 
<div class="jumbotron">
    <header id="header-wrap">
      <section id="header">
        <nav class="navbar navbar-light" data-spy="affix" data-offset-top="250">
          <div class="container">
            <div  id="main-menu">

              <a class="navbar-brand" href="/home">
                <img style="margin-left: -100px; margin-bottom: 0px; margin-top: -40px;" src="{{asset('admin/img/utama.png')}}" alt="">
              </a>

              <div>
                <ul style="margin-left: 500px; margin-top: 0px; margin-bottom: -95px;"  class="nav nav-inline" >
                <li>
                  <a class="nav-link active" href="/home" role="button" aria-haspopup="true" aria-expanded="false">Beranda</a>         
                </li> 

                <li class="dropdown">
                  <a class="nav-link active" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    Profil
                  </a>
                  <div class="dropdown-content">                      
                    <a class="dropdown-item" href="/user/detailProfil">Lihat Profil</a>
                   <!--  <a class="dropdown-item" href="/user/ubahProfil">Edit Profil</a> -->
                    <a class="dropdown-item" href="/user/ubahKataSandi">Ubah Kata Sandi</a>
                  </div>
                </li> 
                 <li class="dropdown">
                  <a class="nav-link active" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                   Informasi Nilai
                  </a>
                  <div class="dropdown-content">                      
                    <a class="dropdown-item" href="/kartuRencana/lihatKRS">Lihat KRS</a>
                    <a class="dropdown-item" href="/hasilStudi/lihatHSM">Lihat HSM</a>
                    <a class="dropdown-item" href="/hasilStudi/lihatTranskrip">Lihat Transkrip</a>
                  </div>
                </li>
                <li class="dropdown">
                  <a class="nav-link active" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                   Kartu Ujian
                  </a>
                  <div class="dropdown-content">                      
                    <a class="dropdown-item" href="/cetak/pdfUTS">Kartu UTS</a>
                    <a class="dropdown-item" href="/cetak/pdfUAS">Kartu UAS</a>
                  </div>
                </li> 
                  
                <li>
                  <a class="nav-link dropdown-item" href="{{ route('logout') }}"onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" class="btn btn-metis-1 btn-sm" role="button" aria-haspopup="true" aria-expanded="false">
                    <b><i>Logout</b></i>
                  </a>                                    
                </li> 
                </ul>
                </div>
          
              <ul>   
                <div class="btn-group">
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>         
              </ul>  

            </div> 
          </div>
        </nav>
      </section>
    </header>
  </div>

     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-12">
          <div  class="col-lg-12" style="width:10%; margin-right:0px !important; margin-left:0px !important;"> 
            @yield('content')
          </div><!-- /.col -->
          </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  

<footer class="main-footer" style="margin-left: 0 !important;  margin-bottom: 0 !important "  >
    <strong>   <a href="">SIAPEN</a>.</strong>
    Akademi Komunitas Negeri Demak.<strong>2019</strong>
    <div class="float-right d-none d-sm-inline-block">
       <a><i class="fa fa-phone"></i> (0291) 6910780</a>
    </div>
  </footer>
@endif  
@if(Auth::user()->kategori == 5) 
<div class="jumbotron">
    <header id="header-wrap">
      <section id="header">
        <nav class="navbar navbar-light" data-spy="affix" data-offset-top="250">
          <div class="container">
            <div  id="main-menu">

              <a class="navbar-brand" href="/home">
                <img style="margin-left: -100px; margin-bottom: 0px; margin-top: -40px;" src="{{asset('admin/img/utama.png')}}" alt="">
              </a>

              <div>
                <ul style="margin-left: 800px; margin-top: 0px; margin-bottom: -95px;"  class="nav nav-inline" >
                <li>
                  <a class="nav-link active" href="/home" role="button" aria-haspopup="true" aria-expanded="false">Beranda</a>         
                </li> 

                <li class="dropdown">
                  <a class="nav-link active" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    Profil
                  </a>
                  <div class="dropdown-content">                      
                    <a class="dropdown-item" href="/user/detailProfil">Lihat Profil</a>
                   <!--  <a class="dropdown-item" href="/user/ubahProfil">Edit Profil</a> -->
                    <a class="dropdown-item" href="/user/ubahKataSandi">Ubah Kata Sandi</a>
                  </div>
                </li> 
                  
                <li>
                  <a class="nav-link dropdown-item" href="{{ route('logout') }}"onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" class="btn btn-metis-1 btn-sm" role="button" aria-haspopup="true" aria-expanded="false">
                    <b><i>Logout</b></i>
                  </a>                                    
                </li> 
                </ul>
                </div>
          
              <ul>   
                <div class="btn-group">
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>         
              </ul>  

            </div> 
          </div>
        </nav>
      </section>
    </header>
  </div>

     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-12">
          <div  class="col-lg-12" style="width:10%; margin-right:0px !important; margin-left:0px !important;"> 
            @yield('content')
          </div><!-- /.col -->
          </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  

<footer class="main-footer" style="margin-left: 0 !important;  margin-bottom: 0 !important "  >
    <strong>   <a href="">SIAPEN</a>.</strong>
    Akademi Komunitas Negeri Demak.<strong>2019</strong>
    <div class="float-right d-none d-sm-inline-block">
       <a><i class="fa fa-phone"></i> (0291) 6910780</a>
    </div>
  </footer>
@endif
@if(Auth::user()->kategori == 6) 
<div class="jumbotron">
    <header id="header-wrap">
      <section id="header">
        <nav class="navbar navbar-light" data-spy="affix" data-offset-top="250">
          <div class="container">
            <div  id="main-menu">

              <a class="navbar-brand" href="/home">
                <img style="margin-left: -100px; margin-bottom: 0px; margin-top: -40px;" src="{{asset('admin/img/utama.png')}}" alt="">
              </a>

              <div>
                <ul style="margin-left: 800px; margin-top: 0px; margin-bottom: -95px;"  class="nav nav-inline" >
                <li>
                  <a class="nav-link active" href="/home" role="button" aria-haspopup="true" aria-expanded="false">Beranda</a>         
                </li> 

                <li class="dropdown">
                  <a class="nav-link active" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    Profil
                  </a>
                  <div class="dropdown-content">                      
                    <a class="dropdown-item" href="/user/detailProfil">Lihat Profil</a>
                   <!--  <a class="dropdown-item" href="/user/ubahProfil">Edit Profil</a> -->
                    <a class="dropdown-item" href="/user/ubahKataSandi">Ubah Kata Sandi</a>
                  </div>
                </li> 
                  
                <li>
                  <a class="nav-link dropdown-item" href="{{ route('logout') }}"onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" class="btn btn-metis-1 btn-sm" role="button" aria-haspopup="true" aria-expanded="false">
                    <b><i>Logout</b></i>
                  </a>                                    
                </li> 
                </ul>
                </div>
          
              <ul>   
                <div class="btn-group">
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>         
              </ul>  

            </div> 
          </div>
        </nav>
      </section>
    </header>
  </div>

     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-12">
          <div  class="col-lg-12" style="width:10%; margin-right:0px !important; margin-left:0px !important;"> 
            @yield('content')
          </div><!-- /.col -->
          </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  

<footer class="main-footer" style="margin-left: 0 !important;  margin-bottom: 0 !important "  >
    <strong>   <a href="">SIAPEN</a>.</strong>
    Akademi Komunitas Negeri Demak.<strong>2019</strong>
    <div class="float-right d-none d-sm-inline-block">
       <a><i class="fa fa-phone"></i> (0291) 6910780</a>
    </div>
  </footer>
@endif  
@if(Auth::user()->kategori == 7) 
<div class="jumbotron">
    <header id="header-wrap">
      <section id="header">
        <nav class="navbar navbar-light" data-spy="affix" data-offset-top="250">
          <div class="container">
            <div  id="main-menu">

              <a class="navbar-brand" href="/home">
                <img style="margin-left: -100px; margin-bottom: 0px; margin-top: -40px;" src="{{asset('admin/img/utama.png')}}" alt="">
              </a>

              <div>
                <ul style="margin-left: 500px; margin-top: 0px; margin-bottom: -95px;"  class="nav nav-inline" >
                <li>
                  <a class="nav-link active" href="/home" role="button" aria-haspopup="true" aria-expanded="false">Beranda</a>         
                </li> 

                <li class="dropdown">
                  <a class="nav-link active" class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                    Profil
                  </a>
                  <div class="dropdown-content">                      
                    <a class="dropdown-item" href="/user/detailProfil">Lihat Profil</a>
                    <!-- <a class="dropdown-item" href="/user/ubahProfil">Edit Profil</a> -->
                    <a class="dropdown-item" href="/user/ubahKataSandi">Ubah Kata Sandi</a>
                  </div>
                </li>                                   

                <li class="dropdown">
                  <a class="nav-link dropdown-item" href="{{ url('/halamanDosen/unggahNilai') }}" role="button" aria-haspopup="true" aria-expanded="false">
                    Unggah Nilai
                  </a>                     
                </li>
                <li class="dropdown">
                  <a class="nav-link dropdown-item" href="{{ url('/halamanDosen/lihatNilai') }}" role="button" aria-haspopup="true" aria-expanded="false">
                    Informasi Nilai
                  </a>                                    
                </li> 
                  
                <li>
                  <a class="nav-link dropdown-item" href="{{ route('logout') }}"onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" class="btn btn-metis-1 btn-sm" role="button" aria-haspopup="true" aria-expanded="false">
                    <b><i>Logout</b></i>
                  </a>                                    
                </li> 
                </ul>
                </div>
          
              <ul>   
                <div class="btn-group">
                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </div>         
              </ul>  

            </div> 
          </div>
        </nav>
      </section>
    </header>
  </div>

     <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-12">
          <div  class="col-lg-12" style="width:10%; margin-right:0px !important; margin-left:0px !important;"> 
            @yield('content')
          </div><!-- /.col -->
          </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  

<footer class="main-footer" style="margin-left: 0 !important;  margin-bottom: 0 !important "  >
    <strong>   <a href="">SIAPEN</a>.</strong>
    Akademi Komunitas Negeri Demak. <strong>2019</strong>
    <div class="float-right d-none d-sm-inline-block">
       <a><i class="fa fa-phone"></i> (0291) 6910780</a>
    </div>
  </footer>
@endif
                            
     
    <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="fa fa-angle-up">
      </i>
    </a>


    <!-- JavaScript & jQuery Plugins -->
    <!-- jQuery Load -->
    <script src="{{asset('assets/js/jquery-min.js')}}"></script>
    <!-- Bootstrap JS -->
    <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
    <!--Text Rotator-->
    <script src="{{asset('assets/js/jquery.mixitup.js')}}"></script>
    <!--WOW Scroll Spy-->
    <script src="{{asset('assets/js/wow.js')}}"></script>
    <!-- OWL Carousel -->
    <script src="{{asset('assets/js/owl.carousel.js')}}"></script>
 
    <!-- WayPoint -->
    <script src="{{asset('assets/js/waypoints.min.js')}}"></script>
    <!-- CounterUp -->
    <script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>
    <!-- ScrollTop -->
    <script src="{{asset('assets//js/scroll-top.js')}}"></script>
    <!-- Appear -->
    <script src="{{asset('assets/js/jquery.appear.js')}}"></script>
    <script src="{{asset('assets/js/jquery.vide.js')}}"></script>
     <!-- All JS plugin Triggers -->
    <script src="{{asset('assets/js/main.js')}}"></script>
   

    <!-- jQuery -->
    <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
    <script src="/js/custom.js"></script>
     <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    @include('sweet::alert')
    @yield('script')
    <!-- jQuery UI 1.11.4 -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="{{asset('plugins/morris/morris.min.js')}}"></script>
    <!-- Sparkline -->
    <script src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
    <!-- jvectormap -->
    <script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{asset('plugins/knob/jquery.knob.js')}}"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
    <!-- datepicker -->
    <script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
    <!-- Slimscroll -->
    <script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
    <!-- FastClick -->
    <script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('admin/js/adminlte.js')}}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{asset('admin/js/pages/dashboard.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('admin/js/demo.js')}}"></script>
    
  </body>
</html>
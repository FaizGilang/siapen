<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SIPENI') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/grayscale.min.css') }}" rel="stylesheet">
    <style>

        .panel-transparent {
            
            /*background:#000;opacity:0.4;filter:alpha(opacity=40);*/
        }

        .panel-transparent .panel-heading{
            background: rgba(4, 13, 27,  0.85)!important;
        }

        .panel-transparent .panel-body{
            background: rgba(4, 13, 27, 0.85)!important;
        }
        .x {
           font-family: "Rockwell ", "Rockwell Bold", monospace;
       }

           
            
       </style>
   </head>
   <body class="download-section">
      
    @yield('content')
    

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="/js/custom.js"></script>
    <script src="{{ URL::asset('js/grayscale.min.js') }}"></script>
</body>
</html>

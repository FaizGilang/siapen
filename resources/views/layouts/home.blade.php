<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> AKN Demak</title>
  <title>@yield('page-title')</title>
  <link rel="icon" href="{{asset('admin/img/logoAKN.png')}}" />
  <!-- Tell the browser to be responsive to screen width -->
   <link rel="stylesheet" type="text/css" href="assets/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugins/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('admin/css/adminlte.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('plugins/iCheck/flat/blue.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{asset('plugins/morris/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{asset('plugins/datepicker/datepicker3.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('plugins/daterangepicker/daterangepicker-bs3.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

  <link href="{{asset('admin/img/admin.jpg')}}" size="76x76" rel="icon">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/home" class="nav-link">Home</a>
      </li>
      
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Messages Dropdown Menu -->
      <div class="btn-group">
            <a href="{{ route('logout') }}"onclick="event.preventDefault();
            document.getElementById('logout-form').submit();" class="btn btn-metis-1 btn-sm">
            <i class="fa fa-power-off"> Logout</i>
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>
    </ul>
     
  </nav>
  <!-- /.navbar -->
     <section class="content">
      <div class="container-fluid">
  @yield('board')
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/home" class="brand-link">
      <img src="{{asset('admin/img/admin.jpg')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">AKN Demak</span>
    </a>

    <!-- Sidebar -->
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @if(Auth::user()->kategori == 1)

          <li class="nav-item has-treeview">
            <a href="{{ url('/home') }}" class="nav-link">
              <i class="nav-icon fa fa-info"></i>
              <p>
                Dashboard
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
          </li>            
          <li class="nav-item has-treeview">
            <a href="{{ url('/kategoriPengguna/lihat') }}" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Kategori Pengguna
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
          </li>
           <li class="nav-item has-treeview">
            <a href="" class="nav-link">
              <i class="nav-icon fa fa-users"></i>
              <p>
                Pengguna
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/user/lihat1') }}" class="nav-link">
                  <i class="fa fa-eye nav-icon"></i>
                  <p>Lihat Data Dosen</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/user/lihat') }}" class="nav-link">
                  <i class="fa fa-eye nav-icon"></i>
                  <p>Lihat Data Mahasiswa</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{ url('/prodi/lihat') }}" class="nav-link">
              <i class="nav-icon fa fa-building"></i>
              <p>
                Program Studi
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{ url('/kurikulum/lihat') }}" class="nav-link">
              <i class="nav-icon fa fa-file"></i>
              <p>
                Kurikulum
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
          </li>
           <li class="nav-item has-treeview">
            <a href="{{ url('/matkul/lihat') }}" class="nav-link">
              <i class="nav-icon fa fa-book"></i>
              <p>
                Mata Kuliah
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            </li>
             <li class="nav-item has-treeview">
            <a href="{{ url('/halamanAdmin/faseAkademik') }}" class="nav-link">
              <i class="nav-icon fa fa-calendar"></i>
              <p>
                Fase Akademik
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            </li>
            <li class="nav-item has-treeview">
            <a href="{{ url('/halamanAdmin/paketKuliah') }}" class="nav-link">
              <i class="nav-icon fa fa-folder"></i>
              <p>
                Paket Kuliah
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            </li>
            
            <li class="nav-item has-treeview">
            <a href="{{ url('/halamanAdmin/daftarAbsen') }}" class="nav-link">
              <i class="nav-icon fa fa-database"></i>
              <p>
                Absen Mahasiswa
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            </li>

          @endif
         
          @if(Auth::user()->kategori == 2)
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-table"></i>
              <p>
                Kartu Rencana Studi
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/kartuRencana/formBukaKRS') }}" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>Fase Akademik</p>
                </a>
              </li>
            </ul>
          </li>
           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-clone"></i>
              <p>
                Paket Kuliah
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/paketKuliah/add') }}" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>Tambah Paket Kuliah</p>
                </a>
              </li>
            </ul>
             <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/paketKuliah/lihatpaket') }}" class="nav-link">
                  <i class="fa fa-eye nav-icon"></i>
                  <p>Lihat Data Paket Kuliah</p>
                </a>
              </li>
            </ul>
          </li>
          @endif
          @if(Auth::user()->kategori == 3)
           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Profil 
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/user/detailProfil') }}" class="nav-link">
                  <i class="fa fa-eye nav-icon"></i>
                  <p>Lihat Profil</p>
                </a>
              </li>
            </ul>
             <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/user/ubahProfil') }}" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>Edit Profil</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-edit"></i>
              <p>
                Informasi Mata Kuliah
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
             <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/halamanDosen/informasiMatkul') }}" class="nav-link">
                  <i class="fa fa-upload nav-icon"></i>
                  <p>Upload Nilai</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/halamanDosen/informasiNilai') }}" class="nav-link">
                  <i class="fa fa-archive nav-icon"></i>
                  <p>Informasi Nilai</p>
                </a>
              </li>
            </ul>
          </li>
          @endif
          @if(Auth::user()->kategori == 4)
           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-user"></i>
              <p>
                Profil 
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/user/detailProfil') }}" class="nav-link">
                  <i class="fa fa-eye nav-icon"></i>
                  <p>Lihat Profil</p>
                </a>
              </li>
            </ul>
             <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/user/ubahProfil') }}" class="nav-link">
                  <i class="fa fa-plus nav-icon"></i>
                  <p>Edit Profil</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ url('/user/ubahKataSandi') }}"  class="nav-link">
                  <i class="fa fa-pencil nav-icon"></i>
                  <p>Ubah Kata Sandi</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{ url('/kartuRencana/lihatKRS') }}" class="nav-link">
              <i class="nav-icon fa fa-edit"></i>
              <p>
                Informasi KRS
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="{{ url('/hasilStudi/lihatHSM') }}" class="nav-link">
              <i class="nav-icon fa fa-clipboard"></i>
              <p>
               Informasi HSM
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            
           <li class="nav-item has-treeview">
            <a href="{{ url('/hasilStudi/lihatTranskrip') }}" class="nav-link">
              <i class="nav-icon fa fa-file"></i>
              <p>
                Informasi Transkrip
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
          </li>
          @endif
      
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

    <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-3">
          <div class="col-lg-12">
            @yield('content')
          </div><!-- /.col -->
                    
              
          </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
  <!-- /.content-wrapper -->
  <footer class="main-footer" style="margin-left: 0 !important; margin-bottom: 0 !important" >
    <strong> <a href="">SIAPEN</a>.</strong>
    Akademi Komunitas Negeri Demak.<strong>2019</strong>
    <div class="float-right d-none d-sm-inline-block">
       <a><i class="fa fa-phone"></i> (0291) 6910780</a>
    </div>
  </footer>

 
</div>
<!-- ./wrapper -->
 <!-- Go To Top Link -->
    <a href="#" class="back-to-top">
      <i class="fa fa-angle-up">
      </i>
    </a>

<!-- jQuery -->
<script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
<script src="/js/custom.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    @include('sweet::alert')
@yield('script')
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="{{asset('plugins/morris/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src="{{asset('plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('admin/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('admin/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('admin/js/demo.js')}}"></script>
</body>
</html>
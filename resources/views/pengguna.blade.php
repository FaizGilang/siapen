

@extends('layouts.pengguna')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card">
            <div style="margin-left: 500px; margin-top: 50px;" class="images"><br>
              <img src="assets/img/about/avatar3.png" alt="">
            </div>
            <h1 style="margin-left: 430px;">Hai <b>{{Auth::user()->nama}}..</b></h1><br>
                    <h5 style="margin-left: 430px;">Selamat Datang di Sistem Ini! </h5>
               <br>
               <br>
               <br>
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
            </div>
            </div>

        </div>
    </div>
</div>


@endsection



@extends('layouts.login')
<head>
    <title>@yield('page-title')</title>
    <link rel="icon" href="{{asset('admin/img/logoAKN.png')}}" />
</head>
@section('content')
<div class="col-md-12">
    <div class="row">
        <div class="col-md-2">
            <div class="panel-transparent"><a href="{{ route('login') }}" style="font-size: 20px; margin-top: -10px; margin-bottom: 370px; margin-left: 1200px; color: white;" align="center" class="panel-heading" >LOGIN</a>
            </div>
        </div>

        <div class="col-md-8">
            <div class="panel-transparent">
                <div style="margin-top: 150px; font-size: 50px; font-family: Times New Rowman;" align="center" class="panel-heading" >SELAMAT DATANG</div>
                <div style="margin-bottom: 50px; font-size: 30px; font-family: Times New Rowman;" align="center" class="panel-heading" >(SISTEM INFORMASI PENGELOLAAN NILAI MAHASISWA) <br> AKN-DEMAK</div>
                
            </div>
        </div>
    </div>
</div>
@endsection

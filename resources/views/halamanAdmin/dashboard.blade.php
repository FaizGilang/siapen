@extends('layouts.pengguna')


@section('content')
<div class="card">
  <div style="margin-left: 500px; margin-top: 25px;" class="images"><br>
    <img height="200" width="180" src="{{URL::asset('/FotoPengguna/'.Auth::user()->file)}}" alt="">
  </div><br>   
        <h1 style="margin-left: 350px; font-size: 50px; font-family: Times New Rowman;">Selamat Datang.. <b><br>{{Auth::user()->nama}}!</b></h1><br><br><br><br><br><br><br><br><br><br>
                 
      @include('sweet::alert')
</div>
       
<!-- Main content -->
@section('board')
  <div class="row">
          <div style="margin-left: 300px;" class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$prodi}}</h3>

                <p>Jumlah Program Studi</p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="/prodi/lihat/" class="small-box-footer">Data Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3><sup style="font-size: 20px">{{$ipk}}</sup></h3>

                <p>Jumlah IPK > 3.5 </p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">Indeks Prestasi Komulatif</a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$mhs}}</h3>

                <p>Jumlah Mahasiswa</p>
              </div>
              <div class="icon">
                <i class="ion ion-person"></i>
              </div>
              <a href="/user/lihat/" class="small-box-footer">Data Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div style="margin-left: 300px;" class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
                <h3>{{$matkul}}</h3>

                <p>Jumlah Mata Kuliah</p>
              </div>
              <div class="icon">
                <i class="ion ion-calendar"></i>
              </div>
              <a href="/matkul/lihat/" class="small-box-footer">Data Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3><sup style="font-size: 20px">{{$ipkdibawah}}</sup></h3>

                <p>Jumlah IPK < 2 </p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">Indeks Prestasi Komulatif</a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$dosen}}</h3>

                <p>Jumlah Dosen</p>
              </div>
              <div class="icon">
                <i class="ion ion-person"></i>
              </div>
              <a href="/user/lihat/" class="small-box-footer">Data Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          
          
        </div>
@endsection
@endsection
@section('page-title')

@endsection
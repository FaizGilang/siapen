<!-- TAMPILAN PROGRAM STUDI (tabel prodi) -->

@extends('layouts.home')

@section('content')
<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}
</style>
<div class="row">
    <div class="col-lg-12">
      
        <div class="card">
          <div class="row">
          <div class="col-lg-12">
            <div class="row">
            </div>
          </div>
          </div>
        <div class="row">
        <div class="col-lg-12"><br>
        <h1 style="font-family: Times New Roman; font-weight: bold;" align="center">
           FASE AKADEMIK
        </h1><br>
                <ol class="breadcrumb">
            </div>
        </div>
         
<!-- /.row -->
<div class="card-body">
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered table-striped table-condensed cf">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Tahun Ajaran</th>
                <th style="text-align:center;">Fase Registrasi</th>
                <th style="text-align:center;">Awal Kuliah</th>
                <th style="text-align:center;">Akhir Kuliah</th>
                <th style="text-align:center;">Fase Eval</th>
                <th style="text-align:center;">Semester</th>
                <th style="text-align:center;">Status</th>
            </tr>
        </thead>
            
        <tbody>
            <?php $count = 1; ?> 
            @foreach($lihat as $data) 
            <tr> 
                <td style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:center;">{{ $data->tahunAjaran }}</td> 
                <td style="text-align:center;">{{ $data->faseRegistrasi }}</td> 
                <td style="text-align:center;">{{ $data->awalKuliah }}</td> 
                <td style="text-align:center;">{{ $data->akhirKuliah }}</td> 
                <td style="text-align:center;">{{ $data->faseEval }}</td> 
                <td style="text-align:center;">{{ $data->semester }}</td> 
                <td style="text-align:center;">{{ $data->status }}</td> 
            </tr> 
            <?php $count++; ?> 
            @endforeach 
        </tbody>
     </table> 
    </div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
</div>
  
        {{ $lihat->links() }}
    </div>

@endsection


<!-- MENAMPILKAN STRUKTUR KURIKULUM YANG SUDAH DIPILIH MATKULNYA -->

@extends('layouts.home')

@section('content')

<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}

</style>

     <div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="row">
          <div class="col-md-12">
            <div class="row">
            </div>
          </div>
          </div>

   
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="row">
          <div class="col-md-12">
            <div class="row">
            </div>
          </div>
          </div>
        <br>
        <center><h1 style="font-family: Times New Roman; font-weight: bold;">Form Absen</h1>{{ $format->tahun }}_{{ $format->nama }}_{{ $format->tahunAjaran }}_{{ $format->komulatif }}</center>
        <br>
        <div class="card-body">
        <div class="col-lg-12">
            <form method="post" action="{{ url('/halamanAdmin/absenMahasiswa') }}">
            {{ csrf_field() }}
            <table class="table table-bordered table-striped table-condensed cf">
                <thead>
                <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">NIM</th>
                <th style="text-align:center;">Izin</th>
                <th style="text-align:center;">Alpa</th>
               
                </tr>
                </thead>    
            <tbody>
            <?php $count = 1; ?> 
            @foreach($paketKuliah as $data) 
            <tr>
                <td style="text-align:center;">{{ $count }}.</td>
                <td style="text-align:left;">{{ $data->nomorInduk }}</td> 
                <td style="text-align:center;">{{ $data->izin }}</td> 
                <td style="text-align:center;">{{ $data->alpa }}</td> 
               
            </tr> 
            <?php $count++; ?> 
            @endforeach    

        </tbody>
     </table> 
    </div>
    <br>
    <br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
        <div class="card-footer">
          <a style="margin-left: 10px" href="{{ url('/halamanAdmin/daftarAbsen') }}" class="fa fa-arrow-circle-left fa-2x"/></a>                            
        </div>
    </form>
    
   </div>
@endsection
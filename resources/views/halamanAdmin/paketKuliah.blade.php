
@extends('layouts.home')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;pengguna</h3>
    </div>       <!-- /.main-bar -->
</header>
                        <!-- /.head -->
<!-- /.row -->
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url('/halamanAdmin/strukturPaketKuliah') }}"> <!--sama route get-->

            {{ csrf_field() }}

          <div class="card card-info">
              <div class="row">
        <div class="col-lg-12"><br>
        <h1 style="font-family: Times New Roman; font-weight: bold;" align="center">
           DATA PAKET KULIAH
        </h1>
                <ol class="breadcrumb">
            </div>
        </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                    <div class="card-body">
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered table-striped table-condensed cf">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Kurikulum</th>
                <th style="text-align:center;">Prodi</th>
                <th style="text-align:center;">Tahun Ajaran</th>
                <th style="text-align:center;">Semester Komulatif</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Aksi</th>
            </tr>
        </thead>
            
        <tbody>
            <?php $count = 1; ?> 
            @foreach($paketKuliah as $data) 
            <tr> 
                <td style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:center;">{{ $data->tahun }}</td> 
                <td style="text-align:left;">{{ $data->namaprodi }}</td> 
                <td style="text-align:center;">{{ $data->tahunAjaran }}</td> 
                <td style="text-align:center;">{{ $data->komulatif }}</td> 
                <td style="text-align:center;">{{ $data->status }}</td> 
                <td style="text-align:center;"> 
                             
   
                  
                   <a  data-placement="top" data-original-title="Detail" data-toggle="tooltip" href="{{ url('/halamanAdmin/strukturPaketKuliah/' . $data->id) }}" class="btn btn-info" align="center"><i style="color: white" class="fa  fa-eye">  Detail</i></a>
                   </td>
              
                   
                   
            </tr> 
            <?php $count++; ?> 
            @endforeach 
        </tbody>
  
     </table>
    
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
     <br>
    </div> 
@endsection

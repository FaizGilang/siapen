@extends('layouts.home')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;pengguna</h3>
    </div>       <!-- /.main-bar -->
</header>
                        <!-- /.head -->
<!-- /.row -->
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <form method="post" action=""> <!--sama route get-->

            {{ csrf_field() }}

          <div class="card card-info">
              <div class="row">
        <div class="col-lg-12"><br>
        <h1 style="font-family: Times New Roman; font-weight: bold;" align="center">
           INFORMASI NILAI
        </h1>
                <ol class="breadcrumb">
            </div>
        </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                   <div>
                      <label>Pilih Mata Kuliah:</label>
                      <select class="form-control" name="lihatNilai">
                        <option value="">-Pilih Mata Kuliah-</option>
                        @foreach($lihatNilai as $a)
                        <option  value="{{ $a->id }}">{{ $a->nama }}</option>
                        @endforeach
                      </select>

      </div>
      <div><br>
        <table class="table table-bordered" >
         <thead>
          <tr>
            <td style="text-align:center;">Nomor Induk Mahasiswa</td>
            <td style="text-align:center;">Nilai</td>
          </tr>
        </thead>
        <tbody id="table">
          </tbody>
        </table>
               <br>
     <br>
     <br>
     <br>
      <br>
     <br>
     <br>
     <br>
     <br>
     <br>
    </div> 
    <br>
<br>
<br>
<br>
<br>
@endsection

@section('script')
 <script type="text/javascript">
    jQuery(document).ready(function ()
    {
            jQuery('select[name="lihatNilai"]').on('click',function(){
               var countryID = jQuery(this).val();
               if(countryID)
               {
                  jQuery.ajax({
                     url : '/halamanDosen/lihatNilai/tabelNilaiA/' +countryID,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('#table').empty();
                        jQuery.each(data, function(key,value){
                         $("#table").append('<tr><td >'+ value +'</td><td align="center">'+ key +'</td></tr>');
                        });
                     }
                  });
               }
            });
    });
    </script>

@endsection

<!--  -->
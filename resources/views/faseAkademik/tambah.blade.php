<!-- TAMPILAN FORM TAMBAH PROGRAM STUDI BARU -->

@extends('layouts.pengguna')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Fase Akademik</h3>
    </div>       <!-- /.main-bar -->
</header>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url('/faseAkademik/tambah') }}">  <!-- url ROUTE /prodi/add -->
            {{ csrf_field() }}
          <div class="card card-primary">
              <div class="card-header">
                <h1 align="center" class="card-title style" style="font-family: Times New Rowman;">Tambah Fase Akademik</h1>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                  
                  <div class="form-group {{ $errors->has('tahunAjaran') ? 'has-error' : '' }}">
                    <label>Tahun Ajaran:</label>
                    <input type="text" name="tahunAjaran" class="form-control" value="{{ old('tahunAjaran') }}" placeholder="2014/2015">
                    {!! $errors->first('tahunAjaran', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div class="form-group {{ $errors->has('faseRegistrasi') ? 'has-error' : '' }}">
                    <label>Fase Regitrasi:</label>
                    <input type="date" name="faseRegistrasi" class="form-control" placeholder="date" value="{{ old('faseRegistrasi') }}">
                     {!! $errors->first('faseRegistrasi', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div class="form-group {{ $errors->has('awalKuliah') ? 'has-error' : '' }}">
                    <label>Awal Kuliah:</label>
                    <input type="date" name="awalKuliah" class="form-control" placeholder="date" value="{{ old('awalKuliah') }}">
                     {!! $errors->first('awalKuliah', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div class="form-group {{ $errors->has('akhirKuliah') ? 'has-error' : '' }}">
                    <label>Akhir Kuliah:</label>
                    <input type="date" name="akhirKuliah" class="form-control" placeholder="date" value="{{ old('akhirKuliah') }}">
                     {!! $errors->first('akhirKuliah', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div class="form-group {{ $errors->has('faseEval') ? 'has-error' : '' }}">
                    <label>Fase Eval:</label>
                    <input type="date" name="faseEval" class="form-control" placeholder="date" value="{{ old('faseEval') }}">
                     {!! $errors->first('faseEval', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div>
                    <label>Semester:</label>
                    <select class="form-control" name="semester">
                        <option value="" selected disabled>-Pilih Semester-</option>
                        <option value="ganjil">Ganjil</option>
                        <option value="genap">Genap</option>
                    </select>
                    {!! $errors->first('semester', '<p class="text-danger">:message</p>') !!}
                </div>

                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <input style="margin-left: 500px" type="submit" name="save" value="Simpan" class="btn btn-md btn-success" data-toggle="modal" data-target="#myModal">
                  
                  <input type="reset" name="reset" class="btn btn-danger" value="Batal" />
                </div>

              </form>

            </div>

        </form>

    </div>
</div>
@endsection

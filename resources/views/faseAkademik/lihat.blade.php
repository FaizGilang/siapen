<!-- TAMPILAN PROGRAM STUDI (tabel prodi) -->

@extends('layouts.pengguna')

@section('content')
<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="card card-primary">
           <div class="card-header">
              <h1 style="margin-left: 500px; font-family: Times New Rowman;" class="card-title">Lihat Fase Akademik</h1>
                <div class="row"></div>
            </div><br>
            <div style="margin-left: 80px;" class="col-md-2">
              <a href="{{ url('/faseAkademik/tambah') }}" class="btn btn-success" style="width:100%; margin-right:0px !important; margin-left:0px !important;">
                  <h4  class="fa fa-plus nav-icon">     Fase Akademik </h4>
              </a>
            </div>
<!-- /.row -->
<div class="card-body">
  <div style="margin-left: 50px;" class="col-lg-11">
    <input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
      <table class="table table-bordered table-striped table-condensed cf">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Tahun Ajaran</th>
                <th style="text-align:center;">Fase Registrasi</th>
                <th style="text-align:center;">Awal Kuliah</th>
                <th style="text-align:center;">Akhir Kuliah</th>
                <th style="text-align:center;">Fase Eval</th>
                <th style="text-align:center;">Semester</th>
                <th style="text-align:center;">Status</th>
                <th style="text-align:center;">Aksi</th>
            </tr>
        </thead>
            
        <tbody>
            <?php $count = 1; ?> 
            @foreach($lihat as $data) 
            <tr> 
                <td style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:center;">{{ $data->tahunAjaran }}</td> 
                <td style="text-align:center;">{{ $data->faseRegistrasi }}</td> 
                <td style="text-align:center;">{{ $data->awalKuliah }}</td> 
                <td style="text-align:center;">{{ $data->akhirKuliah }}</td> 
                <td style="text-align:center;">{{ $data->faseEval }}</td> 
                <td style="text-align:center;">{{ $data->semester }}</td> 
                <td style="text-align:center;">{{ $data->status }}</td> 
                <td style="text-align:left;"> 
                @if($data->status==="tidak")
               <!--  <a style="margin-left: 50px;" data-placement="top" data-original-title="Status" data-toggle="tooltip" href="{{ url('/faseAkademik/status/' . $data->id) }}" class="btn btn-success" align="center"><i style="color: white" class="fa  fa-check-circle"">  Aktif</i></a>|| -->
                 <a data-placement="top" data-id="{{$data->id}}" class="delete-modal btn btn-xs btn-danger"><i style="color: white" class="fa fa-trash">  Hapus</i></a> ||               
                @endif
                 
                    <a  data-placement="top" data-original-title="Edit" data-toggle="tooltip" href="{{ url('/faseAkademik/ubah/' . $data->id) }}" class="btn btn-warning" align="center"><i style="color: white" class="fa  fa-pencil">  Ubah</i></a>

                </td>
            </tr> 
            <?php $count++; ?> 
            @endforeach 
        </tbody>
     </table> 
     <br>
                 <br>
                 <br>
                 <br>
    </div>
        {{ $lihat->links() }}
    </div>
  </div>
</div>

</div>
<script src="/pathto/js/sweetalert.js"></script>
     <script type="text/javascript">
        $(document).on('click', '.delete-modal', function() {
            var id = $(this).attr('data-id');
            var result = confirm("Apakah data fase akademik ini yakin dihapus?");
            if (result) {
                window.location.href = '{{ url('/faseAkademik/hapus')}}/'+id;
            }

        }); 
    </script>

@endsection


<!-- TAMPILAN FORM EDIT PROGRAM STUDI BARU -->

@extends('layouts.pengguna')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}
            <div class="card card-primary">
              <div class="card-header">
                <h1 style="margin-left: 500px; font-family: Times New Rowman;" class="card-title">Ubah Fase Akademik</h1>
              </div>
               <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">

                <div class="form-group {{ $errors->has('tahunAjaran') ? 'has-error' : '' }}">
                    <label>Tahun Ajaran:</label>
                    <input type="text" name="tahunAjaran" class="form-control" value="{{ $fase->tahunAjaran }}" placeholder="2014/2015">
                    {!! $errors->first('tahunAjaran', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div class="form-group {{ $errors->has('faseRegistrasi') ? 'has-error' : '' }}">
                    <label>Fase Regitrasi:</label>
                    <input type="date" name="faseRegistrasi" class="form-control" placeholder="date" value="{{ $fase->faseRegistrasi }}">
                     {!! $errors->first('faseRegistrasi', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div class="form-group {{ $errors->has('awalKuliah') ? 'has-error' : '' }}">
                    <label>Awal Kuliah:</label>
                    <input type="date" name="awalKuliah" class="form-control" placeholder="date" value="{{ $fase->awalKuliah }}">
                     {!! $errors->first('awalKuliah', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div class="form-group {{ $errors->has('akhirKuliah') ? 'has-error' : '' }}">
                    <label>Akhir Kuliah:</label>
                    <input type="date" name="akhirKuliah" class="form-control" placeholder="date" value="{{ $fase->akhirKuliah }}">
                     {!! $errors->first('akhirKuliah', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div class="form-group {{ $errors->has('faseEval') ? 'has-error' : '' }}">
                    <label>Fase Eval:</label>
                    <input type="date" name="faseEval" class="form-control" placeholder="date" value="{{ $fase->faseEval }}">
                     {!! $errors->first('faseEval', '<p class="text-danger">:message</p>') !!}
                  </div>


                  <div class="form-group {{ $errors->has('semester') ? 'has-error' : '' }}">
                    <label>Semester:</label>
                    <select class="form-control" value="{{ $fase->semester }}" name="semester">
                        <option value="" selected disabled>-Pilih Semester-</option>
                        <option value="ganjil">Ganjil</option>
                        <option value="genap">Genap</option>
                    </select>
                    {!! $errors->first('semester', '<p class="text-danger">:message</p>') !!}
                </div>


            <div class="card-footer">              
                <input style="margin-left: 500px" type="submit" name="save" value="Perbarui" class="btn btn-md btn-success">
                <a href="{{ url('/faseAkademik/lihat') }}"  name="reset" class="btn btn-danger" value="Batal" /> Batal   </a>             
            </div>


          </div>
        </form>
      </div>
    </form>
  </div>
</div>
    
@endsection

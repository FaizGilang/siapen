<!-- MENAMPILKAN DATA KURIKULUM & CHECKBOX-->

@extends('layouts.pengguna')

@section('content')
<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}

</style>
<div class="row">
    <div class="col-md-12">
      @if(Session::has('alert-success'))
      <div class="alert alert-success">
        Program Studi  : {{Session::get('alert-success')}}<br>
        Angkatan       : {{Session::get('alert-success2')}}<br>
        Semester       : {{Session::get('alert-success3')}}<br>
        Kurikulum      : {{Session::get('alert-success4')}}  
      </div>
      @endif
    </div>
  </div>

<div class="row">
    <div class="col-md-12">
        <div class="card">
          
        <br>
        <center><h1 style="font-family: Times New Roman;" >
          Daftar Mata Kuliah

        </h1>{{ $judul->tahun }}_{{ $judul->namaprodi }}_{{ $judul->tahunAjaran }}_{{ $judul->komulatif }}</center><br>

        @if ($message = Session::get('success'))
          <div class="alert alert-success" role="alert">
            {{ Session::get('success') }}
          </div>
        @endif
        @if ($message = Session::get('error'))
          <div class="alert alert-danger" role="alert">
            {{ Session::get('error') }}
          </div>
        @endif
       
<div class="col-lg-12">
         <form method="post" action="{{ url()->current() }}">  <!-- ke web-> route -->
            
            {{ csrf_field() }}
     <table class="table table-bordered table-striped table-condensed cf">
        <thead>
           <tr>
            <th style="text-align:center;">Pilih</th>
                <th style="text-align:center;">No</th>
                <th style="text-align:center;">Nama</th>
                <th style="text-align:center;">Kode</th>
                <th style="text-align:center;">SKS</th>
                <th style="text-align:center;">Teori</th>
                <th style="text-align:center;">Praktek</th>   
            </tr>
        </thead>        
        <tbody>

       <?php $count = 1; ?> 
        @if( !empty($lama))
        @foreach($lama as $data) 
            <tr>
                 <td><input style="text-align:center; margin-left: 35px;" checked type="checkbox" name="matkul[]" value="{{$data->id}}"> </td>
                <td style="text-align:center;">{{ $count }}.</td>
                <td style="text-align:left;">{{ $data->nama }}</td> 
                <td style="text-align:center;">{{ $data->kode }}</td> 
                <td style="text-align:center;">{{ $data->SKS }}</td> 
                <td style="text-align:center;">{{ $data->SKSteori }}</td> 
                <td style="text-align:center;">{{ $data->SKSpraktek }}</td> 
            </tr> 
            <?php $count++; ?> 
            @endforeach 
        @endif
         
            @foreach($paketKuliah as $data) 
            <tr>
                <td><input style="text-align:center; margin-left: 35px;" type="checkbox" name="matkul[]" value="{{$data->id}}"> </td>
                <td style="text-align:center;">{{ $count }}.</td>
                <td style="text-align:left;">{{ $data->nama }}</td> 
                <td style="text-align:center;">{{ $data->kode }}</td> 
                <td style="text-align:center;">{{ $data->SKS }}</td> 
                <td style="text-align:center;">{{ $data->SKSteori }}</td> 
                <td style="text-align:center;">{{ $data->SKSpraktek }}</td> 
            </tr> 
            <?php $count++; ?> 
            @endforeach
               @foreach($paketKuliah1 as $data) 
            <tr>
                 <td><input style="text-align:center; margin-left: 35px;" type="checkbox" name="matkul[]" value="{{$data->id}}"> </td>
                <td style="text-align:center;">{{ $count }}.</td>
                <td style="text-align:left;">{{ $data->nama }}</td> 
                <td style="text-align:center;">{{ $data->kode }}</td> 
                <td style="text-align:center;">{{ $data->SKS }}</td> 
                <td style="text-align:center;">{{ $data->SKSteori }}</td> 
                <td style="text-align:center;">{{ $data->SKSpraktek }}</td> 
            </tr> 
            <?php $count++; ?> 
            @endforeach  
        </tbody>
     </table> 
    </div>

    <div class="card-footer">
        
        <input style="margin-left: 600px" type="submit" name="save" value="Simpan" class="btn btn-md btn-success">

    </div>
  </form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection
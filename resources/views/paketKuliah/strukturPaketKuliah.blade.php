<!-- MENAMPILKAN STRUKTUR KURIKULUM YANG SUDAH DIPILIH MATKULNYA -->

@extends('layouts.pengguna')

@section('content')

<style>
.label1 {;
  min-width: 80px !important;
  display: inline-block !important
}

</style>

     <div class="row">
    <div class="col-md-12">
        <div class="">
          <div class="row">
          <div class="col-md-12">
            <div class="row">
            </div>
          </div>
          </div>

   
<div class="row">
    <div class="col-md-12">
        <div class="card">
          <div class="row">
          <div class="col-md-12">
            <div class="row">
            </div>
          </div>
          </div>
        <br>
        <center><h1>Daftar Paket Kuliah</h1>
        {{ $coba->tahun }}_{{ $coba->namaprodi }}_{{ $coba->tahunAjaran }}_{{ $coba->komulatif }}</center>
        <br>
        <div class="card-body">
        <div class="col-lg-12">
            <form method="post" action="{{ url('/paketKuliah/strukturPaketKuliah') }}">
            {{ csrf_field() }}
            <table class="table table-bordered table-striped table-condensed cf">
                <thead>
                <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Nama Matkul</th>
                <th style="text-align:center;">Kode Matkul</th>
                <th style="text-align:center;">Teori</th>
                <th style="text-align:center;">Praktek</th>
                <th style="text-align:center;">SKS</th>
                </tr>
                </thead>    
            <tbody>
            <?php $count = 1;
                  $jumlahsks = 0
            ?> 
            @foreach($paketKuliah as $data) 
            <tr>
                <td style="text-align:center;">{{ $count }}.</td>
                <td style="text-align:left;">{{ $data->nama }}</td> 
                <td style="text-align:center;">{{ $data->kode }}</td> 
                <td style="text-align:center;">{{ $data->SKSteori }}</td> 
                <td style="text-align:center;">{{ $data->SKSpraktek }}</td> 
                <td style="text-align:center;">{{ $data->SKS }}</td>

                <?php $jumlahsks = $jumlahsks+$data->SKS ?> 
            </tr> 
            <?php $count++; ?> 
            @endforeach    

            <td style="text-align:center;" colspan="5">Jumlah:</td> 
            <td style="text-align:center;" colspan="1">{{ $jumlahsks }}</td>
        </tbody>
        @include('sweet::alert')
     </table> 
    </div>
    <br>
    </form>

  </div>
              <div class="card-footer"> 
                <a style="margin-left: 10px" href="{{ url('/paketKuliah/lihat') }}" class="fa fa-arrow-circle-left fa-2x"/></a>         
            </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection

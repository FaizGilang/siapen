
@extends('layouts.pengguna')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;pengguna</h3>
    </div>       <!-- /.main-bar -->
</header>
                        <!-- /.head -->
<!-- /.row -->
@endsection

@section('content')

<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url('/paketKuliah/strukturPaketKuliah') }}"> <!--sama route get-->

            {{ csrf_field() }}

          <div class="card card-primary">
              <div class="card-header">
                <h1 style="margin-left: 500px; font-family: Times New Roman;" class="card-title">Lihat Data Paket Kuliah</h1>
              </div><br>
            <div style="margin-left: 50px;" class="col-md-2">
              <a href="{{ url('/paketKuliah/tambah') }}" class="btn btn-success" style="width:100%; margin-right:0px !important; margin-left:0px !important;">
                  <h4  class="fa fa-plus nav-icon">     Paket Kuliah </h4>
              </a>
                          </div>
            

              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
                    <div class="card-body">
<div class="col-lg-12">
<input id="signup-token" name="_token" type="hidden" value="{{csrf_token()}}">
     <table class="table table-bordered table-striped table-condensed cf">
        <thead>
           <tr>
                <th style="text-align:center;">No.</th>
                <th style="text-align:center;">Kurikulum</th>
                <th style="text-align:center;">Tahun Ajaran</th>
                <th style="text-align:center;">Semester</th>
               
                <th style="text-align:center;">Aksi</th>
            </tr>
        </thead>
            
        <tbody>
            <?php $count = 1; ?> 
            @foreach($paketKuliah as $data) 
            <tr> 
                <td style="text-align:center;">{{ $count }}.</td> 
                <td style="text-align:left;">{{ $data->tahun }} - {{$data->namaprodi}}</td> 
                <td style="text-align:center;">{{ $data->tahunAjaran }} - {{$data->semester}}</td> 
                <td style="text-align:center;">{{ $data->komulatif }}</td> 
                 
                <td style="text-align:left;"> 
                             
                @if($data->status==="belum")

                  <a style="margin-left: 0px;" data-placement="top" data-original-title="Status" data-toggle="tooltip" href="{{ url('/paketKuliah/status/' . $data->id) }}" class="btn btn-success" align="center"><i style="color: white" class="fa  fa-check-circle"">  Aktif</i></a>||
                 
                  <a  data-placement="top" data-original-title="Edit" data-toggle="tooltip" href="{{ url('/paketKuliah/ubah/' . $data->id) }}" class="btn btn-warning" align="center"><i style="color: white" class="fa  fa-pencil">  Ubah</i></a> ||

                 <a data-placement="top" data-id="{{$data->id}}" class="delete-modal btn btn-xs btn-danger"><i style="color: white" class="fa fa-trash">  Hapus</i></a> ||
                 

                  <a  data-placement="top" data-original-title="Detail" data-toggle="tooltip" href="{{ url('/paketKuliah/strukturPaketKuliah/' . $data->id) }}" class="btn btn-info" align="center"><i style="color: white" class="fa  fa-eye">  Detail</i></a>
                   </td>
                 @elseif($data->status==="tidak")
                  <a style="margin-left: 0px;" data-placement="top" data-original-title="Status" data-toggle="tooltip" href="" class="btn btn-default" align="center"><i style="color: black" class="fa  fa-check-circle"">  Aktif</i></a>||

                  <a  data-placement="top" data-original-title="Edit" data-toggle="tooltip" href="" class="btn btn-default" align="center"><i style="color: black" class="fa  fa-pencil">  Ubah</i></a> ||

                 <a data-placement="top" data-original-title="Delete" data-toggle="tooltip" value="hidden" class="btn btn-default"><i class="fa fa-trash">  Hapus     </i></a> ||

                  <a  data-placement="top" data-original-title="Detail" data-toggle="tooltip" href="{{ url('/paketKuliah/strukturPaketKuliah/' . $data->id) }}" class="btn btn-info" align="center"><i style="color: white" class="fa  fa-eye">  Detail</i></a>
                 @elseif($data->status==="aktif")
                 <span style="margin-left: 0px;"  class="btn btn-default"><i class="fa fa-check-circle">  Aktif</i></span> ||
                 <a data-placement="top" data-original-title="Ubah" data-toggle="tooltip" value="hidden" class="btn btn-default"><i class="fa fa-pencil">  Ubah    </i></a> ||

                 <a data-placement="top" data-original-title="Delete" data-toggle="tooltip" value="hidden" class="btn btn-default"><i class="fa fa-trash">  Hapus     </i></a> ||
                  
                   <a  data-placement="top" data-original-title="Detail" data-toggle="tooltip" href="{{ url('/paketKuliah/strukturPaketKuliah/' . $data->id) }}" class="btn btn-info" align="center"><i style="color: white" class="fa  fa-eye">  Detail</i></a>
                   </td>
                @endif
                 
                   
                   
            </tr> 
            <?php $count++; ?> 
            @endforeach 

        </tbody>
        
     </table> 
     {{ $paketKuliah->links() }}

    </div> 
  </div>
</div>
</form>
</div>
</form>
</div>
</div>

 <div id="Modal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    
                </div>
                <div class="modal-body">
                    <h5 class="text-center">Apakah Anda yakin ingin menghapus data ini?</h5>
                    <br />
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="id">ID:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="id_delete" disabled>
                            </div>
                        </div>
                   </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger delete" id="deletebos" data-dismiss="modal">
                            <span id="" class='glyphicon glyphicon-trash'></span> Hapus
                        </button>
                        <button type="button" style="color:white" class="btn btn-info" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Batal
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="/pathto/js/sweetalert.js"></script>
     <script type="text/javascript">
// delete a post
        // delete a post
        $(document).on('click', '.delete-modal', function() {
            // alert('jhbjhvjgvjvhj');
            // $('.modal-title').text('Delete');
            // $('#id_delete').val($(this).attr('data-id'));          
            // $('#Modal').modal('show');
            var id = $(this).attr('data-id');

            var result = confirm("Apakah data paket kuliah ini yakin dihapus?");
            if (result) {
                // $.ajax({
                //             type: 'get',
                //             url: '/paketKuliah/hapus/' + id,
                //             success: function(data) {
                window.location.href = '{{ url('/paketKuliah/hapus')}}/'+id;
                        //       }
                        // });
            }

        });
        // $('.modal-footer').on('click', '#deletebos', function() {
        //     $.ajax({
        //         type: 'get',
        //         url: '/paketKuliah/hapus' + id,
        //         success: function(data) {
        //             window.location.href = '{{ url('/paketKuliah/lihat')}}';
        //           }
        //     });
        // });
        </script>

     <!-- <script type="text/javascript">
     $('.sa-remove').click(function () {
            var postId = $(this).data('id'); 
            swal({
                title: "are u sure?",
                text: "lorem lorem lorem",
                type: "error",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                confirmButtonText: "Delete",
                cancelButtonText: "Cancel",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(){
                window.location.href = "/paketKuliah/hapus" + postId;
            }); 
     </script> -->
@endsection

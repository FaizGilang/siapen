@extends('layouts.pengguna')
@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Paket Kuliah</h3>
    </div>       <!-- /.main-bar -->
</header>
                        <!-- /.head -->
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url('/paketKuliah/tambah') }}">

            {{ csrf_field() }}

          <div class="card card-primary">
              <div class="card-header">
                <h1 style="font-family: Times New Roman;" align="center" class="card-title">Tambah Paket Kuliah</h1>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">

              
                 <div class="form-group">
                    <b for="kurikulum">Kurikulum:</b>
                    <select class="form-control" name="kurikulum">
                    <option value="" selected disabled>-Pilih Kurikulum-</option>   
                    @foreach($kurikulum as $a)
                        <option value="{{ $a->id }}">{{ $a->tahun }} - {{$a->namaprodi}}</option>
                    @endforeach
                    </select>
                    {!! $errors->first('kurikulum', '<p class="text-danger">:message</p>') !!}
                 </div>

                 <div class="form-group">
                    <b for="tahunAjaran">Tahun Ajaran:</b>
                    <select class="form-control" name="tahunAjaran">
                    <option value="" selected disabled>-Pilih Tahun Ajaran-</option>   
                    @foreach($tahunAjaran as $a)
                        <option value="{{ $a->id }}">{{ $a->tahunAjaran }} - {{$a->semester}}</option>
                    @endforeach
                    </select>
                    {!! $errors->first('tahunAjaran', '<p class="text-danger">:message</p>') !!}
                 </div>

                  <div>
                    <label>Semester Komulatif:</label>
                    <select class="form-control" name="komulatif">
                        <option value="" selected disabled>-Pilih Semester-</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                    </select>
                    {!! $errors->first('komulatif', '<p class="text-danger">:message</p>') !!}
                </div>

                
                <br>
     <br>
     <br>
                <div align="center" class="card-footer">
                  <input type="submit" name="save" value="Simpan" class="btn btn-md btn-success">
                  <input type="reset" name="reset" class="btn btn-danger" value="Batal" />
                </div>

              </form>
            </div>
        </form>

    </div>
    
     
</div>

 
@endsection
@section('script')
 <script type="text/javascript">
    jQuery(document).ready(function ()
    {
            jQuery('select[name="prodi"]').on('change',function(){
               var countryID = jQuery(this).val();
               if(countryID)
               {
                  jQuery.ajax({
                     url : '/kurikulum/tambah/getKurikulum/' +countryID,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="kurikulum"]').empty();
                        jQuery.each(data, function(key,value){
                           $('select[name="kurikulum"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="kurikulum"]').empty();
               }
            });
    });
    </script>
@endsection

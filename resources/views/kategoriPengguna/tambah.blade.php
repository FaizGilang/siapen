@extends('layouts.home')

@section('heading')
<!-- Page Heading -->
<header class="head">                               
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;KategoriPengguna</h3>
    </div>       <!-- /.main-bar -->
</header>
                        <!-- /.head -->
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url('/kategoriPengguna/tambah') }}">

            {{ csrf_field() }}

          <div class="card card-primary">
              <div class="card-header">
                <h3 style="margin-left: 450px; font-family: Times New Rowman;" class="card-title">Tambah Kategori Pengguna</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">

                  <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                    <label>Nama Kategori Pengguna:</label>
                    <input type="string" name="nama" class="form-control" placeholder="Nama Kategori Pengguna" value="{{ old('nama') }}">
                     {!! $errors->first('nama', '<p class="text-danger">:message</p>') !!}
                  </div>


                 

                </div>
                <!-- /.card-body -->
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
                <div class="card-footer">
                    <a style="margin-left: 10px" href="{{ url('/kategoriPengguna/lihat') }}" class="fa fa-arrow-circle-left fa-2x"/></a>       
                    <input style="margin-left: 450px" type="submit" name="save" value="Simpan" class="btn btn-md btn-success">
                    <input type="reset" name="reset" class="btn btn-danger" value="Batal" />                 
                </div>

              </form>
            </div>
            <br>
<br>
<br>
<br>
<br>


        </form>
    </div>
</div>
@endsection
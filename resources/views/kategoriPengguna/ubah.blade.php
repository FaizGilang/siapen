@extends('layouts.home')

@section('heading')
<!-- Page Heading -->
<header class="head">
    
 
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;KategoriPengguna</h3>
    </div>
    <!-- /.main-bar -->
</header>
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url()->current() }}">

            {{ csrf_field() }}

            <div class="card card-primary">
              <div class="card-header">
                <h3 style="margin-left: 450px; font-family: Times New Rowman;" class="card-title">Ubah Kategori Pengguna</h3>
              </div>
               <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">

            <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                <label>Nama Kategori Pengguna</label>
                <input type="text" name="nama" class="form-control" placeholder="Nama Kategori Pengguna" value="{{ $kategoriPengguna->nama }}">
                {!! $errors->first('nama', '<p class="text-danger">:message</p>') !!}
            </div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
            <div class="card-footer">    
                <a style="margin-left: 10px" href="{{ url('/kategoriPengguna/lihat') }}" class="fa fa-arrow-circle-left fa-2x"/></a>   
                <input style="margin-left: 450px" type="submit" name="save" value="Perbarui" class="btn btn-md btn-success">
                <a href="{{ url('/kategoriPengguna/lihat') }}"  name="reset" class="btn btn-danger" value="Batal" /> Batal   </a>             
            </div>
        </form>
    </div>

</div>
@endsection

<!-- TAMPILAN FORM EDIT MATA KULIAH -->

@extends('layouts.home')

@section('heading')
<!-- Page Heading -->
<header class="head">
    
 
    <div class="main-bar">
        <h3><i class="fa fa-user"></i>&nbsp;Matkul</h3>
    </div>
    <!-- /.main-bar -->
</header>
<!-- /.row -->
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <form method="post" action="{{ url()->current() }}">
            {{ csrf_field() }}
            <div class="card card-primary">
              <div class="card-header">
                <h3 style="margin-left: 450px; font-family: Times New Rowman;" class="card-title">Ubah Mata Kuliah</h3>
              </div>
               <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">
              
                
              

                   

                <div class="form-group">
                    <label for="prodi">Program Studi:</label>
                    <select class="form-control" name="prodi">

                    @if($cek->kodeprodi=='99')
                        <option value="99" >AKND (Umum)</option>
                    @else   
                        <option value="{{ $cek1->id }}" >{{$cek1->nama}}</option>
                    @endif                        

                        @foreach($prodi as $a)
                        <option value="{{ $a->id }}">{{ $a->nama }}</option>
                        @endforeach

                        
                    </select>
                </div>

                <div class="form-group">
                    <label for="dosen1">Dosen 1:</label>
                    <select class="form-control" name="dosen1">
                   <option value="{{ $cek->dosen1 }}" >{{$cek->dosen1}}</option>
                    </select>
                </div>
                
                <div class="form-group">
                    <label for="dosen2">Dosen 2:</label>
                    <select name="dosen2" class="form-control">
                    <option value="{{ $cek->dosen2 }}" >{{$cek->dosen2}}</option> 
                    </select>
                </div>


                  <div class="form-group {{ $errors->has('nama') ? 'has-error' : '' }}">
                    <label>Nama Mata Kuliah:</label>
                    <input type="string" name="nama" class="form-control" placeholder="Nama Mata Kuliah" value="{{ $cek->nama}}">
                     {!! $errors->first('nama', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div class="form-group {{ $errors->has('komulatif') ? 'has-error' : '' }}">
                    <label>Semester Komulatif:</label>
                    <input type="integer" name="komulatif" class="form-control" value="{{ $cek->komulatif }}" placeholder="komulatif">
                    {!! $errors->first('komulatif', '<p class="text-danger">:message</p>') !!}
                  </div>

                   <div class="form-group {{ $errors->has('SKS') ? 'has-error' : '' }}">
                    <label>SKS:</label>
                    <input type="integer" name="SKS" class="form-control" value="{{ $cek->SKS }}" placeholder="SKS">
                    {!! $errors->first('SKS', '<p class="text-danger">:message</p>') !!}
                  </div>

                 <div class="form-group {{ $errors->has('SKSteori') ? 'has-error' : '' }}">
                    <label>Teori:</label>
                    <input type="integer" name="SKSteori" class="form-control" value="{{ $cek->SKSteori }}" placeholder="SKS Teori">
                    {!! $errors->first('SKSteori', '<p class="text-danger">:message</p>') !!}
                  </div>

                   <div class="form-group {{ $errors->has('SKSpraktek') ? 'has-error' : '' }}">
                    <label>Praktek:</label>
                    <input type="integer" name="SKSpraktek" class="form-control" value="{{ $cek->SKSpraktek }}" placeholder="SKS Praktek">
                    {!! $errors->first('SKSpraktek', '<p class="text-danger">:message</p>') !!}
                  </div>

                  <div class="form-group {{ $errors->has('jpm') ? 'has-error' : '' }}">
                    <label>JPM:</label>
                    <input type="integer" name="jpm" class="form-control" value="{{ $cek->jpm }}" placeholder="jpm">
                    {!! $errors->first('jpm', '<p class="text-danger">:message</p>') !!}
                  </div>

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <a style="margin-left: 10px" href="{{ url('/matkul/lihat') }}" class="fa fa-arrow-circle-left fa-2x"/></a>                  
                    <input style="margin-left: 450px" type="submit" name="save" value="Simpan" class="btn btn-md btn-success">
                    <input type="reset" name="reset" class="btn btn-danger" value="Batal" />                 
                </div>
        </form>
    </div>
</div>
@endsection
@section('script')
 <script type="text/javascript">
    jQuery(document).ready(function ()
    {
            jQuery('select[name="prodi"]').on('change',function(){
               var countryID = jQuery(this).val();
               if(countryID)
               {
                  jQuery.ajax({
                     url : '/matkul/ubah/getdosen/' +countryID,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="dosen1"]').empty();
                        jQuery.each(data, function(key,value){
                           $('select[name="dosen1"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="dosen1"]').empty();
               }
            });
    });
    </script>

 <script type="text/javascript">
    jQuery(document).ready(function ()
    {
            jQuery('select[name="prodi"]').on('change',function(){
               var countryID = jQuery(this).val();
               if(countryID)
               {
                  jQuery.ajax({
                     url : '/matkul/ubah/getdosen2/' +countryID,
                     type : "GET",
                     dataType : "json",
                     success:function(data)
                     {
                        console.log(data);
                        jQuery('select[name="dosen2"]').empty();
                        jQuery.each(data, function(key,value){
                           $('select[name="dosen2"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                     }
                  });
               }
               else
               {
                  $('select[name="dosen2"]').empty();
               }
            });
    });
    </script>
@endsection
